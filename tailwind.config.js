/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],

  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'black': '#0F0E0E',
      'green': '#26AF60',
      'lightGray': '#B6B6B6',
      'blue': '#0090FD',
      'gray': '#757575',
      'silver': '#757575',
      'lightSilver':'#F8F8F8',
      'white': '#FFFFFF',
      'red': '#CE0202',
      'blackSilver': '#7E7E7E',


    },
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    fontFamily: {
      inter: ['Inter', 'sans-serif'],
    },
    extend: {
      boxShadow: {
        '3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.3)',
        '4xl': '0px 3px 5px 0px rgba(0, 0, 0, 0.09), 0px 1px 18px 0px rgba(0, 0, 0, 0.09), 0px 6px 10px 0px rgba(245, 245, 245, 0.14);',
        '5xl': '0 5px 30px 0 rgba(0,0,0,0.2);',
      }
    },
  },
  plugins: [],
}