
export const gender = [
  { value: "MR", label: "Mr" },
  { value: "MRS", label: "Mrs" },
];
export const typeOfFreelancing = [
  { value: "business", label: "Business" },
  { value: "noBusiness", label: "No Business" },
];
export const months = [
  { value: "01", label: "January" },
  { value: "02", label: "February" },
  { value: "03", label: "March" },
  { value: "04", label: "April" },
  { value: "05", label: "May" },
  { value: "06", label: "June" },
  { value: "07", label: "July" },
  { value: "08", label: "August" },
  { value: "09", label: "September" },
  { value: "10", label: "October" },
  { value: "11", label: "November" },
  { value: "12", label: "December" },
];

export const monthsEnums = [
  { value: "January", label: "January" },
  { value: "February", label: "February" },
  { value: "March", label: "March" },
  { value: "April", label: "April" },
  { value: "May", label: "May" },
  { value: "June", label: "June" },
  { value: "July", label: "July" },
  { value: "August", label: "August" },
  { value: "September", label: "September" },
  { value: "October", label: "October" },
  { value: "November", label: "November" },
  { value: "December", label: "December" },
];

export const accounting = [
  { value: "BALANCE_SHEET", label: "Business sheet" },
  { value: "CASH_BASED_ACCOUNTING", label: "Cash based accounting" },
];
export const yesNo = [
  { value: "Yes", label: "Yes" },
  { value: "No", label: "No" },
];
export const paymentFrequencyOptions = [
  { value: "REGULARLY_MONTHLY", label: "Regularly monthly" },
  { value: "REGULARLY_NOT_MONTHLY", label: "Regularly, not monthly" },
  { value: "IRREGULARLY", label: "Irregulery" },
];
export const pensionSecurity = [{ value: 'LIFE_INSURANCE', label: 'Life insurance' },
{ value: 'OTHER_FROM', label: 'Other from' },
{ value: 'NO', label: 'No' }]

export const workPlaceArray = [
  { value: "PRIVATE_HOME", label: "Private home" },
  { value: "SEPARATE_BUSINESS_OFFICE", label: "Separate business office" },
  {
    value: "RENTED_BEDROOM_OFFICE_COMBO",
    label: "Rented bedroom office combo",
  },
  { value: "OTHER_PLACE", label: "Other place" },
];

const currentYear: number = new Date().getFullYear();
export const years: Array<{ value: string; label: string }> = [];

for (let year = 2005; year <= currentYear; year++) {
  years.push({ value: year.toString(), label: year.toString() });
}

export const fourYears = [
  { value: "2020", label: "2020" },
  { value: "2021", label: "2021" },
  { value: "2022", label: "2022" },
];

export  const cities = [
  { value: "Bishkek", label: "Bishkek" },
  { value: "Paris", label: "Paris" },
  { value: "London", label: "London" },
  { value: "New York", label: "New York" },
  { value: "Tokyo", label: "Tokyo" },
  { value: "Rome", label: "Rome" },
  { value: "Sydney", label: "Sydney" },
  { value: "Dubai", label: "Dubai" },
  { value: "Barcelona", label: "Barcelona" },
  { value: "Singapore", label: "Singapore" },
];
