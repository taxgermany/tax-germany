export const optionsDocument = [
    {value: "", label: 'None'},
    {value: "1", label: 'Audit Report'},
    {value: '2', label: 'Net profit determination'},
    {value: '3', label: 'Income Confirmation'},
    {value: '4', label: 'Income Confirmation + Net profit determination'},
]




export const optionsStatus = [
    {value: "", label: 'None'},
    {value: "IN_PROGRESS", label: 'In progress'},
    {value: "NEW_APPLICATION", label: 'New application'},
    {value: 'COMPLETED', label: 'Completed'},
    {value: 'PAID', label: 'Paid'},
    {value: 'APPROVED_BY_CLIENT', label: 'Approved by client'},
    {value: 'REJECTED_BY_CLIENT', label: 'Rejected by client'},
    {value: 'APPLICATION_UPDATED_BY_CLIENT', label: 'Application updated by client'},
    {value: 'INFORMATION_REQUESTED_BY_EXPERT_ADMIN', label: 'Information requested by expert admin'},
    {value: 'NEW_MESSAGE_FROM_CLIENT', label: 'New message from client'},
]
