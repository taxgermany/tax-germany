import React from "react";
import {Table} from "antd";

export interface DataType {
    key:React.Key;
    titleRaw: string | string[];
    january?: string;
    february?: string;
    march?: string;
    april?: string;
    may?: string;
    june?: string;
    july?: string;
    august?: string;
    september?: string;
    october?: string;
    november?: string;
    december?: string;
    total: string;
}

export type EditableTableProps = Parameters<typeof Table>[0];
export type ColumnTypes = Exclude<EditableTableProps['columns'], undefined>;

export const dataSourceMini: DataType[] = [
    {    key: "1",
        titleRaw:['Health Insurance'],
        january: "0",
        february: "0",
        march: "0",
        april: '0',
        may: "0",
        june: '0',
        july: '0',
        august: '0',
        september: '0',
        october: '0',
        november: '0',
        december: '0',
        total: '0'
    },
];
export const dataSourceMiniObj: DataType=
    {    key: "1",
        titleRaw:['Health Insurance'],
        january: "0",
        february: "0",
        march: "0",
        april: '0',
        may: "0",
        june: '0',
        july: '0',
        august: '0',
        september: '0',
        october: '0',
        november: '0',
        december: '0',
        total: '0'
    }



export const columns:  (ColumnTypes[number] & { editable?: boolean; dataIndex: string })[] = [
    {
        title: '',
        dataIndex: 'titleRaw',
        width: 180,
        key: 'titleRaw',
        render: (_, record) =>
            (<div>
                    <div className={" h-full flex justify-center items-center font-semibold"}>{record.titleRaw[0]}</div>
                    <div className={" h-full flex justify-center items-center text-[12px] text-[#B6B6B6]"}>{record.titleRaw[1]}</div>
            </div>

            )
    },
    {
        title: 'January',
        dataIndex: 'january',
        key: 'january',
        width: 100,
        editable: true,
    },
    {
        title: 'February',
        dataIndex: 'february',
        key: 'february',
        width: 100,
        editable: true,

    },
    {
        title: 'March',
        dataIndex: 'march',
        key: 'march',
        width: 100,
        editable: true,
    },
    {
        title: 'April',
        dataIndex: 'april',
        key: 'april',
        width: 100,
        editable: true,
    },
    {
        title: 'May',
        dataIndex: 'may',
        key: 'may',
        width: 100,
        editable: true,
    },
    {
        title: 'June',
        dataIndex: 'june',
        key: 'june',
        width: 100,
        editable: true,
    },
    {
        title: 'July',
        dataIndex: 'july',
        key: 'july',
        width: 100,
        editable: true,

    },
    {
        title: 'August',
        dataIndex: 'august',
        key: 'august',
        width: 100,
        editable: true,

    },
    {
        title: 'September',
        dataIndex: 'september',
        key: 'september',
        width: 100,
        editable: true,

    },
    {
        title: 'October',
        dataIndex: 'october',
        key: 'october',
        width: 100,
        editable: true,

    },
    {
        title: 'November',
        dataIndex: 'november',
        key: 'november',
        width: 100,
        editable: true,

    },
    {
        title: 'December',
        dataIndex: 'december',
        key: 'december',
        width: 100,
        editable: true,
    },
    {
        title: 'Total',
        dataIndex: 'total',
        width: 100,
        rowScope: 'row',
        key: 'total',
        render: (_, record) =>
            (
                <div className={"bg-[#FAFAFA] h-full flex justify-center items-center font-semibold"}>{record.total}</div>
            )
    },
];

/*export const columns:  (ColumnTypes[number] & { editable?: boolean; dataIndex: string })[] = [
    {
        title: '',
        dataIndex: 'titleRaw',
        width: 150,
        rowScope: 'row',
        key: 'titleRaw',
    },
    {
        title: 'January',
        dataIndex: 'january',
        key: 'january',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"january"} initialValue={record.january}/>
            )
    },
    {
        title: 'February',
        dataIndex: 'february',
        key: 'february',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"february"} initialValue={record.february}/>
            )

    },
    {
        title: 'March',
        dataIndex: 'march',
        key: 'march',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"march"} initialValue={record.march}/>
            )
    },
    {
        title: 'April',
        dataIndex: 'april',
        key: 'april',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"april"} initialValue={record.april}/>
            )
    },
    {
        title: 'May',
        dataIndex: 'may',
        key: 'may',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"may"} initialValue={record.may}/>
            )
    },
    {
        title: 'June',
        dataIndex: 'june',
        key: 'june',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"june"} initialValue={record.june}/>
            )
    },
    {
        title: 'July',
        dataIndex: 'july',
        key: 'july',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"july"} initialValue={record.july}/>
            )
    },
    {
        title: 'August',
        dataIndex: 'august',
        key: 'august',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"august"} initialValue={record.august}/>
            )
    },
    {
        title: 'September',
        dataIndex: 'september',
        key: 'september',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"september"} initialValue={record.september}/>
            )
    },
    {
        title: 'October',
        dataIndex: 'october',
        key: 'october',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"october"} initialValue={record.october}/>
            )
    },
    {
        title: 'November',
        dataIndex: 'november',
        key: 'november',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"november"} initialValue={record.november}/>
            )
    },
    {
        title: 'December',
        dataIndex: 'december',
        key: 'december',
        width: 100,
        editable: true,
        render: (_, record) =>
            (
                <InputForTable name={"december"} initialValue={record.december}/>
            )
    },
    {
        title: 'Total',
        dataIndex: 'total',
        width: 100,
        rowScope: 'row',
        key: 'total',
        render: (_, record) =>
            (
                <div className={"bg-[#FAFAFA] h-full flex justify-center items-center"}>{record.total}</div>
            )
    },
];*/

