export const obj = {
    "year": "2020-01-01",
    "months": [
        {
            "id": 1,
            "month": "2020-05-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 2,
            "month": "2020-06-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 3,
            "month": "2020-07-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 4,
            "month": "2020-08-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 5,
            "month": "2020-09-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 6,
            "month": "2020-10-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 7,
            "month": "2020-11-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        },
        {
            "id": 8,
            "month": "2020-12-01",
            "grossRevenueInclVat": 0.00,
            "grossRevenue": 0.00,
            "expenses": 0.00,
            "officeSupplies": 0.00,
            "officeRent": 0.00,
            "insuranceExclHealth": 0.00,
            "publicTransport": 0.00,
            "telephone": 0.00,
            "otherExpenses": 0.00,
            "monthlyProfits": 0.00
        }
    ],
    "totalYearProfit": null,
    "total": {
        "grossRevenueInclVat": 0.00,
        "grossRevenue": 0.00,
        "vatReceived": 0.00,
        "expenses": 0.00,
        "officeSupplies": 0.00,
        "officeRent": 0.00,
        "insuranceExclHealth": 0.00,
        "publicTransport": 0.00,
        "telephone": 0.00,
        "sumOfVat": 0.00,
        "vatPaidToTaxOffice": 0.00,
        "otherExpenses": 0.00,
        "monthlyProfits": 0.00
    }
}