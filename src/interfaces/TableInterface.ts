
export interface IMonthsVot {
    "id": number,
    "month": string,
    "grossRevenueInclVat": number,
    "grossRevenue": number,
    "vatReceived": number,
    "expenses": number,
    "officeSupplies": number,
    "officeRent": number,
    "insuranceExclHealth": number,
    "publicTransport": number,
    "telephone": number,
    "sumOfVat": number,
    "vatPaidToTaxOffice": number,
    "otherExpenses": number,
    "monthlyProfits": number
}

export interface ITableVot {
    "year": string,
    "months": IMonthsVot[],
    "totalYearProfit": number,
    "total": {
        "grossRevenueInclVat": number,
        "grossRevenue": number,
        "vatReceived": number,
        "expenses": number,
        "officeSupplies": number,
        "officeRent": number,
        "insuranceExclHealth": number,
        "publicTransport": number,
        "telephone": number,
        "sumOfVat": number,
        "vatPaidToTaxOffice": number,
        "otherExpenses": number,
        "monthlyProfits": number
    }
}

export interface ITable {
    "year": string,
    "months":IMonths[],
    "totalYearProfit": number,
    "total": {
        "grossRevenueInclVat": number,
        "grossRevenue": number,
        "expenses": number,
        "officeSupplies": number,
        "officeRent": number,
        "insuranceExclHealth": number,
        "publicTransport": number,
        "telephone": number,
        "otherExpenses": number,
        "monthlyProfits": number
    }
}



export interface IMonths {
    "id": number,
    "month": string,
    "grossRevenueInclVat": number,
    "grossRevenue": number,
    "expenses": number,
    "officeSupplies": number,
    "officeRent": number,
    "insuranceExclHealth": number,
    "publicTransport": number,
    "telephone": number,
    "otherExpenses": number,
    "monthlyProfits": number
}




export interface IEmployer {
    "id": number,
    "year": string,
    "commercialEmployees": number,
    "trainees": number,
    "partTimeWorkers": number,
    "familyMembers": number
}




export interface IMiniTableMonths  {
    "id": number,
    "month": string,
    "value": number
}
export interface IPensionContributionTable {
    "id": number,
    "month": string,
    "value": number
}


export interface IMiniTableWithTotal {
    "year": string,
    "months":IMiniTableMonths[] | IPensionContributionTable[]
    "total": number

}


export interface IMiniTable {
    "year": string,
    "months":IMiniTableMonths[]
}

export interface IMicroTable {
    "id": number,
    "year": string,
    "commercialEmployees": number,
    "trainees": number,
    "partTimeWorkers": number,
    "familyMembers": number
}
