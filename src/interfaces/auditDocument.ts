export interface IAuditDocument {
    "firstPage": {
        "auditPersonalDetailsDto": {
            "date": string,
            "gender": string,
            "firstname": string,
            "lastname": string,
            "dateOfBirth": string,
            "country": string,
            "nationality": string,
            "logos": [
                string,
            ]
        },
        "auditBusinessDetailsDto": {
            "businessName": string,
            "businessAddress": string,
            "businessPhone": string,
            "businessEmail": string,
            "website": string,
        }
    },
    "secondPage": {
        "commercial": boolean,
        "intellectualOrArtistic": boolean,
        "cantUploadTradeRegistration": boolean,
        "privateHome": boolean,
        "separateBusinessOffice": boolean,
        "rentedBedroomOfficeCombo": boolean,
        "otherPlace": boolean,
        "otherPlaceText": string,
    },
    "thirdPage": {
        "totalProfitsTableDtoList": [
            {
                "year": number,
                "grossRevenueInclVat": number,
                "grossRevenueWithoutVat": number
            }
        ],
        "accountingRecordTypeDto": {
            "balanceSheet": boolean,
            "cashBased": boolean,
            "regularly": boolean,
            "irregularly": boolean
        },
        "employersTableForAuditDtoList": [
            {
                "year": number,
                "commercialEmployers": number,
                "trainees": number,
                "total": number,
                "fullTimeEmployees": number,
                "partTimeWorkers": number,
                "familyMembers": number
            }
        ],
        "employerNumber": number
    },
    "fourthPage": {
        "monthlyProfitsTableDtoList": [
            {
                "year": number,
                "months": [
                    {
                        "month": string,
                        "monthlyProfits": number
                    }
                ],
                "total": number
            }
        ]
    },
    "fifthPage": {
        "regularlyMonthly": boolean,
        "regularlyNotMonthly": boolean,
        "irregularly": boolean,
        "healthInsuranceTableDtoList": [
            {
                "year": number,
                "monthHealthForTableDtoList": [
                    {
                        "month": string,
                        "healthInsurance": number
                    }
                ],
                "total": 0
            }
        ],
        "healthInsuranceDetailsDto": {
            "doYouPayForHealthInsurance": boolean,
            "companyName": string,
            "companyAddress": string,
            "membershipNumber": number,
            "cityPostalCodeAddress": string,
            "contactPerson": string,
            "healthInsurancePhoneNumber": string,
            "regularly": string,
            "reasonWhyIrregularly": string,
        }
    },
    "sixthPage": {
        "auditPensionContributionDto": {
            "makePensionContribution": boolean,
            "pensionNumber": number,
            "lifeInsurance": boolean,
            "otherForm": boolean,
            "no": boolean,
            "outsideThePeriodText": string,
            "otherFormPension": string,
        },
        "lifeInsuranceDetailsDto": {
            "insuranceAmount": 0,
            "companyName": string,
            "address": string,
            "insuranceNumber": string,
        },
        "yearDtoPensions": [
            {
                "year": string,
                "monthDtoPensionList": [
                    {
                        "id": number,
                        "month": string,
                        "value": number
                    }
                ],
                "total": number
            }
        ]
    }
}


export interface IAuditAddDocument {
    nameExport: string,
    nameFirma: string,
    addressExpert: string,
    telFaxEmailExpert: string,
    "randomSelection":boolean,
    "submittedDocuments":boolean,
    "randomSampling":boolean,
    notInsolvent: boolean,
    notOverindebted: boolean,
    pruferInterruption:boolean,
    pruferSupervised: boolean,
    pruferFirst: boolean,
    Kein: boolean,
    violation: string
    currentYear: number

}

/*

export interface IAuditDocumentGeneration {
    "firstPage": {
        export:{
            nameExport:string,
            firma:string,
            Address:string,
            phone:string,
        },
        "auditPersonalDetailsDto": {
            "date": string,
            "gender": string,
            "firstname": string,
            "lastname": string,
            "dateOfBirth": string,
            "country": string,
            "nationality": string,
            "logos": [
                string,
            ]
        },
        "auditBusinessDetailsDto": {
            "businessName": string,
            "businessAddress": string,
            "businessPhone": string,
            "businessEmail": string,
            "website": string,
        }
    },
    "secondPage": {
        "randomSelection":boolean,
        "submittedDocuments ":boolean,
        "randomSampling ":boolean,
        "commercial": boolean,
        "intellectualOrArtistic": boolean,
        "cantUploadTradeRegistration": boolean,
        "privateHome": boolean,
        "separateBusinessOffice": boolean,
        "rentedBedroomOfficeCombo": boolean,
        "otherPlace": boolean,
        "otherPlaceText": string,
    },
    "thirdPage": {
        "totalProfitsTableDtoList": [
            {
                "year": number,
                "grossRevenueInclVat": number,
                "grossRevenueWithoutVat": number
            }
        ],
        "accountingRecordTypeDto": {
            "balanceSheet": boolean,
            "cashBased": boolean,
            "regularly": boolean,
            "irregularly": boolean
        },
        "employersTableForAuditDtoList": [
            {
                "year": number,
                "commercialEmployers": number,
                "trainees": number,
                "total": number,
                "fullTimeEmployees": number,
                "partTimeWorkers": number,
                "familyMembers": number
            }
        ],
        "employerNumber": number
    },
    "fourthPage": {
        "monthlyProfitsTableDtoList": [
            {
                "year": number,
                "months": [
                    {
                        "month": string,
                        "monthlyProfits": number
                    }
                ],
                "total": number
            }
        ]
    },
    "fifthPage": {
        "regularlyMonthly": boolean,
        "regularlyNotMonthly": boolean,
        "irregularly": boolean,
        "healthInsuranceTableDtoList": [
            {
                "year": number,
                "monthHealthForTableDtoList": [
                    {
                        "month": string,
                        "healthInsurance": number
                    }
                ],
                "total": 0
            }
        ],
        "healthInsuranceDetailsDto": {
            "doYouPayForHealthInsurance": boolean,
            "companyName": string,
            "companyAddress": string,
            "membershipNumber": number,
            "cityPostalCodeAddress": string,
            "contactPerson": string,
            "healthInsurancePhoneNumber": string,
            "regularly": string,
            "reasonWhyIrregularly": string,
        }
    },
    "sixthPage": {
        "auditPensionContributionDto": {
            "makePensionContribution": boolean,
            "pensionNumber": number,
            "lifeInsurance": boolean,
            "otherForm": boolean,
            "no": boolean,
            "outsideThePeriodText": string,
            "otherFormPension": string,
        },
        "lifeInsuranceDetailsDto": {
            "insuranceAmount": 0,
            "companyName": string,
            "address": string,
            "insuranceNumber": string,
        },
        "yearDtoPensions": [
            {
                "year": string,
                "monthDtoPensionList": [
                    {
                        "id": number,
                        "month": string,
                        "value": number
                    }
                ],
                "total": number
            }
        ]
    }
}
*/

