export interface IDetailsData {
    documentsNames: string[];
    expert: string;
    releaseDate: string;
}

export interface Notification {
    "userName": string,
    "textMessage": string,
    "timeOfCreation": string,
    "textLink": string,
    "link": string
}

export interface IFile {
    documentName: { documentName: string };
    fileDto: {
        id: number;
        name: string;
        type: string;
        file: string;
    }
    documentId: number
    status: string;
}
