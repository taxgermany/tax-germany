import {IOption} from "./antdInterface";

export interface IClients {
    "id": number,
    "clientName": string,
    "type": string,
    "documentNames": string[],
    "status": string,
    "responsibleAgent": IOption,
    "applicationDate": string,
}

export interface getAllClients {
    "responseList": IClients[],
    "page": number,
    "size": number,
    "total": number
}

export interface IExport {
    "value": string,
    "label": string
}


export interface IPaginationClient {
    users: IClients[],
    page: number,
    size: number
    total: number
}

export interface INotifications {
    "userName": string,
    "textMessage": string,
    "timeOfCreation": string,
    "textLink": string,
    "link": string
    "status": string
}


export interface IExportAdmin {
    "id": number,
    "n": number,
    "firstName": string,
    "lastName": string,
    "email": string
}

export interface IAddExport {
    "firstName"?: string,
    "lastName"?: string,
    "email"?: string
}


export interface IFilterParams {
    docId?: string,
    status?: string,
    expertId?: string,
    page: number,
    size: number
}

export interface ISearchParams {
    expertId?: number,
    page: number,
    size: number,
    search?: string
}

export interface ISearchParamsAdmin {
    page: number,
    size: number,
    request: string
}

export function identity<T>(arg: T): T {
    return arg;
}

export interface ApiData {
    gender: "MR" | "MRS";
    firstName: string;
    lastName: string;
    address: string;
    street: string;
    postalCode: string;
    generationYear: number;
    generationMonth: string;
    fromYear: number;
    fromMonth: string;
    toYear: number;
    toMonth: string;
    total: number;
    "logos": [
        {
            "id": 0,
            "fileName": "string",
            "type": "string",
            "file": [
                "string"
            ]
        }
    ]
}
