export interface IReviewYour {
    gender: string,
    firstname: string
    lastname: string,
    dateOfBirth: string
    country: string,
    nationality: string
    typeOfFreelance: string,
    becomeFreelancerDate: string
    fromMonth: string,
    fromYear: number,
    toMonth: string,
    toYear: number,
    id: number,
    registeredAt: string,
    lastUpdatedAt: string,
}
export interface IReviewUser {
    email: string,
    password: string,
    phoneNumber: string
}
export interface IReviewProfits {
    accountingType: string
    regularly: boolean
    haveEmployees: boolean
    employerNumber: string
    vat: boolean
}
export interface IReaviewBussines {
    businessName: string,
    city: string,
    street: string,
    postalCode: string,
    building: string,
    businessPhone: string,
    businessEmail: string,
    website: string,
    freelanceType: string,
    workPlace: string,
    anotherWorkPlace: string,
    tradeRegistration: [string]
}
export interface IReviewBussines {
    businessName: string,
    city: string,
    street: string,
    postalCode: string,
    building: string,
    businessPhone: string,
    businessEmail: string,
    website: string,
    freelanceType: string,
    tradeRegistration: string,
    workPlace: string,
    anotherWorkPlace: string,
    cantUploadTradeRegistration: string
}
export interface IReviewHealth {
    doYouPayForHealthInsurance: boolean,
    companyName: string,
    membershipNumber: number,
    cityPostalCodeAddress: string,
    contactPerson: string,
    regularly: string,
    reasonWhyIrregularly: string,
    address: string,
    phoneNumber: string
    paidFully: boolean
    explainWhyNot: string
}
export interface IReviewPension {
    makePensionContribution: boolean,
    pensionNumber: string,
    pensionSecurity: string,//LIFE_INSURANCE,
    otherFormPension: string,
    planningToHaveForm: boolean,
    outsideThePeriod: boolean,
    outsideThePeriodText: string,
    paidFully: boolean
}
export interface IReviewLife {
    insuranceAmount: string,
    companyName: string,
    address: string,
    insuranceNumber: string
}
export const dateMonthe = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
]
