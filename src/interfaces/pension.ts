export interface ILifeData {
    insuranceAmount?: number,
    companyName?: string,
    address?: string,
    insuranceNumber?: number
}
export interface IPensionData {
    "id": number,
    "makePensionContribution": boolean,
    "pensionNumber": number,
    "pensionSecurity": string,
    "otherFormPension": string,
    "planningToHaveForm": boolean,
    "outsideThePeriod": boolean,
    "outsideThePeriodText": string
  }
