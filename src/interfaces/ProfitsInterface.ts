export interface ProfitAndExpenses {
    accountingType?: string | null,
    regularly?: boolean | null,
    haveEmployees?: boolean | null,
    employerNumber?:string | null,
    vat?: boolean | null,
}

export interface profitsType {
    "accountingType": string | null,
    "regularly": boolean | null,
    "haveEmployees": boolean | null,
    "employerNumber": string | null,
    "vat": boolean | null,
    /*"freelancer": {
        "id":number
        "email": string | null,
        "password": string |null,
        "phoneNumber": string | null
    }*/
}
export interface IFileNameNew {
    "id": number,
    "name": string,
    "type": string,
    "file": File,
}

export interface IFileName {
    "id": number,
    "name": string,
    "type": string,
    "file": File,
}/*
{
    "id": 0,
    "type": "string",
    "file": [
    "string"
],
    "name": "string"
}
*/
