export interface IDocumentController {
    "id": number,
    "documentName": string,
    "description": string,
    "additionalInformation": string,
    "price": number,
}

export interface IEditDocumentController{
    description: string,
    additionalInformation: string,
    price: number
}
