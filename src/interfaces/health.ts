export interface IHealthInsuranceData {    
  id: number,
  companyName: string,
  cityPostalCodeAddress: string,
  address: string,
  membershipNumber: number,
  contactPerson: string,
  phoneNumber: string,
  regularly: string,
  reasonWhyIrregularly: string
}

export interface IHealthInsuranceRegularData{
    regularly? : string;
    reasonWhyIrregularly?: string
}