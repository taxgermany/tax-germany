export interface IDetailsYouGet {
    "id": number,
    "gender": string,
    "firstname":string,
    "lastname": string,
    "dateOfBirth":string,
    "country": string,
    "nationality":string,
    "typeOfFreelance": string,
    "becomeFreelancerDate":string,
    "fromMonth":string,
    "fromYear": number,
    "toMonth": string,
    "toYear": number,
    "registeredAt": string,
    "lastUpdatedAt": string,
    "freelancer": {
        "id": number,
        "email": string,
        "password": string,
        "phoneNumber": number
    }
}
export interface IDetailsProfitGet{
    "id": number,
    "accountingType": string,
    "regularly": boolean,
    "haveEmployees": boolean,
    "employerNumber": number,
    "vat": boolean,
    "freelancerId": number
}

export interface IDetailsDocuments {
    "documentName": string
    "id": number,
}
