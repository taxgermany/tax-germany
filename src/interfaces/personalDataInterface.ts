import { Dayjs } from 'dayjs'

export interface IYourDetailsFetch {
    "gender": string,
    "firstname": string,
    "lastname": string,
    "dateOfBirth": string,
    "country": string,
    "nationality": string,
    "typeOfFreelance": string
}

export interface IYourDetails {
    "gender": string,
    "firstname": string,
    "lastname": string,
    "dateOfBirth": Dayjs,
    "country": string,
    "nationality": string,
    "typeOfFreelance": string
}

export interface IBusinessDetails {
    "businessName": string,
    "city": string,
    "street": string,
    "postalCode": string,
    "building": string,
    "businessPhone": string,
    "businessEmail": string,
    "website": string
  }
    export interface IDropDownItem {
        value: string;
        label: string;
    }

export type IDropDown = IDropDownItem[];

