
export interface IClient {
  id: number,
  email: string,
  userRole: string,
  token: string
}
