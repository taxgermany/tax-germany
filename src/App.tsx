import AppRouter from 'router/AppRouter';
import AdminRoute from "./router/AdminRoute";
import ExportRoute from "./router/ExportRoute";

function App() {
  return (
       <>
         <AppRouter/>
         <ExportRoute/>
         <AdminRoute/>
       </>

  );
}

export default App;
