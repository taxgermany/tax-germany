import React, {useState} from 'react';
import {useNavigate} from "react-router-dom";
import ButtonGreen from 'components/ui/button/ButtonGreen';
import ButtonWhiteBg from 'components/ui/button/ButtonWhiteBg';
import BigTitle from 'components/ui/button/BigTitle';
import ValidationText from "../../components/ui/validation/ValidationText";


const RegisterToStart = () => {
    const [changeActive, setChangeActive] = useState("")
    const navigate = useNavigate();

    const handleSubmit = () => {
        if (!changeActive) setChangeActive("error")
        if (changeActive === "Freelancer")
            navigate("/freelancer/register")
    }
    return (
        <div className="ForgotPassword h-[90vh] font-inter flex justify-center items-center relative">
            {changeActive === "error" && <ValidationText
                className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"}/>}
            {changeActive === "Company" && <ValidationText text={"Company section under development!"}
                                                           className={"z-[999]"}/>}
            <div className="content inline">
                <div className="grid gap-[40px]">
                    <BigTitle text='Register to start'/>
                    <div className='leading-[26px] text-[16px] font-[Inter] font-[400]'>Choose which category you belong
                        to
                    </div>
                    <div className="flex gap-[10px] text-green">
                        <ButtonWhiteBg active={changeActive} setActive={setChangeActive} text="Freelancer"/>
                        <ButtonWhiteBg active={changeActive} setActive={setChangeActive} text="Company"/>
                    </div>
                    <ButtonGreen text={'Confirm'}
                                 className="text-[14px] bg-green px-[38px] text-white leading-[26px] py-[13px] w-fit rounded-[3px]"
                                 onClick={handleSubmit}

                    />
                </div>
            </div>
        </div>
    );
};

export default RegisterToStart;