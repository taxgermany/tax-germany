import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {useNavigate} from "react-router-dom";
import {useAppSelector} from "hooks/useAppSelector";
import {ServicesCard} from "components/ui/card/ServicesCard";
import {useState} from "react";
import Footer from "components/layout/Footer";
import {useCreateDocumentMutation, useFetchDocumentQuery} from "../../services/documentService";
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {setActiveCard} from "../../redux/priceCard/priceCard";
import ExpertSidebar from "../../components/layout/ExpertSidebar";
import AdminSidebar from "../../components/layout/AdminSidebar";
import {getStoreLocal} from "../../utils/local-storage/localStorage";
import {current} from "@reduxjs/toolkit";

const HomePage = () => {

    const {client, role} = useAppSelector((state) => state.authSlice);
    const dispatch = useAppDispatch()
    const {data, isLoading, isSuccess, isError, error} = useFetchDocumentQuery()
    const {data: dataActive} = useAppSelector(state => state.priceCardSlice)
    const [activeAll, setActiveAll] = useState<{ [p: number]: boolean }>(dataActive);

    const navigate = useNavigate();

    const handleSubmit = () => {

        const role = getStoreLocal("user")?.userRole
        const id = getStoreLocal("user")?.id
        dispatch(setActiveCard(activeAll))
        if (role === "ROLE_SUPER_ADMIN") {
            navigate("/super-admin/clients")
            return
        }

        if (role === "ROLE_FREELANCER" && id) {
          /*  const path = getStoreLocal(id)?.currentPath*/
            navigate("/freelancer/choose-package")
            return
        }
        if (role === "ROLE_EXPERT") {
            navigate("/expert/clients")
            return
        }
        return navigate("/freelancer/register-to-start")


    }

    const handleChoose = (id: number) => {

        if (activeAll[4]) {
            return setActiveAll({...activeAll, [id]: !activeAll[id], [2]: false, [3]: false})
        }
        if (activeAll[2] || activeAll[3]) {
            return setActiveAll({...activeAll, [id]: !activeAll[id], [4]: false})
        }
        setActiveAll({...activeAll, [id]: !activeAll[id]})
    }

    return (
        <div className="font-inter">
            <div className="Header mt-[30px] max-w-[1100px] flex mx-auto">
                <div className="text mt-7">
                    <p className="font-[700] text-[26px] leading-[30px] mb-5">
                        Get your proof of income statement to prolong your freelancer visa
                        or find apartment. Online and hassle free
                    </p>
                    <div className="check  leading-[30px]">
                        <div className="flex items-center text-[14px] font-[600]">
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <mask
                                    id="mask0_3495_7005"
                                    maskUnits="userSpaceOnUse"
                                    x="0"
                                    y="0"
                                    width="24"
                                    height="24"
                                >
                                    <rect width="24" height="24" fill="#D9D9D9"/>
                                </mask>
                                <g mask="url(#mask0_3495_7005)">
                                    <path
                                        d="M9.5501 18L3.8501 12.3L5.2751 10.875L9.5501 15.15L18.7251 5.97498L20.1501 7.39998L9.5501 18Z"
                                        fill="#26AF60"
                                    />
                                </g>
                            </svg>
                            <p className="text-[14px] font-[600] ml-4">
                                Audit report / Prüfbericht
                            </p>
                        </div>
                        <div className="flex items-center text-[14px] font-[600]">
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <mask
                                    id="mask0_3495_7005"
                                    maskUnits="userSpaceOnUse"
                                    x="0"
                                    y="0"
                                    width="24"
                                    height="24"
                                >
                                    <rect width="24" height="24" fill="#D9D9D9"/>
                                </mask>
                                <g mask="url(#mask0_3495_7005)">
                                    <path
                                        d="M9.5501 18L3.8501 12.3L5.2751 10.875L9.5501 15.15L18.7251 5.97498L20.1501 7.39998L9.5501 18Z"
                                        fill="#26AF60"
                                    />
                                </g>
                            </svg>
                            <p className="text-[14px] font-[600] ml-4">
                                Income confirmation / Einkommensbestätigung
                            </p>
                        </div>
                        <div className="flex items-center ">
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <mask
                                    id="mask0_3495_7005"
                                    maskUnits="userSpaceOnUse"
                                    x="0"
                                    y="0"
                                    width="24"
                                    height="24"
                                >
                                    <rect width="24" height="24" fill="#D9D9D9"/>
                                </mask>
                                <g mask="url(#mask0_3495_7005)">
                                    <path
                                        d="M9.5501 18L3.8501 12.3L5.2751 10.875L9.5501 15.15L18.7251 5.97498L20.1501 7.39998L9.5501 18Z"
                                        fill="#26AF60"
                                    />
                                </g>
                            </svg>
                            <p className="text-[14px] font-[600] ml-4">
                                Net profit determination / Nettogewinnermittlung
                            </p>
                        </div>
                    </div>
                    <p className="text-[13px] font-[600] leading-[60px]">
                        Difficulties in finding a tax advisor for your VISA documents or in
                        order to get an apartment?
                    </p>
                    <p className="text-[14px] ">
                        Complete the simple question-and-answer procedure and your desired
                        proof of income statement (audit report / Prüfbericht, net profit
                        determination / Nettogewinnermittlung, income confirmation /
                        Einkommensbestätigung) will be prepared online and verified by
                        certified tax advisors from taxgermany.
                    </p>
                    <div className="btn-start my-9 ">
                        <NewButtonGreen onClick={handleSubmit} text='START'/>
                    </div>
                    <div className="safety1 flex pb-4">
                        <svg
                            width="14"
                            height="16"
                            viewBox="0 0 16 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M6.95 13.55L12.6 7.9L11.175 6.475L6.95 10.7L4.85 8.6L3.425 10.025L6.95 13.55ZM8 20C5.68333 19.4167 3.77083 18.0875 2.2625 16.0125C0.754167 13.9375 0 11.6333 0 9.1V3L8 0L16 3V9.1C16 11.6333 15.2458 13.9375 13.7375 16.0125C12.2292 18.0875 10.3167 19.4167 8 20ZM8 17.9C9.73333 17.35 11.1667 16.25 12.3 14.6C13.4333 12.95 14 11.1167 14 9.1V4.375L8 2.125L2 4.375V9.1C2 11.1167 2.56667 12.95 3.7 14.6C4.83333 16.25 6.26667 17.35 8 17.9Z"
                                fill="#26AF60"
                            />
                        </svg>
                        <p className="text-[12px] font-[600] ml-2">
                            Validated and 100% guaranteed by a certified tax advisor
                        </p>
                    </div>
                    <div className="safety2 flex">
                        <svg
                            width="14"
                            height="16"
                            viewBox="0 0 16 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M2 20C1.45 20 0.979167 19.8135 0.5875 19.4405C0.195833 19.0675 0 18.619 0 18.0952V8.57143C0 8.04762 0.195833 7.59921 0.5875 7.22619C0.979167 6.85317 1.45 6.66667 2 6.66667H3V4.7619C3 3.44444 3.4875 2.32143 4.4625 1.39286C5.4375 0.464286 6.61667 0 8 0C9.38333 0 10.5625 0.464286 11.5375 1.39286C12.5125 2.32143 13 3.44444 13 4.7619V6.66667H14C14.55 6.66667 15.0208 6.85317 15.4125 7.22619C15.8042 7.59921 16 8.04762 16 8.57143V18.0952C16 18.619 15.8042 19.0675 15.4125 19.4405C15.0208 19.8135 14.55 20 14 20H2ZM8 15.2381C8.55 15.2381 9.02083 15.0516 9.4125 14.6786C9.80417 14.3056 10 13.8571 10 13.3333C10 12.8095 9.80417 12.3611 9.4125 11.9881C9.02083 11.6151 8.55 11.4286 8 11.4286C7.45 11.4286 6.97917 11.6151 6.5875 11.9881C6.19583 12.3611 6 12.8095 6 13.3333C6 13.8571 6.19583 14.3056 6.5875 14.6786C6.97917 15.0516 7.45 15.2381 8 15.2381ZM5 6.66667H11V4.7619C11 3.96825 10.7083 3.29365 10.125 2.7381C9.54167 2.18254 8.83333 1.90476 8 1.90476C7.16667 1.90476 6.45833 2.18254 5.875 2.7381C5.29167 3.29365 5 3.96825 5 4.7619V6.66667Z"
                                fill="#26AF60"
                            />
                        </svg>
                        <p className="text-[12px] font-[600] ml-2">100% protected data</p>
                    </div>
                </div>
                <div className="image">
                    <img
                        className="h-[540px] w-[1800px] p-[10px]"
                        src="./header.jpg"
                        alt="header"
                    />
                </div>
            </div>
            <div className="max-w-[1100px] mx-auto mt-20 mb-8">
                <p className="font-[600] text-[26px]">Our Services <span className="text-green">for Freelancer</span>
                </p>
            </div>

            <div className="max-w-[1100px] mx-auto flex items-center justify-around mb-14 gap-[10px]">

                {data && data.map(item => <ServicesCard currentId={item.id}
                                                        popoverText={item.additionalInformation}
                                                        key={item.id} active={activeAll[item.id]} price={item.price}

                                                        text={item.description}
                                                        handleChoose={handleChoose}
                                                        title={item.documentName}/>)}
            </div>


            <Footer/>
        </div>
    );
};

export default HomePage;
