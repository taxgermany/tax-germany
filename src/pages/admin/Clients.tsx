import React, {useEffect, useState} from 'react';
import TextTitle from "../../components/ui/text/textForAdmin/TextTitle";
import {ReactComponent as NotificationImg} from '../../assets/icons/notification.svg'
import Modal from "../../components/screen/AdminClient/Modal/modal";
import Notification from "../../components/screen/AdminClient/notification/Notification";
import {
    useLazyFetchAllClientsQuery,
    useFetchAllExportsQuery,
    useUpdateClientMutation,
    useLazyFetchFilterQuery,
    useLazyFetchSearchExportsQuery,
    useFetchNotificationsQuery,
    useLazyFetchNotificationsQuery
} from "../../services/exportsServices";
import {useAppSelector} from "../../hooks/useAppSelector";
import {
    getAllClients,
    IClients,
    IExport,
    IFilterParams, INotifications,
    ISearchParams
} from "../../interfaces/admin";
import AdminClientTable from "../../components/screen/AdminClient/AdminClientTable/AdminClientTable";
import Filter from "../../components/screen/AdminClient/Filter/Filter";
import {IOption} from "../../interfaces/antdInterface";
import {Spin} from "antd";
import {useLazyGetNotificationsQuery} from "../../services/superAdminService";
import clsx from "clsx";


const Clients = () => {
    const [clients, setClients] = useState<getAllClients | undefined>(undefined)
    const [edit, setEdit] = useState<IClients | null>(null)
    const [showModal, setShowModal] = useState(false)
    const [currentPage, setCurrentPage] = useState(1);
    const [totalNumber, setTotalNumber] = useState(0);
    const [sizePage, setSizePage] = useState(8);
    const [totalPages, setTotalPage] = useState<number | undefined>(undefined);
    const [experts, setExpert] = useState<IOption[] | null>([] as IOption[])

    const {client} = useAppSelector(state => state.authSlice)


    const [getFilter, {
        data: dataFilter = {} as getAllClients,
        error: errorFilter,
        isFetching: isFetchingFilter,
        isLoading: isLoadingFilter,
        isSuccess: isSuccessFilter
    }] = useLazyFetchFilterQuery()

    const [getSearch, {
        data: dataSearch = {} as getAllClients,
        error: errorSearch,
        isFetching: isFetchingSearch,
        isLoading: isLoadingSearch,
        isSuccess: isSuccessSearch
    }] = useLazyFetchSearchExportsQuery()


    const [getClients, {
        data: dataClients = {} as getAllClients,
        error: errorClients,
        isFetching: isFetchingClients,
        isLoading: isLoadingClients,
        isSuccess: isSuccessClients,
    }] = useLazyFetchAllClientsQuery()

    const [updateClient, {
        data: updateData = {} as getAllClients,
        error: errorUpdateClients,
        isLoading: isLoadingUpdateClients,
        isSuccess: isSuccessUpdateClients
    }] = useUpdateClientMutation()

    const {
        data: dataExperts = [] as IExport[],
        error: errorExperts,
        isFetching: isFetchingExperts,
        isLoading: isLoadingExperts,
        isSuccess: isSuccessExperts
    } = useFetchAllExportsQuery()

    const [getNotifications, {
        data: Notifications = [] as INotifications[],
        error: errorNotifications,
        isFetching: isFetchingNotifications,
        isLoading: isLoadingNotifications,
        isSuccess: isSuccessNotifications
    }] = useLazyFetchNotificationsQuery()

    const handlePageChange = (pageNumber: number) => {
        setCurrentPage(pageNumber);
    }

    const handleSearch = (params: ISearchParams) => {
        let newParams: ISearchParams = {...params}
        if (!params.search) delete newParams.search
        if (client) getSearch({...newParams}).then((res) => setClients(res.data))

    }
    const handleFilter = async (params: IFilterParams) => {

        let newParams: IFilterParams = {...params}

        if (!params.status) delete newParams.status
        if (!params.expertId) delete newParams.expertId
        if (!params.docId) delete newParams.docId
        if (client) {
            const res = await getFilter({...newParams, id: client.id}).unwrap()
            setClients(res)
        }
    }
    const handleUpdate = async (edit: IClients) => {
        if (client) {
            const res = await updateClient({
                expertId: client.id,
                clientId: edit.id,
                status: edit.status,
                selectedExpertId: edit.responsibleAgent.value,
                page: currentPage,
                size: sizePage
            }).unwrap()
            setClients(res)
        }

    }

    const fetchClient = async () => {
        if (client) {
            const res = await getClients({
                expertId: client.id,
                page: currentPage,
                size: sizePage

            }).unwrap()
            setClients(res)
        }
    }
    useEffect(() => {
        if (client)
            getNotifications({expertId: client?.id})
        /* if (client) getClients({page: currentPage, size: sizePage}).then((res) => setClients(res.data))*/
    }, [showModal])

    useEffect(() => {
        fetchClient()
        /* if (client) getClients({
             expertId: client.id,
             page: currentPage,
             size: sizePage
         }).then((res) => setClients(res.data))*/

    }, [currentPage])


    useEffect(() => {
        clients && setTotalPage(clients.total)
    }, [clients])

    useEffect(() => {
        edit &&
        clients && setTotalPage(clients.total)
    }, [edit])

    useEffect(() => {
        if (isSuccessExperts && client) {
            const expert = dataExperts.find(item => +item.value === (client.id))
            console.log(expert)
            console.log(dataExperts)
            if (expert)
                setExpert([expert])
        }
    }, [isSuccessExperts])

    return (
        <div className="grid gap-5">
            <div className="flex justify-between items-center"><TextTitle title={"Clients"}/>
                <div  onClick={() => setShowModal(!showModal)}>
                    <NotificationImg className={"w-[50px] hoverAnimation"}/>
                </div>
            </div>
            {
                isLoadingExperts
                    ? <Spin size="large" className="absolute top-2/4 left-2/4"/>

                    : <div>{experts &&
                        <Filter setClients={setClients} experts={experts} sizePage={sizePage} currentPage={1}
                                isSuccessExperts={isSuccessExperts} getFilter={handleFilter} getSearch={handleSearch}/>}
                    </div>
            }
            {(isLoadingSearch || isLoadingClients || isLoadingFilter || isLoadingUpdateClients) &&
                <Spin size="large" className="absolute top-2/4 left-2/4"/>}
            {experts && clients && totalPages &&
                <AdminClientTable experts={experts} setTotalNumber={setTotalNumber} sizePage={sizePage}
                                  totalNumber={totalNumber}
                                  allClients={clients.responseList} setEdit={setEdit} edit={edit}
                                  currentPage={currentPage} handleUpdate={handleUpdate}
                                  totalPages={totalPages} onPageChange={handlePageChange}/>}

            {showModal && <Modal active={showModal} setActive={setShowModal} children={
                <Notification notifications={Notifications}/>}/>}

        </div>
    );
};

export default Clients;
