import { FC, useEffect, useState } from "react"
import LoadingMain from "components/ui/loading/LoadingMain"
import { Button, Form } from "antd"
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import NewButtonGreen from "components/ui/button/NewButtonGreen"
import SelectClient from "components/ui/select/selectForAdmin/SelectClient"
import { paymentFrequencyOptions, yesNo } from "utils/Arrays"
import InputAdminClient from "components/ui/input/inputForAdmin/InputAdminClient"
import ButtonEdit from "components/ui/button/btnForAdmin/ButtonEdit"
import ButtonGray from "components/ui/button/ButtonGray"
import CustomTextAreaAdminClient from "components/ui/input/inputForAdmin/CustomTextAreaAdminClient"
import cls from './ClientDetials.module.scss'
// table
import RowTable from "components/ui/table/RowTable"
import { IMiniTable, IMiniTableWithTotal } from "interfaces/TableInterface"
import { useFetchAllYearQuery } from "services/yearService"
import { IYear } from "interfaces/YearInterface"
import { useFetchClientsHealthQuery, useLazyFetchClientsHealthMiniTableQuery, useUpdateClientsHealthMiniTableMutation, useUpdateClientsHealthMutation, } from "services/clientControllerService"
import { IReviewHealth } from "interfaces/reaview"
interface IClientHealth {
    id: string | undefined
}
const ClientHealth: FC<IClientHealth> = ({ id }) => {

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState<IReviewHealth>({} as IReviewHealth)
    const [oldData, setOldData] = useState<IReviewHealth>({} as IReviewHealth)
    const [isDisabled, setIsDisabled] = useState(true)

    // NetTable

    const currentDate = new Date();
    const currYear = currentDate.getFullYear();

    const [currentYear, setCurrentYear] = useState(1)

    const [dataMiniChange, setDataMiniChange] = useState<boolean>(false);

    const [dataMini, setDataMini] = useState<IMiniTableWithTotal | null>(null);



    const [getTable, { data: table = {} as IMiniTableWithTotal, error, isSuccess }] = useLazyFetchClientsHealthMiniTableQuery()


    const {
        data: years = [] as IYear[],
        isSuccess: isSuccessYear
    } = useFetchAllYearQuery(id || 0);

    const [UpdateTableMini] = useUpdateClientsHealthMiniTableMutation()

    const updateData = async () => {
        if (dataMiniChange) {
            const { total, ...newData } = dataMini as IMiniTableWithTotal
            const res = await UpdateTableMini({ data: newData as IMiniTable, yearId: currentYear })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini(res.data as IMiniTableWithTotal)
            setDataMiniChange(false)
        }
    }

    const handleYear = (idYear: number) => {
        setCurrentYear(idYear)
        if (idYear && id) {
            getTable({
                userId: id,
                yearId: idYear,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        } updateData()

    }


    useEffect(() => {
        if (isSuccessYear && id && years.length > 0) {
            setCurrentYear(years[0].id || 0)
            getTable({
                userId: id,
                yearId: years[0].id || 0,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        }
    }, [isSuccessYear])
    //

    // get Client-Health
    const { data: getHealth, isSuccess: isSuccessHealth } = useFetchClientsHealthQuery({ Id: id || '' })

    // update Client-Health
    const [updateDataHealth] = useUpdateClientsHealthMutation()

    const fetchingUserDatas = async () => {
        try {
            setLoading(true)
            if (getHealth) {
                await setData(getHealth)
                await setOldData({ ...getHealth })
                await setLoading(false)
            }
        } catch (error: any) {
            console.log(error.message);
        }
    }
    useEffect(() => {
        (async () => {
            await fetchingUserDatas()
        })()
    }, [isSuccessHealth])
    const ofTextBoolean = (item: string, text: string) => {
        if ('Yes' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: true
            }))
        } else if ('No') {
            setData((prevData) => ({
                ...prevData,
                [item]: false
            }))

        }
    }
    const ofBooleanText = (item: string) => {
        if (data[item as keyof IReviewHealth]) {
            return 'Yes';
        } else {
            return 'No';
        }

    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const initialValuess = (dat: IReviewHealth) => ({
        companyName: dat.companyName || '',
        address: dat.address || '',
        membershipNumber: dat.membershipNumber || '',
        contactPerson: dat.contactPerson || '',
        phoneNumber: dat.phoneNumber || '',
        paidFully: ofBooleanText('paidFully'),
        regularly: textNomal(`${dat.regularly || ''}`),
        reasonWhyIrregularly: dat.reasonWhyIrregularly || '',
        explainWhyNot: dat.explainWhyNot || ''


    })
    const btnPut = async () => {
        if (data.doYouPayForHealthInsurance) {
            await updateDataHealth({
                Id: id || '',
                data: data
            })
            setIsDisabled(true)
        } else {
            await setData({
                ...data,
                companyName: '',
                address: '',
                membershipNumber: 0,
                cityPostalCodeAddress: '',
                contactPerson: '',
                phoneNumber: '',
                regularly: '',
                reasonWhyIrregularly: '',
            })
            await updateDataHealth({
                Id: id || '',
                data: data
            })
            setIsDisabled(true)
        }

    }

    const btnCancel = () => {
        setData(oldData)
        setIsDisabled(true)
    }

    return (<>
        {
            loading ? <LoadingMain />
                : <Form
                    name="appData"
                    layout="horizontal"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    onFinish={btnPut}
                    initialValues={initialValuess(data)}
                    className="box-border ">
                    <div className={`flex box-border items-center justify-between border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[10px]`}>
                        <div className="w-fit">
                            <TextTitle title="Health insurance details" />
                        </div>
                        <div className="w-[50%] ">
                            <div className=" flex gap-x-[10px] justify-end ">
                                {!isDisabled ? <>
                                    <ButtonGray onClick={btnCancel} text="Cancel" backraund={false} size="!w-[94px] !h-[48px]" className="!p-0" />
                                    <NewButtonGreen text="Save" height="48px" className="w-[94px] !p-0" /></>
                                    : <ButtonEdit onClick={() => setIsDisabled(false)} />}
                            </div>
                        </div>
                    </div>
                    <div className="grid gap-y-[12px] gap-x-[10px]">
                        <div className="flex items-center gap-x-[10px] max-w-[550px]">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Health insurance company name:</p>
                            {data.doYouPayForHealthInsurance
                                ? <InputAdminClient
                                    text={data.companyName || ''}
                                    disabled={isDisabled}
                                    placeholder="company name"
                                    inputname="companyName"
                                    onChange={e => setData({ ...data, companyName: e.target.value })}
                                />
                                : ''}
                        </div>
                        <div className="flex items-center gap-x-[10px] max-w-[550px]">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Health insurance company address:</p>
                            {data.doYouPayForHealthInsurance
                                ? <InputAdminClient
                                    text={data.address || ''}
                                    disabled={isDisabled}
                                    placeholder="company address"
                                    inputname="address"
                                    onChange={e => setData({ ...data, address: e.target.value })}
                                />
                                : ''}
                        </div>
                        <div className="flex items-center gap-x-[10px] max-w-[550px]">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Membership number:</p>
                            {data.doYouPayForHealthInsurance
                                ? <InputAdminClient
                                    text={data.membershipNumber.toString() || ''}
                                    disabled={isDisabled}
                                    placeholder="membership number"
                                    inputname="membershipNumber"
                                    onChange={e => setData({ ...data, membershipNumber: Number(e.target.value) })}
                                />
                                : ''}
                        </div>
                        <div className="flex items-center gap-x-[10px] max-w-[550px]">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Contact person:</p>
                            {data.doYouPayForHealthInsurance
                                ? <InputAdminClient
                                    text={data.contactPerson || ''}
                                    disabled={isDisabled}
                                    placeholder="contact"
                                    inputname="contactPerson"
                                    onChange={e => setData({ ...data, contactPerson: e.target.value })}
                                />
                                : ''}
                        </div>
                        <div className="flex items-center gap-x-[10px] max-w-[550px]">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Health insurance phone number:</p>
                            {data.doYouPayForHealthInsurance
                                ? <InputAdminClient
                                    text={data.phoneNumber || ''}
                                    disabled={isDisabled}
                                    placeholder='phone number'
                                    inputname="phoneNumber"
                                    onChange={e => setData({ ...data, phoneNumber: e.target.value })}
                                />
                                : ''}
                        </div>
                    </div>
                    <div className="grid gap-y-[45px]">
                        <div className="border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[5px] mt-[50px]">
                            <TextTitle title="Health insurance expenses for the audited period" className="!text-[18px]" />
                        </div>
                        {data.doYouPayForHealthInsurance && !error && isSuccess
                            ? <>
                                <div className="flex gap-5">
                                    {
                                        isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                            className={`${currentYear === item.id
                                                ? "border border-black"
                                                : ""}    !h-[32px] rounded-[15px]`}

                                            onClick={() => handleYear(item.id)}>
                                            {currYear === item.yearValue
                                                ? "Current year"
                                                : item.yearValue}</Button>)
                                    }
                                </div>
                                <div className={cls.scroll}>
                                    {dataMini && isSuccess && <div className={cls.scroll}>
                                        <RowTable
                                            name={"Health insurance"}
                                            disabled={isDisabled}
                                            data={dataMini}
                                            setDataSource={setDataMini}
                                            setDataMiniChange={setDataMiniChange} />
                                    </div>}
                                </div>
                            </>
                            : ''
                        }
                    </div>
                    <div className='grid gap-y-[12px]'>
                        <div className="flex items-center gap-x-[20px]">
                            <div className="flex items-center gap-x-[10px] max-w-[550px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Health insurance for this year was paid fully during this year:</p>
                                {data.doYouPayForHealthInsurance !== null
                                    ? <SelectClient
                                        text={ofBooleanText('paidFully')}
                                        disabled={isDisabled}
                                        options={yesNo}
                                        placeholder="paid fully"
                                        selectName="paidFully"
                                        onChange={(selectedOption) => ofTextBoolean('paidFully', `${selectedOption}`)}
                                        sizeText="w-[130px]"
                                    />
                                    : ''}
                            </div>
                            {data.paidFully ?
                                ''
                                : <div className={`flex items-start gap-x-[10px] max-w-[356px] ${data.paidFully ? 'hidden' : ''}`}>
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Reason:</p>
                                    <CustomTextAreaAdminClient
                                        text={data.explainWhyNot}
                                        disabled={isDisabled}
                                        placeholder="What is the reason of irreglar payments?"
                                        inputName="explainWhyNot"
                                        onChange={e => setData({ ...data, explainWhyNot: e.target.value })}
                                    />
                                </div>}
                        </div>
                        <div className="flex items-center gap-x-[20px]">
                            <div className="flex items-center gap-x-[10px] max-w-[550px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Payment for medical insurance:</p>
                                {data.doYouPayForHealthInsurance
                                    ? <SelectClient
                                        text={textNomal(`${data.regularly}`)}
                                        options={paymentFrequencyOptions}
                                        disabled={isDisabled}
                                        placeholder="medical insurance"
                                        selectName="regularly"
                                        onChange={(select) => setData({ ...data, regularly: `${select}` })}
                                        sizeText="w-[130px]"
                                    />
                                    : ''}
                            </div>
                            {data.doYouPayForHealthInsurance && data.regularly === 'IRREGULARLY'
                                ?
                                <div className="flex items-start gap-x-[10px] max-w-[356px] ">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Reason:</p>
                                    <CustomTextAreaAdminClient
                                        text={data.reasonWhyIrregularly}
                                        disabled={isDisabled}
                                        placeholder="What is the reason of irreglar payments?"
                                        inputName="reasonWhyIrregularly"
                                        onChange={e => setData({ ...data, reasonWhyIrregularly: e.target.value })}
                                    />
                                </div>
                                : ''}
                        </div>
                    </div>
                </Form>
        }
    </>)
}
export default ClientHealth
