import React, {useState} from 'react'
import {ButtonBack} from "components/ui/button/ButtonBack"
import TextLinkClient from "components/ui/text/textForAdmin/TextLinkClient"
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import ButtonAddInfoClient from "../../../components/ui/button/btnForAdmin/ButtonAddInfoClient";
import ClientData from './ClientData';
import ClientHealth from './ClientHealth';
import ClientProfits from './ClientProfits';
import ClientPension from './ClientPension';
import ClientUploud from './ClientUploud';
import ClientDocuments from './ClientDocuments';
import {ReactComponent as IconCancel} from '../../../assets/icons/close.svg'

import 'react-quill/dist/quill.snow.css'; // Импорт стилей
import NewButtonGreen from 'components/ui/button/NewButtonGreen';
import {useParams, useSearchParams} from "react-router-dom";
import ReactQuill from "react-quill";
import {useFetchClientsDocumentsQuery, useFetchClientsYourQuery} from 'services/clientControllerService';
import DropdownStatus from "../../../components/screen/AdminClient/Dropdawn/DropdownStatus";
import StatusClient from "../../../components/screen/AdminClient/Dropdawn/Status";
import {useUpdateStatusMutation} from "../../../services/detailsService";

const ClientDetials = () => {
    const [additionalInformation, setAdditionalInformation] = useState('')
    const [checkModal, setCheckModal] = useState(false)
    const [activeLink, setActiveLink] = useState('Personal Data')

    const [searchParams, setSearchParams] = useSearchParams();

    let {id} = useParams()
    const [editStatus, setEditStatus] = useState(searchParams.get('status') || "")

    const setStatusEdit = async (status: string) => {
        if (id) {
            const res = await updateStatus({id: id, status: status}).unwrap()
            setSearchParams({status: res.status})
            setEditStatus(res.status)
        }

    }
    const [updateStatus, {isLoading}] = useUpdateStatusMutation()
    const {data: getYour} = useFetchClientsYourQuery({Id: id || ''})
    const {data: getDocument} = useFetchClientsDocumentsQuery({Id: id || ''})
    const modalMessage = () => {
        return (
            <div className='absolute top-0 bottom-0 right-0 left-0 bg-gray/50 flex items-center justify-center'>
                <div className="w-[600px] h-[445px] bg-white flex flex-col px-[20px] py-[25px] gap-y-[22px]">
                    <div className="flex justify-between items-center p-0">
                        <p className="font-roboto font-medium text-[18px] text-[#0F0E0E] leading-[22px] p-0 m-0">Message
                            for Client</p>
                        <div onClick={() => setCheckModal(false)}>
                            <IconCancel/>
                        </div>
                    </div>
                    <ReactQuill
                        theme="snow"
                        value={additionalInformation}
                        onChange={setAdditionalInformation}
                        modules={{
                            toolbar: [
                                ['bold', 'italic', 'underline'],
                                [{'align': []}],
                                [{'list': 'ordered'}, {'list': 'bullet'}],
                                ['clean']
                            ],
                        }}
                        placeholder="Write message"
                        className="h-[276px] max-w-[556px] text-end z-999"
                    />
                    <div className="flex items-center justify-end mt-[25px]">
                        <NewButtonGreen
                            text="Send"
                            height="41px"
                        />
                    </div>
                </div>
            </div>
        )
    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    };
    return (
        <div className={`max-w-[1500px] box-border`}>
            {checkModal ? modalMessage() : ''}
            <div className={`grid gap-y-[20px]`}>
                <div className="flex items-center justify-between">
                    <TextTitle
                        title={`${getDocument?.map((item, index) => (`${index > 0 ? ` ${item.documentName}` : `${item.documentName}`}`))} for ${textNomal(getYour?.gender || '')}. ${getYour?.firstname}`}
                        className='!text-[28px]'/>
                    <ButtonBack text="Back to client list" className='!w-fit'/>
                </div>
                <div className="flex items-center justify-between">
                    <StatusClient status={editStatus} setEdit={setStatusEdit}/>
                    <ButtonAddInfoClient onClick={() => setCheckModal(true)}/>
                </div>
            </div>
            <div className="flex justify-between  items-center gap-x-[40px] mt-[50px]">
                <TextLinkClient text="Personal Data" setActive={setActiveLink} active={activeLink}/>
                <TextLinkClient text="Profits and expenses" setActive={setActiveLink} active={activeLink}/>
                <TextLinkClient text="Health insurance details" setActive={setActiveLink} active={activeLink}/>
                <TextLinkClient text="Pension contributions" setActive={setActiveLink} active={activeLink}/>
                <TextLinkClient text="Review Documents" setActive={setActiveLink} active={activeLink}/>
                <TextLinkClient text="Upload Documents" setActive={setActiveLink} active={activeLink}/>

            </div>
            <div className='pt-[50px]'>
                {activeLink === "Personal Data" && <ClientData id={id}/>}
                {activeLink === "Health insurance details" && <ClientHealth id={id}/>}
                {activeLink === "Profits and expenses" && <ClientProfits id={id}/>}
                {activeLink === "Pension contributions" && <ClientPension id={id}/>}
                {activeLink === "Review Documents" && <ClientDocuments id={id}/>}
                {activeLink === "Upload Documents" && <ClientUploud id={id}/>}
            </div>
        </div>
    )
}

export default ClientDetials
