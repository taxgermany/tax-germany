import { ChangeEvent, FC, useEffect, useState } from "react"
import LoadingMain from "components/ui/loading/LoadingMain"
import { Form } from "antd"
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import SelectClient from "components/ui/select/selectForAdmin/SelectClient"
import { gender, months, workPlaceArray, years } from "utils/Arrays"
import InputAdminClient from "components/ui/input/inputForAdmin/InputAdminClient"
import InputAdminLink from "components/ui/input/inputForAdmin/InputAdminLink"
import ButtonEdit from "components/ui/button/btnForAdmin/ButtonEdit"
import ButtonGray from "components/ui/button/ButtonGray"
import ButtonGreen from "components/ui/button/ButtonGreen"
import Upload from "components/ui/upload/Upload"
import { IReviewBussines, IReviewUser, IReviewYour } from "interfaces/reaview"
import { useAppSelector } from "hooks/useAppSelector"
import { useLazyFetchClientsBussinesQuery, useLazyFetchClientsUserQuery, useLazyFetchClientsYourQuery, useUpdateClientsBussinesMutation, useUpdateClientsUserMutation, useUpdateClientsYourMutation } from "services/clientControllerService"
import { IFileName } from "interfaces/ProfitsInterface"
import { useCreateFileMutation, useCreateFileNameBusinessMutation, useLazyFetchFileNameBusinessQuery } from "services/fileService"
import { useFetchCountryQuery, useFetchNationalityQuery } from "services/arayServices"


interface IClintData {
    id: string | undefined
}

const ClientData: FC<IClintData> = ({ id }) => {

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState<IReviewYour>({} as IReviewYour)
    const [oldData, setOldData] = useState<IReviewYour>({} as IReviewYour)
    const [dataBussines, setDataBussines] = useState<IReviewBussines>({} as IReviewBussines)
    const [oldDataBusineses, setOldDataBusiness] = useState<IReviewBussines>({} as IReviewBussines)
    const [dataUser, setDataUser] = useState<IReviewUser>({} as IReviewUser)
    const [oldDataUser, setOldDataUser] = useState<IReviewUser>({} as IReviewUser)
    const [isDisabled, setIsDisabled] = useState(true)
    const [country, setCountry] = useState<any[]>([])
    const [nation, setNation] = useState<any[]>([])
    const [fileBusiness, setFileBusiness] = useState<File | null | IFileName>(null);
    const [dateOfBirth, setDateOfBirth] = useState<any[]>([])
    const { client } = useAppSelector(state => state.authSlice)
    // 
    const [fetchData, { data: getData, isSuccess }] = useLazyFetchClientsYourQuery()
    const [fetchBusiness, { isSuccess: isSuccessBusiness }] = useLazyFetchClientsBussinesQuery()
    const [fetchUser, { isSuccess: isSuccessUser }] = useLazyFetchClientsUserQuery()
    const [getFileBusiness] = useLazyFetchFileNameBusinessQuery()
    const { data: getContr } = useFetchCountryQuery()
    const { data: getNation } = useFetchNationalityQuery()
    // 

    const [updateData] = useUpdateClientsYourMutation()
    const [updateBusiness] = useUpdateClientsBussinesMutation()
    const [updateUser] = useUpdateClientsUserMutation()
    const [createFile] = useCreateFileNameBusinessMutation()
    // const [updateFile] = useUpdate

    const fetchingUserDatas = async () => {
        try {
            setLoading(true);

            const [resYour, resBusiness, resUser] = await Promise.all([
                fetchData({ Id: id || '' }).unwrap(),
                fetchBusiness({ Id: id || '' }).unwrap(),
                fetchUser({ Id: id || '' }).unwrap(),
            ]);

            if (resYour && resBusiness && resUser) {
                const resFile = await getFileBusiness({ userId: id || 0 }).unwrap();
                setDataUser(resUser);
                setOldDataUser({ ...resUser });
                setData(resYour);
                setOldData({ ...resYour });
                setDataBussines(resBusiness);
                setOldDataBusiness({ ...resBusiness });
                setDateOfBirth(resYour.dateOfBirth?.split('-'));
                setFileBusiness(resFile);
                setLoading(false);
            }
        } catch (error: any) {
            console.error(error.message);
        }
    };


    async function fetchDropDauwn() {
        try {
            if (getContr && getNation) {
                await setCountry(getContr);
                await setNation(getNation);
            }
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        (async () => {
            await fetchingUserDatas()
            fetchDropDauwn()
        })()

    }, [isSuccess, isSuccessBusiness, isSuccessUser])

    const textDate = (originDate: string) => {
        const part = originDate.split('-')
        return `${part[2]}/${part[1]}/${part[0]}`
    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    };
    // const dateOfBirth = data.dateOfBirth.split('-')

    const initialValuess = (dat: IReviewYour, datUser: IReviewUser, datBussines: IReviewBussines) => (
        {
            email: datUser.email || '',
            phoneNumber: datUser.phoneNumber || '',
            gender: `${data.gender || ''}`,
            firstName: `${dat.firstname || ''}`,
            lastname: dat.lastname || '',
            dateOfBirth: dat.dateOfBirth || '',
            country: dat.country || '',
            nationality: dat.nationality || '',
            typeOfFreelance: dat.typeOfFreelance || '',
            // from, to
            montheOfBirthe: months
                .filter(item => dateOfBirth ? item.value === dateOfBirth[1] : [{ label: '' }])
                .map(item => item.label),
            yearOfBirthe: dateOfBirth ? dateOfBirth[0] : '',
            businessName: datBussines.businessName || '',
            city: datBussines.city || '',
            postalCode: datBussines.postalCode || '',
            street: datBussines.street || '',
            building: datBussines.building || '',
            businessPhone: datBussines.businessPhone || '',
            businessEmail: datBussines.businessEmail || '',
            website: datBussines.website || '',
            freelanceType: `${datBussines.freelanceType === 'COMMERCIAL' ? 'Commercial (Gewerblich)' : datBussines.freelanceType === 'INTELLECTUAL' ? 'Intellectual / artisitic (Freiberuflich)' : ''}`,
            workPlace: textNomal(`${datBussines.workPlace || ''}`),
            lastUpdatedAt: dat.lastUpdatedAt || ''
        })
    const btnPut = async () => {
        await updateData({
            Id: id || '',
            data: data
        });

        await updateBusiness({
            Id: id || '',
            data: dataBussines
        });

        await updateUser({
            Id: id || '',
            data: dataUser
        });
        if (fileBusiness) {
            let fileToUpload: File | null = null;

            // Проверяем, является ли fileBusiness экземпляром FormData
            if (fileBusiness instanceof FormData) {
                // Извлекаем File из FormData
                const file = fileBusiness.get("file") as File;

                // Проверяем, является ли извлеченное значение File
                if (file instanceof File) {
                    fileToUpload = file;
                } else {
                    // Обработка случая, если fileBusiness - это FormData, но внутри нет File
                    // Вы можете обработать этот случай в соответствии с логикой вашего приложения
                }
            } else if (fileBusiness instanceof File) {
                fileToUpload = fileBusiness;
            }

            if (fileToUpload) {
                await createFile({
                    userId: id || 0,
                    data: fileToUpload
                });
            }
        }

        setIsDisabled(true)
    }



    const btnCancel = () => {
        setData(oldData)
        setDataBussines(oldDataBusineses)
        setDataUser(oldDataUser)
        setIsDisabled(true)
    }

    return (<>
        {
            loading ? <LoadingMain />
                : <Form
                    name="appData"
                    layout="horizontal"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    onFinish={btnPut}
                    initialValues={initialValuess(data, dataUser, dataBussines)}
                    className="box-border ">
                    <div className={`flex box-border items-center justify-between border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[10px]`}>

                        <div className="w-[45%]">
                            <TextTitle title="Freelancer personal data" />
                        </div>
                        <div className="w-[45%] ">
                            <TextTitle title="Business details" />
                        </div>
                        <div className="w-[18%] ">
                            <div className=" flex gap-x-[10px] justify-end ">
                                {!isDisabled ? <>
                                    <ButtonGray onClick={btnCancel} text="Cancel" backraund={false} size="!w-[95px] !h-[48px]" className="!p-0" />
                                    <ButtonGreen text="Save" size="w-[95px] !h-[48px]" /></>
                                    : <ButtonEdit onClick={() => setIsDisabled(false)} />}
                            </div>
                        </div>
                    </div>
                    <div className="flex items-start justify-between gap-x-[20px]">
                        <div className="w-[45%] grid gap-y-[12px] ">
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Gender:</p>
                                <SelectClient
                                    options={gender}
                                    disabled={isDisabled}
                                    selectName="gender"
                                    placeholder="gender"
                                    text={textNomal(`${data.gender || ''}`)}
                                    className="text-start"
                                    onChange={(selectedOption) => setData({ ...data, gender: `${selectedOption}` })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">First name:</p>
                                <InputAdminClient
                                    type='text'
                                    disabled={isDisabled}
                                    placeholder="first name"
                                    inputname="firstName"
                                    text={data.firstname || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, firstname: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Last name:</p>
                                <InputAdminClient
                                    type='text'
                                    disabled={isDisabled}
                                    placeholder="last name"
                                    inputname="lastname"
                                    text={data.lastname || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, lastname: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Date of birth:</p>
                                <InputAdminClient
                                    type='date'
                                    disabled={isDisabled}
                                    placeholder="birthday"
                                    inputname="dateOfBirth"
                                    text={textDate(`${data.dateOfBirth || ''}`)}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, dateOfBirth: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Country of birth:</p>
                                <SelectClient
                                    disabled={isDisabled}
                                    placeholder="Country of birth"
                                    selectName="country"
                                    options={country}
                                    text={data.country || ''}
                                    className=" text-start"
                                    onChange={(selectedOption) => setData({ ...data, country: `${selectedOption}` })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Nationality:</p>
                                <SelectClient
                                    disabled={isDisabled}
                                    selectName="nationality"
                                    placeholder="nationality"
                                    text={data.nationality || ''}
                                    options={nation}
                                    className=" text-start"
                                    onChange={(selectedOption) => setData({ ...data, nationality: `${selectedOption}` })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Type of freelancing industry:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    placeholder="industry"
                                    inputname="typeOfFreelance"
                                    text={data.typeOfFreelance || ''}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, typeOfFreelance: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Phone number:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    placeholder="phone number"
                                    inputname="phoneNumber"
                                    text={dataUser.phoneNumber || ''}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataUser({ ...dataUser, phoneNumber: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Email:</p>
                                <InputAdminClient
                                    type='email'
                                    disabled={isDisabled}
                                    placeholder="email"
                                    inputname="email"
                                    text={dataUser.email || ''}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataUser({ ...dataUser, email: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Registered at:</p>
                                <InputAdminClient
                                    type='date'
                                    disabled={true}
                                    placeholder="Registered at"
                                    inputname="fromDate"
                                    text={textDate(`${data.registeredAt || ''}`)}
                                    className=" text-start"
                                // onChange={(e) => setData({ ...data, fromMonth: e })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Last updated at:</p>
                                <InputAdminClient
                                    type='date'
                                    disabled={isDisabled}
                                    placeholder="Last updated at"
                                    inputname="lastUpdatedAt"
                                    text={textDate(`${data.lastUpdatedAt || ''}`)}
                                    className=" text-start"
                                    onChange={(e) => setData({ ...data, lastUpdatedAt: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className={`font-roboto font-[400] leading-[19px] text-black/30 w-[140px] ${!isDisabled ? 'mt-[9px]' : ''}`}>Freelance since:</p>
                                {isDisabled
                                    ? <p>
                                        {months.map(item => item.value === dateOfBirth[1] ? item.label.substring(0, 3) : '')},
                                        {dateOfBirth[0] || ''}
                                    </p>
                                    : <div className="grid gap-y-[12px]">
                                        <SelectClient
                                            disabled={isDisabled}
                                            selectName="montheOfBirthe"
                                            placeholder="mothe"
                                            text={'data.toDate'}
                                            options={months}
                                            className=" text-start"
                                        />
                                        <SelectClient
                                            disabled={isDisabled}
                                            selectName="yearOfBirthe"
                                            placeholder="year"
                                            text={'data.fromDate'}
                                            options={years}
                                            className=" text-start"
                                        />
                                    </div>}
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]"> </p>
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[140px]">Income period:</p>
                                <div className="w-[254px]">
                                    {data.fromMonth && data.fromYear && data.toMonth && data.toYear ?
                                        <p>from <b>{data.fromMonth?.substring(0, 3)}, {data.fromYear}</b> to <b>{data.toMonth?.substring(0, 3)}, {data.toYear}</b></p>
                                        : ''}
                                </div>
                            </div>
                        </div>
                        <div className="w-[45%] grid gap-y-[12px] ">
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Business name:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    placleholder="business name"
                                    inputname="businessName"
                                    text={dataBussines.businessName || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, businessName: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Business address:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    placeholder="business address"
                                    inputname="city"
                                    text={dataBussines.city || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, city: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Postal code:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    placeholder="postale code"
                                    inputname="postalCode"
                                    text={dataBussines.postalCode || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, postalCode: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Street:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    inputname="street"
                                    placeholder="street"
                                    text={dataBussines.street || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, street: e.target.value })} />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Building:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    inputname="building"
                                    placeholder="building"
                                    text={dataBussines.building || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, building: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Business phone:</p>
                                <InputAdminClient
                                    type="text"
                                    disabled={isDisabled}
                                    placeholder="business phone"
                                    inputname="businessPhone"
                                    text={dataBussines.businessPhone || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, businessPhone: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Business email:</p>
                                <InputAdminClient
                                    type='email'
                                    disabled={isDisabled}
                                    placeholder="business email"
                                    inputname="businessEmail"
                                    text={dataBussines.businessEmail || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, businessEmail: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Business website:</p>
                                <InputAdminLink
                                    type="text"
                                    disabled={isDisabled}
                                    placeholder="business website"
                                    inputname="website"
                                    text={dataBussines.website || ''}
                                    className=" text-start"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, website: e.target.value })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Type of freelance:</p>
                                <SelectClient
                                    type="text"
                                    disabled={isDisabled}
                                    options={[
                                        { value: 'COMMERCIAL', label: 'Commercial (Gewerblich)' },
                                        { value: 'INTELLECTUAL', label: 'Intellectual / artisitic (Freiberuflich)' }]}
                                    placeholder="type of freelance"
                                    selectName="freelanceType"
                                    text={textNomal(`${dataBussines.freelanceType || ''}`)}
                                    className=" text-start"
                                    onChange={(selectedOption) => setDataBussines({ ...dataBussines, freelanceType: `${selectedOption}` })}
                                />
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className={`font-roboto font-[400] leading-[19px] text-black/30 w-[205px] ${`COMMERCIAL` === dataBussines.freelanceType ? '' : 'text-[#0F0E0E4D]'}`}>Trade Registration Form (Gewerbeanmeldung):</p>
                                {`COMMERCIAL` === dataBussines.freelanceType ?
                                    <>
                                        <Upload
                                            fileId="file"
                                            fileCheck={fileBusiness as File | null}
                                            setFileCheck={setFileBusiness as React.Dispatch<React.SetStateAction<File | null>>}
                                            disabled={isDisabled}
                                        />

                                    </>
                                    : ''

                                }
                            </div>
                            <div className={`w-[80%]`}>
                                <textarea
                                    className={`w-[100%] h-[92px] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none ${isDisabled ? 'hidden' : ''}`}
                                    name="freelanceType"
                                    id="freelanceType"
                                    placeholder="Why you can’t submit the document?"
                                    defaultValue={dataBussines.cantUploadTradeRegistration}
                                    disabled={isDisabled}
                                ></textarea>
                            </div>
                            <div className="flex items-center gap-x-[10px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[205px]">Workplace:</p>
                                <SelectClient
                                    type="text"
                                    disabled={isDisabled}
                                    options={workPlaceArray}
                                    selectName="workPlace"
                                    placeholder="work place"
                                    text={textNomal(`${dataBussines.workPlace || ''}`)}
                                    className="text-start"
                                    onChange={(selectedOption) => setDataBussines({ ...dataBussines, workPlace: `${selectedOption}` })}
                                />
                            </div>
                            <div className={`w-[100%] ${isDisabled && dataBussines.workPlace === 'OTHER_PLACE' ? 'hidden' : ''}`}>
                                <textarea
                                    className="w-[100%] h-[92px] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none"
                                    defaultValue={dataBussines.anotherWorkPlace}
                                    name="workPlace"
                                    id="workPlace"
                                    placeholder="What is the other place?"
                                    onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setDataBussines({ ...dataBussines, anotherWorkPlace: e.target.value })}
                                ></textarea>
                            </div>
                        </div>
                        <div className="w-[18%]"></div>
                    </div>
                </Form>
        }
    </>)
}
export default ClientData
