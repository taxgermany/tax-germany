import { ReactComponent as IconReview } from "../../../assets/icons/review.svg";

export const ClientReview = () => {
  return (
    <div>
      <div className="w-[900px] h-[900px] border">
        <div className="icon flex justify-end mt-4 mr-10">
          <IconReview className="w-[440px]" />
        </div>
        <div className="flex">
          <div className="mr-auto">
            <p className="text-[12px]">
              Steuerberater Suat Göydeniz & Grit Birka GbR <br /> Kottbusser
              Damm 68 in 10967 Berlin
            </p>
            <div className="text-[20px] font-[500]">
              <p>Herr</p>
              <p>Mehmet Nuri Kar</p>
              <p>Karl-Marx-Straße 212</p>
              <p>12051 Berlin</p>
            </div>
            <div>
              <p className="font-[500]">10. Juli 2023</p>
              <p>OK/GB/99999</p>
            </div>
           
          </div>
          <div className="">
            <p>
              Suat Göydeniz <br /> Steuerberater
            </p>
            <p>
              Grit Birka <br /> Steuerberater
            </p>
            <p>
              Kottbusser Damm 68 <br />
              10967 Berlin
            </p>
            <p>
              Fon: 030 355 214 65 <br />
              Fax: 030 922 86 398
            </p>
            <p>
              mail@stb-goeydeniz-birka.de <br />
              www.stb-goeydeniz-birka.de
            </p>
            <p>
              Steuernummer: <br />
              16/311/00545
            </p>
            <p>
              Bankverbindung: <br />
              IBAN: <br />
              DE78120300001051531885 <br />
              BIC: <br />
              BYLADEM1001 <br />
              Kreditinstitut: <br />
              Deutsche Kreditbank Berlin
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
