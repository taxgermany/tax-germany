import React, {FC, useEffect, useState} from "react"
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import Upload from "components/ui/upload/Upload"
import {
    useDeleteFileMutation,
    useUploadFileWithIdDocMutation
} from "../../../services/fileService";
import {useGetFilesQuery, useLazyGetFilesQuery} from "../../../services/statusService";
import {ReactComponent as IconTask} from "../../../assets/icons/task.svg";
import {ReactComponent as IconDelete} from "../../../assets/icons/delete.svg";
import {IFile} from "../../../interfaces/statusInterface";
import {Spin} from "antd";


interface IUploadDoc {
    name: string
    id: string | number
    idDoc: number
    idFile: number
    fileData: {
        id: number,
        type: string,
        file: string
        name: string
    } | null
}

export const UploadDoc: FC<IUploadDoc> = ({name, id, idDoc, fileData, idFile}) => {
    const [file, setFile] = useState<File | null>(null)
    const [fileId, setFileId] = useState<number | null>(idFile)
    const [newFileData, setNewFileId] = useState<{
        id: number,
        type: string,
        file: string
        name: string
    } | null>(fileData)

    console.log(idFile)

    const [CreatFile, {isLoading: isLoading}] = useUploadFileWithIdDocMutation()

    const [deleteFile, {isLoading: isLoadingDelete}] = useDeleteFileMutation()

    const fetchDelete = async () => {
        if (newFileData) {
            await deleteFile({fileId: newFileData.id})
            setFile(null)
            setFileId(null)
            setNewFileId(null)
        }

    }

    const setFileUpload = async (file: File | null) => {

        if (!file && fileId) {
            await deleteFile({fileId})
            setFile(null)
            setFileId(null)
        }
        if (file && id) {
            setFile(file)
            const formData = new FormData();
            formData.append("file", file)
            const res = await CreatFile({data: formData, userId: id, idDoc: idDoc}).unwrap()
            setFileId(res.id)
        }
    }

    return (
        <div>
            <div
                className={`flex box-border items-center justify-between border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[10px]`}>
                <TextTitle title={`Upload ${name} `}/>
            </div>
            <div className="flex items-center justify gap-x-[55px] mt-[30px]">
                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[195px]">{name} </p>
                {!newFileData
                    ? <Upload fileId={`fileAudit_${idDoc}`} fileCheck={file} setFileCheck={setFileUpload}
                              background="bg-green text-white text-center"/>
                    :
                    <div>
                        <div className="flex  justify-between items-center gap-y-[10px]">
                            <div className="flex jusify-between items-center gap-x-[22px]">
                        <span className="flex items-center gap-x-[10px]">
                            <IconTask/>
                            <p className={` text-[14px] underline decoration-solid cursor-pointer`}>
                                {newFileData.name}
                            </p>
                        </span>
                                <button
                                    onClick={() => fetchDelete()}
                                    className='cursor-pointer'>
                                    <IconDelete/>
                                </button>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}


interface IClientUploud {
    id: string | undefined
}

const ClientUploud: FC<IClientUploud> = ({id = 0}) => {
        /*const {data: netData, isLoading} = useGetFilesQuery(+id)*/
    const [document, setDocument] = useState<IFile[] | null>(null)
    const [getData, {data, isError: isFilesError,isLoading:isLoadingDoc}] = useLazyGetFilesQuery();

    const fetchDoc = async () => {
        if (id) {
            const res = await getData(+id).unwrap()
            setDocument(res)
        }
    }

    useEffect(() => {
        fetchDoc()
        return (setDocument(null) )

    }, [])
    return (
        <div className="box-border grid gap-y-[40px]">
            {isLoadingDoc && <Spin size="large" className="absolute top-2/4 left-2/4"/>}
            {document && id && document.map((item) =>
                <div key={item.documentId}>
                    <UploadDoc id={id} idDoc={item.documentId} name={item.documentName.documentName}
                               fileData={item.fileDto} idFile={item.fileDto?.id} />
                </div>
            )}
        </div>
    )
}
export default ClientUploud
