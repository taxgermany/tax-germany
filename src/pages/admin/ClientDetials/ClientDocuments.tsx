import AdminRewDocument from "components/ui/upload/AdminUpload/AdminRewDocument";
import {ReactComponent as IconWar} from "../../../assets/icons/warning.svg";
import React, {FC, useEffect, useState} from 'react'

import IncomeConfirmDocument from "components/screen/generationDocument/IncomeConfirmDocument";

import Net from "../../../components/screen/AdminClient/AdminNetTable/Net";
import {ReactComponent as Arrow} from "../../../assets/icons/east.svg";
import Audit from "../../../components/screen/generationDocument/Audit";
import {useFetchDetailsDocumentQuery} from "../../../services/detailsService";



interface IClientDocuments {
    id: string | undefined
}

const ClientDocuments: FC<IClientDocuments> = ({id}) => {
    const [genaration, setGeneration] = useState('')
    const [active, setActive] = useState(false)

const {data:netData,isLoading} = useFetchDetailsDocumentQuery({id:id || "0"})
   /* let netData = [
        {documentName: "Audit Report"},
        {documentName: "Income Confirmation"},
        {documentName: "Net profit determination"}
    ]*/
    const handleChoose = (name: string) => {
        setGeneration(name)
        setActive(true)
    }

    return (

        <div className="grid gap-y-[40px]">

            <div>
                {!active ? <div>
                        <p className="mb-[5px] flex gap-x-[5px]">
                            <IconWar />
                            Before you send the document to the
                            client, make sure that everything is correct
                        </p>
                        {netData && netData.map(item => <AdminRewDocument text={item.documentName}
                                                                          onClick={() => handleChoose(item.documentName)}/>)}


                </div>
                    : <div>
                        <div onClick={() => setActive(false)}
                            className={` flex gap-[11px] w-fit items-center cursor-pointer`}>
                            <Arrow />
                            <button className="inter text-green font-[500]">Back to review documents</button>
                        </div>
                        {genaration === "Audit Report" && id && <Audit id={id}/>}
                        {genaration === "Income Confirmation" && <IncomeConfirmDocument id={id}/>}
                        {genaration === "Net profit determination" && <Net id={id}/>}
                    </div>}
            </div>

        </div>
    )
}
export default ClientDocuments
