import { FC, useEffect, useState } from "react"
import { Button, Form } from "antd"
import LoadingMain from "components/ui/loading/LoadingMain"
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import NewButtonGreen from "components/ui/button/NewButtonGreen"
import SelectClient from "components/ui/select/selectForAdmin/SelectClient"
import { accounting, yesNo } from "utils/Arrays"
import ButtonEdit from "components/ui/button/btnForAdmin/ButtonEdit"
import ButtonGray from "components/ui/button/ButtonGray"
import cls from './ClientDetials.module.scss'
// large table
import LargeTable from "components/ui/table/LargeTable"
import { IEmployer, ITable, ITableVot } from "interfaces/TableInterface"
import { useFetchAllYearQuery } from "services/yearService"
import {
    useCreateFileProfitsMutation,
    useLazyFetchFileNameProfitsQuery
} from "services/fileService"
import { IYear } from "interfaces/YearInterface"
import { totalNull } from "pages/freelancerPage/profitandexpenses/ProfitAndExpensesLargeTable"
import MicroTable from "components/ui/table/microTable"
import { IReviewProfits } from "interfaces/reaview"
import {
    useLazyFetchClientsProfitsQuery,
    useLazyFetchClientsTableEmployerQuery,
    useLazyFetchClientsTableQuery,
    useUpdateClientsProfitsMutation,
    useUpdateClientsTableEmployerMutation,
    useUpdateClientsTableMutation
} from "services/clientControllerService"
import { IFileName } from "../../../interfaces/ProfitsInterface";

interface IClientProfits {
    id: string | undefined
}

const ClientProfits: FC<IClientProfits> = ({ id }) => {


    const [loading, setLoading] = useState(false)
    const [data, setData] = useState<IReviewProfits>({} as IReviewProfits)
    const [oldData, setOldData] = useState<IReviewProfits>({} as IReviewProfits)
    const [isDisabled, setIsDisabled] = useState(true)

    // Large NetTable

    const currentDate = new Date();
    const currYear = currentDate.getFullYear();

    const [dataLarge, setDataLarge] = useState<ITableVot | ITable | null>(null);
    const [fileCheck, setFileCheck] = useState<File | null>(null);
    const [file, setFile] = useState<IFileName | null>(null);
    const [currentYear, setCurrentYear] = useState(0)
    const [dataLargeChange, setDataLargeChange] = useState<boolean>(false);
    const [updateEmployTable] = useUpdateClientsTableEmployerMutation()

    const [getEmployer, {
        data: tableEmployer = [] as IEmployer[],
        error: errorEmployer,
        isLoading: isLoadingEmployer,
        isSuccess: isSuccessEmployer
    }] = useLazyFetchClientsTableEmployerQuery()

    const [dataMicroTable, setDataMicroTable] = useState<IEmployer[] | null>(null);
    // [...tableEmployer]
    const {
        data: years = [] as IYear[],
        error: errorYear,
        isLoading: isLoadingYear,
        isSuccess: isSuccessYear
    } = useFetchAllYearQuery(id || 0);


    const [getTable, {
        data: table = {} as ITable | ITableVot,
        error,
        isSuccess
    }] = useLazyFetchClientsTableQuery()

    const [UpdateTable] = useUpdateClientsTableMutation()

    const [CreatFile] = useCreateFileProfitsMutation()

    const updateData = async () => {

        let newDataLarge = {
            ...JSON.parse(JSON.stringify(dataLarge)),
            total: totalNull,
            totalYearProfit: 0
        } as ITableVot | ITable

        if (dataLargeChange) {

            newDataLarge?.months.forEach(item => {
                item.grossRevenueInclVat = 0;
                item.expenses = 0;
                item.monthlyProfits = 0;
            });
            console.log(newDataLarge)
            UpdateTable({ data: newDataLarge as ITableVot | ITable, yearId: currentYear, vat: data.vat })
            setDataLargeChange(false)
        }

        if (fileCheck && id) {
            const formData = new FormData();
            formData.append("file", fileCheck)
            await CreatFile({ data: formData, userId: +id })
        }

    }
    const handleYear = async (idYear: number) => {
        setCurrentYear(idYear)
        if (id) {
            getTable({
                userId: id,
                yearId: idYear,
                vat: data.vat
            }).then((res) => setDataLarge(res.data as ITableVot | ITable))
            setDataLarge(table)

            updateData()
        }

        if (fileCheck && id) {
            const formData = new FormData();
            formData.append("file", fileCheck)
            await CreatFile({ data: formData, userId: +id })
        }

    }
    
    const [getProfitsData] = useLazyFetchClientsProfitsQuery()

    const [UpdateProfits] = useUpdateClientsProfitsMutation()

    const [getFileProfits, { isLoading: isLoadingFileProfits }] = useLazyFetchFileNameProfitsQuery()

    const changeVat = (vat: string) => {

        if (id)
            getTable({
                userId: id,
                yearId: currentYear,
                vat: vat === 'Yes'
            }).then((data) => setDataLarge(data.data as ITableVot | ITable))
    }


    const getDataProfits = async () => {
        const res = await getProfitsData({ Id: id || '' }).unwrap()
        await setData(res)
        await setOldData({ ...res })
        await setLoading(false)
        console.log(initialValuess);

        if (isSuccessYear && id && years.length > 0 && data) {
            setCurrentYear(years[0].id || 0)
            const resFile = await getFileProfits({ userId: +id }).unwrap()
            setFileCheck(resFile.file)
            setFile(resFile)
            getTable({
                userId: id,
                yearId: years[0].id || 0,
                vat: res.vat
            }).then((data) => setDataLarge(data.data as ITableVot | ITable))
            getEmployer(id || 0).then(res => setDataMicroTable(res.data as IEmployer[]))
        }
    }

    useEffect(() => {
        getDataProfits()
    }, [isSuccessYear])

    useEffect(() => {
        if (isSuccess) setDataMicroTable(tableEmployer)
    }, [isSuccess])
    //

    const ofTextBoolean = (item: string, text: string) => {
        if ('Yes' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: true,
                vat: data.vat
            }))
        } else if ('No' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: false,
                vat: data.vat
            }))

        }
    }
    const ofBooleanText = (item: string) => {
        console.log(`${item}: ${data[item as keyof IReviewProfits]}`);

        if (data[item as keyof IReviewProfits]) {
            return 'Yes';
        } else {
            return 'No';
        }

    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const initialValuess = {
        accountingType: textNomal(`${data.accountingType}`),
        regularly: ofBooleanText('regularly'),
        haveEmployees: ofBooleanText('haveEmployees'),
        vat: ofBooleanText('vat')
    }
    const putData = async () => {
        try {
            updateData()
            if(dataMicroTable) await updateEmployTable(dataMicroTable)
            UpdateProfits({
                Id: id || '',
                data: data
            })
        } catch (error) {
            console.log(error)
        }
    }
    const btnPut = () => {
        putData()
    }

    const btnCancel = () => {
        setData(oldData)
        setIsDisabled(true)
    }

    //

    return (<>
        {
            loading ? <LoadingMain />
                : <Form
                    name="appData"
                    layout="horizontal"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    onFinish={btnPut}
                    initialValues={initialValuess}
                    className="box-border ">
                    <div
                        className={`flex box-border items-center justify-between border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[10px] `}>
                        <div className="w-fit">
                            <TextTitle title="Profits and expenses" />
                        </div>
                        <div className="w-[50%] ">
                            <div className=" flex gap-x-[10px] justify-end ">
                                {!isDisabled ? <>
                                    <ButtonGray onClick={btnCancel} text="Cancel" backraund={false}
                                        size="!w-[94px] !h-[48px]" className="!p-0" />
                                    <NewButtonGreen text="Save" height="48px" className="w-[94px] !p-0" /></>
                                    : <ButtonEdit onClick={() => setIsDisabled(false)} />}
                            </div>
                        </div>
                    </div>
                    <div className="grid gap-y-[12px]">
                        <div className="flex items-center justify-between gap-x-[10px] max-w-[550px] w-1/2">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 max-w-[205px] w-fit">Bookkeeping
                                type:</p>
                            <SelectClient
                                text={textNomal(`${data.accountingType || ''}`)}
                                disabled={isDisabled}
                                options={accounting}
                                placeholder="bookkeeping type"
                                selectName="accountingType"
                                onChange={e => setData({ ...data, accountingType: `${e}` })}
                            />
                        </div>
                        <div className="flex items-center justify-between gap-x-[10px] max-w-[550px] w-1/2">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 max-w-[205px] w-fit">Preparing
                                books regularly:</p>
                            <SelectClient
                                text={data.regularly !== null ? ofBooleanText('regularly') : ''}
                                background={false}
                                options={yesNo}
                                disabled={isDisabled}
                                border={true}
                                onChange={(selectedOption) => ofTextBoolean('regularly', `${selectedOption}`)}
                                selectName="regularly"
                                placeholder="regularly"
                            />
                        </div>
                        <div className="flex items-center justify-between gap-x-[10px] max-w-[550px] w-1/2">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 max-w-[205px] w-fit">VAT:</p>
                            <SelectClient
                                text={data.vat !== null ? ofBooleanText('vat') : ''}
                                background={false}
                                options={yesNo}
                                disabled={isDisabled}
                                border={true}
                                onChange={(selectedOption) => {
                                    ofTextBoolean('vat', `${selectedOption}`)
                                    changeVat(`${selectedOption}`)
                                }}
                                selectName="vat"
                                placeholder="vat"
                            />
                        </div>
                        <div className="flex items-center justify-between gap-x-[10px] max-w-[550px] w-1/2">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 max-w-[205px] w-fit">Employees
                                availability:</p>
                            <SelectClient
                                text={data.haveEmployees !== null ? ofBooleanText('haveEmployees') : ''}
                                background={false}
                                options={yesNo}
                                disabled={isDisabled}
                                border={true}
                                onChange={(selectedOption) => ofTextBoolean('haveEmployees', `${selectedOption}`)}
                                placeholder="have employees"
                                selectName="haveEmployees"
                            />
                        </div>

                    </div>
                    <div>
                        {data.haveEmployees && isSuccessEmployer && !isLoadingEmployer && !errorEmployer
                            ? <div className="relative">
                                {isSuccessEmployer && !errorEmployer && dataMicroTable &&
                                    <MicroTable
                                        data={dataMicroTable}
                                        setDataSource={setDataMicroTable}
                                        disabled={isDisabled}
                                    />
                                }
                            </div>
                            : ''}
                    </div>
                    <div
                        className={`flex box-border items-center justify-between border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[5px] mt-[50px]`}>
                        <div className="w-fit">
                            <TextTitle title="Profits" className="!text-[18px]" />
                        </div>
                    </div>
                    <div>
                        {data.vat
                            ? <>
                                <div>
                                    {isLoadingYear && <h1>loading</h1>}
                                    {errorYear && <h1>error</h1>}
                                    {isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                        className={`${currentYear === item.id
                                            ? "border border-black"
                                            : ""} !h-[32px] rounded-[15px]`}

                                        onClick={() => handleYear(item.id)}>
                                        {currYear === item.yearValue
                                            ? "Current year"
                                            : item.yearValue}</Button>)
                                    }
                                </div>
                                <div className={cls.scroll}>
                                    {dataLarge && file && isSuccess && !error && <LargeTable
                                        file={file}
                                        vat={data.vat}
                                        data={dataLarge}
                                        setDataLargeChange={setDataLargeChange}
                                        setDataSource={setDataLarge}
                                        fileCheck={fileCheck}
                                        setFileCheck={setFileCheck}
                                        disabled={isDisabled}
                                    />
                                    }

                                </div>
                            </>
                            : <p className="font-roboto font-[400] leading-[18.75px]">No</p>}
                    </div>
                </Form>
        }
    </>)
}
export default ClientProfits
