import { FC, useEffect, useState } from "react"
import LoadingMain from "components/ui/loading/LoadingMain"
import { Button, Form } from "antd"
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import NewButtonGreen from "components/ui/button/NewButtonGreen"
import SelectClient from "components/ui/select/selectForAdmin/SelectClient"
import { pensionSecurity, yesNo } from "utils/Arrays"
import InputAdminClient from "components/ui/input/inputForAdmin/InputAdminClient"
import ButtonEdit from "components/ui/button/btnForAdmin/ButtonEdit"
import ButtonGray from "components/ui/button/ButtonGray"
import CustomTextAreaAdminClient from "components/ui/input/inputForAdmin/CustomTextAreaAdminClient"
import cls from './ClientDetials.module.scss'
// table
import RowTable from "components/ui/table/RowTable"
import { IMiniTable, IMiniTableWithTotal } from "interfaces/TableInterface"
import { useFetchAllYearQuery } from "services/yearService"
import { IYear } from "interfaces/YearInterface"
import { IReviewLife, IReviewPension } from "interfaces/reaview"
import { useFetchClientsLifeQuery, useFetchClientsPensionQuery, useLazyFetchClientsPensionMiniTableQuery, useUpdateClientsLifeMutation, useUpdateClientsPensionMiniTableMutation, useUpdateClientsPensionMutation } from "services/clientControllerService"
interface IClientPension {
    id: string | undefined
}
const ClientPension: FC<IClientPension> = ({ id }) => {

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState<IReviewPension>({} as IReviewPension)
    const [oldData, setOldData] = useState<IReviewPension>({} as IReviewPension)
    const [dataLife, setDataLife] = useState<IReviewLife>({} as IReviewLife);
    const [oldDataLife, setOldDataLife] = useState<IReviewLife>({} as IReviewLife)
    const [isDisabled, setIsDisabled] = useState(true)
    // NetTable
    const currentDate = new Date();
    const currYear = currentDate.getFullYear();

    const [currentYear, setCurrentYear] = useState(1)

    const [dataMiniChange, setDataMiniChange] = useState<boolean>(false);

    const [dataMini, setDataMini] = useState<IMiniTableWithTotal | null>(null);



    const [getTable, { data: table = {} as IMiniTableWithTotal, error, isSuccess }] = useLazyFetchClientsPensionMiniTableQuery()


    const {
        data: years = [] as IYear[],
        isSuccess: isSuccessYear
    } = useFetchAllYearQuery(id || 0);


    const [UpdateTableMini] = useUpdateClientsPensionMiniTableMutation()

    const updateData = async () => {
        if (dataMiniChange) {
            const { total, ...newData } = dataMini as IMiniTableWithTotal
            const res = await UpdateTableMini({ data: newData as IMiniTable, yearId: currentYear })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini(res.data as IMiniTableWithTotal)
            setDataMiniChange(false)
        }
    }
    const handleYear = (idYear: number) => {

        setCurrentYear(idYear)
        if (idYear && id) {
            getTable({
                userId: id,
                yearId: idYear,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        } updateData()

    }

    useEffect(() => {
        if (isSuccessYear && id && years.length > 0) {
            setCurrentYear(years[0].id || 0)
            getTable({
                userId: id,
                yearId: years[0].id || 0,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        }
    }, [isSuccessYear])
    //
    //end Table

    const { data: getData, isSuccess: isSuccessPension } = useFetchClientsPensionQuery({ Id: id || '' })
    const { data: getLifeData, isSuccess: isSuccessLife } = useFetchClientsLifeQuery({ Id: id || '' })
    // Update
    const [updateDataPension] = useUpdateClientsPensionMutation()
    const [updateDataLife] = useUpdateClientsLifeMutation()
    //
    const fetchingUserDatas = async () => {
        try {
            setLoading(true)
            if (getData && getLifeData) {
                console.log(getData);
                const copyData = await { ...getData }
                const copyLifeData = await { ...getLifeData }
                await setData(getData)
                await setOldData(copyData)
                await setDataLife(getLifeData)
                await setOldDataLife(copyLifeData)
                await setLoading(false)
            }
        } catch (error: any) {
            console.log(error.message);

        }
    }
    useEffect(() => {
        (async () => {
            await fetchingUserDatas()
        })()
    }, [isSuccessPension, isSuccessLife])
    const ofTextBoolean = (item: string, text: string) => {
        if ('Yes' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: true
            }))
        } else if ('No') {
            setData((prevData) => ({
                ...prevData,
                [item]: false
            }))

        }
    }
    const ofBooleanText = (item: string) => {
        if (data[item as keyof IReviewPension]) {
            return 'Yes';
        } else {
            return 'No';
        }

    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const initialValuess = {
        makePensionContribution: ofBooleanText('makePensionContribution') || '',
        pensionNumber: `${data.pensionNumber || ''}`,
        pensionSecurity: textNomal(`${data.pensionSecurity || ''}`),//"LIFE_INSURANCE",
        otherFormPension: data.otherFormPension || '',
        planningToHaveForm:textNomal(`${data.planningToHaveForm}`) || '',
        outsideThePeriod: ofBooleanText(`outsideThePeriod`),
        outsideThePeriodText: data.outsideThePeriodText || '',
        insuranceAmount: dataLife.insuranceAmount || '',
        companyName: dataLife.companyName || '',
        address: dataLife.address || '',
        insuranceNumber: dataLife.insuranceNumber || '',

    }
    const btnPut = async () => {
        updateData()
        if (data.makePensionContribution === false) {
            await setData({
                ...data,
                pensionNumber: '',
                pensionSecurity: '',
                otherFormPension: '',
                planningToHaveForm: false,
                outsideThePeriod: false,
                outsideThePeriodText: '',
                paidFully: false,
            })
        }
        await updateDataPension({
            Id: id || '',
            data: data
        });
        if (data.pensionSecurity !== 'LIFE_INSURANCE') {
            await setDataLife({
                ...dataLife,
                insuranceAmount: '',
                companyName: '',
                address: '',
                insuranceNumber: ''
            })
        }
        await updateDataLife({
            Id: id || '',
            data: dataLife
        });

        setIsDisabled(true)
    }
    const btnCancel = () => {
        setData(oldData)
        setDataLife(oldDataLife)
        setIsDisabled(true)
    }

    return (<>
        {
            loading ? <LoadingMain />
                : <Form
                    name="appData"
                    layout="horizontal"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    onFinish={btnPut}
                    initialValues={initialValuess}
                    className="box-border ">
                    <div className={`flex box-border items-center justify-between border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[10px] `}>
                        <div className="w-fit">
                            <TextTitle title="Pension contributions" />
                        </div>
                        <div className="w-[50%] ">
                            <div className=" flex gap-x-[10px] justify-end ">
                                {!isDisabled ? <>
                                    <ButtonGray onClick={btnCancel} text="Cancel" backraund={false} size="!w-[94px] !h-[48px]" className="!p-0" />
                                    <NewButtonGreen text="Save" height="48px" className="w-[94px] !p-0" /></>
                                    : <ButtonEdit onClick={() => setIsDisabled(false)} />}
                            </div>
                        </div>
                    </div>
                    <div className="grid gap-y-[12px] gap-x-[10px]">
                        <div className="flex items-center gap-x-[10px] max-w-[550px]">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Pension contributions previously:</p>
                            <SelectClient
                                text={ofBooleanText(`makePensionContribution`)}
                                background={false}
                                options={yesNo}
                                disabled={isDisabled}
                                border={true}
                                onChange={(selectedOption) => ofTextBoolean('makePensionContribution', `${selectedOption}`)}
                                placeholder="pension contribution"
                                selectName='makePensionContribution'
                            />
                        </div>
                        <div className="flex items-center gap-x-[10px] max-w-[550px] ">
                            <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px] ">Pension number:</p>
                            {data.makePensionContribution
                                ? <InputAdminClient
                                    text={`${data.pensionNumber || ''}`}
                                    disabled={isDisabled}
                                    onChange={e => setData({ ...data, pensionNumber: `${e.target.value}` })}
                                    placeholder="pension number"
                                    inputname="pensionNumber"
                                    className="mr-auto"
                                />
                                : ''}
                        </div>
                    </div>
                    <div className={`border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[5px] mt-[50px]`}>
                        <div className="w-fit">
                            <TextTitle title="Pension contributions for the audited period" className="!text-[18px]" />
                        </div>
                    </div>
                    <div>
                        {data.makePensionContribution && !error && isSuccess
                            ? <div className='grid gap-y-[35px] mt-[20px] mb-[50px]'>
                                <div className="flex gap-5">
                                    {
                                        years && isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                            className={`${currentYear === item.id
                                                ? "border border-black"
                                                : ""}    !h-[32px] rounded-[15px]`}

                                            onClick={() => handleYear(item.id)}>
                                            {currYear === item.yearValue
                                                ? "Current year"
                                                : item.yearValue}</Button>)
                                    }
                                </div>
                                {dataMini && isSuccess ? (
                                    <div className={cls.scroll}>
                                        <RowTable
                                            name={"Pension contributions"}
                                            disabled={data.makePensionContribution ? false : true}
                                            data={dataMini}
                                            setDataSource={setDataMini}
                                            setDataMiniChange={setDataMiniChange}
                                        />
                                    </div>
                                ) : (
                                    <p>No data available.</p>
                                )}
                            </div>
                            : ''}
                    </div>
                    <div className="grid items-start gap-y-[12px] box-border">
                        <div className="flex items-center gap-x-[25px] ">
                            <div className="flex items-center gap-x-[10px] max-w-[550px]">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Contributions outside of the audited period:</p>
                                {data.makePensionContribution !== null
                                    ? <SelectClient
                                        text={data.outsideThePeriod !== null ? ofBooleanText(`outsideThePeriod`) : ''}
                                        background={false}
                                        options={yesNo}
                                        disabled={isDisabled}
                                        border={true}
                                        onChange={(selectedOption) => ofTextBoolean('outsideThePeriod', `${selectedOption}`)}
                                        placeholder="audited period"
                                        selectName="outsideThePeriod"
                                        sizeText="w-[130px]"
                                    />
                                    : ''}
                            </div>
                            {data.outsideThePeriod
                                ? <div className="flex items-start gap-x-[10px] !h-[46px] max-w-[360px]">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[65px] ">Reason:</p>
                                    <CustomTextAreaAdminClient
                                        text={`${data.outsideThePeriodText || ''}`}
                                        disabled={isDisabled}
                                        border={true}
                                        onChange={e => setData({ ...data, outsideThePeriodText: e.target.value })}
                                        placeholder="audited period reason"
                                        inputname="outsideThePeriodText"
                                    />
                                </div>
                                : ''}
                        </div>
                        <div className="flex items-center gap-x-[25px] ">
                            <div className="flex items-center gap-x-[10px] max-w-[550px] w-fit">
                                <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Another form of pension security:</p>
                                {data.makePensionContribution !== null
                                    ?
                                    <SelectClient
                                        text={textNomal(`${data.pensionSecurity || ''}`)}
                                        options={pensionSecurity}
                                        background={false}
                                        disabled={isDisabled}
                                        border={true}
                                        onChange={(selectedOption) => setData({ ...data, pensionSecurity: `${selectedOption}` })}
                                        placeholder="pension security"
                                        selectName="pensionSecurity"
                                        sizeText="w-[130px]"
                                    />
                                    : ''}
                            </div>
                            {data.pensionSecurity === 'OTHER_FROM'
                                ?
                                <div className="flex items-start gap-x-[10px] !h-[46px] max-w-[360px]">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[65px] ">Reason:</p>
                                    <CustomTextAreaAdminClient
                                        text={`${data.otherFormPension || ''}`}
                                        disabled={isDisabled}
                                        border={true}
                                        onChange={e => setData({ ...data, otherFormPension: e.target.value })}
                                        placeholder="other from reason"
                                        inputname="otherFormPension"
                                    />
                                </div>
                                : ''
                            }
                            {data.pensionSecurity === 'NO'
                                ?
                                <div className="flex items-start gap-x-[10px] !h-[46px] max-w-[360px]">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[65px] ">Reason:</p>
                                    <SelectClient
                                        background={false}
                                        text={ofBooleanText('planningToHaveForm')}
                                        options={yesNo}
                                        disabled={isDisabled}
                                        border={true}
                                        size="!w-[210px] !h-[48px]"
                                        onChange={(selectedOption) => ofTextBoolean('planningToHaveForm', `${selectedOption}`)}
                                        selectName="planningToHaveForm"
                                    />
                                </div>
                                : ''
                            }
                        </div>

                    </div>
                    {data.pensionSecurity === 'LIFE_INSURANCE'
                        ? <>
                            <div className={`border border-t-0 border-r-0 border-l-0 border-b-[#EEEEEE] pb-[5px] mt-[50px]`}>
                                <div className="w-fit">
                                    <TextTitle title="Share life insurance details" className="!text-[24px]" />
                                </div>
                            </div>
                            <div className="grid gap-y-[12px]">
                                <div className="flex items-center gap-x-[10px] max-w-[550px] w-fit">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Insurance amount:</p>
                                    <InputAdminClient
                                        text={`${dataLife.insuranceAmount || ''}`}
                                        disabled={isDisabled}
                                        onChange={(e) => setDataLife({ ...dataLife, insuranceAmount: `${e.target.value}` })}
                                        placeholder="insurance amount"
                                        inputname="insuranceAmount"
                                    />
                                </div>
                                <div className="flex items-center gap-x-[10px] max-w-[550px] w-fit">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Name of life insurance company:</p>
                                    <InputAdminClient
                                        text={`${dataLife.companyName || ''}`}
                                        disabled={isDisabled}
                                        onChange={(e) => setDataLife({ ...dataLife, companyName: e.target.value })}
                                        placeholder="company name"
                                        inputname="companyName"
                                    />
                                </div>
                                <div className="flex items-center gap-x-[10px] max-w-[550px] w-fit">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Life insurance company address:</p>
                                    <InputAdminClient
                                        text={`${dataLife.address || ''}`}
                                        disabled={isDisabled}
                                        onChange={(e) => setDataLife({ ...dataLife, address: e.target.value })}
                                        placeholder="company address"
                                        inputname="address"
                                    />
                                </div>
                                <div className="flex items-center gap-x-[10px] max-w-[550px] w-fit">
                                    <p className="font-roboto font-[400] leading-[19px] text-black/30 w-[245px]">Insurance number:</p>
                                    <InputAdminClient
                                        text={`${dataLife.insuranceNumber || ''}`}
                                        disabled={isDisabled}
                                        onChange={(e) => setDataLife({ ...dataLife, insuranceNumber: e.target.value })}
                                        placeholder="insurance number"
                                        inputname="insuranceNumber"
                                    />
                                </div>

                            </div>
                        </>
                        : ''}
                </Form>
        }
    </>)
}
export default ClientPension
