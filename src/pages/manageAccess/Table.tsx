  import { EditOutlined } from "@ant-design/icons";
  import Holder from "components/ui/popover/Holder";
  import { useState, useEffect } from "react";

  interface TableProps {
    data: { id: number; firstName: string; lastName: string; email: string }[];
  }

  const Table: React.FC<TableProps> = ({ data }) => {

    const [tableData, setTableData] = useState(data);
    useEffect(() => {
      setTableData(data);
    }, [data]);



    return (
      <div className=" my-2">
        <table className="mx-10 w-[700px]">
          <thead>
            <tr className="bg-gray-100 text-lightGray border-b border-lightGray">
              <th className="font-[400] text-left py-2 px-1 text-gray-600">№</th>
              <th className="font-[400] text-left py-2 px-1 text-gray-600">
                First Name
              </th>
              <th className="font-[400] text-left py-2 px-1 text-gray-600">
                Last Name
              </th>
              <th className="font-[400] text-left py-2 px-1 text-gray-600">
                Email Address
              </th>
              <th className="font-[400] text-left py-2 px-1 text-gray-600">
                <EditOutlined />
              </th>
            </tr>
          </thead>
          <tbody>
            {tableData.map((item, index) => (
              <tr
                key={index}
                className={`${
                  index % 2 === 0 ? "bg-gray-50" : ""
                } border-b border-lightGray`}
              >
                <td className="font-[500] py-4 px-1">{index + 1}</td>
                <td className="font-[500] py-4 px-1">{item.firstName}</td>
                <td className="font-[500] py-4 px-1">{item.lastName}</td>
                <td className="font-[500] py-4 px-1">{item.email}</td>
                <td className="font-[500] py-4 px-1">
                <Holder email={item.email} firstname={item.firstName} lastname={item.lastName} id={item.id}/>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  };

  export default Table;
