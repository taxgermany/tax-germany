import {PlusOutlined} from "@ant-design/icons";
import Modal from "components/screen/AdminClient/Modal/modal";
import Notification from "components/screen/AdminClient/notification/Notification";
import AddUserButton from "components/ui/button/AddUserButton";
import BigTitle from "components/ui/button/BigTitle";
import SearchInput from "components/ui/input/inputForAdmin/SearchInput";
import AddExpertModal from "components/ui/modal/AddExpertModal";
import {ReactComponent as NotificationImg} from "../../assets/icons/notification.svg";
import React, {ChangeEventHandler, useState, useEffect} from "react";
import {
    useAddExpertMutation, useGetNotificationsQuery,
    useLazyGetExpertsQuery,
    useLazyGetSearchExportsQuery,
} from "services/superAdminService";
import Table from "./Table";
import {IExportAdmin, INotifications} from "../../interfaces/admin";
import {useAppSelector} from "../../hooks/useAppSelector";

interface ItableData {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
}

const ManageAccess: React.FC = () => {
    const [getExperts, {data = [], isSuccess}] = useLazyGetExpertsQuery();
    const [getSearch, {data: searchData = [], isSuccess: isSuccessSearch}] =
        useLazyGetSearchExportsQuery();
    const [addExpert, {isLoading}] = useAddExpertMutation();

    const [isModalOpen, setIsModalOpen] = useState(false);

    const [dataExp, setDataExp] = useState<IExportAdmin[] | null>(null);

    const [showModal, setShowModal] = useState(false);

    const {client} = useAppSelector((state) => state.authSlice);

    const {
        data: Notifications = [] as INotifications[],
        error: errorNotifications,
        isFetching: isFetchingNotifications,
        isLoading: isLoadingNotifications,
        isSuccess: isSuccessNotifications,
    } = useGetNotificationsQuery();

    const handleAddUser = async (userData: ItableData) => {
        try {
            await addExpert(userData).unwrap();
            setIsModalOpen(false);
        } catch (error: any) {
            console.log(error.message);
        }
    };

    useEffect(() => {
        getExperts();
    }, []);

    useEffect(() => {
        if (isSuccess) {
            setDataExp(data);
            console.log("Data updated:", data);
        }
    }, [isSuccess, data]);

    const handleSearch: ChangeEventHandler<HTMLInputElement> = (e) => {
        const value = e.target.value;
        getSearch(value);

        if (isSuccessSearch) {
            if (value === "") {
                setDataExp(data);
            } else {
                setDataExp(searchData);
            }
        }
    };

    return (
        <div className="grid gap-5">
            <div className="flex items-center">
                <BigTitle className=" mr-auto" text="Experts"/>
                <AddUserButton
                    onClick={() => setIsModalOpen(true)}
                    className="flex items-center mr-4"
                    icon={<PlusOutlined/>}
                    text="Add User"
                />
                <div onClick={() => setShowModal(!showModal)}>
                    <NotificationImg className="w-[50px]"/>
                </div>
            </div>
            <div className="w-[330px] mt-4 ml-6">
                <SearchInput onChange={handleSearch}/>
            </div>
            <div className="mt-1">{dataExp && <Table data={dataExp}/>}</div>
            {isModalOpen && (
                <AddExpertModal
                    isLoading={isLoading}
                    onAddUser={handleAddUser}
                    isOpen={isModalOpen}
                    setIsOpen={setIsModalOpen}
                    title={"Add Expert"}
                />
            )}
            {showModal && (
                <Modal
                    active={showModal}
                    setActive={setShowModal}
                    children={<Notification notifications={Notifications}/>}
                />
            )}
        </div>
    );
};

export default ManageAccess;
