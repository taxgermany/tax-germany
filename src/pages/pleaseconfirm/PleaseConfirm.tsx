import Title from "components/ui/button/Title";
import {useNavigate} from "react-router-dom";
import {useConfirmStatusReviewMutation} from "../../services/reviewServices";
import NewButtonGreen from "../../components/ui/button/NewButtonGreen";
import {Checkbox, Form} from "antd";
import {setRegister} from "../../redux/auth/authSlice";
import {useAppSelector} from "../../hooks/useAppSelector";

const PleaseConfirm = () => {
    const navigate = useNavigate()
    const {client} = useAppSelector(state => state.authSlice)
    const [createConfirm, {isLoading}] = useConfirmStatusReviewMutation()
    const onFinish = async (values: any) => {
        if (!client) {
            return
        }
        try {

            await createConfirm(client.id).unwrap();
            navigate('/freelancer/payment')
        } catch (error: any) {


        }
    }

    return (
        <div className="ForgotPassword h-[90vh] font-inter flex justify-center items-center">
            <div className="content inline">
                <div className="grid gap-[40px]">
                    <Form name="basic" layout="horizontal" onFinish={onFinish} autoComplete="off">
                        <Title
                            text="Please confirm that all provided information and data is complete and no crucial details are missing"
                            className="pb-[0px] max-w-[500px] text-[20px]"/>
                        <Form.Item
                            name="agreement"
                            valuePropName="checked"
                            className="my-[30px]"
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}
                        >
                            <Checkbox>
                                <p className="text-[12px] font-[Inter] leading-[17px] font-[600] max-w-[644px] pl-[15px]">I confirm
                                    that provided information is complete</p>
                            </Checkbox>
                        </Form.Item>
                        <NewButtonGreen text="Confirm" loading={isLoading} />
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default PleaseConfirm;
