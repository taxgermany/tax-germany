import {ButtonBack} from "components/ui/button/ButtonBack";
import ButtonGreen from "components/ui/button/ButtonGreen";
import BigTitle from "components/ui/button/BigTitle";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import React, {useState, useEffect} from 'react';
import {useNavigate} from "react-router-dom";
import ValidationText from "components/ui/validation/ValidationText";
import {setUser} from "../../redux/auth/authSlice";
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {useConfirmEmailMutation, useResendMutation} from "../../services/authServics";
import {IClient} from "../../interfaces";

const ConfirmEmail = () => {
    const [emailCode, setEmailCode] = useState("");

    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    const initialSeconds = 60;
    const [seconds, setSeconds] = useState<number>(initialSeconds);
    const [active, setActive] = useState<boolean>(false);
    const [errorActive, setErrorActive] = useState(false);

    const [confirmEmail, {data, error, isSuccess, isError, isLoading}] = useConfirmEmailMutation()
    const [resendEmail, {
        error: ErrorResend,
        isSuccess: isSuccessResend,
        isError: isErrorResend,
        isLoading: isLoadingResend
    }] = useResendMutation()
    const userId = localStorage.getItem("userId")
    useEffect(() => {
        let intervalId: NodeJS.Timeout;

        // Define the function to update the countdown
        const updateCountdown = () => {
            if (seconds > 0) {
                setSeconds((prevSeconds) => prevSeconds - 1);
            }
        };

        intervalId = setInterval(() => {
            updateCountdown();
            if (seconds === 0) {
                setActive(true);
            }
        }, 1000);

        return () => clearInterval(intervalId);
    }, [seconds,]);


    const onSendCode = async () => {

        try {
            if (userId) {
                const data = await confirmEmail({userId: userId, code: emailCode}).unwrap()
                localStorage.setItem("user", JSON.stringify(data))
                dispatch(setUser(data as IClient))
                localStorage.removeItem("userId")
                localStorage.removeItem("email")
                navigate('/freelancer/choose-package')

            }

        } catch (error) {
            setErrorActive(true)
        }
    }

    const onResend = async () => {
        const email = localStorage.getItem("email")
        if (email && userId) {
            await resendEmail({email, id: +userId})
            setActive(false);
            setSeconds(initialSeconds)
        }
    }


    return (
        <div className="p-[40px] relative">
            {errorActive ? <ValidationText text={"incorrect code"}/> : ""}
            <ButtonBack/>
            <div className="ResetPassword h-[80vh] font-inter flex justify-center items-center">
                <div className="grid gap-[20px]">
                    <BigTitle text="Confirm your email"/>
                    <p className="text-[12px] text-left ">A code for verification was sent to your email, please enter
                        it below.</p>
                    <div className="flex items-center gap-[10px]">
                        <InputNoAppearance background={true} placeholder="Enter code" className="h-[40px]"
                                           onChange={(e) => setEmailCode(e.target.value)} border={true}
                                           wh="w-[241px] h-[30px]"/>
                        <ButtonGreen text="Send code" height="40px" onClick={onSendCode} loading={isLoading}/>
                    </div>
                    <div className="flex items-center gap-[10px]">
                        {active
                            ?
                            <p className="text-[12px] text-lightGray text-left w-[143px] ">You can resend the code</p>
                            :
                            <p className="text-[12px] text-lightGray text-left w-[143px] ">{`You can resend the code after
                                ${seconds} seconds.`}</p>}
                        {active
                            ? <ButtonGreen text={"Resend"} onClick={onResend} loading={isLoading}
                                           className={`text-[12px] text-white border border-[#D9D9D9] px-[24px] py-[8px] w-fit bg-green rounded-[3px]`}/>
                            : <div
                                className={'text-[12px] text-gray border border-[#D9D9D9] px-[24px]  py-[8px] w-fit rounded-[3px]'}>
                                {`Resend (${seconds})`}</div>}
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ConfirmEmail;