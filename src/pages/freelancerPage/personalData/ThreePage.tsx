import {useNavigate} from "react-router-dom";
import {ButtonBack} from "components/ui/button/ButtonBack";
import SelectComponent from "components/ui/select/SelectComponent";
import {Form} from "antd";
import {monthsEnums, fourYears} from "utils/Arrays";
import {useAppSelector} from "hooks/useAppSelector";
import {useSetGapForDocumentMutation} from "services/personalServices";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import {useAppDispatch} from "hooks/useAppDispatch";
import {setIncomeProofPeriod} from "redux/personalData/personalDataSlice";
import {useLazyFetchReviewYourQuery} from "../../../services/reviewServices";
import {format, parseISO} from "date-fns";
import {useEffect, useState} from "react";

const ThreePage = () => {

    const {client} = useAppSelector((state) => state.authSlice);

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/personal-data/three-page"})
    );

    const dispatch = useAppDispatch();

    const currentYear = new Date().getFullYear();

    const [setGapForDocument, {isLoading}] = useSetGapForDocumentMutation();
    const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewYourQuery()
    const navigate = useNavigate();


    const onFinish = async (values: any) => {
        console.log(values, "data for submit");
        try {
            await setGapForDocument({
                id: client?.id || 0,
                fromYear: values.fromYear,
                fromMonth: values.fromMonth,
                toYear: values.toYear,
                toMonth: values.toMonth,
            });
            dispatch(setIncomeProofPeriod({
                fromMonth: values.months1,
                fromYear: values.years1,
                toMonth: values.months2,
                toYear: values.years2
            }));
            navigate("/freelancer/personal-data/four-page");
        } catch (error) {
            console.log(error);
        }
    };

    const [dataPersonal, setDataPersonal] = useState<null | {
        fromYear: number | null,
        fromMonth: string | null,
        toYear: number | null,
        toMonth: string | null
    }>(null)

    const fetchPersonal = async () => {
        if (client) {
            const res = await getPersonal({clientId: client.id}).unwrap()
            if (res.fromYear &&
                res.fromMonth &&
                res.toYear &&
                res.toMonth) {
                setDataPersonal({
                    fromMonth: res.fromMonth, fromYear: res.fromYear, toMonth: res.toMonth, toYear: res.toYear
                })
            } else {
                setDataPersonal({fromMonth: null, fromYear: null, toMonth: null, toYear: 2023})
            }
        }
    }

    useEffect(() => {
        fetchPersonal()
    }, [])

    return (
        <div className="p-[40px]">
            <ButtonBack/>
            <div className="max-w-[850px] grid gap-[20px] justify-center items-center px-[16px] py-[50px]">
                <p className="text-[25px] font-[600]">
                    For what period do you need your proof of income?
                </p>
                {dataPersonal && <Form onFinish={onFinish} autoComplete="off" initialValues={dataPersonal}>
                    <div className="flex items-center">
                        <div className="flex items-center my-6">
                            <p className="font-[500] mr-4">FROM</p>
                            <div className="w-[140px] mr-2">
                                <SelectComponent
                                    size="h-[48px]"
                                    selectName="fromMonth"
                                    background={true}
                                    placeholder={"mоnth"}
                                    options={monthsEnums}
                                />
                            </div>

                            <div className="w-[140px] mr-6">
                                <SelectComponent
                                    size="h-[48px]"
                                    selectName="fromYear"
                                    background={true}
                                    placeholder={"year"}
                                    options={fourYears}
                                />
                            </div>
                        </div>
                        <div className="flex items-center my-6">
                            <p className="font-[500] mr-4">TO</p>
                            <div className="w-[140px] mr-2">
                                <SelectComponent
                                    size="h-[48px]"
                                    selectName="toMonth"
                                    background={true}
                                    placeholder={"month"}
                                    options={monthsEnums}
                                />
                            </div>
                            <div className="w-[140px]">
                                <InputNoAppearance
                                    wh={"w-[50-px]"}
                                    inputName="toYear"
                                    background={true}
                                    border={true}
                                    readOnly
                                />
                            </div>
                        </div>
                    </div>
                    <Form.Item>
                        <NewButtonGreen
                            className="mt-4"
                            loading={isLoading}
                            text={"Confirm"}
                        />
                    </Form.Item>
                </Form>}
            </div>
        </div>
    );
};

export default ThreePage;
