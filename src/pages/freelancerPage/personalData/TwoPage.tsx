import {useNavigate} from "react-router-dom";
import {ButtonBack} from "components/ui/button/ButtonBack";
import SelectComponent from "components/ui/select/SelectComponent";
import {Form} from "antd";
import {monthsEnums, years} from "utils/Arrays";
import {useEffect, useState} from "react";
import {useAppSelector} from "hooks/useAppSelector";
import {useUpdateYourDetailsMutation} from "services/personalServices";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {updateFreelancingStart} from "redux/personalData/personalDataSlice";
import {useAppDispatch} from "hooks/useAppDispatch";
import {useLazyFetchReviewYourQuery} from "../../../services/reviewServices";
import dayjs from "dayjs";
import {IYourDetailsFetch} from "./PersonalData";
import {format, parseISO} from "date-fns";

const TwoPage = () => {
    const {client} = useAppSelector((state) => state.authSlice);
    const {monthStartedFreelancing, yearStartedFreelancing} = useAppSelector((state) => state.personalDataSlice)
    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/personal-data/two-page"})
    );

    const dispatch = useAppDispatch();


    const [updateYourDetails, {isLoading}] = useUpdateYourDetailsMutation();

    const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewYourQuery()
    const navigate = useNavigate();
    const inititalValues = {month: monthStartedFreelancing, year: yearStartedFreelancing}

    const onFinish = async (values: any) => {
        try {
            await updateYourDetails({
                id: client?.id || 0,
                year: values.year,
                month: values.month,
            });/*
            dispatch(updateFreelancingStart({month: values.month, year: values.year}));*/
            navigate("/freelancer/personal-data/three-page");
        } catch (error) {
            console.log(error);
        }
    };

    const [dataPersonal, setDataPersonal] = useState<null | { month: string | null, year: string | null }>(null)

    const fetchPersonal = async () => {
        if (client) {
            const res = await getPersonal({clientId: client.id}).unwrap()
            if (res.becomeFreelancerDate) {
                const year = format(parseISO(res.becomeFreelancerDate), "yyyy");
                const month = format(parseISO(res.becomeFreelancerDate), "LLLL");
                setDataPersonal({
                    month, year
                })
            }
            else {
                setDataPersonal({
                    month: null, year: null
                })
            }
        }
    }

    useEffect(() => {
        fetchPersonal()
    }, [])


    return (
        <div className="p-[40px]">
            <ButtonBack/>
            <div className="max-w-[850px] grid gap-[20px] justify-center items-center px-[16px] py-[50px]">
                <p className="text-[25px] font-[600]">
                    When have you become a freelancer?
                </p>
                {dataPersonal &&
                    <Form onFinish={onFinish} autoComplete="off" initialValues={dataPersonal}>
                        <div className="flex w-[600px] items-center my-8 gap-y-[5px]">
                            <div className="w-[300px] mr-4">
                                <SelectComponent
                                    size="w-[300px] h-[48px]"
                                    selectName="month"
                                    background={true}
                                    placeholder={"month"}
                                    options={monthsEnums}
                                />
                            </div>
                            <div className="w-[300px]">
                                <SelectComponent
                                    size="w-[300px] h-[48px]"
                                    selectName="year"
                                    background={true}
                                    placeholder={"year"}
                                    options={years.reverse()}
                                />
                            </div>
                        </div>
                        <Form.Item>
                            <NewButtonGreen
                                className="mt-4"
                                loading={isLoading}
                                text={"Confirm"}
                            />
                        </Form.Item>
                    </Form>}
            </div>
        </div>
    );
};

export default TwoPage;
