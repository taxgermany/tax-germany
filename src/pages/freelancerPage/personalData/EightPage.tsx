import {ButtonBack} from "components/ui/button/ButtonBack";
import ButtonCard from "components/ui/button/ButtonCard";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {activeLink} from "../../../redux/sidebar/sidebarSlice";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import ValidationText from "components/ui/validation/ValidationText";
import CustomTextArea from "components/ui/input/CustomTextArea";
import {Form} from "antd";
import axios from "axios";
import {URL_API} from "utils/url";
import {useAppSelector} from "hooks/useAppSelector";
import {useSetWorkPlaceMutation} from "services/personalServices";
import {useLazyFetchReviewBussinesQuery, useLazyFetchReviewYourQuery} from "../../../services/reviewServices";

interface IEightPage {
    isActive: string;
}

const EightPage: React.FC = () => {
    const {client} = useAppSelector((state) => state.authSlice);

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/personal-data/eight-page"})
    );


    const [active, setActive] = useState("");
    const [anotherWorkPlace, setAnotherWorkPlace] = useState<string | null>(null);
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const [setWorkPlace, {isLoading, isError, error}] =
        useSetWorkPlaceMutation();
    const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewBussinesQuery()

    const handleSubmit = (value: any) => {
        if (!active) setActive("error");

        if (active !== "error" && active) {


            let workPlaceValue = "";
            let valueText = "";

            switch (active) {
                case "Private home":
                    workPlaceValue = "PRIVATE_HOME";
                    break;
                case "Separate business office":
                    workPlaceValue = "SEPARATE_BUSINESS_OFFICE";
                    break;
                case "Rented bedroom office combo":
                    workPlaceValue = "RENT_BEDROOM_OFFICE_COMBO";
                    break;
                case "Other place":
                    workPlaceValue = "OTHER_PLACE";
                    valueText = value.textArea;
                    break;
                default:
                    return;
            }


            setWorkPlace({
                id: client?.id || 0,
                workPlace: workPlaceValue,
                anotherWorkPlace: active === "Other place" ? value.textArea : null,
            })
                .unwrap()
                .then((response) => {
                    navigate("/freelancer/profits-and-expenses");
                    localStorage.setItem(
                        `storage-${client?.id}`,
                        JSON.stringify({
                            profits: true,
                            health: false,
                            pension: false,
                            review: false,
                            status: false,
                        })
                    );
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        dispatch(activeLink("profits"));
    };

    const fetchPersonal = async () => {
        if (client) {
            const res = await getPersonal({clientId: client.id}).unwrap()
            switch (res.workPlace) {
                case  null:
                    setActive("");
                    break;
                case  "PRIVATE_HOME":
                    setActive("Private home");
                    break;
                case "SEPARATE_BUSINESS_OFFICE":
                    setActive("Separate business office");
                    break;
                case "RENT_BEDROOM_OFFICE_COMBO":
                    setActive("Rented bedroom office combo");
                    break;
                case "OTHER_PLACE":
                    setAnotherWorkPlace(res.anotherWorkPlace ? res.anotherWorkPlace : " ")
                    setActive("Other place");

                    break;
                default:
                    return;
            }
        }
    }

    useEffect(() => {
        fetchPersonal()
    }, [])

    console.log(active)
    return (
        <div className="relative">
            {active === "error" && <ValidationText/>}
            <div className="p-[40px] w-[100%]">
                <ButtonBack/>
                <div className="m-auto w-[760px] h-[80%] ">
                    <p className="text-[24px] font-[600] mt-4 mb-12">
                        From where do you work as a freelancer?
                    </p>
                    <div className="flex mb-8 flex-wrap">
                        <ButtonCard
                            className="mr-6 font-[600]"
                            size={"w-[210px] h-[110px]"}
                            active={active}
                            setActive={setActive}
                            text={"Private home"}
                        />

                        <ButtonCard
                            className="mr-6 font-[600]"
                            size={"w-[210px] h-[110px]"}
                            active={active}
                            setActive={setActive}
                            text={"Separate business office"}
                            turn={true}
                        />

                        <ButtonCard
                            className="mr-6 font-[600]"
                            size={"w-[210px] h-[110px]"}
                            active={active}
                            setActive={setActive}
                            text={"Rented bedroom office combo"}
                            turn={true}
                        />

                        <ButtonCard
                            className="mr-6 mt-6  font-[600]"
                            size={"w-[210px] h-[110px]"}
                            active={active}
                            setActive={setActive}
                            text={"Other place"}
                        />
                    </div>
                    <Form
                        name="personal"
                        layout="horizontal"
                        labelCol={{span: 8}}
                        wrapperCol={{span: 16}}
                        style={{maxWidth: 650}}
                        initialValues={{textArea: anotherWorkPlace}}
                        onFinish={handleSubmit}
                        autoComplete="off"
                        className={"grid gap-y-[10px]"}
                    >
                        {active === "Other place" &&
                            <CustomTextArea
                                className="!w-[900px] !h-[44px]"
                                inputName="textArea"
                                placeholder={"What is the other place?"}
                            />
                        }
                        <Form.Item>
                            <NewButtonGreen text={"Confirm"}/>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
};
export default EightPage;
