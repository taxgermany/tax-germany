import {ButtonBack} from "../../../components/ui/button/ButtonBack";
import {useNavigate} from "react-router-dom";
import GreenPopover from "components/ui/popover/GreenPopover";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import {Form} from "antd";
import SelectComponent from "components/ui/select/SelectComponent";
import InputDate from "components/ui/input/inputDate";
import {gender} from "utils/Arrays";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {IYourDetails} from "../../../interfaces/personalDataInterface";
import {useCreateYouDetailsMutation} from "../../../services/personalServices";
import {IYourDetailsModified, updateData} from "redux/personalData/personalDataSlice";

import {useEffect, useState} from "react";
import dayjs, {Dayjs} from 'dayjs'
import {useFetchCountryQuery, useFetchNationalityQuery} from "services/arayServices";
import {useFetchReviewYourQuery, useLazyFetchReviewYourQuery} from "../../../services/reviewServices";
import {IReviewYour} from "../../../interfaces/reaview";

export interface IYourDetailsFetch {
    "gender": string | null
    "firstname": string
    "lastname": string
    "dateOfBirth": Dayjs | null
    "country": string | null
    "nationality": string | null
    "typeOfFreelance": string
}


export interface IOption {
    value: string;
    label: string;
}

const PersonalData = () => {
    const [country, setCountry] = useState<any[]>([])
    const [nationality, setNationality] = useState<any[]>([])

    const {client} = useAppSelector((state) => state.authSlice);

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/personal-data"})
    );

    const {data} = useAppSelector((state) => state.personalDataSlice);
    console.log(data);

    const [dataPersonal, setDataPersonal] = useState<null | IYourDetailsFetch>()
    const [
        createYourDetails,
        {isLoading},
    ] = useCreateYouDetailsMutation();
    const {data: getContr, isSuccess: isSuccessCount} = useFetchCountryQuery()
    const {data: getNation, isSuccess: isSuccessNat} = useFetchNationalityQuery()
    const {data: personalData, isSuccess: isSuccessPersonalData} = useFetchReviewYourQuery({clientId: client?.id || 0})
    const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewYourQuery()

    const fetchPersonal = async () => {
        if (client) {
            const res = await getPersonal({clientId: client.id}).unwrap()
            res.dateOfBirth &&
            res.firstname &&
            res.lastname &&
            res.country &&
            res.nationality &&
            res.typeOfFreelance &&
            res.typeOfFreelance
                ? setDataPersonal({
                    gender: res.gender, firstname: res.firstname, lastname: res.lastname,
                    country: res.country, nationality: res.nationality, typeOfFreelance: res.typeOfFreelance,
                    dateOfBirth: dayjs(res.dateOfBirth),
                })
                : setDataPersonal({
                    gender:null, firstname: "", lastname: "",
                    country: null, nationality:null, typeOfFreelance: "",
                    dateOfBirth: null })
        }
    }

    useEffect(() => {
        fetchPersonal()
    }, [])

    useEffect(() => {
        async function fetchDropDauwn() {
            try {
                if (getContr && getNation) {
                    await setCountry(getContr);
                    await setNationality(getNation);
                }
            } catch (error) {
                console.error(error);
            }
        }

        fetchDropDauwn();
    }, [isSuccessCount, isSuccessNat])

    const navigate = useNavigate();

    const onFinish = async (values: IYourDetails) => {
        try {
            const newObj = {...values, dateOfBirth: values.dateOfBirth.toISOString()}
            console.log(newObj)
            await createYourDetails({id: client?.id || 0, values: newObj});
            // const newDateOfBirth = values.dateOfBirth.toDate()
            /* dispatch(updateData(newObj));*/
            navigate("/freelancer/personal-data/two-page");
        } catch (error) {
            console.log(error);
        }
    };
    console.log(dataPersonal)
    return (
        <div className="p-[40px]">
            <ButtonBack/>
            {dataPersonal && <div className="max-w-[800px] grid gap-[20px] justify-center items-center px-[16px] py-[20px]">
                <p className="text-[24px] font-[600]">Your details</p>
                <Form
                    initialValues={dataPersonal}
                    autoComplete="off"
                    className="grid gap-y-[10px]"
                    onFinish={onFinish}
                >
                    <SelectComponent
                        background={true}
                        placeholder={"Mr / Mrs"}
                        options={gender}
                        selectName={"gender"}
                    />
                    <InputNoAppearance
                        inputName={"firstname"}
                        placeholder={"First name"}
                        background={true}
                        border={true}
                        type="text"
                    />

                    <InputNoAppearance
                        inputName={"lastname"}
                        placeholder={"Last name"}
                        background={true}
                        border={true}
                        type="text"
                    />

                    <InputDate
                        className="h-[48px]"
                        inputName={"dateOfBirth"}
                        placeholder={"Date of birth"}
                        background={true}
                        border={true}
                        type="text"
                    />

                    <SelectComponent
                        placeholder={"Country of birth"}
                        options={country}
                        background={true}
                        selectName={"country"}
                    />

                    <SelectComponent
                        placeholder={"Nationality"}
                        options={nationality}
                        background={true}
                        selectName={"nationality"}
                    />

                    <div className="flex items-center relative m-0">
                        <InputNoAppearance
                            inputName={"typeOfFreelance"}
                            placeholder={"Type of freelancing (industry)"}
                            background={true}
                            border={true}
                            wh={"w-[560px]"}
                            type="text"
                        />
                        <GreenPopover
                            classNamePopover="absolute right-[-35px]"
                            popoverText={"lorem lorem"}
                            width={"20px"}
                            height={"20px"}
                        />
                    </div>
                    <Form.Item>
                        <NewButtonGreen
                            className="mt-8"
                            text={"Confirm"}
                            loading={isLoading}
                        />
                    </Form.Item>
                </Form>
            </div>}
        </div>
    );
};

export default PersonalData;
