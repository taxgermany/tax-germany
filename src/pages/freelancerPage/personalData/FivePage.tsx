import { ButtonBack } from "components/ui/button/ButtonBack";
import ButtonCard from "components/ui/button/ButtonCard";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import ValidationText from "components/ui/validation/ValidationText";
import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";
import { useAppSelector } from "hooks/useAppSelector";
import { useSetTypeOfFreelancerMutation } from "services/personalServices";
import {useLazyFetchReviewBussinesQuery} from "../../../services/reviewServices";

const FivePage: React.FC = () => {
  const { client } = useAppSelector((state) => state.authSlice);
  const [setTypeOfFreelancer] = useSetTypeOfFreelancerMutation();

  localStorage.setItem(
    `${client?.id || 0}`,
    JSON.stringify({ currentPath: "/freelancer/personal-data/five-page" })
  );

  const [active, setActive] = useState("");
  const navigate = useNavigate();

  const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewBussinesQuery()
  const btnActive = (btnName: string) => {
    setActive(btnName);
  };

  const btnNavigate = () => {
    if (!active) setActive("error");

    if (active === "Commercial (Gewerblich)") {
      setTypeOfFreelancer({
        id: client?.id,
        freelanceType: "COMMERCIAL",
      })
        .unwrap()
        .then((data) => {
          console.log(data);
          navigate("/freelancer/personal-data/six-page");
        })
        .catch((error) => {
          console.error(error);
        });
    } else if (active === "Intellectual / artisitic (Freiberuflich)") {
      setTypeOfFreelancer({
        id: client?.id,
        freelanceType: "INTELLECTUAL",
      })
        .unwrap()
        .then((data) => {
          console.log(data);
          navigate("/freelancer/personal-data/eight-page");
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };

  const fetchPersonal = async () => {
    if (client) {
      const res = await getPersonal({clientId: client.id}).unwrap()
      setActive(res.freelanceType === null
          ? ""
          : res.freelanceType === "COMMERCIAL"
              ? "Commercial (Gewerblich)"
              : "Intellectual / artisitic (Freiberuflich)")
    }
  }

  useEffect(() => {
    fetchPersonal()
  }, [])


  return (
    <div className="relative">
      {active === "error" && <ValidationText />}
      <div className="p-[40px]">
        <ButtonBack />
        <div className="max-w-[700px]  grid gap-[40px] justify-center items-center px-[16px] py-[50px]">
          <p className="text-[24px] font-[600]">
            What type of freelancer are you?
          </p>
          <p className="font-[500]">
            <span className="text-blue font-[500]">Commercial freelancers</span>{" "}
            usually resell services or goods
          </p>
          <div className="flex items-center font-[500]">
            <ButtonCard
              className="mr-4"
              size={"w-[210px] h-[94px]"}
              active={active}
              setActive={setActive}
              text={"Commercial (Gewerblich)"}
              turn={true}
            />

            <ButtonCard
              size={"w-[210px] h-[94px]"}
              active={active}
              setActive={setActive}
              text={"Intellectual / artisitic (Freiberuflich)"}
              turn={true}
            />
          </div>
          <NewButtonGreen text="Confirm" onClick={btnNavigate} />
        </div>
      </div>
    </div>
  );
};

export default FivePage;
