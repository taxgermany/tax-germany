import {ButtonBack} from "components/ui/button/ButtonBack";
import GreenPopover from "components/ui/popover/GreenPopover";
import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {Form} from "antd";
import CustomTextArea from "components/ui/input/CustomTextArea";
import {ReactComponent as IconTask} from "../../../assets/icons/task.svg";
import {ReactComponent as IconDelete} from "../../../assets/icons/delete.svg";
import {useAppSelector} from "hooks/useAppSelector";
import {useCanIUploadFileMutation, useUploadFileMutation,} from "services/personalServices";
import {useLazyFetchReviewBussinesQuery, useLazyFetchReviewYourQuery} from "../../../services/reviewServices";
import {useDeleteFileMutation, useLazyFetchFileNameBusinessQuery} from "../../../services/fileService";
import {IFileName} from "../../../interfaces/ProfitsInterface";

const SixPage: React.FC = () => {
    const {client} = useAppSelector((state) => state.authSlice);

    const [canUploadFile] = useCanIUploadFileMutation();
    const [uploadFile] = useUploadFileMutation();

    const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewBussinesQuery()
    const [deleteFile, {isLoading: isLoadingDelete}] = useDeleteFileMutation()
    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/personal-data/six-page"})
    );

    const [isDisabled, setIsDisabled] = useState<boolean>(false);
    const [showForm, setShowForm] = useState(false);
    const [fileUploaded, setFileUploaded] = useState(false);
    const [fileCheck, setFileCheck] = useState<File | null>(null);

    const [file, setFile] = useState<IFileName | null>(null)
    const [fileName, setFileName] = useState("");
    const [comment, setComment] = useState<null | string>(null);
    const [change, setChange] = useState("");

    const handleLinkClick = () => {
        setIsDisabled(!isDisabled);
        setShowForm(!showForm);
    };

    const navigate = useNavigate();

    const handleSubmit = async (value: any) => {
        try {
            if (showForm && value.textArea) {
                await canUploadFile({
                    id: client?.id || '',
                    UploadFile: false,
                    noWhyUploadFile: value.textArea,
                }).unwrap();
                navigate("/freelancer/personal-data/eight-page");
            } else if (fileCheck) {
                const formData = new FormData();
                formData.append("file", fileCheck);
                uploadFile({
                    id: client?.id || 0,
                    formData,
                }).unwrap();
                navigate("/freelancer/personal-data/eight-page");
            } else {
                console.log("Пожалуйста, загрузите файл или укажите причину.");
                return;
            }
        } catch (error) {
            console.error("Произошла ошибка:", error);
        }
    };

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            setFileCheck(file);
            setFileUploaded(true);
            setFileName(file.name);
        }
    };
    const fileDelete = async () => {
        setFileCheck(null)
        setFileName("");
        setFileUploaded(false);
        if (fileCheck && file) {
            await deleteFile({fileId: file.id})
            setFile(null)
            setFileUploaded(false);

        }

    };
    const [getFileBusiness] = useLazyFetchFileNameBusinessQuery()


    function base64ToArrayBuffer(base64: string): Uint8Array {
        const binaryString = atob(base64);
        const length = binaryString.length;
        const byteArray = new Uint8Array(length);

        for (let i = 0; i < length; i++) {
            byteArray[i] = binaryString.charCodeAt(i);
        }

        return byteArray;
    }

    function byteArrayToFile(byteArray: Uint8Array, fileName: string, fileType: string): File {
        const blob = new Blob([byteArray], {type: fileType});
        return new File([blob], fileName, {type: fileType});
    }

    const fetchPersonal = async () => {
        if (client) {

            const resFile = await getFileBusiness({userId: client?.id}).unwrap()
            if (!resFile.file) {
                const resPerson = await getPersonal({clientId: client.id}).unwrap()
                setComment(resPerson.cantUploadTradeRegistration)

            }
            const byteArray = base64ToArrayBuffer(String(resFile.file));
            const file = byteArrayToFile(byteArray, resFile.name, resFile.type)
            setFile(resFile)
            setFileCheck(file)
            setFileUploaded(!!resFile.file);
            setFileName(resFile.name ? resFile.name : "")
            setChange(" ")


        }
    }

    useEffect(() => {
        fetchPersonal()
    }, [])


    return (
        <div className="p-[40px] w-[100%]">
            <ButtonBack/>

            {change && <div className="m-auto w-[660px] h-[450px] flex flex-col justify-center">
                <div className="flex">
                    <p className="text-[24px] font-[600] relative">
                        Upload Trade Registration Form from Trade office <br/>
                        (Gewerbeanmeldung)
                        <span className="absolute bottom-[5px] left-[240px]">
                    <GreenPopover
                        popoverText={"lorem lorem"}
                        width={"20px"}
                        height={"20px"}
                    />
            </span>
                    </p>
                </div>
                <div className="flex items-center my-10">
                    <div>
                        <p className="text-[17px] font-[500]">
                            Trade registration form (Gewerbeanmeldung)
                        </p>
                        <p className="text-lightGray text-[14px] mr-10">
                            *Max file size: 10MB. Supported formats: .pdf, .jpeg, .png
                        </p>
                    </div>
                    <div className="file-input-container flex flex-col">
                        {fileName ? (
                            <div className="flex  justify-between items-center gap-y-[10px]">
                                <div className="flex jusify-between items-center gap-x-[22px]">
                  <span className="flex items-center gap-x-[10px]">
                    <IconTask/>
                    <p className="text-[14px] text-green underline decoration-solid cursor-pointer">
                      {fileName}
                    </p>
                  </span>

                                    <p className="w-[80px] text-[14px]">23.43 Kb</p>
                                    <button onClick={fileDelete}>
                                        <IconDelete/>
                                    </button>
                                </div>
                            </div>
                        ) : (
                            <label
                                htmlFor="fileInput"
                                className={`${
                                    isDisabled
                                        ? "text-lightGray cursor-not-allowed "
                                        : "text-green cursor-pointer hover:bg-green rounded hover:text-white"
                                } font-[500] border border-lightGray shadow py-2 px-6 ${
                                    isDisabled ? "bg-gray-200 rounded" : "bg-white"
                                } block`}
                            >
                                <input
                                    disabled={isDisabled}
                                    type="file"
                                    id="fileInput"
                                    onChange={handleFileChange}
                                    className={`file-input hidden`}
                                />
                                Upload file*
                            </label>
                        )}
                    </div>
                </div>
                <div className="w-[210px] cursor-pointer">
                    {(!fileCheck || !fileUploaded) &&
                        <div
                            className={`text-green underline decoration-1 font-[500] ${
                                isDisabled ? "cursor-not-allowed" : "cursor-pointer"}`}
                            onClick={handleLinkClick}
                        >
                            I can’t upload this document
                        </div>
                    }
                </div>

                <Form
                    name="personal"
                    layout="horizontal"
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                    style={{maxWidth: 650}}
                    initialValues={{remember: true, textArea: comment}}
                    onFinish={handleSubmit}
                    autoComplete="off"
                    className={"grid gap-y-[10px]"}
                >
                    {showForm && (
                        <CustomTextArea
                            className="!w-[800px] !h-[44px] mt-4"
                            inputName="textArea"
                            placeholder={"What is the other place?"}
                        />
                    )}
                    {showForm || fileName ? (
                        <Form.Item>
                            <NewButtonGreen className="mt-6" text={"Confirm"}/>
                        </Form.Item>
                    ) : (
                        ""
                    )}
                </Form>
            </div>}
        </div>
    );
};

export default SixPage;
