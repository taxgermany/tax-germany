import {Form} from "antd";
import {ButtonBack} from "components/ui/button/ButtonBack";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import GreenPopover from "components/ui/popover/GreenPopover";
import SelectComponent from "components/ui/select/SelectComponent";
import {useAppDispatch} from "hooks/useAppDispatch";
import {useAppSelector} from "hooks/useAppSelector";
import {IBusinessDetails} from "interfaces/personalDataInterface";
import {useNavigate} from "react-router-dom";
import {updateData} from "redux/businessDetailsSlice";
import {cities} from "utils/Arrays";
import {useCreateBusinessDetailsMutation} from "../../../services/personalServices";
import {useLazyFetchReviewBussinesQuery, useLazyFetchReviewYourQuery} from "../../../services/reviewServices";
import {useEffect, useState} from "react";

const FourPage = () => {
    const {client} = useAppSelector((state) => state.authSlice);

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/personal-data/four-page"})
    );

    const dispatch = useAppDispatch();
    const {data} = useAppSelector((state) => state.businessDetailsSlice);
    const [createYourDetails, {isLoading}] = useCreateBusinessDetailsMutation();

    const [getPersonal, {isLoading: isLoadingPersonal}] = useLazyFetchReviewBussinesQuery()
    const navigate = useNavigate();

    const onFinish = async (values: IBusinessDetails) => {
        try {
            await createYourDetails({id: client?.id || 0, values});
            dispatch(updateData(values));
            navigate("/freelancer/personal-data/five-page");
        } catch (error) {
            console.log(error);
        }
    };

    const [dataPersonal, setDataPersonal] = useState<null | {
        "businessName": string | null,
        "city": string | null,
        "street": string | null
        "postalCode": string | null
        "building": string | null
        "businessPhone": string | null
        "businessEmail": string | null
        "website": string | null
    }>(null)

    const fetchPersonal = async () => {
        if (client) {
            const res = await getPersonal({clientId: client.id}).unwrap()
            if (res.businessName &&
                res.city &&
                res.street &&
                res.postalCode &&
                res.building &&
                res.businessPhone &&
                res.businessPhone &&
                res.website) {
                setDataPersonal({
                    postalCode: res.postalCode, building: res.building, businessPhone: res.businessPhone,
                    businessName: res.businessName, city: res.city, street: res.street, businessEmail: res.businessEmail,
                    website: res.website
                })
            } else {
                setDataPersonal({
                    postalCode: null, building: null, businessPhone: null, businessName: null, city: null, street: null,
                    businessEmail: null, website: null
                })
            }
        }
    }

    useEffect(() => {
        fetchPersonal()
    }, [])

    return (
        <div className="p-[40px]">
            <ButtonBack/>
            <div className="max-w-[900px] grid gap-[20px] justify-center items-center px-[16px] py-[20px]">
                {dataPersonal && <Form
                    initialValues={dataPersonal}
                    onFinish={onFinish}
                    className="grid gap-y-[10px]"
                    autoComplete="off"
                >
                    <p className="text-[24px] font-[600] ">Business details</p>
                    <div className="grid gap-2">
                        <div className="flex items-center">
                            <InputNoAppearance
                                inputName={"businessName"}
                                placeholder={"Business name"}
                                background={true}
                                border={true}
                                type="text"
                            />
                            <GreenPopover
                                popoverText={"lorem lorem"}
                                width={"20px"}
                                height={"20px"}
                            />
                        </div>

                        <div className="flex items-center">
                            <InputNoAppearance
                                className="mr-4"
                                inputName={"city"}
                                placeholder={"Business address: City"}
                                background={true}
                                border={true}
                                wh={"w-[272px]"}
                            />

                            <InputNoAppearance
                                inputName={"postalCode"}
                                placeholder={"Postal code"}
                                background={true}
                                border={true}
                                wh={"w-[272px]"}
                                type="number"
                            />
                            <GreenPopover
                                // classNamePopover="ml-2 mt-2"
                                popoverText={"lorem lorem"}
                                width={"40px"}
                                height={"20px"}
                            />
                        </div>
                        <div className="flex items-center">
                            <InputNoAppearance
                                inputName={"street"}
                                placeholder={"Street"}
                                background={true}
                                border={true}
                                wh={"w-[272px]"}
                                type="text"
                            />
                            <InputNoAppearance
                                className="mr-10"
                                inputName={"building"}
                                placeholder={"Building"}
                                background={true}
                                border={true}
                                wh={"w-[272px]"}
                                type="text"
                            />
                        </div>
                        <div className="flex items-center">
                            <InputNoAppearance
                                inputName={"businessPhone"}
                                placeholder={"Business phone"}
                                background={true}
                                border={true}
                                type="number"
                            />
                            <GreenPopover
                                // classNamePopover="ml-2 mt-2"
                                popoverText={"lorem lorem"}
                                width={"20px"}
                                height={"20px"}
                            />
                        </div>
                        <div className="flex items-center">
                            <InputNoAppearance
                                inputName={"businessEmail"}
                                placeholder={"Business email address"}
                                background={true}
                                border={true}
                                type="text"
                            />
                            <GreenPopover
                                // classNamePopover="ml-2 mt-2"
                                popoverText={"lorem lorem"}
                                width={"20px"}
                                height={"20px"}
                            />
                        </div>
                        <InputNoAppearance
                            inputName={"website"}
                            placeholder={"Business website"}
                            background={true}
                            border={true}
                            type="text"
                        />
                    </div>
                    <Form.Item>
                        <NewButtonGreen text={"Confirm"} loading={isLoading}/>
                    </Form.Item>
                </Form>}
            </div>
        </div>
    );
};

export default FourPage;
