import {ButtonBack} from "components/ui/button/ButtonBack"
import ButtonWhiteBg from "components/ui/button/ButtonWhiteBg";
import React, {ChangeEvent, useState, useEffect} from 'react'
import {useNavigate} from "react-router-dom";
import {ReactComponent as IconInfo} from '../../../assets/icons/info.svg'
import ValidationText from "components/ui/validation/ValidationText";
import CustomTextArea from "components/ui/input/CustomTextArea";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {useAppSelector} from "hooks/useAppSelector";
import {useFetchPensionQuery, useUpdatePensionSecuratyMutation} from "services/PensionServices";
import {IPensionData} from "interfaces/pension";
import {useLazyFetchReviewPensionQuery} from "../../../services/reviewServices";
import {Form} from "antd";

interface IPensionSecurity {
    isActive: string
    isSecondAcitive: string
}

const PensionSecurity: React.FC = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/pension-security"}));


    const [state, setState] = useState<IPensionSecurity>({isActive: '', isSecondAcitive: ''})
    const [comment, setComment] = useState<string | null>(null)
    const [error, setError] = useState(false)
    const [data, setData] = useState<IPensionData>({} as IPensionData)
    const navigate = useNavigate()

    const [updatePension] = useUpdatePensionSecuratyMutation()



    const transformetUp = (string: string) => {
        return string.toUpperCase().replace(/\s/g, '_');
    }
    const transformToLower = (string: string) => {
        return string.replace(/_/g, ' ').toLowerCase();
    };
    const atciveCheck = () => {
        setState({
            ...state,
            isActive: data.pensionSecurity ? transformToLower(data.pensionSecurity) : ''
        })
    }

    const btnClickMain = (buttonName: string) => {
        setState((prevState) => ({
            ...prevState,
            isActive: buttonName
        }))
    }

    const btnClickSecond1 = (buttonName: string) => {
        setState((prevState) => ({
            ...prevState,
            isSecondAcitive: buttonName
        }))
    }

    const checkText = (e: ChangeEvent<HTMLTextAreaElement>) => {
        const value = e.target.value
        setComment(value)
    }
    const nextNavigate = () => {
        if (state.isActive === 'Life insurance') {
            navigate('/freelancer/pension-insurance')
        } else if (state.isActive === 'Other form') {
            if (comment) {
                navigate('/freelancer/review')
            } else {
                setError(true)
            }
        } else if (state.isActive === 'No') {
            if (state.isSecondAcitive === 'Yes' || state.isSecondAcitive === 'No') {
                navigate('/freelancer/review')
            } else {
                setState((prevState) => ({
                    ...prevState,
                    isActive: 'error'
                }))
            }
        }
    }
    const btnPushPage = async (value: any) => {
        setComment(value.textArea)
        if (!state.isActive) setState((prevState) => ({
            ...prevState,
            isActive: 'error'
        }))
        try {
            if (state.isActive) {
                await updatePension({
                    clientId: client?.id || '',
                    queryString: `pensionSecurity=${transformetUp(state.isActive)}${state.isActive === "Other form" ? `&otherForm=${value.textArea}` :
                        state.isActive === "No" ? `&areYouPlanning=${state.isSecondAcitive === 'Yes'}` : ''}`
                });
                nextNavigate()
            }
        } catch (error) {
        }
    }


    const [getPension, {isLoading: isLoadingPension}] = useLazyFetchReviewPensionQuery()

    const fetchHealth = async () => {
        if (client) {
            const res = await getPension({clientId: client.id}).unwrap()
            switch (res.pensionSecurity) {

                case  null:
                    const newState = {...state,isActive: " ", isSecondAcitive: "" }
                    setState(newState);
                    break;
                case  "LIFE_INSURANCE":
                    const newState1 = {...state,isActive: "Life insurance", isSecondAcitive: "" }
                    setState(newState1);
                    break;
                case "OTHER_FORM":
                    const newState2 = {...state,isActive: "Other form", isSecondAcitive: res.otherFormPension }
                    setComment(res.otherFormPension)
                    setState(newState2);
                    break;
                case "NO":
                    const newState3 = {...state,isActive: "No", isSecondAcitive: res.planningToHaveForm ? "Yes" : "No" }
                    setState(newState3);
                    break;
                default:
                    return;
            }
        }
    }

    useEffect(() => {
        fetchHealth()
    }, [])


    return (
        <div className="relative">
            {state.isActive === 'error' && <ValidationText/>}
            {state.isActive && <div className="pt-[40px] px-[40px] w-[100%] h-[80vh]">
                <ButtonBack/>
                {state && <div className="px-[16px] py-[50px] ">
                    <div className="grid m-auto gap-y-[50px] max-w-[750px] relative transition">
                        <div>
                            <p className="text-[28px] font-[600] font-[inter]">Do you have any other form of pension
                                security?</p>
                        </div>
                        <div className="flex gap-x-[30px]">
                            <ButtonWhiteBg
                                className="!w-[204px] !h-[114px] font-[500]"
                                text='Life insurance' active={state.isActive} setActive={btnClickMain}/>
                            <div className="relative">
                                <ButtonWhiteBg
                                    className="!w-[204px] !h-[114px] font-[500]"
                                    text='Other form' active={state.isActive} setActive={btnClickMain}/>
                                <IconInfo
                                    className="absolute top-[12px] right-[15px]"
                                    width='16'
                                    height='16'/>
                            </div>
                            <div className="relative">
                                <ButtonWhiteBg
                                    className="!w-[204px] !h-[114px] font-[500]"
                                    text='No' active={state.isActive} setActive={btnClickMain}/>
                                <IconInfo
                                    className="absolute top-[12px] right-[15px]"
                                    width='16'
                                    height='16'/>
                            </div>
                        </div>

                        {state.isActive === 'No' && <div
                            className={`flex gap-x-[25px] mt-[10px] transition `}>
                            <div className="w-[153px]  relative">
                                <p className="text-[12px] ">Are you planning to have any form of pension
                                    security?</p>
                                <IconInfo
                                    className="absolute bottom-0 right-0"
                                    width='14px'
                                    height='14px'/>
                            </div>
                            <div className={`flex gap-x-[20px]`}>
                                <ButtonWhiteBg size="w-[170px] h-[50px]" text="Yes" active={state.isSecondAcitive}
                                               setActive={btnClickSecond1}/>
                                <ButtonWhiteBg size="w-[170px] h-[50px]" text="No" active={state.isSecondAcitive}
                                               setActive={btnClickSecond1}/>
                            </div>
                        </div>}

                        <Form
                            name="personal"
                            layout="horizontal"
                            labelCol={{span: 8}}
                            wrapperCol={{span: 16}}
                            style={{maxWidth: 650}}
                            initialValues={{textArea: comment}}
                            onFinish={btnPushPage}
                            autoComplete="off"
                            className={"grid gap-y-[10px]"}
                        >
                            {state.isActive === 'Other form' &&
                                <CustomTextArea
                                    wh={"w-[100%]"}
                                    placeholder={"What other form of penstion security do you have?"}
                                    inputName={"textArea"}/>}
                            <Form.Item>
                                <NewButtonGreen className="rounded-[3px] px-[38px] py-[13px]" text="Confirm"/>
                            </Form.Item>
                        </Form>
                    </div>
                </div>}
            </div>}
        </div>
    )
}

export default PensionSecurity
