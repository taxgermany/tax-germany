import {ButtonBack} from "components/ui/button/ButtonBack"
import React, {useEffect, useState} from 'react'
import {useNavigate} from "react-router-dom"
import {ReactComponent as IconInfo} from '../../../assets/icons/info.svg'
import ButtonWhiteBg from "components/ui/button/ButtonWhiteBg"
import ButtonGreen from "components/ui/button/ButtonGreen"
import ValidationText from "components/ui/validation/ValidationText"
import {useAppSelector} from "hooks/useAppSelector"
import {setPensionID} from "redux/pensionSlice"
import {useDispatch} from "react-redux"
import {useCreatePensionMutation} from "services/PensionServices"
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import {useLazyFetchReviewHealthQuery, useLazyFetchReviewPensionQuery} from "../../../services/reviewServices";

const PensionContr: React.FC = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/pension-contributions"}));

    const [createPension, {isLoading}] = useCreatePensionMutation()
    const dispatch = useDispatch()

    const [state, setState] = useState('Yes')
    const navigate = useNavigate()


    const btnClickActive = (buttonName: string) => {
        setState(buttonName)
    };

    const btnPushPage = async () => {
        if (!state) setState("error");

        try {
            if (state === 'No') {
                await createPension({clientId: client?.id || 0, doYouMakePension: false});
                return navigate("/freelancer/review")
            }
            const response = await createPension({clientId: client?.id || 0, doYouMakePension: 'Yes' === state});

            if ('data' in response) {
                dispatch(setPensionID(response.data.id));
                navigate("/freelancer/pension-number")
            }

        } catch (error) {
            console.log(error);
        }

    };


    const [getPension, {isLoading: isLoadingPension}] = useLazyFetchReviewPensionQuery()

    const fetchHealth = async () => {
        const res = await getPension({clientId: client?.id || 0}).unwrap()
        setState(res.makePensionContribution === null
            ? ""
            : res.makePensionContribution
                ? "Yes"
                : "No")
    }


    useEffect(() => {
        fetchHealth()
    }, [])


    return (
        <div className="relative">
            {state === "error" && <ValidationText/>}
            <div className="p-[40px]">
                <ButtonBack/>
                <div className="px-[16px] py-[50px]  ">
                    <div className="grid m-auto gap-y-[50px]  max-w-[750px]">
                        <p className="text-[28px] font-[600] w-fit relative font-[inter]">
                            Did you make any pension contributions?
                            <IconInfo className="absolute right-[-35px] bottom-[7px]"
                                      width='26px'
                                      height='26px'/>
                        </p>
                        <div className="flex gap-x-[20px] relative">
                            <ButtonWhiteBg className="w-[240px] h-[65px]" text='Yes' active={state}
                                           setActive={btnClickActive}/>
                            <ButtonWhiteBg className="w-[240px] h-[65px]" text='No' active={state}
                                           setActive={btnClickActive}/>
                        </div>
                        <NewButtonGreen loading={isLoading} text={"Confirm"} onClick={btnPushPage}/>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default PensionContr
