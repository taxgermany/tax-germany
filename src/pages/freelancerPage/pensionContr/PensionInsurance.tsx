import { Form } from "antd"
import { ButtonBack } from "components/ui/button/ButtonBack"
import NewButtonGreen from "components/ui/button/NewButtonGreen"
import InputNoAppearance from "components/ui/input/InputNoAppearance"
import { useNavigate } from "react-router-dom"
import { useAppDispatch } from "../../../hooks/useAppDispatch";
import { activeLink } from "../../../redux/sidebar/sidebarSlice";
import { ChangeEvent, useEffect, useState } from "react"
import { useAppSelector } from "hooks/useAppSelector"
import { useCreateLifeMutation, useLazyFetchLifeQuery } from "services/lifeService"
import { ILifeData } from "interfaces/pension"
import LoadingMain from "components/ui/loading/LoadingMain"

const PensionInsurance = () => {

    const { client } = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({ currentPath: "/freelancer/pension-insurance" }));

    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const [data, setData] = useState<ILifeData>({})
    const [loading, setLoading] = useState(true)

    const [getLife]= useLazyFetchLifeQuery()
    const [createData] = useCreateLifeMutation()
    useEffect(() => {
        (async () => {
            setLoading(true);
            const res = await getLife({ clientId: client?.id || '' }).unwrap();
            if (res) {
                await setData(res)
                await setLoading(false)
            }else{
                await setLoading(false)
            }
        })()
    }, [client])
    const btnPushPage = async () => {
        try {
            await createData({
                clientId: client?.id || "",
                data: data
            })

            navigate('/freelancer/review')
            localStorage.setItem(`storage-${client?.id}`, JSON.stringify({
                profits: true,
                health: true,
                pension: true,
                review: true,
                status: false
            }))
            dispatch(activeLink("review"))

        } catch (error) {

        }
    }
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className="p-[20px] md:p-[40px] overflow-hidden ">
            <ButtonBack />
            {loading ? <LoadingMain /> :
                <div className=" grid justify-center mx-auto max-w-[750px]  items-center px-[16px] py-[20px]">
                    <div className="mb-[20px]">
                        <p className="text-[28px] font-[600] font-[inter]">Share life insurance details</p>
                    </div>
                    <Form
                        name="reveaw"
                        layout="horizontal"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        style={{ maxWidth: 650 }}
                        initialValues={{
                            insuranceAmount: data.insuranceAmount,
                            companyName: data.companyName,
                            insuranceNumber: data.insuranceNumber,
                            address: data.address
                        }}
                        onFinish={btnPushPage}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                        className="grid gap-y-[10px]"
                    >
                        <InputNoAppearance
                            inputName="insuranceAmount"
                            placeholder="Insurance amount"
                            background={true}
                            border={true}
                            type='number'
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, insuranceAmount: parseInt(e.target.value) })} />
                        <InputNoAppearance
                            inputName="companyName"
                            placeholder="Name of life insurance company"
                            background={true}
                            border={true}
                            type='text'
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, companyName: e.target.value })} />
                        <InputNoAppearance
                            inputName="insuranceNumber"
                            placeholder="Insurance number"
                            background={true}
                            border={true}
                            type='number'
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, insuranceNumber: parseInt(e.target.value) })} />
                        <InputNoAppearance
                            inputName="address"
                            placeholder="Address of life insurance company (Postal code, City, Street, Building number)"
                            type='text'
                            background={true}
                            border={true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, address: e.target.value })} />
                        <Form.Item>
                            <NewButtonGreen className="rounded-[3px] px-[38px] py-[13px]" text="Confirm" />
                        </Form.Item>
                    </Form>
                </div>
                }
        </div>
    )
}
export default PensionInsurance
