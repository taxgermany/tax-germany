import {ButtonBack} from "components/ui/button/ButtonBack";
import React, {useState, useEffect, ChangeEvent} from "react";
import {useNavigate} from "react-router-dom";
import {ReactComponent as IconInfo} from '../../../assets/icons/info.svg'
import {Button, Checkbox, Form, Spin} from "antd";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {columns, DataType} from "../../../utils/MiniTable";
import CustomTextArea from "../../../components/ui/input/CustomTextArea";
import CheckBox from "../../../components/ui/button/CheckBox";
import {useAppSelector} from "hooks/useAppSelector";
import RowTable from "components/ui/table/RowTable";
import {
    useLazyFetchPensionMiniTableQuery, useLazyFetchPensionQuery,
    useUpdatePensionMiniTableMutation, useUpdatePensionMutation
} from "services/PensionServices";
import {IMiniTable, IMiniTableWithTotal} from "interfaces/TableInterface";
import {useFetchAllYearQuery} from "services/yearService";
import {IYear} from "interfaces/YearInterface";
import cls from "../profitandexpenses/ProfitAndExpenses16.module.scss";
import {useLazyFetchMiniTableQuery, useUpdateMiniTableMutation} from "../../../services/healthInsurenceServices";
import {IPensionData} from "../../../interfaces/pension";
import GreenPopover from "../../../components/ui/popover/GreenPopover";

interface IPensionAuditPeriod {
    isYearActive: string;
}

interface IData {
    outsideThePeriodText?: string,
}


const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};
const PensionAuditPeriod: React.FC = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/pension-audit-period"}));


    const [state, setState] = useState<IPensionAuditPeriod>({
        isYearActive: "nowYear"
    });
    const [auditPeriod, setAuditPeriod] = useState(false)
    const navigate = useNavigate();
    const [data, setData] = useState<IData>({})


    const {id} = useAppSelector(state => state.pensionSlice)
    const [getPension, {
        data: Pension,
        error: errorPension,
        isLoading: isLoadingPension,
        isSuccess: isSuccessPension
    }] = useLazyFetchPensionQuery()

    const currentDate = new Date();
    const currYear = currentDate.getFullYear();

    const [currentYear, setCurrentYear] = useState(0)
    const [dataMiniChange, setDataMiniChange] = useState<boolean>(false);

    const [dataMini, setDataMini] = useState<IMiniTableWithTotal | null>(null);


    const [getTable, {
        data: table = {} as IMiniTableWithTotal,
        error,
        isLoading,
        isSuccess
    }] = useLazyFetchPensionMiniTableQuery()


    const {
        data: years = [] as IYear[],
        isSuccess: isSuccessYear

    } = useFetchAllYearQuery(client?.id || 0);

    const [UpdateTableMini, {isLoading: isLoadingUpdate}] = useUpdatePensionMiniTableMutation()
    const [UpdatePension] = useUpdatePensionMutation()

    const updateData = async () => {
        if (dataMiniChange) {
            const {total, ...newData} = dataMini as IMiniTableWithTotal
            const res = await UpdateTableMini({data: newData as IMiniTable, yearId: currentYear})
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini(res.data as IMiniTableWithTotal)
            setDataMiniChange(false)
        }
    }

    const handleYear = (id: number) => {

        setCurrentYear(id)
        if (client) {
            getTable({
                userId: client.id,
                yearId: id,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        }
        updateData()

    }


    const btnClickAudit = () => setAuditPeriod(!auditPeriod);


    const handleSubmit = async () => {
        await updateData()
        if (client) {
            await UpdatePension({id: client.id, data: {...data, outsideThePeriod: auditPeriod} as IPensionData})
            localStorage.setItem(`storage-${client?.id}`, JSON.stringify({
                profits: true,
                health: true,
                pension: true,
                review: true,
                status: false
            }));
            navigate("/freelancer/pension-security")
        }
    }


    useEffect(() => {
        if (isSuccessYear && client) {
            setCurrentYear(years[0].id)
            getTable({
                userId: client.id,
                yearId: years[0].id,
            }).then((res) => {
                console.log(res)
                setDataMini(res.data as IMiniTableWithTotal)
            })
        }
    }, [isSuccessYear])


    return (
        <div className="p-[40px] w-[100%] h-[80%] grid gap-y-[20px]">
            <ButtonBack/>
            <div className="w-[90%] h-[100%] flex flex-col justify-center items-start m-auto gap-y-[40px]">
                <div className="grid gap-y-[20px]">
                    <p className="text-[28px] font-[600] font-[inter]">
                        State your pension contributions for the audited period
                    </p>
                    <p>Contributions should be based on the date of payment</p>
                </div>
                <Form
                    name="basic"
                    layout="horizontal"
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                    initialValues={{remember: true}}
                    onFinish={handleSubmit}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <div className="grid gap-y-[20px]">
                        <div className="flex gap-x-[10px] ml-[194px]">
                            {
                                isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                                                           className={`${currentYear === item.id
                                                                               ? "border border-black"
                                                                               : ""}    !h-[32px] rounded-[15px]`}

                                                                           onClick={() => handleYear(item.id)}>
                                    {currYear === item.yearValue
                                        ? "Current year"
                                        : item.yearValue}</Button>)
                            }
                        </div>
                        <div className="w-[100%]">
                            {(isLoading || isLoadingUpdate) &&
                                <Spin size="large" className="absolute top-1/3 left-1/2"/>}
                            {error && <h1>error</h1>}
                            {dataMini && isSuccess && <div className={cls.scroll}>
                                <RowTable name={"Pension contributions"} disabled={false} data={dataMini}
                                          setDataSource={setDataMini}
                                          setDataMiniChange={setDataMiniChange}
                                />
                            </div>}
                        </div>
                    </div>
                    <div className="flex gap-x-[20px] w-[100%]">
                        <div className={"w-[140px]"}>
                            <p className="relative text-[12px] font-[500]">
                                I made contributions outside of the audited period
                                <IconInfo className="absolute right-0 bottom-[2px]"/>
                                {/*<GreenPopover
                                    popoverText={"I made contributions outside of the audited period"}
                                    height="20px"
                                    width="20px"
                                    classNamePopover={"absolute right-0 bottom-[2px]"}/>*/}
                            </p>
                        </div>
                        <CheckBox active={auditPeriod} setActive={btnClickAudit}/>
                        {/*<Form.Item
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}
                        >
                            <Checkbox/>
                        </Form.Item>*/}
                        <div>
                            {auditPeriod ? (
                                <CustomTextArea
                                    inputName="Explain reason why not during the audited period"
                                    placeholder="Explain reason why not during the audited period"
                                    wh="w-[650px] h-[120px]"
                                    onChanges={(e: ChangeEvent<HTMLTextAreaElement>) => setData({
                                        ...data,
                                        outsideThePeriodText: e.target.value
                                    })}
                                />
                            ) : (
                                ""
                            )}
                        </div>
                    </div>
                    <Form.Item>
                        <NewButtonGreen text="Confim"
                                        className="text-[16px] bg-green px-[38px] text-white leading-[26px] py-[13px] w-fit rounded-[3px]"/>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
};

export default PensionAuditPeriod;
