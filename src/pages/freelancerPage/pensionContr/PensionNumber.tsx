import {ButtonBack} from "components/ui/button/ButtonBack"
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import {useNavigate} from "react-router-dom";
import {ReactComponent as IconInfo} from '../../../assets/icons/info.svg'
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {Form} from "antd";
import {ChangeEvent, useState, useEffect} from "react";
import {useAppSelector} from "hooks/useAppSelector";
import {useLazyFetchPensionQuery, useUpdatePensionNumberMutation} from "services/PensionServices";
import {IPensionData} from "interfaces/pension";
import LoadingMain from "components/ui/loading/LoadingMain";
import {useLazyFetchReviewPensionQuery} from "../../../services/reviewServices";


const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};


const PensionNumber = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/pension-number"}));

    const navigate = useNavigate()

    const [updatePensionNumber] = useUpdatePensionNumberMutation()

    const onFinish = async (values: any) => {
        try {
            await updatePensionNumber({clientId: client?.id || null, pensionNumber: values.pensionNumber})
            navigate('/freelancer/pension-audit-period')
        } catch (error) {
            console.log(error);

        }

    }

    const [number, setNumber] = useState<string | null>(null)
    const [getPension, {isLoading: isLoadingPension}] = useLazyFetchReviewPensionQuery()

    const fetchHealth = async () => {
        const res = await getPension({clientId: client?.id || 0}).unwrap()
        setNumber(res.pensionNumber?res.pensionNumber:" ")
    }


    useEffect(() => {
        fetchHealth()
    }, [])

    return (
        <div className="p-[20px] md:p-[40px] overflow-hidden w-[100%] h-[80vh]">
            <ButtonBack/>
            {isLoadingPension && <LoadingMain/>}

            {number && <div className="px-[16px] py-[50px]">
                <Form
                    name="basic"
                    layout="horizontal"
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                    initialValues={{
                        pensionNumber: number
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}

                    autoComplete="off">

                    <div className="grid m-auto gap-y-[50px] max-w[750px] relative">
                        <div className="w-[412px]">
                            <p className="text-[28px] font-[600] relative font-[inter]">
                                What is your pension number?
                                <IconInfo
                                    className="absolute right-[-2rem] bottom-[7px]"
                                    width='26px'
                                    height='26px'/>
                            </p>
                        </div>

                        <InputNoAppearance
                            inputName="pensionNumber"
                            type='number'
                            placeholder="Pension number"
                            wh="w-[604px]"
                            background={true}
                            border={true}/>
                        <Form.Item
                            className="!mb-0 !h-[52px]">
                            <NewButtonGreen className="rounded-[4px] !mb-0" text="Confirm"/>
                        </Form.Item>
                    </div>
                </Form>
            </div>}

        </div>
    )
}

export default PensionNumber
