import ButtonGreen from "components/ui/button/ButtonGreen";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import React, {useState} from "react";
import axios from "axios";
import SendMessageModal from "components/ui/modal/SendMessageModal";
import {useNavigate} from "react-router-dom";
import {useAppSelector} from "hooks/useAppSelector";
import {
    useConfirmStatusMutation,
    useGetDetailsQuery,
    useGetFilesQuery,
    useGetNotificationsStatusQuery,
    useLazyDownloadFileQuery,
} from "services/statusService";

const ApplicationStatus: React.FC = () => {
    const {client} = useAppSelector((state) => state.authSlice);
    console.log(client);

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/status"})
    );

    localStorage.setItem(`storage-${client?.id}`, JSON.stringify({
        profits: true,
        health: true,
        pension: true,
        review: true,
        status: true
    }));

    const navigate = useNavigate();
    const [isModalOpen, setIsModalOpen] = useState(false);

    const {data: notificationsData, isError: isNotificationsError} =
        useGetNotificationsStatusQuery(client?.id || 0);

    const {data: detailsData, isError: isDetailsError} = useGetDetailsQuery(
        client?.id || 0
    );

    const {data: filesData, isError: isFilesError} = useGetFilesQuery(
        client?.id || 0
    );

    const [confirmStatus, {isLoading, isError}] = useConfirmStatusMutation();

    const [download, {data: link}] = useLazyDownloadFileQuery();

    if (!client?.id) return <p>Client ID not found</p>;

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({currentPath: "/freelancer/status"})
    );

    const handleConfirm = async () => {
        try {
            await confirmStatus(client?.id);
        } catch (error) {
            console.error("Error confirming:", error);
        }
    };

    const downloadFileHandler = async (id: number, fileName: string) => {
        try {
            fetch(`${process.env.REACT_APP_URL}/file/${id}`, {
                method: 'GET',
                headers: {
                    authorization: `Bearer ${client.token}`,
                    'Content-Type': 'application/json'
                }
            })
                .then((res) => {
                    if (!res.ok) {
                        throw new Error("error");
                    }
                    return res.blob();
                })
                .then((blob) => {
                    const url = window.URL.createObjectURL(blob);
                    const link = document.createElement("a");
                    link.href = url;
                    link.download = fileName;
                    link.click();
                    window.URL.revokeObjectURL(url);
                })
                .catch((error) => {
                    console.error("Error confirming:", error);
                });
        } catch (error) {
            console.error("Error confirming:", error);
        }
    };

    return (
        <div className="p-[40px]">
            <p className="text-[26px] font-[600] mb-2">Details</p>
            <div className="border-b-[1px] border-green w-[900px] mb-10"></div>

            {detailsData && detailsData.documentsNames ? (
                <div>
                    <div className="flex font-[500]">
                        <p className="text-lightGray mr-12">Document name</p>
                        <div>
                            {detailsData.documentsNames.map((documentName: string, index) => (
                                <div>
                                    <p className="mb-2">{documentName}</p>

                                    {index !== detailsData.documentsNames.length - 1 && (
                                        <div className="border-b border-lightGray my-4 w-[730px]"></div>
                                    )}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="flex my-4 font-[500]">
                        <p className="text-lightGray mr-9">Responsible agent</p>
                        <p>{detailsData.expert}</p>
                    </div>
                    <div className="flex font-[500]">
                        <p className="text-lightGray mr-12 ">Application Date</p>
                        <p>{detailsData.releaseDate}</p>
                    </div>
                </div>
            ) : (
                <p>Loading...</p>
            )}

            <p className="text-[26px] font-[600] mb-2 mt-16">Application Status</p>
            <div className="border-b-[1px] border-green w-[900px]"></div>
            <div>
                <p className="text-[22px] font-[500] mt-8">
                    {filesData && filesData.some((file) => file.fileDto?.name)
                        ? "Your document is ready. Pay for your document and you can download it."
                        : "Thank you for contacting us. Your document is being prepared, we will notify you."}
                </p>
                <div className="border-y border-lightGray my-8 p-8 w-[900px]">
                    <div className="flex">
                        <p className="text-lightGray font-[500] mr-12 mt-2">Document</p>
                        <div>
                            {filesData &&
                                filesData.map((file, index) => (
                                    <div
                                        key={file.documentName.documentName}
                                        className=" w-[730px]"
                                    >
                                        <div className="flex justify-between items-center">
                                            <div className="flex-1 my-2">
                                                <p className="font-[500]">
                                                    {file.documentName.documentName}
                                                </p>
                                            </div>

                                            <div
                                                className="text-blue flex-1 whitespace-nowrap overflow-hidden w-full max-w-xs cursor-pointer">
                                                {file.fileDto?.name ? (
                                                    <div
                                                        onClick={() =>
                                                            downloadFileHandler(file.fileDto?.id, file.fileDto?.name)
                                                        }
                                                    >
                                                        {file.fileDto?.name}
                                                    </div>
                                                ) : (
                                                    "---"
                                                )}
                                            </div>

                                            <button
                                                className="border border-[#B0C6FF] py-1 rounded-sm font-[500] hover:shadow flex-1">
                                                {file.status}
                                            </button>
                                        </div>
                                        {index !== filesData.length - 1 && (
                                            <div className="border-b border-lightGray my-4 w-[730px]"></div>
                                        )}
                                    </div>
                                ))}
                        </div>
                    </div>
                </div>
                <div className="flex items-center">
                    <p className="mr-14 text-[14px] font-[500]">
                        Please review and approve <br/> the prepared documents?
                    </p>

                    <NewButtonGreen
                        text="Confirm"
                        onClick={handleConfirm}
                        loading={isLoading}
                    />

                    <ButtonGreen
                        onClick={() => setIsModalOpen(true)}
                        background="white"
                        className="border border-green h-[52px] ml-4"
                        text="Send Message"
                    />
                    {isModalOpen && (
                        <SendMessageModal
                            isOpen={isModalOpen}
                            setIsOpen={setIsModalOpen}
                            title={"Add Expert"}
                        />
                    )}
                </div>
            </div>
            <p className="text-[26px] font-[600] mb-2 mt-10">
                Notification from Expert
            </p>
            <div className="border-b-[1px] border-green w-[900px]"></div>
            <div className="my-8">
                {notificationsData &&
                    notificationsData.map((notification) => (
                        <div>
                            <div>
                                <p className="text-lightGray text-[12px] font-[600]">
                                    {notification.timeOfCreation}
                                </p>
                                <p className="font-[500]">
                                    {notification.textMessage}
                                    <a className="font-[500] text-blue" href={notification.link}>
                                        {notification.textLink}
                                    </a>
                                </p>
                            </div>
                            <div className="border-b border-lightGray my-6 w-[900px]"></div>
                        </div>
                    ))}
                {/* <div>
          <p className="text-lightGray text-[12px] font-[600] ">
            12 Apr, 13:32 am
          </p>
          <p className="font-[500]">
            A new query from Expert.
            <span className="ml-4">
              <a
                className="font-[500] text-blue underline decoration-1"
                href="mailto:emilovbaku530@gmail.com"
              >
                View details
              </a>
            </span>
          </p>
        </div> */}
            </div>
        </div>
    );
};

export default ApplicationStatus;
