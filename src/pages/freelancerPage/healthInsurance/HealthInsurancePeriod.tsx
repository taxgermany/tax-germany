import React, { useState, useEffect } from 'react';
import { ButtonBack } from "../../../components/ui/button/ButtonBack";
import styles from './HealthInsurance.module.scss'
import clsx from "clsx";
import { useNavigate } from "react-router-dom";
import { Button, Spin } from "antd";
import ButtonWhiteBg from "../../../components/ui/button/ButtonWhiteBg";
import GreenPopover from "../../../components/ui/popover/GreenPopover";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import BigTitle from "../../../components/ui/button/BigTitle";
import ValidationText from 'components/ui/validation/ValidationText';
import CustomTextArea from "../../../components/ui/input/CustomTextArea";
import cls from "../profitandexpenses/ProfitAndExpenses16.module.scss";

import {IMiniTable, IMiniTableWithTotal, ITable, ITableVot} from "../../../interfaces/TableInterface";
import RowTable from "../../../components/ui/table/RowTable";
import { IYear } from "../../../interfaces/YearInterface";
import { useFetchAllYearQuery } from "../../../services/yearService";
import { useUpdateMiniTableMutation, useLazyFetchMiniTableQuery } from "../../../services/healthInsurenceServices";
import LoadingMain from 'components/ui/loading/LoadingMain';
import { useAppSelector } from "../../../hooks/useAppSelector";


const HealthInsurancePay = () => {

    const { client } = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({ currentPath: "/freelancer/health-insurance-period" }));


    const [active, setActive] = useState("")

    const navigate = useNavigate()

    const currentDate = new Date();

    const currYear = currentDate.getFullYear();

    const [currentYear, setCurrentYear] = useState(0)

    const [dataMiniChange, setDataMiniChange] = useState<boolean>(false);

    const [dataMini, setDataMini] = useState<IMiniTableWithTotal | null>(null);

    const [getTable, { data: table = {} as IMiniTableWithTotal, error, isLoading, isSuccess }] = useLazyFetchMiniTableQuery()


    const {
        data: years = [] as IYear[],
        isSuccess: isSuccessYear

    } = useFetchAllYearQuery(client?.id || 0);


    const [UpdateTableMini, { isLoading: isLoadingUpdate }] = useUpdateMiniTableMutation()

    const updateData = async () => {
        if (dataMiniChange) {
            const { total, ...newData} = dataMini  as IMiniTableWithTotal
            const res = await UpdateTableMini({ data:newData as IMiniTable, yearId: currentYear })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini(res.data as IMiniTableWithTotal)
            setDataMiniChange(false)
        }
    }


    const handleYear = async (id: number) => {
        await updateData()
        setCurrentYear(id)
        if (client) {
            const res = await getTable({
                userId: client.id,
                yearId: id,
            })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini({...res.data} as IMiniTableWithTotal)

        }

    }

    const handleSubmit = async () => {
        await updateData()
        if (!active) setActive("error")
        if (active !== "error" && active) navigate("/freelancer/health-insurance-regular")
    }


    useEffect(() => {
        if (isSuccessYear && client) {
            setCurrentYear(years[0].id)
            getTable({
                userId: client.id,
                yearId: years[0].id,
            }).then((data) => setDataMini({...data.data}as IMiniTableWithTotal))
        }
    }, [isSuccessYear])


    return (
        <div className='relative'>
            {active === "error" && <ValidationText />}
            <div
                className={clsx(styles.container)}>
                <ButtonBack />
                {isLoading && <LoadingMain className='my-auto' />}
                {error && <h1>error</h1>}
                {isSuccess && !error &&
                    <div className="grid gap-[20px] w-full py-[20px] md:py-[50px]">
                        <div className="grid gap-[20px]">
                            <BigTitle text={"State your health insurance expenses for the audited period"} />
                            <div className="font-inter text-base mt-[]">
                                Expenses should be based on the date of payment, not invoice.
                            </div>
                            <div className="flex gap-5">
                                {
                                    isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                        className={`${currentYear === item.id
                                            ? "border border-black"
                                            : ""}    !h-[32px] rounded-[15px]`}

                                        onClick={() => handleYear(item.id)}>
                                        {currYear === item.yearValue
                                            ? "Current year"
                                            : item.yearValue}</Button>)
                                }
                            </div>
                        </div>
                        {(isLoading || isLoadingUpdate) && <Spin size="large" className="absolute top-1/3 left-1/2" />}
                        {error && <h1>error</h1>}
                        {dataMini && isSuccess && <div className={cls.scroll}>
                            <RowTable name={"Health insurance"} disabled={false} data={dataMini} setDataSource={setDataMini}
                                setDataMiniChange={setDataMiniChange}/>
                        </div>}
                        <div className="grid mt-[30px]">

                            <div className="flex relative gap-5 relative">
                                <div className="text-[12px] font-inter w-[130px]">Health insurance for <br />
                                    this year was paid fully <br /> during this year
                                </div>
                                <GreenPopover
                                    popoverText={"Health insurance for this year was paid fully during this year"}
                                    width={"17px"} height={"17px"} placement={"bottomLeft"}
                                    classNamePopover={"absolute top-[35px] left-[110px]"} />
                                <div>
                                    <div className={"flex gap-[20px] relative"}>
                                        <ButtonWhiteBg active={active} text={"Yes"} setActive={setActive}
                                            size={"w-[170px] h-[52px]"} />
                                        <ButtonWhiteBg active={active} text={"No"} setActive={setActive}
                                            size={"w-[170px] h-[52px]"} />
                                    </div>
                                </div>
                            </div>
                            {active === "No" &&
                                <CustomTextArea wh={"w-[500px] !h-[92px] mt-[20px]"} inputName={"textArea"}
                                    placeholder={"Explain reason why not"} />
                            }
                        </div>
                        <div className="flex gap-[15px] mt-[15px]">
                            <NewButtonGreen text={"Confirm"} onClick={handleSubmit} />
                        </div>
                    </div>
                }
            </div>
        </div>
    );
};

export default HealthInsurancePay;
