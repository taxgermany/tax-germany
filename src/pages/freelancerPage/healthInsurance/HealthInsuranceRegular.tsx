import {useEffect, useState} from 'react';
import clsx from "clsx";
import styles from "./HealthInsurance.module.scss";
import {ButtonBack} from "../../../components/ui/button/ButtonBack";
import {useNavigate} from "react-router-dom";
import ButtonCard from "../../../components/ui/button/ButtonCard";
import CustomTextArea from "../../../components/ui/input/CustomTextArea";
import ValidationText from "../../../components/ui/validation/ValidationText";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import BigTitle from "../../../components/ui/button/BigTitle";
import {activeLink} from "../../../redux/sidebar/sidebarSlice";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {ChangeEvent} from 'react';
import LoadingMain from 'components/ui/loading/LoadingMain';
import {useAppSelector} from 'hooks/useAppSelector';
import {useFeatchHealthQuery, useUpdateHealthRegularMutation} from 'services/healthInsurenceServices';
import {IHealthInsuranceData} from 'interfaces/health';
import {useLazyFetchReviewHealthQuery} from "../../../services/reviewServices";
import {Form} from "antd";

interface iData {
    regularly?: string;
    reasonWhyIrregularly?: string
}

const HealthInsuranceRegular = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/health-insurance-regular"}));

    const [anotherWorkPlace, setAnotherWorkPlace] = useState<string | null>(null);

    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const [active, setActive] = useState("")
    const [healthData, setHealthData] = useState<iData | null>({
        regularly: 'REGULARLY_MONTHLY'
    })

    const [updateHealth] = useUpdateHealthRegularMutation()


    const handleSubmit = async (value: any) => {
        console.log(value)
        try {
            if (!active) setActive("error")


            if (active !== "error" && active) {

                if (healthData && healthData?.regularly !== 'IRREGULARLY') {
                    await updateHealth({
                        clientId: client?.id || '',
                        queryString: `regularly=${healthData?.regularly ? healthData.regularly : 'REGULARLY_MONTHLY'}`
                    })
                } else {
                    await updateHealth({
                        clientId: client?.id || '',
                        queryString: `reasonIrregularly=${value.textArea}&regularly=${healthData?.regularly}`
                    })
                }
                await localStorage.setItem(`storage-${client?.id}`, JSON.stringify({
                    profits: true,
                    health: true,
                    pension: true,
                    review: false,
                    status: false
                }));
                await dispatch(activeLink("pension"))
                navigate("/freelancer/pension-contributions")
            }
        } catch (error) {
            // setActive()
        }
    }


    const checkText = (e: ChangeEvent<HTMLTextAreaElement>) => {
        const value = e.target.value
        setHealthData({...healthData, reasonWhyIrregularly: value})
    }


    const [getHealth, {isLoading: isLoadingHealth}] = useLazyFetchReviewHealthQuery()

    const fetchHealthData = async () => {
        const res = await getHealth({clientId: client?.id || 0}).unwrap()
        switch (res.regularly) {
            case  null:
                setActive("");
                break;
            case  "REGULARLY_MONTHLY":
                setHealthData({...healthData, regularly: 'REGULARLY_MONTHLY'})
                setActive("Regularly monthly");
                break;
            case "REGULARLY_NOT_MONTHLY":
                setHealthData({...healthData, regularly: 'REGULARLY_NOT_MONTHLY'})
                setActive("Regularly, not monthly");
                break;
            case "IRREGULARLY":
                setAnotherWorkPlace(res.reasonWhyIrregularly)
                setHealthData({...healthData, regularly: 'IRREGULARLY'})
                setActive("Irregularly");
                break;
            default:
                return;
        }
    }


    useEffect(() => {
        fetchHealthData()
    }, [anotherWorkPlace])


    return (
        <div className='relative'>
            {active === "error" && <ValidationText/>}
            <div className={clsx(styles.container)}>
                <ButtonBack/>
                {isLoadingHealth && <LoadingMain className='my-auto'/>}

                <div className="py-[20px] md:py-[50px] grid gap-[50px] justify-center px-[16px] py-[50px]">
                    <BigTitle text={"How regularly do you pay for health insurance?"}/>
                    <div className="flex gap-[20px] relative">
                        <ButtonCard size={"w-[204px] !h-[114px] font-[500]"}
                                    active={active} setActive={setActive}
                                    text={"Regularly monthly"}
                                    onClick={() => setHealthData({...healthData, regularly: 'REGULARLY_MONTHLY'})}
                        />
                        <ButtonCard size={"w-[204px] !h-[114px] font-[500]"}
                                    active={active} setActive={setActive}
                                    text={"Regularly, not monthly"} turn={true}
                                    popoverText={"Regularly, not monthly"}
                                    onClick={() => setHealthData({
                                        ...healthData,
                                        regularly: 'REGULARLY_NOT_MONTHLY'
                                    })}/>
                        <ButtonCard size={"w-[204px] !h-[114px] font-[500]"}
                                    active={active} setActive={setActive}
                                    text={"Irregularly"} turn={true} popoverText={"Irregularly"}
                                    onClick={() => setHealthData({...healthData, regularly: 'IRREGULARLY'})}/>

                    </div>
                    <Form
                        name="personal"
                        layout="horizontal"
                        labelCol={{span: 8}}
                        wrapperCol={{span: 16}}
                        style={{maxWidth: 650}}
                        initialValues={{textArea: anotherWorkPlace}}
                        onFinish={handleSubmit}
                        autoComplete="off"
                        className={"grid gap-y-[10px]"}
                    >
                        {active === "Irregularly" &&
                            <CustomTextArea
                                inputName="textArea"
                                className="!w-[900px] !h-[44px]"
                                placeholder={"What is the reason of irregular payments?"}/>
                        }
                        <Form.Item>
                            <NewButtonGreen text={"Confirm"}/>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
};

export default HealthInsuranceRegular;
