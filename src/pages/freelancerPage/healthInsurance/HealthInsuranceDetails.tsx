import { useNavigate } from "react-router-dom";
import clsx from "clsx";
import styles from "./HealthInsurance.module.scss";
import { ButtonBack } from "../../../components/ui/button/ButtonBack";
import { Form } from "antd";
import InputNoAppearance from "../../../components/ui/input/InputNoAppearance";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import BigTitle from "../../../components/ui/button/BigTitle";
import { ChangeEvent, useState, useEffect } from 'react'
import LoadingMain from "components/ui/loading/LoadingMain";
import { useAppSelector } from "hooks/useAppSelector";
import { useFeatchHealthQuery, useUpdateHealthMutation } from "services/healthInsurenceServices";
import { IHealthInsuranceData } from "interfaces/health";
const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};


const HealthInsuranceDetails = () => {

    const { client } = useAppSelector(state => state.authSlice)
    const { id } = useAppSelector(state => state.healthSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({ currentPath: "/freelancer/health-insurance-details" }));

    const navigate = useNavigate()
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState<IHealthInsuranceData>({} as IHealthInsuranceData)

    const { data: fetchHealth, isSuccess, } = useFeatchHealthQuery({ clientId: client?.id || '' })
    const [updateHealth] = useUpdateHealthMutation()

    useEffect(() => {
        if (fetchHealth) {
            const responseData: IHealthInsuranceData = fetchHealth
            console.log(responseData);
            setData(responseData)
            setLoading(false)
        } else {
            setLoading(true)
        }

    }, [isSuccess])

    console.log(data);

    const onFinish = async (values: any) => {
        try {
            if (data) {
                await updateHealth({ data: data, clientId: client?.id || '' })
                navigate("/freelancer/health-insurance-period")
            }

        }
        catch (error: any) {
            console.log(error)
        }
    }
    return (
        <div className={clsx(styles.container, 'w-[100%] h-auto')}>
            <ButtonBack />
            {loading ? (
                <LoadingMain className="my-auto" />
            ) :
                <div className={clsx("w-[100%] h-[100%] grid justify-center items-center px-[16px] py-[20px]")}>
                    <BigTitle text={"Health insurance details"} className={"pb-[40px]"} />
                    <Form
                        name="basic"
                        layout="horizontal"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        style={{ maxWidth: 650 }}
                        initialValues={{
                            remember: true,
                            name: data.companyName !== 'string' ? data.companyName : '',
                            cityAndPostalCode: data.cityPostalCodeAddress !== 'string' ? data.cityPostalCodeAddress : "",
                            StreetBuildingNumber: data.address !== 'string' ? data.address : "",
                            membershipNumber: data.membershipNumber !== 0 ? data.membershipNumber : '',
                            contactPerson: data.contactPerson !== 'string' ? data.contactPerson : '',
                            phoneNumber: data.phoneNumber !== 'string' ? data.phoneNumber : ''
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                        className={"grid gap-y-[10px]"}
                    >
                        <InputNoAppearance inputName={"name"}
                            placeholder={"Health insurance company name"}
                            background={true} wh={"w-[400px] md:w-[560px]"} border={true}
                            type="text"
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, companyName: e.target.value })} />
                        <InputNoAppearance inputName={"cityAndPostalCode"} background={true} border={true}
                            wh={"w-[400px] md:w-[560px]"}
                            placeholder={`Health insurance company address (Postal code, City)`}
                            type="text"
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, cityPostalCodeAddress: e.target.value })} />
                        <InputNoAppearance inputName={"StreetBuildingNumber"} background={true} border={true}
                            wh={"w-[400px] md:w-[560px]"}
                            placeholder={"Health insurance company address (Street, Building number)"}
                            type="text"
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, address: e.target.value })} />
                        <InputNoAppearance inputName={"membershipNumber"} background={true} border={true}
                            wh={"w-[400px] md:w-[560px]"}
                            placeholder={"Membership number"} type="text"
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, membershipNumber: parseInt(e.target.value) || 0 })} />
                        <InputNoAppearance inputName={"contactPerson"} background={true} border={true}
                            wh={"w-[400px] md:w-[560px]"}
                            placeholder={"Contact person"} type="text"
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, contactPerson: e.target.value })} />
                        <InputNoAppearance inputName={"phoneNumber"} background={true} border={true}
                            wh={"w-[400px] md:w-[560px]"}
                            placeholder={"Health insurance phone number"} type="text"
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, phoneNumber: e.target.value })} />
                        <Form.Item>
                            <NewButtonGreen text={"Confirm"} />
                        </Form.Item>
                    </Form>

                </div>}
        </div>
    );
};

export default HealthInsuranceDetails;
