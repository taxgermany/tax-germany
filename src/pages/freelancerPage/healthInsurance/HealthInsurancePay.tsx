import {useEffect, useState} from 'react';
import {ButtonBack} from "../../../components/ui/button/ButtonBack";
import styles from './HealthInsurance.module.scss'
import clsx from "clsx";
import {useNavigate} from "react-router-dom";
import ButtonWhiteBg from "../../../components/ui/button/ButtonWhiteBg";
import GreenPopover from "../../../components/ui/popover/GreenPopover";
import ValidationText from "../../../components/ui/validation/ValidationText";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import BigTitle from "../../../components/ui/button/BigTitle";
import {useAppSelector} from 'hooks/useAppSelector';
import {useDispatch} from 'react-redux';
import {setHealthID} from 'redux/health/healthSlice';
import {useCreateHealthMutation} from 'services/healthInsurenceServices';
import {useLazyFetchReviewHealthQuery, useLazyFetchReviewYourQuery} from "../../../services/reviewServices";
import dayjs from "dayjs";
import {IYourDetailsFetch} from "../personalData/PersonalData";


const HealthInsurancePay = () => {

    const {client} = useAppSelector(state => state.authSlice)
    const [createHealth, {isLoading}] = useCreateHealthMutation()
    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/health-insurance-pay"}));


    const navigate = useNavigate()
    const [active, setActive] = useState("")

    const dispatch = useDispatch()

    const handleSubmit = async () => {
        try {
            if (!active) setActive("error")

            if (client) {
                if (active === "No") {
                    await createHealth({clientId: client.id, doYouDayHealth: false});
                    navigate("/freelancer/pension-contributions");
                    return
                }
                const response = await createHealth({clientId: client.id, doYouDayHealth: active === 'Yes'});
                if ("data" in response) {
                    const responseData = response.data as { id: number };
                    if ("id" in responseData)
                        await dispatch(setHealthID(responseData.id));
                }
                navigate("/freelancer/health-insurance-details");
            }
        } catch (error) {
            console.error('Ошибка при выполнении запроса: ', error);
        }
        ;
    }


    const [getHealth, {isLoading: isLoadingHealth}] = useLazyFetchReviewHealthQuery()

    const fetchHealth = async () => {
        const res = await getHealth({clientId: client?.id || 0}).unwrap()
        setActive(res.doYouPayForHealthInsurance === null
            ? ""
            : res.doYouPayForHealthInsurance
                ? "Yes"
                : "No")

    }


    useEffect(() => {
        fetchHealth()
    }, [])

    return (
        <div className='relative'>
            {active === "error" && <ValidationText/>}
            <div
                className={clsx(styles.container)}>
                <ButtonBack/>
                <div className={clsx(styles.content, "px-[16px] py-[50px]")}>
                    <div className="grid gap-[50px] max-w-[750px] w-full m-auto">
                        <div className="flex gap-2">
                            <BigTitle text={"Do you pay for health insurance?"}/>
                            <GreenPopover popoverText={"pay for health insurance"} width={"26px"} height={"26px"}/>
                        </div>

                            <div className="flex gap-[20px] text-green relative">
                                <ButtonWhiteBg active={active} text={"Yes"} setActive={setActive}/>
                                <ButtonWhiteBg active={active} text={"No"} setActive={setActive}/>
                            </div>
                        <NewButtonGreen loading={isLoading} text={"Confirm"} onClick={handleSubmit}/>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default HealthInsurancePay;
