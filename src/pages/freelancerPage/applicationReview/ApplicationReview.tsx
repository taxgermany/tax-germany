import { ButtonBack } from "components/ui/button/ButtonBack"
import { useState, useEffect } from "react";
import ButtonGreen from "components/ui/button/ButtonGreen";
import ButtonGray from "components/ui/button/ButtonGray";
import { useNavigate } from "react-router-dom";
import AppReaviewData from "components/screen/appReaview/AppReviewData";
import ApplicationReviewProfitAndExpenses from "components/screen/appReaview/ApplicationReaviewProfitsAndExpenses";
import AppReavHealthInsurance from "components/screen/appReaview/AppReavHealthInsurance";
import AppReviewPensionContr from "components/screen/appReaview/AppReviewPensionCont";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {useFetchReviewDocumentsQuery} from "services/reviewServices";
import {useLazyFetchIsPaidQuery} from "../../../services/paymentService";

interface IApplicationReview {
    isEditData: string
    isChekConfirm: boolean
}

const ApplicationReview = () => {

    const {client} = useAppSelector(state => state.authSlice)


    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/review"}));

    const {data: getDoc = [], isSuccess} = useFetchReviewDocumentsQuery({clientId: client?.id || ''})
    const [getPaid, {data}] = useLazyFetchIsPaidQuery()

    const navigate = useNavigate()
    const [state, setState] = useState<IApplicationReview>({
        isEditData: '',
        isChekConfirm: false
    });
    const btnEditData = (buttonEdit: string) => {
        setState((prevState) => ({
            ...prevState,
            isEditData: buttonEdit
        }))

    }
    const btnChekConfirm = () => {
        setState((prevState) => ({
            ...prevState,
            isChekConfirm: !state.isChekConfirm
        }))
    }

    const handleSubmit = async () => {
        const res = await getPaid({clientId: client?.id || 0}).unwrap()
        if (res.isPaid) {
            navigate('/freelancer/status')
        } else {
            navigate('/freelancer/please-confirm')
        }

    }

    return (
        <div className="px-[40px] pr-0">
            <ButtonBack />
            <div className="w-[100%] ">
                {
                    state.isChekConfirm ? <div className="absolute top-0 right-0 left-0 bottom-0 bg-gray/50 z-[9999] flex justify-center items-center">
                        <div className="w-[36%] bg-white grid gap-y-[30px] p-3 rounded-[3px]">
                            <p className="font-[700] text-[22px] ">Confirm information</p>

                            <p>Please confirm that all provided information and data is complete and no crucial details
                                are missing</p>
                            <div className="flex justify-end items-center gap-x-[10px]">
                                <ButtonGreen className="rounded-[4px]" height="40px" width="90px" onClick={handleSubmit}
                                             text='Confirm'/>
                                <ButtonGray className="rounded-[4px]" size="w-[90px] h-[40px]" onClick={btnChekConfirm}
                                            text='Cancel'/>
                            </div>
                        </div>
                    </div> : ''
                }
                {/* заголовок и audit report */}
                <div >
                    <div>
                        <p className="font-[600] text-[30px] mb-[20px]">Application Review</p>
                    </div>
                    <div className="w-fit flex flex-wrap gap-2">
                        {getDoc && getDoc.map(item => <div
                            className="min-w-[235px] h-[30px] bg-[#BAF0D6] font-[500] text-[14px] px-[13px] text-black/50 flex justify-start items-center">
                            {item}
                        </div>)}
                    </div>
                </div>
                {/* Pesonal Data */}
                <div className="grid gap-y-[15px]">
                    <AppReaviewData text="Personal Data" active={state.isEditData} setActive={btnEditData}/>
                    <ApplicationReviewProfitAndExpenses text="pl" active={state.isEditData} setActive={btnEditData}/>
                    <AppReavHealthInsurance text="hi" active={state.isEditData} setActive={btnEditData}/>
                    <AppReviewPensionContr text="pens" active={state.isEditData} setActive={btnEditData}/>
                </div>
                <div className="fixed bottom-2 right-7 z-[99]">
                    <ButtonGreen text="App Status" className="rounded-[4px]" onClick={btnChekConfirm}/>
                </div>
            </div>
        </div>
    )
}
export default ApplicationReview
