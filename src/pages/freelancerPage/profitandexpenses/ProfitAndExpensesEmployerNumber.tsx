import {ButtonBack} from "components/ui/button/ButtonBack"
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import {useLocation, useNavigate} from "react-router-dom";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {Form} from "antd";
import GreenPopover from "../../../components/ui/popover/GreenPopover";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {
    addAccounting,
    addEmployerNumber,
    addHaveEmployees
} from "../../../redux/profitandexpension/profitAndExpensesSlice";
import {useUpdateProfitsMutation} from "../../../services/profitServices";
import {useAppSelector} from "../../../hooks/useAppSelector";
import ValidationText from "../../../components/ui/validation/ValidationText";
import React, {useEffect, useState} from "react";
import {useFetchReviewProfitsQuery, useLazyFetchReviewProfitsQuery} from "../../../services/reviewServices";


const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};


const ProfitAndExpensesEmployerNumber = () => {

    const {client} = useAppSelector(state => state.authSlice)

    let pathname = useLocation().pathname

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/profits-and-expenses-number"}));

    const {data} = useAppSelector(state => state.profitAndExpensesSlice)
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const [number,setNumber] = useState<string | null>(null)
    const [UpdateProfits, {
        isLoading: isLoadingUpdate,
        error: errorUpdate
    }] = useUpdateProfitsMutation()


    const [getProfits, {
        data: profits,
        isLoading: isLoadingProfits,
        isSuccess: isSuccessProfits
    }] = useLazyFetchReviewProfitsQuery()

    const fetchProfits = async () => {
        if(client) {
            const res = await getProfits({clientId: client.id}).unwrap()
            setNumber(res.employerNumber?res.employerNumber : " ")
        }
    }
    useEffect(()=>{
        fetchProfits()
    },[isSuccessProfits,pathname])

    const onFinish = async (values: any) => {
        try {
          /*
            dispatch(addEmployerNumber(values.employerNumber))*/
            await UpdateProfits({data: {employerNumber: values.employerNumber}, clientId: client?.id || 1})
            navigate('/freelancer/profits-and-expenses-vat')
        } catch (error) {
            console.error("An error occurred:", error);
        }
    }

    return (
        <div className="p-[20px] md:p-[40px] overflow-hidden w-[100%] h-[80vh]">
            {errorUpdate && <ValidationText text={"ошибка при запросе"}
                                            className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"}/>}

            <ButtonBack/>
            {number && <div className="px-[16px] py-[50px]">
                <Form
                    name="employerNumber"
                    layout="horizontal"
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                    initialValues={{employerNumber:number?number:" "}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off">

                    <div className="grid m-auto gap-y-[50px] max-w-[750px] relative">
                        <div className="w-[412px]">
                            <p className="text-[28px] font-[600] relative font-[inter]">
                                What is your Employer number (Betriebsnummer) ?
                                <GreenPopover popoverText={" What is your Employer number"} width='26px'
                                              height='26px'/>
                            </p>
                        </div>
                        <InputNoAppearance
                            inputName="employerNumber"
                            type='number'
                            placeholder="Pension number"
                            wh="w-[604px]"
                            background={true}
                            border={true}
                        />

                        <Form.Item
                            className="!mb-0 !h-[52px]">
                            <NewButtonGreen className="rounded-[4px] !mb-0" text="Confirm" loading={isLoadingUpdate}/>
                        </Form.Item>
                    </div>
                </Form>
            </div>}
        </div>
    )
}

export default ProfitAndExpensesEmployerNumber
