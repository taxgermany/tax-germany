import React, {useEffect, useState} from 'react';
import {useLocation, useNavigate} from "react-router-dom";
import {ButtonBack} from "../../../components/ui/button/ButtonBack";
import ButtonWhiteBg from 'components/ui/button/ButtonWhiteBg';
import ValidationText from 'components/ui/validation/ValidationText';
import {
    addEmployerNumber,
    addHaveEmployees,
    addRegularly
} from "../../../redux/profitandexpension/profitAndExpensesSlice"
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import {useUpdateProfitsMutation} from "../../../services/profitServices";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {useFetchReviewProfitsQuery, useLazyFetchReviewProfitsQuery} from "../../../services/reviewServices";

const ProfitAndExpensesPeriod = () => {

    const {client} = useAppSelector(state => state.authSlice)


    let pathname = useLocation().pathname

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/profits-and-expenses-period"}));

    const dispatch = useAppDispatch()

    const {data} = useAppSelector(state => state.profitAndExpensesSlice)
    const navigate = useNavigate()

    const [active, setActive] = useState("");

    const [UpdateProfits, {
        isLoading: isLoadingUpdate,
        isSuccess: isSuccessUpdate,
        error: errorUpdate
    }] = useUpdateProfitsMutation()


    const [getProfits, {
        data: profits,
        isLoading: isLoadingProfits,
        isSuccess: isSuccessProfits
    }] = useLazyFetchReviewProfitsQuery()

    const fetchProfits = async () => {
        if(client) {
            const res = await getProfits({clientId: client?.id || 0}).unwrap()
           /* dispatch(addHaveEmployees(profits.haveEmployees))*/
            setActive(res.haveEmployees === null
                ? ""
                : res.haveEmployees
                    ? "Yes"
                    : "No")

        }
    }
    useEffect(()=>{
        fetchProfits()
    },[isSuccessProfits,pathname])


    const handleSubmit = async () => {
        if (!active) {
            setActive("error")
            return true
        }
        if (active === 'Yes' || active === 'No') {
            try {
                await UpdateProfits({data: {haveEmployees: active === 'Yes'}, clientId: client?.id || 1})
             /*   dispatch(addHaveEmployees(active === 'Yes'))*/
                active === 'Yes'
                    ? navigate('/freelancer/profits-and-expenses-employer')
                    : navigate('/freelancer/profits-and-expenses-vat')
            } catch (error) {
                console.error("An error occurred:", error);
            }
        }
    }
    return (
        <div className='relative'>
            {active === "error" && <ValidationText/>}
            {errorUpdate && <ValidationText text={"ошибка при запросе"}
                                            className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"}/>}

            <div className="p-[40px] ">
                <ButtonBack/>

                <div className="grid gap-[50px] justify-center items-center px-[16px] py-[50px]">
                    <div className="font-bold text-[28px] leading-[34px]">
                        Did you have any employees for the audited period?
                    </div>
                    <div className="flex gap-[20px] text-green">
                        <ButtonWhiteBg active={active} setActive={setActive} text="Yes"/>
                        <ButtonWhiteBg active={active} setActive={setActive} text="No"/>
                    </div>
                    <NewButtonGreen text={"Confirm"} onClick={handleSubmit} loading={isLoadingUpdate}/>
                </div>

            </div>
        </div>
    );
};

export default ProfitAndExpensesPeriod;
