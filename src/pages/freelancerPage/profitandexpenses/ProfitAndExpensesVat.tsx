import {ButtonBack} from 'components/ui/button/ButtonBack';
import React, {useEffect, useState} from 'react';
import {useLocation, useNavigate} from 'react-router-dom';
import ButtonWhiteBg from 'components/ui/button/ButtonWhiteBg';
import ValidationText from 'components/ui/validation/ValidationText';
import {useUpdateProfitsMutation} from "../../../services/profitServices";

import {useAppSelector} from "../../../hooks/useAppSelector";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {addHaveEmployees, addVat} from "../../../redux/profitandexpension/profitAndExpensesSlice"
import {useFetchReviewProfitsQuery, useLazyFetchReviewProfitsQuery} from "../../../services/reviewServices";


const ProfitAndExpensesVat = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/profits-and-expenses-vat"}));

    const {data} = useAppSelector(state => state.profitAndExpensesSlice)

    const dispatch = useAppDispatch()


    let pathname = useLocation().pathname

    const [fileCheck, setFileCheck] = useState<File | null>(null);


    const [UpdateProfits, {
        isLoading: isLoadingUpdate,
        error: errorUpdate
    }] = useUpdateProfitsMutation()

    const navigate = useNavigate()
    const [active, setActive] = useState("");

    const [getProfits, {
        data: profits,
        isLoading: isLoadingProfits,
        isSuccess: isSuccessProfits
    }] = useLazyFetchReviewProfitsQuery()

    const fetchProfits = async () => {
        if(client) {
            const res = await getProfits({clientId: client?.id || 0}).unwrap()
       /*     dispatch(addVat(profits.vat))*/
            setActive(res.vat === null
                ? ""
                : res.vat
                    ? "Yes"
                    : "No")

        }
    }
    useEffect(()=>{
        fetchProfits()
    },[isSuccessProfits,pathname])

    const handleSubmit = async () => {

        if (active === "Yes" || active === 'No') {
            await UpdateProfits({data: {...data, vat: active === "Yes"}, clientId: client?.id || 1})
        /*    dispatch(addVat(active === "Yes"))*/
            navigate("/freelancer/profits-and-expenses-table")
        }
    }


    // if(isSuccessCreate) navigate("/freelancer/profits-and-expenses-profit")
    return (
        <div className='relative'>
            {active === "error" && <ValidationText/>}
            {errorUpdate && <ValidationText text={"ошибка при запросе"}/>}
            <div className="pt-[40px] pl-[40px]">
                <ButtonBack/>
                <div className="grid gap-[50px] max-w-[750px] justify-center items-center px-[16px] pt-[50px]">
                    <div className="font-bold text-[28px] leading-[34px]">Do you pay VAT?</div>
                    <div className="flex gap-[10px] text-green">
                        <ButtonWhiteBg active={active} setActive={setActive} text="Yes"/>
                        <ButtonWhiteBg active={active} setActive={setActive} text="No"/>
                    </div>
                    <NewButtonGreen text={"Confirm"} onClick={handleSubmit} loading={isLoadingUpdate}/>
                </div>
            </div>
        </div>
    );
};

export default ProfitAndExpensesVat;
