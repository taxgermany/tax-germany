import {ButtonBack} from 'components/ui/button/ButtonBack';
import LargeTable from 'components/ui/table/LargeTable';
import {useNavigate} from "react-router-dom";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import {activeLink} from "../../../redux/sidebar/sidebarSlice";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {Button, Spin} from 'antd';
import React, {useEffect, useState} from 'react';
import cls from './ProfitAndExpenses16.module.scss'
import {ITable, ITableVot} from "../../../interfaces/TableInterface";
import {useFetchAllYearQuery,} from "../../../services/yearService";
import {IYear} from "../../../interfaces/YearInterface";
import {
    useLazyFetchTableQuery,
    useUpdateTableMutation,
} from "../../../services/profitServices";
import {useAppSelector} from "../../../hooks/useAppSelector";
import ButtonWhiteBg from "../../../components/ui/button/ButtonWhiteBg";
import {useCreateFileProfitsMutation, useLazyFetchFileNameProfitsQuery} from "../../../services/fileService";
import {IFileName} from "../../../interfaces/ProfitsInterface";


export const totalNull = {
    "grossRevenueInclVat": 0,
    "grossRevenue": 0,
    "vatReceived": 0,
    "expenses": 0,
    "officeSupplies": 0,
    "officeRent": 0,
    "insuranceExclHealth": 0,
    "publicTransport": 0,
    "telephone": 0,
    "sumOfVat": 0,
    "vatPaidToTaxOffice": 0,
    "otherExpenses": 0,
    "monthlyProfits": 0
}


const ProfitAndExpensesLargeTable = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/profits-and-expenses-table"}));

    const {data} = useAppSelector(state => state.profitAndExpensesSlice)

    const currentDate = new Date();
    const currYear = currentDate.getFullYear();


    const [dataLarge, setDataLarge] = useState<ITableVot | ITable | null>(null);


    const [fileCheck, setFileCheck] = useState<File | null>(null);
    const [file, setFile] = useState<IFileName | null>(null);

    const [dataLargeChange, setDataLargeChange] = useState<boolean>(true);

    const {
        data: years = [] as IYear[],
        error: errorYear,
        isLoading: isLoadingYear,
        isSuccess: isSuccessYear
    } = useFetchAllYearQuery(client?.id || 0);

    const [getFileProfits, {isLoading: isLoadingFileProfits}] = useLazyFetchFileNameProfitsQuery()

    const [currentYear, setCurrentYear] = useState(0)

    const [getTable, {
        data: table = {} as ITable | ITableVot,
        error,
        isLoading,
        isSuccess
    }] = useLazyFetchTableQuery()


    const [UpdateTable, {
        isLoading: isLoadingUpdate,
    }] = useUpdateTableMutation()

    const [CreatFile] = useCreateFileProfitsMutation()


    const dispatch = useAppDispatch()
    const navigate = useNavigate();


    const updateData = async () => {

        let newDataLarge = {
            ...JSON.parse(JSON.stringify(dataLarge)),
            total: totalNull,
            totalYearProfit: 0
        } as ITableVot | ITable

        if (dataLargeChange) {

            newDataLarge?.months.forEach(item => {
                item.grossRevenueInclVat = 0;
                item.expenses = 0;
                item.monthlyProfits = 0;
            });

            const res = await UpdateTable({
                data: newDataLarge as ITableVot | ITable,
                yearId: currentYear,
                vat: data.vat
            })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataLarge(res.data as ITableVot | ITable)

            setDataLargeChange(false)
        }

        if (fileCheck && client) {
            const formData = new FormData();
            formData.append("file", fileCheck)
            await CreatFile({data: formData, userId: client.id || 0})
        }

    }

    const handleYear = async (yearId: number) => {
        await updateData()
        setCurrentYear(yearId)
        if (client) {
            getTable({
                userId: client.id,
                yearId: yearId,
                vat: data.vat
            }).then((data) => setDataLarge(data.data as ITableVot | ITable))
        }


        if (fileCheck && client) {
            const formData = new FormData();
            formData.append("file", fileCheck)
            await CreatFile({data: formData, userId: client.id || 0})
        }

    }
    const handleFetch = async () => {
        await updateData()
    }
    const handleSubmit = async () => {
        if (!client) return
        const sidebarItems = localStorage.getItem(`storage-${client.id}`);
        if (sidebarItems !== null) {
            const items = JSON.parse(sidebarItems);
            localStorage.setItem(`storage-${client.id}`, JSON.stringify({
                    ...items,
                    profits: true,
                }
            ));
        } else {
            navigate("/freelancer/personal-data")
        }

        localStorage.setItem(`storage-${client?.id}`, JSON.stringify({
            profits: true,
            health: true,
            pension: false,
            review: false,
            status: false
        }));
        await updateData();
        /* dispatch(activeLink("health"));*/
        navigate("/freelancer/health-insurance-pay")
    }

    const fetchData = async () => {
        if (isSuccessYear && client && years.length > 0) {
            const res = await getFileProfits({userId: client.id}).unwrap()
            setFileCheck(res.file)
            setFile(res)
            setCurrentYear(years[0].id || 0)
            getTable({
                userId: client.id,
                yearId: years[0].id || 0,
                vat: data.vat
            }).then((data) => setDataLarge(data.data as ITableVot | ITable))
        }
    }

    useEffect(() => {
        fetchData()

    }, [isSuccessYear])


    return (
        <div className="p-[40px]">
            <ButtonBack/>


            <div className="grid gap-[40px] pt-[40px]">
                <div className="font-bold text-[28px] leading-[34px]">Specify what were your profits</div>
                <div className='leading-[26px] text-[16px] w-[800px] font-[Inter] font-[500]'>
                    Mention expenses, related only to your freelancer activities. Expenses should be based on
                    the date of payment, not invoice. Please, exclude VAT from your expenses and include this
                    VAT in a separate line called Sum of VAT (from all mentioned expenses) .
                    Also in a separate line mention VAT paid to the Tax Office (its also an expense).
                    Don t include health insurance and pension contribution.
                </div>

                <div className="flex gap-5">

                    {isLoadingYear && <h1>loading</h1>}
                    {errorYear && <h1>error</h1>}
                    {isSuccessYear && years.map(item => <Button type="text"
                                                                className={`${currentYear === item.id
                                                                    ? "border border-black"
                                                                    : ""} !h-[32px] rounded-[15px]`}

                                                                onClick={() => handleYear(item.id)}>
                        {currYear === item.yearValue
                            ? "Current year"
                            : item.yearValue}</Button>)
                    }
                </div>
                <div className="">
                    <div className={cls.scroll}>

                        {(isLoading || isLoadingUpdate || isLoadingFileProfits) && <Spin size="large" className="absolute top-1/3 left-1/2"/>}
                        {error && <h1>error</h1>}
                        {dataLarge && file && isSuccess && !error && <LargeTable file={file} vat={data.vat} data={dataLarge}
                                                                         setDataLargeChange={setDataLargeChange}
                                                                         setDataSource={setDataLarge}
                                                                         fileCheck={fileCheck}
                                                                         setFileCheck={setFileCheck}
                                                                         disabled={false}/>

                        }

                    </div>
                </div>
                <div className="flex gap-[20px]">
                    <NewButtonGreen text='Confirm' onClick={handleSubmit} loading={isLoadingUpdate}/>
                    <ButtonWhiteBg text={"Calculate"} setActive={handleFetch} size={"h-[52px]"}
                                   className={"px-[35px]"}/>
                </div>

            </div>


        </div>
    );
};

export default ProfitAndExpensesLargeTable;
