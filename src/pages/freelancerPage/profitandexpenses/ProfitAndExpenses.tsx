import {useEffect, useState} from 'react';
import {useLocation, useNavigate} from "react-router-dom";
import {ButtonBack} from "../../../components/ui/button/ButtonBack";
import ButtonCard from 'components/ui/button/ButtonCard';

import ValidationText from "../../../components/ui/validation/ValidationText";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {useCreateProfitAndExpensesMutation, useUpdateProfitsMutation} from "../../../services/profitServices";
import {addAccounting} from "../../../redux/profitandexpension/profitAndExpensesSlice";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useFetchReviewProfitsQuery, useLazyFetchReviewProfitsQuery} from "../../../services/reviewServices";


const ProfitAndExpenses = () => {


    const {client} = useAppSelector(state => state.authSlice)

    let pathname = useLocation().pathname

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/profits-and-expenses"}));
    const {data} = useAppSelector(state => state.profitAndExpensesSlice)
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const [active, setActive] = useState("")
    /* const [active, setActive] = useState(data.accountingType === null
         ? ""
         : data.accountingType === "BALANCE_SHEET"
             ? "Balance sheet"
             : "Cash based accounting");*/

    const [UpdateProfits, {
        isLoading: isLoadingUpdate,
        error: errorUpdate,

    }] = useUpdateProfitsMutation()

    const [createProfits, {isSuccess}] = useCreateProfitAndExpensesMutation()
    const [getProfits, {
        data: profits,
        isLoading: isLoadingProfits,
        isSuccess: isSuccessProfits
    }] = useLazyFetchReviewProfitsQuery()

    const fetchProfits = async () => {
        const res = await getProfits({clientId: client?.id || 0}).unwrap()
        /*   dispatch(addAccounting(profits.accountingType))*/
        setActive(res.accountingType === null
            ? ""
            : res.accountingType === "BALANCE_SHEET"
                ? "Balance sheet"
                : "Cash based accounting")

    }

    useEffect(() => {
        fetchProfits()
    }, [isSuccessProfits, pathname])

    const handleSubmit = async () => {
        if (!active) setActive("error")
        try {
            if (active === "Balance sheet" || active === 'Cash based accounting') {
                let choose = active === "Balance sheet" ? "BALANCE_SHEET" : "CASH_BASED"
                await createProfits({data: {accountingType: choose}, clientId: client?.id || 1})
                /* dispatch(addAccounting(choose))*/
                navigate("/freelancer/profits-and-expenses-regularly")
            }
        } catch (error: any) {
            setActive("error")
        }
    }


    return (<div className='relative w-full'>
            {active === "error" && <ValidationText
                className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"}/>}
            {errorUpdate && <ValidationText text={"ошибка при запросе"}
                                            className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"}/>}
            <div className="p-[40px] w-full relative">
                <ButtonBack/>
                <div className="grid gap-[40px] justify-center items-center px-[16px] py-[20px]">
                    <div className="font-[600] font-[inter] text-[28px] leading-[34px]">What kind of bookkeeping do you
                        have?
                    </div>
                    <div className='leading-[26px] text-[16px] w-[800px] font-[Inter] font-[400]'>
                        <mark className='text-[#0090FD] bg-transparent'>Balance sheets (Bilanzierung)</mark>
                        are usually prepared by commercial freelancers with an annual profit of over €60,000. The <mark
                        className='text-[#0090FD] bg-transparent'>cash-based accounting (EÜR
                        Einnahmenüberschussrechnung)</mark> is generally used by intellectual and artist freelancers.
                    </div>
                    <div className="flex gap-[20px] text-green">
                        <ButtonCard active={active} setActive={setActive} text="Balance sheet"
                                    size="!w-[204px]  h-[114px]" turn={true} popoverText="Balance sheet"/>
                        <ButtonCard active={active} setActive={setActive} text="Cash based accounting"
                                    size="!w-[204px]  h-[114px]" turn={true} popoverText="Cash based accounting"/>
                    </div>
                    <NewButtonGreen text={"Confirm"} onClick={handleSubmit} loading={isLoadingUpdate}/>
                </div>
            </div>

        </div>
    );
};

export default ProfitAndExpenses;
