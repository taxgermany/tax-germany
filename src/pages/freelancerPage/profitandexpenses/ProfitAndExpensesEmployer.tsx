import { ButtonBack } from 'components/ui/button/ButtonBack';
import MicroTable from 'components/ui/table/microTable';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from "../../../hooks/useAppDispatch";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import React, { useEffect, useState } from "react";
import { IEmployer, ITableVot } from "../../../interfaces/TableInterface";
import {
    useFetchTableEmployerQuery,
    useUpdateTableEmployerMutation
} from "../../../services/profitServices";
import { useAppSelector } from "../../../hooks/useAppSelector";
import ValidationText from "../../../components/ui/validation/ValidationText";


const ProfitAndExpensesEmployer = () => {

    const { client } = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({ currentPath: "/freelancer/profits-and-expenses-employer" }));

    const {
        data: tableEmployer = [],
        error,
        isLoading,
        isFetching,
        isSuccess
    } = useFetchTableEmployerQuery(client?.id || 1)

    const [UpdateTable, {
        isLoading: isLoadingUpdate,
        isSuccess: isSuccessUpdate,
        error: errorUpdate
    }] = useUpdateTableEmployerMutation()

    const [dataMicroTable, setDataMicroTable] = useState<IEmployer[]>([]);
    const navigate = useNavigate()
    const dispatch = useAppDispatch()

    const handleSubmit = async () => {

        try {
            await UpdateTable(dataMicroTable);
            navigate("/freelancer/profits-and-expenses-number");
        } catch (error) {
            console.error("Request failed:", error);
        }
    };

    useEffect(() => {
        if (isSuccess) setDataMicroTable(tableEmployer)
    }, [isSuccess])
    return (
        <div className="pt-[40px] pl-[40px] ">
            <ButtonBack />
            {isLoading && <h1>loading</h1>}
            {errorUpdate && <ValidationText text={"ошибка при запросе"}
                className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"} />}

            {isSuccess && !isLoading && !error &&
                <div className="grid gap-[40px] justify-center items-center px-[16px] py-[20px]">
                    <div className="grid gap-y-[30px] font-bold text-[28px] leading-[34px]">
                        How many employees per year did you have on average?
                        <div className='leading-[26px] text-[16px] w-[800px] font-[Inter] font-[400]'>
                            Specify how many of empoyees were by function. Also mention, how many of them were part-time
                            or
                            your family members.
                        </div>
                    </div>
                    <div>
                        <MicroTable data={dataMicroTable} setDataSource={setDataMicroTable} disabled={false} />
                    </div>

                    <NewButtonGreen text={"Confirm"} onClick={handleSubmit} loading={isLoadingUpdate} />
                </div>}


        </div>
    );
};

export default ProfitAndExpensesEmployer;
