import React, {useEffect, useState} from 'react';
import {useLocation, useNavigate} from "react-router-dom";
import {ButtonBack} from "../../../components/ui/button/ButtonBack";
import ButtonWhiteBg from 'components/ui/button/ButtonWhiteBg';
import ValidationText from 'components/ui/validation/ValidationText';
import {
    addAccounting,
    addHaveEmployees,
    addRegularly,
    addVat
} from "../../../redux/profitandexpension/profitAndExpensesSlice"
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import NewButtonGreen from "../../../components/ui/button/NewButtonGreen";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {useUpdateProfitsMutation} from "../../../services/profitServices";
import {useFetchReviewProfitsQuery, useLazyFetchReviewProfitsQuery} from "../../../services/reviewServices";
import {Spin} from "antd";

const ProfitAndExpensesRegularly = () => {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/profits-and-expenses-regularly"}));

    const dispatch = useAppDispatch()

    const {data} = useAppSelector(state => state.profitAndExpensesSlice)
    const navigate = useNavigate()

    const [UpdateProfits, {
        isLoading: isLoadingUpdate,
        error: errorUpdate
    }] = useUpdateProfitsMutation()


    let pathname = useLocation().pathname

    const [active, setActive] = useState("");


    const [getProfits, {
        data: profits,
        isLoading: isLoadingProfits,
        isSuccess: isSuccessProfits
    }] = useLazyFetchReviewProfitsQuery()

    const fetchProfits = async () => {
        if(client) {
            const res = await getProfits({clientId: client?.id || 0}).unwrap()
            /*dispatch(addRegularly(profits.regularly))*/
            setActive(res.regularly === null
                ? ""
                : res.regularly
                    ? "Yes"
                    : "No")

        }
    }
    useEffect(()=>{
        fetchProfits()
    },[isSuccessProfits,pathname])


    const handleSubmit = async () => {
        if (active === "Yes" || active === 'No') {
            await UpdateProfits({data: {...data, regularly: active === "Yes"}, clientId: client?.id || 1})
            /*dispatch(addRegularly(active === "Yes"))*/
            navigate("/freelancer/profits-and-expenses-period")
        }
    }
    return (
        <div className='relative'>
            {isLoadingProfits ? <Spin size="large" className="absolute top-2/4 left-2/4"/>
                : <div>
                    {active === "error" && <ValidationText/>}
                    {errorUpdate && <ValidationText text={"ошибка при запросе"}
                                                    className={"absolute top-[0] px-[20px] p-[5px] w-full bg-[#ff4d4f] text-white overflow-none"}/>}

                    <div className="p-[40px] ">
                        <ButtonBack/>
                        {/* <div className="flex gap-[11px]"><Arrow/>
                <div className="text-green">Back</div>
            </div>*/}
                        <div className="grid gap-[50px] justify-center items-center px-[16px] py-[50px]">
                            <div className="font-bold text-[28px] leading-[34px]">Do you prepare your books regularly?
                            </div>
                            <div className='leading-[26px] text-[16px] w-[800px] font-[Inter] font-[400]'>
                                <mark className='text-[#0090FD] bg-transparent'>
                                    {"Regularly "}
                                </mark>
                                means every month.
                                <mark className='text-[#0090FD] bg-transparent'> {"Irregulary "}</mark>
                                means one time a year or less frequently
                            </div>
                            <div className="flex gap-[20px] text-green">
                                <ButtonWhiteBg active={active} setActive={setActive} text="Yes"/>
                                <ButtonWhiteBg active={active} setActive={setActive} text="No"/>
                            </div>
                            <NewButtonGreen text={"Confirm"} onClick={handleSubmit} loading={isLoadingUpdate}/>
                        </div>

                    </div>
                </div>}
        </div>
    );
};

export default ProfitAndExpensesRegularly;
