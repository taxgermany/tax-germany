import React, {useEffect, useState} from "react";

import {Elements,} from "@stripe/react-stripe-js";
import CheckoutForm from "./CheckoutForm";
import {loadStripe, Appearance} from "@stripe/stripe-js";
import clsx from "clsx";
import styles from "../freelancerPage/healthInsurance/HealthInsurance.module.scss";
import {ButtonBack} from "../../components/ui/button/ButtonBack";
import {ReactComponent as Img} from "../../assets/icons/payment.svg";

import {
    useFetchKeyPaymentQuery,
    useCreateProfitMutation,
    useFetchPriceQuery,
    useCreatePayMutation
} from "../../services/paymentService";
import {useAppSelector} from "../../hooks/useAppSelector";

const appearance = {
    labels: 'resting',
    variables: {

        colorBackground: '#F7F7F7',
        borderRadius: '4px',
    },
    rules: {
        '.Label': {
            display: "none"
        },
        '.Label--floating': {
            height: "52px",
        },
        ".Input": {
            height: "52px",
        }
    }
};

function Payment() {

    const {client} = useAppSelector(state => state.authSlice)

    localStorage.setItem(`${client?.id || 0}`, JSON.stringify({currentPath: "/freelancer/payment"}));

    const [stripePromise, setStripePromise] = useState(null);
    const [clientSecret, setClientSecret] = useState("");


    const {
        data,
        error: errorKey,
        isLoading: isLoadingKey,
        isFetching: isFetchingKey,
        isSuccess: isSuccessKey
    } = useFetchKeyPaymentQuery();
    const [createPayment, {}] = useCreateProfitMutation()



    const {
        data: price,
        error: errorPrice,
        isLoading: isLoadingPrice,
        isFetching: isFetchingPrice,
        isSuccess: isSuccessPrice
    } = useFetchPriceQuery(client.id)
    /*    const [getKey,
            {data,
            error: errorKey,
            isLoading: isLoadingKey,
            isFetching: isFetchingKey,
            isSuccess: isSuccessKey}] = useLazyFetchKeyPaymentQuery()

    */
    /*  useEffect(() => {

          getKey().then(res=>setStripePromise(loadStripe(res.data.message)))

        /!*
          fetch("/config").then(async (r) => {
              const {publishableKey} = await r.json();
              setStripePromise(loadStripe(publishableKey));
          });*!/

      }, []);*/

    useEffect(() => {
        createPayment({data: JSON.stringify({}), userId: client.id}).then(data => setClientSecret(data.data.message))
        /*  fetch("/create-payment-intent", {
              method: "POST",
              body: JSON.stringify({}),
          }).then(async (result) => {
              var {clientSecret} = await result.json();
              setClientSecret(clientSecret);
          });*/
    }, []);

    return (
        <>
            <div
                className={clsx(styles.container)}>
                <ButtonBack/>
                <div className={clsx("flex  justify-center px-[16px] px-[16px] py-[20px]")}>
                    <div className="grid grid-cols-2 gap-[90px] max-w-[780px] w-full m-auto items-center">
                        <div className="grid gap-5 justify-items-center">
                            <Img/>
                            <p className="font-inter font-bold text-[22px]">Your document is in processing. Please make
                                a payment to review them</p>
                        </div>
                        {price && clientSecret && data && (
                            <div>
                                <Elements stripe={loadStripe(data.message)} options={{clientSecret, appearance}}>
                                    <CheckoutForm price={price}/>
                                </Elements>
                            </div>

                        )}
                    </div>
                </div>

            </div>
        </>
    );
}

export default Payment;
