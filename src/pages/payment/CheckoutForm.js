import {LinkAuthenticationElement, PaymentElement} from "@stripe/react-stripe-js";
import React, {useState} from "react";
import {useStripe, useElements,} from "@stripe/react-stripe-js";
import clsx from "clsx";
import s from "./Payment.module.scss"

import "./PaymentStyle.scss"
import {activeLink} from "../../redux/sidebar/sidebarSlice";
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {useCreatePayMutation} from "../../services/paymentService";
import {useAppSelector} from "../../hooks/useAppSelector";

const appearance = {
    theme: 'flat',
    variables: {
        colorBackground: '#F7F7F7',
        borderRadius: '4px',
        // See all possible variables below
    }
};

export default function CheckoutForm({price}) {

    const {client} = useAppSelector(state => state.authSlice)
    const dispatch = useAppDispatch()
    const stripe = useStripe();
    const elements = useElements();

    const [email, setEmail] = useState('');
    const [message, setMessage] = useState(null);
    const [isProcessing, setIsProcessing] = useState(false);

    const [createPay, {}] = useCreatePayMutation()

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (client) createPay({userId: client.id})

        if (!stripe || !elements) {
            // Stripe.js has not yet loaded.
            // Make sure to disable form submission until Stripe.js has loaded.
            return;
        }

        setIsProcessing(true);

        localStorage.setItem(`storage-${client?.id}`, JSON.stringify({
            profits: true,
            health: true,
            pension: true,
            review: true,
            status: true
        }));

        const {error} = await stripe.confirmPayment({
            elements,
            confirmParams: {
                // Make sure to change this to your payment completion page
                return_url: `${window.location.origin}/freelancer/status`,
            },
        });


        if (error.type === "card_error" || error.type === "validation_error") {
            setMessage(error.message);
        } else {
            setMessage("An unexpected error occured.");
        }

        setIsProcessing(false);

        dispatch(activeLink("status"))
    };


    return (
        <>
            <div className="font-inter font-bold text-[22px] pb-[20px]">Pay with card</div>
            <form id="payment-form" onSubmit={handleSubmit}>
                <LinkAuthenticationElement
                    id="link-authentication-element"
                    appearance={appearance}

                />
                <PaymentElement id="payment-element" appearance={appearance}/>
                <button disabled={isProcessing || !stripe || !elements} id="submit"
                        className={clsx(s.buttonSubmit)}>
                      <span id="button-text">
                          {isProcessing ? "Processing ... " : "Pay $"} {price}
                     </span>
                </button>
                {/*  Show any error or success messages
                  {message && <div id="payment-message">{message}</div>}*/}
            </form>
        </>

    );
}
