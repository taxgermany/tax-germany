import React, {useEffect, useState} from 'react';
import TextTitle from "../../components/ui/text/textForAdmin/TextTitle";
import {ReactComponent as NotificationImg} from '../../assets/icons/notification.svg'
import Modal from "../../components/screen/AdminClient/Modal/modal";
import Notification from "../../components/screen/AdminClient/notification/Notification";
import {useAppSelector} from "../../hooks/useAppSelector";
import {
    getAllClients,
    IClients,
    IExportAdmin, IFilterParams, INotifications,
    ISearchParams,
} from "../../interfaces/admin";
import AdminClientTable from "../../components/screen/AdminClient/AdminClientTable/AdminClientTable";
import Filter from "../../components/screen/AdminClient/Filter/Filter";
import {IOption} from "../../interfaces/antdInterface";
import {
    useGetExpertsQuery, useLazyGetPaginationClientsQuery, useLazyGetSearchAdminQuery, useLazyGetFilterQuery,
    useUpdateClientAdminMutation, useGetNotificationsQuery, useLazyGetNotificationsQuery
} from "../../services/superAdminService";
import {useFetchNotificationsQuery, useUpdateClientMutation} from "../../services/exportsServices";
import {Spin} from "antd";


const ClientsSuperAdmin = () => {
    const [clients, setClients] = useState<getAllClients | undefined>({} as getAllClients)
    const [edit, setEdit] = useState<IClients | null>(null)
    const [showModal, setShowModal] = useState(false)
    const [currentPage, setCurrentPage] = useState(1);
    const [sizePage, setSizePage] = useState(8);
    const [totalNumber, setTotalNumber] = useState(0);
    const [totalPages, setTotalPage] = useState<number | undefined>(clients?.total);
    const [experts, setExpert] = useState<IOption[] | null>([] as IOption[])

    const {client} = useAppSelector(state => state.authSlice)

    const {
        data: dataExperts = [] as IExportAdmin[],
        error: errorExperts,
        isFetching: isFetchingExperts,
        isLoading: isLoadingExperts,
        isSuccess: isSuccessExperts
    } = useGetExpertsQuery()

    const [getClients, {
        data: dataClients = {} as getAllClients,
        error: errorClients,
        isFetching: isFetchingClients,
        isLoading: isLoadingClients,
        isSuccess: isSuccessClients
    }] = useLazyGetPaginationClientsQuery()


    const [getFilter, {
        data: dataFilter = {} as getAllClients,
        error: errorFilter,
        isFetching: isFetchingFilter,
        isLoading: isLoadingFilter,
        isSuccess: isSuccessFilter
    }] = useLazyGetFilterQuery()

    const [getSearch, {
        data: dataSearch = {} as getAllClients,
        error: errorSearch,
        isFetching: isFetchingSearch,
        isLoading: isLoadingSearch,
        isSuccess: isSuccessSearch
    }] = useLazyGetSearchAdminQuery()

    const [updateClient, {
        data: updateData,
        error: errorUpdateClients,
        isLoading: isLoadingUpdateClients,
        isSuccess: isSuccessUpdateClients
    }] = useUpdateClientAdminMutation()


    const [getNotifications,{
        data: Notifications = [] as INotifications[],
        error: errorNotifications,
        isFetching: isFetchingNotifications,
        isLoading: isLoadingNotifications,
        isSuccess: isSuccessNotifications
    }] = useLazyGetNotificationsQuery()

    const handleSearch = (params: ISearchParams) => {
        let newParams: ISearchParams = {...params}
        if (!params.search) delete newParams.search
        if (client) getSearch({...newParams}).then((res) => setClients(res.data))

    }
    const handleFilter =async (params: IFilterParams) => {
        let newParams: IFilterParams = {...params}

        if (!params.status) delete newParams.status
        if (!params.expertId) delete newParams.expertId
        if (!params.docId) delete newParams.docId
        const  res = await getFilter({...newParams}).unwrap()
        setClients(res)
        /*getFilter({...newParams}).then((res) => setClients(res.data))*/
    }

    const handlePageChange = (pageNumber: number) => {
        setCurrentPage(pageNumber);
    }


    const handleUpdate = async (edit: IClients) => {
        if (client) {
            const res = await updateClient({
                clientId: edit.id,
                status: edit.status,
                selectedExpertId: edit.responsibleAgent.value,
                page: currentPage,
                size: sizePage
            }).unwrap()
            setClients(res)
        }

    }
    const fetchClient = async () => {
        if (client) {
            const res = await getClients({
                page: currentPage,
                size: sizePage

            }).unwrap()
            setClients(res)
        }
    }

    useEffect(() => {
        fetchClient()
       /* if (client) getClients({page: currentPage, size: sizePage}).then((res) => setClients(res.data))*/
    }, [currentPage])

   useEffect(() => {
       getNotifications()
       /* if (client) getClients({page: currentPage, size: sizePage}).then((res) => setClients(res.data))*/
    }, [showModal])


    useEffect(() => {
        if (isSuccessExperts) {
            const newDataExpert: IOption[] = dataExperts.map(item => {
                return {value: String(item.id), label: `${item.firstName} ${item.lastName}`}
            })
            setExpert(newDataExpert)

        }
    }, [isSuccessExperts])
    useEffect(() => {
        setTotalPage(clients?.total)
    }, [clients])

    useEffect(() => {
        setClients(updateData)
    }, [isSuccessUpdateClients])

    return (
        <div className="grid gap-5">
            <div className="flex justify-between items-center"><TextTitle title={"Clients"}/>
                <div onClick={() => setShowModal(!showModal)}><NotificationImg className="w-[50px] hoverAnimation"/></div>
            </div>
            {experts && <Filter setClients={setClients} experts={experts} sizePage={sizePage} currentPage={1}
                                getSearch={handleSearch} getFilter={handleFilter} isSuccessExperts={isSuccessExperts}/>}
            {(isLoadingSearch || isLoadingClients || isLoadingFilter || isLoadingUpdateClients) &&
               <Spin size="large" className="absolute top-2/4 left-2/4"/>}
            {experts && clients && totalPages &&
                <AdminClientTable experts={experts} allClients={clients.responseList}
                                  totalNumber={totalNumber}
                                  setTotalNumber={setTotalNumber}  sizePage={sizePage}
                                  setEdit={setEdit} edit={edit}
                                  currentPage={currentPage} handleUpdate={handleUpdate}
                                  totalPages={totalPages} onPageChange={handlePageChange}/>}


            {showModal && <Modal active={showModal} setActive={setShowModal} children={
                <Notification notifications={Notifications}/>}/>}

        </div>
    );
};

export default ClientsSuperAdmin;
