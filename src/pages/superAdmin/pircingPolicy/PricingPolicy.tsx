import React, { useEffect, useState } from 'react'
import TextTitle from "components/ui/text/textForAdmin/TextTitle"
import { ReactComponent as Icon } from '../../../assets/icons/notifications.svg'
import { ReactComponent as IconEdit } from '../../../assets/icons/edit.svg'
import { ReactComponent as IconCheck } from '../../../assets/icons/check_circle.svg'
import { ReactComponent as IconCancel } from '../../../assets/icons/cancel.svg'
import LoadingMain from 'components/ui/loading/LoadingMain'
import InputAdminClient from 'components/ui/input/inputForAdmin/InputAdminClient'
import { Form } from 'antd'
import {useFetchDocumentQuery, useLazyFetchDocumentQuery, useUpdateDocumentMutation} from 'services/documentService'
import {ReactComponent as NotificationImg} from "../../../assets/icons/notification.svg";
import Modal from "../../../components/screen/AdminClient/Modal/modal";
import Notification from "../../../components/screen/AdminClient/notification/Notification";
import {INotifications} from "../../../interfaces/admin";
import {useLazyGetNotificationsQuery} from "../../../services/superAdminService";

interface IData {
    id: number,
    documentName: string,
    description: string,
    additionalInformation: string,
    price: number
}
interface IEditData {
    description: string,
    additionalInformation: string,
    price: number
}
const PricingPolicy = () => {

    const [data, setData] = useState<IData[] | null>()
    const [dataOld, setDataOld] = useState<IData[]>([])
    const [editData, setEditData] = useState<IEditData>({} as IEditData)
    const [editId, setEditId] = useState<number | null>(null)
    const [showModal, setShowModal] = useState(false)

    const [getData, {isSuccess,isLoading} ] = useLazyFetchDocumentQuery()
    const [updateDoc,{isSuccess:isSuccessUpdate,isLoading:isLoadingUpdate}] = useUpdateDocumentMutation()

    const [getNotifications,{
        data: Notifications = [] as INotifications[],
        error: errorNotifications,
        isFetching: isFetchingNotifications,
        isLoading: isLoadingNotifications,
        isSuccess: isSuccessNotifications
    }] = useLazyGetNotificationsQuery()


    const fetchData = async () => {
        try {
           const res =  await getData().unwrap()
            await setData(res)

        } catch (error: any) {
            console.log(error.message)
        }
    }


    useEffect(() => {
        fetchData()
    }, [data])

    useEffect(() => {
        getNotifications()
    }, [showModal])


    const onFinish = async () => {
        try {
            if (editId) {
              /*  setData(updatedData(editId, editData));*/
                await updateDoc({
                    id: editId,
                    data: editData
                })
            }
            // Если запрос на сервер выполнен успешно, обновите данные с сервера
            await fetchData(); // Вызовите функцию, которая обновляет data с сервера
            await setEditId(null);
        } catch (error) {
            console.error(error);
        }
    }
    console.log(data);

    return (
        <div className="max-w-[1550px] box-border">
            {
                isLoading && isLoadingUpdate && data
                    ? <LoadingMain />
                    : <><div className="flex items-center justify-between">
                        <TextTitle title="Pricing Policy" />
                        <div onClick={() => setShowModal(!showModal)}><NotificationImg className="w-[50px]"/></div>
                    </div>
                        <Form
                            name='pircingPolicy'
                            onFinish={onFinish}
                        >
                            <div className="px-[15px] py-[5px] ">
                                <div className=" flex items-center justify-between gap-x-[20px] border border-b-[#EEEEEE] border-t-0 border-l-0 border-r-0 text-start">
                                    <div className="w-[5%]">
                                        <p className="text-[#0F0E0E5C] leading-[19px]">№</p>
                                    </div>
                                    <div className="w-[25%]">
                                        <p className="text-[#0F0E0E5C] leading-[19px] text-start">Title of Services</p>
                                    </div>
                                    <div className="w-[25%]">
                                        <p className="text-[#0F0E0E5C] leading-[19px] text-start">Description</p>
                                    </div>
                                    <div className="w-[8%]">
                                        <p className="text-[#0F0E0E5C] leading-[19px] text-start">{'Price (in Euro)'}</p>
                                    </div>
                                    <div className="w-[30%]">
                                        <p className="text-[#0F0E0E5C] leading-[19px] text-start">Additional Information</p>
                                    </div>
                                    <div className="w-[7%]">
                                        <p className="text-[#0F0E0E5C] leading-[19px] text-start">Manage</p>
                                    </div>
                                </div>
                                {data && data.map((item, index) =>
                                    <div key={item.id} className=" flex items-center justify-between gap-x-[20px] py-[10px] border border-b-[#EEEEEE] border-t-0 border-l-0 border-r-0">
                                        <div className="w-[5%]">
                                            <p className="text-black font-roboto font-[500] leading-[22px]">{index + 1}</p>
                                        </div>
                                        <div className="w-[25%]">
                                            <p className="text-black font-roboto font-[500] leading-[22px]">{item.documentName}</p>
                                        </div>
                                        <div className="w-[25%]">
                                            <InputAdminClient
                                                text={item.description}
                                                wh='w-full'
                                                className='!bg-[#F4F4F433] !border-green'
                                                inputname={`description${item.id}`}
                                                placeholder='Additional Information'
                                                initialValue={item.description}
                                                disabled={editId === item.id ? false : true}
                                                onChange={(e) => {
                                                    setEditData({ ...editData, description: e.target.value });
                                                }}
                                            />
                                        </div>
                                        <div className="w-[8%]">
                                            <InputAdminClient
                                                text={`${item.price}`}
                                                wh='w-full !justify-center'
                                                className='!bg-[#F4F4F433] !border-green'
                                                inputname={`price${item.id}`}
                                                placeholder='price'
                                                initialValue={`${item.price}`}
                                                disabled={editId === item.id ? false : true}
                                                onChange={(e) => {
                                                    setEditData({ ...editData, price: parseFloat(e.target.value) });
                                                }}
                                            />
                                        </div>
                                        <div className="w-[30%]">
                                            <InputAdminClient
                                                text={item.additionalInformation}
                                                wh='w-full'
                                                className='!bg-[#F4F4F433] !border-green'
                                                inputname={`additionalInformation${item.id}`}
                                                placeholder='Additional Information'
                                                initialValue={item.additionalInformation}
                                                disabled={editId === item.id ? false : true}
                                                onChange={(e) => {
                                                    setEditData({ ...editData, additionalInformation: e.target.value });
                                                }}
                                            />
                                        </div>
                                        <div className="w-[7%] text-center flex gap-x-[5px]">
                                            {editId === item.id
                                                ? <div className='m-auto flex gap-x-[5px]'>
                                                    <button>
                                                        <IconCheck />
                                                    </button>
                                                    <span onClick={() => {
                                                        setData(dataOld)
                                                        setEditId(null)
                                                    }}
                                                        className='cursor-pointer'>
                                                        <IconCancel />
                                                    </span>
                                                </div>
                                                : <span onClick={() => {
                                                    setEditId(item.id);
                                                    setEditData({
                                                        price: item.price,
                                                        additionalInformation: item.additionalInformation,
                                                        description: item.description
                                                    });
                                                }}
                                                    className='cursor-pointer m-auto'>
                                                    <IconEdit />
                                                </span>
                                            }
                                        </div>
                                    </div>)
                                }
                            </div>
                        </Form>
                    </>
            }
            {showModal && <Modal active={showModal} setActive={setShowModal} children={
                <Notification notifications={Notifications}/>}/>}
        </div >
    )
}
export default PricingPolicy
