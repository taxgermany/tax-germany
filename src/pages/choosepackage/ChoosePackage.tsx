import {ButtonBack} from "components/ui/button/ButtonBack";
import BigTitle from "components/ui/button/BigTitle";
import {ServicesCard} from "components/ui/card/ServicesCard";
import {useState} from "react";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {useNavigate} from "react-router-dom";
import {useCreateDocumentMutation, useFetchDocumentQuery} from "../../services/documentService";
import {useAppSelector} from "../../hooks/useAppSelector";
import {setActiveCard} from "../../redux/priceCard/priceCard";
import {useAppDispatch} from "../../hooks/useAppDispatch";


const ChoosePackage = () => {


    const { client } = useAppSelector((state) => state.authSlice);

    localStorage.setItem(
        `${client?.id || 0}`,
        JSON.stringify({ currentPath: '/freelancer/choose-package' })
    );
    const {data, error, isLoading, isSuccess} = useFetchDocumentQuery()
    const {data: dataActive} = useAppSelector(state => state.priceCardSlice)
    const [activeAll, setActiveAll] = useState<{ [p: number]: boolean }>(dataActive);

    const navigate = useNavigate()
    const [createDoc, {isLoading: isLoadingUpdateDoc}] = useCreateDocumentMutation()


    const dispatch = useAppDispatch()

    const handleSubmit = async () => {
        dispatch(setActiveCard(activeAll))
        let documentId: string[] = []
        if (activeAll[1]) documentId.push("1")
        if (activeAll[2]) documentId.push("2")
        if (activeAll[3]) documentId.push("3")
        if (activeAll[4]) documentId.push("4")
        if (client) {
            await createDoc({userId: client.id, docId: documentId.join(",")})
            navigate("/freelancer/personal-data")
        }
    }

    const handleChoose = (id: number) => {

        if (activeAll[4]) {
            return setActiveAll({...activeAll, [id]: !activeAll[id], [2]: false, [3]: false})
        }
        if (activeAll[2] || activeAll[3]) {
            return setActiveAll({...activeAll, [id]: !activeAll[id], [4]: false})
        }
        setActiveAll({...activeAll, [id]: !activeAll[id]})
    }

    return (
        <div className="p-[40px]">
            <ButtonBack/>
            <div className="ResetPassword h-[80vh] font-inter flex justify-center items-center">
                {isLoading &&
                    <>Loading...</>}
                {isSuccess &&
                    <div className="grid gap-[30px]">
                        <BigTitle text="What do you need to prove your income?"/>
                        <p className="text-[12px] leading-[26px]  font-[Inter] font-[400]">
                            Please, confirm with your visa officer or landlord which document you need and for what
                            period
                            of time
                        </p>
                        <div className="flex items-center justify-around gap-[10px]">
                            {data.map(item => <ServicesCard currentId={item.id}
                                                            key={item.id} active={activeAll[item.id]}
                                                            price={item.price}
                                                            popoverText={item.additionalInformation}
                                                            text={item.description}
                                                            handleChoose={handleChoose}
                                                            title={item.documentName}/>)}
                        </div>

                        <NewButtonGreen text="Confirm" onClick={handleSubmit} loading={isLoadingUpdateDoc}/>
                    </div>}
            </div>
        </div>
    )
}

export default ChoosePackage;
