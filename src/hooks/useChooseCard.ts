



export const useChooseCard = (id: number, setActiveAll: (pr: { [p: number]: boolean }) => void,activeAll: { [p: number]: boolean }) => {
    if (activeAll[4]) {
       setActiveAll({...activeAll, [id]: !activeAll[id], [2]: false, [3]: false})
        return
    }
    if (activeAll[2] || activeAll[3]) {
        setActiveAll({...activeAll, [id]: !activeAll[id], [4]: false})
        return
    }
    setActiveAll({...activeAll, [id]: !activeAll[id]})
    return
}