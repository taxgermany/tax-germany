import {
    createApi,
    FetchArgs,
    fetchBaseQuery,
} from '@reduxjs/toolkit/query/react'
import { RootState } from "../redux/store";


export const baseQuery = fetchBaseQuery({
    baseUrl: process.env.REACT_APP_URL,
    prepareHeaders: (headers, { getState }) => {
        const token = (getState() as RootState).authSlice.token
        if (token) {
            headers.set("authorization", `Bearer ${token}`)
        }
        return headers
    }
});
export const baseQueryWithReAuth = async (
    args: FetchArgs | string,
    api: any,
    extraOptions: any
) => {
    const result: any = await baseQuery(args, api, extraOptions)
    return result
}
export const apiSlice = createApi({
    reducerPath: 'api',
    baseQuery: baseQueryWithReAuth,
    tagTypes: ['AuthAPI', 'User', 'ProfitsAPI', 'healthAPI', 'admin', 'DocumentAPI', 'exportsAPI',
        'fileApi', 'lifeAPI', 'paymentAPI', 'pensionAPI', 'personalAPI', 'reviewHealthMiniTableAPI',
        'reviewPensionMiniTableAPI', 'reviewProfitsAPI', 'yearAPI', "StatusAPI", "reviewServicesAPI", "clientControllerAPI",
        "arayAPI"],
    endpoints: builder => ({})
})
