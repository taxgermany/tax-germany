import {createApi,} from "@reduxjs/toolkit/query/react";
import {IEmployer, IMicroTable, ITable, ITableVot} from "../interfaces/TableInterface";
import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";


export const reviewProfitsAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchReviewTable: build.query<ITableVot | ITable, { userId: number, yearId: number, vat: boolean }>({
            query: ({userId, yearId, vat}) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/rev/profits/get/table${vat ? '/vat' : ''}/${userId}/${yearId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['reviewProfitsAPI']
        }),
        /* fetchTable: build.query<ITable, { userId: number, yearId: number }>({
             query: ({userId, yearId}) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                 // для запроса: это может быть тело запросо,какие-то параметры,url           //
                 url: `/profits/table/${userId}/${yearId}`,           //query  нужен для того чтобы получать get
                 //  mutation нужен для того чтобы изменять сервер post | put
                 /!* params: {
                      _limit: limit
                  }*!/
             }),
             providesTags: (result, error, arg) => ['profitsAPI']
         }),*/
        fetchReviewTableEmployer: build.query<IMicroTable[], number>({
            query: (yearId) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/rev/profits/get/empl/table/${yearId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['reviewProfitsAPI']
        }),

        updateReviewTable: build.mutation<ITable | ITableVot, { data: ITable, yearId: number,vat: boolean }>({
            query: ({data, yearId,vat}) => ({
                url: `/rev/profits/update/table${vat ? '/vat' : ''}/${yearId}`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['reviewProfitsAPI']
        }),

        updateReviewTableEmployer: build.mutation<IEmployer[], IEmployer[]>({
            query: (data: IEmployer[]) => ({
                url: `/rev/profits/update/empl/table`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['reviewProfitsAPI']
        }),
    })
})

export const {
     useUpdateReviewTableMutation,
    useUpdateReviewTableEmployerMutation,
    useFetchReviewTableEmployerQuery, useFetchReviewTableQuery, useLazyFetchReviewTableQuery
} = reviewProfitsAPI;
