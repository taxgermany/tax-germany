import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {profitsType} from "../interfaces/ProfitsInterface";
import {IEmployer, ITable, ITableVot} from "../interfaces/TableInterface";
import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";
import {IDocumentController, IEditDocumentController} from "../interfaces/documentController";
import { INet} from "../interfaces/adminNet";
import {IAuditDocument} from "../interfaces/auditDocument";



export const documentAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchDocument: build.query<IDocumentController[],void>({
            query: () => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/incomeConfirmation/get/all`,           //query  нужен для того чтобы получать get
            }),
        }),
        fetchDocumentReport: build.query<IAuditDocument,number>({
            query: (id) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/incomeConfirmation/report/get/${id}`,           //query  нужен для того чтобы получать get
            }),
        }),
        createDocument: build.mutation<void, {docId:string,userId:number,}>({
            query: ({docId,userId}) => ({
                url: `/incomeConfirmation/set/document/to/freelancer/${docId}/${userId}`,
                method: "POST",
                body: docId
            }),
        }),

        updateDocument: build.mutation<IEditDocumentController, { data:IEditDocumentController,id:number }>({
            query: ({data,id}) => ({
                url: `/incomeConfirmation/update/${id}`,
                method:"PUT",
                body: data
            }),
        }),

        netDocument: build.query<INet,{id:string,page: number, size: number}>({
            query: ({page,id,size}) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/net/get/${id}?page=${page}`,           //query  нужен для того чтобы получать get
            }),
        }),



    })
})

export const {useCreateDocumentMutation,useUpdateDocumentMutation,useFetchDocumentQuery,useLazyFetchDocumentQuery,
    useFetchDocumentReportQuery,useNetDocumentQuery,useLazyNetDocumentQuery,
    useLazyFetchDocumentReportQuery} = documentAPI;
