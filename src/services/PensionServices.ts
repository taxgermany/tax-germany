import {createApi} from "@reduxjs/toolkit/query/react";
import {IMiniTable, IMiniTableWithTotal} from "../interfaces/TableInterface";
import {apiSlice, baseQueryWithReAuth} from "../api/api";
import { IPensionData } from "interfaces/pension";



export const pensionAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({

        fetchPensionMiniTable: build.query<IMiniTableWithTotal, {userId:number,yearId:number}>({
            query: ({userId,yearId}) => ({
                url: `/pensionContribution/table/get/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['pensionAPI']
        }),

        updatePensionMiniTable: build.mutation<IMiniTableWithTotal, { data: IMiniTable,yearId:number }>({
            query: (data) => ({
                url: `/pensionContribution/table/fill/${data.yearId}`,
                method: "PUT",
                body: data.data
            }),
            invalidatesTags: ['pensionAPI']
        }),
        createPension: build.mutation<IPensionData, { clientId: number; doYouMakePension: boolean, }>({
            query: ({ clientId, doYouMakePension  }) => ({
                url: `/pensionContribution/doYouMakePension/${clientId}?doYouMakePension=${doYouMakePension}`,
                method: "POST",
            }),
            invalidatesTags: ['pensionAPI'],
        }),
        fetchPension: build.query<IPensionData, { pensionID: number | string, }>({
            query: ({ pensionID }) => ({
                url: `/pensionContribution/get/${pensionID}`
            }),
            providesTags: ['pensionAPI'],
        }),
        updatePensionNumber: build.mutation<void, { clientId: number | null; pensionNumber: string }>({
            query: ({ pensionNumber, clientId,}) => ({
                url: `/pensionContribution/setPensionNumber/${clientId}?pensionNumber=${pensionNumber}`,
                method: "PUT",
            }),
            invalidatesTags: ['pensionAPI'],
        }),
        updatePensionSecuraty: build.mutation<void, { clientId: number | string; queryString: string, }>({
            query: ({ queryString, clientId, }) => ({
                url: `/pensionContribution/setPensionSecurity/${clientId}?${queryString}`,
                method: "PUT",
            }),
            invalidatesTags: ['pensionAPI'],
        }),
        updatePension: build.mutation<void, { id:number,data:IPensionData }>({
            query: ({ id,data }) => ({
                url: `/pensionContribution/update/${id}`,
                method: "PUT",
                body:data
            }),
            invalidatesTags: ['pensionAPI'],
        }),
    }),
});

export const {
    useFetchPensionMiniTableQuery,
    useLazyFetchPensionMiniTableQuery,
    useUpdatePensionMiniTableMutation,
    useCreatePensionMutation,
    useFetchPensionQuery,
    useLazyFetchPensionQuery,
    useUpdatePensionNumberMutation,
    useUpdatePensionSecuratyMutation,
    usePrefetch,
    useUpdatePensionMutation

} = pensionAPI;
