
import {apiSlice,} from "../api/api";
import {IDetailsDocuments, IDetailsProfitGet, IDetailsYouGet} from "../interfaces/details";



export const detailsAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({

        fetchDetailsYouGet: build.query<IDetailsYouGet,string>({
            query: (id) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/details/your/get/${id}`,           //query  нужен для того чтобы получать get
            }),
        }),
        fetchDetailsDocument: build.query<IDetailsDocuments[], { id:string }>({
            query: ({id}) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/details/review/documents/${id}`,           //query  нужен для того чтобы получать get
            }),
        }),
        fetchDetailsProfitsGet: build.query<IDetailsProfitGet,string>({
            query: (id) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/details/profits/get/${id}`,           //query  нужен для того чтобы получать get
            }),
        }),
        updateStatus: build.mutation<{status:string}, { status:string,id:string }>({
            query: ({status,id}) => ({
                url: `/details/changeStatus/${id}?status=${status}`,
                method:"PUT",
            }),
        })
   /*     fetchDocumentReport: build.query<IDocumentController[],number>({
            query: (id) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                url: `/incomeConfirmation/report/get/${id}`,           //query  нужен для того чтобы получать get
            }),
        }),
        createDocument: build.mutation<void, {docId:string,userId:number,}>({
            query: ({docId,userId}) => ({
                url: `/incomeConfirmation/set/document/to/freelancer/${docId}/${userId}`,
                method: "POST",
                body: docId
            }),
        }),

        updateDocument: build.mutation<IEditDocumentController, { data:IEditDocumentController,id:number }>({
            query: ({data,id}) => ({
                url: `/incomeConfirmation/update/${id}`,
                method:"PUT",
                body: data
            }),
        }),*/



    })
})

export const {useFetchDetailsDocumentQuery,useUpdateStatusMutation} = detailsAPI;
