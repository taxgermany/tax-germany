import {createApi} from "@reduxjs/toolkit/query/react";
import {IMiniTable} from "../interfaces/TableInterface";
import {apiSlice, baseQueryWithReAuth, baseQuery} from "../api/api";
import {IHealthInsuranceData, IHealthInsuranceRegularData} from "interfaces/health";


export const healthAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchMiniTable: build.query<IMiniTable, { userId: number, yearId: number }>({
            query: ({userId, yearId}) => ({
                url: `/health/table/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['healthAPI']
        }),

        updateMiniTable: build.mutation<IMiniTable, { data: IMiniTable, yearId: number }>({
            query: (data) => ({
                url: `/health/table/enter/${data.yearId}`,
                method: "PUT",
                body: data.data
            }),
            invalidatesTags: ['healthAPI']
        }),

        createHealth: build.mutation<IHealthInsuranceData | IHealthInsuranceRegularData, { clientId: number | string; doYouDayHealth: boolean | any, }>({
            query: ({doYouDayHealth, clientId}) => ({
                url: `/health/doYouPayHealth/${clientId}?doYouDayHealth=${doYouDayHealth}`,
                method: "POST",
            }),
            invalidatesTags: ['healthAPI'],
        }),
        featchHealth: build.query<IHealthInsuranceData, { clientId: number | string }>({
            query: ({clientId}) => ({
                url: `/health/get/${clientId}`,
            }),
            providesTags: ['healthAPI'],
        }),
        updateHealth: build.mutation<IHealthInsuranceData, { clientId: number | string, data: IHealthInsuranceData | null, }>({
            query: ({data, clientId,}) => ({
                url: `/health/create/${clientId}`,
                method: "PUT",
                body: data,
            }),
            invalidatesTags: ['healthAPI'],
        }),
        updateHealthRegular: build.mutation<IHealthInsuranceData, { clientId: number | string, queryString: string | any }>({
            query: ({queryString, clientId,}) => ({
                url: `/health/setReq/${clientId}?${queryString ? queryString : ''}`,
                method: "PUT",
            }),
            invalidatesTags: ['healthAPI'],
        }),

    })
})

export const {
    useFetchMiniTableQuery,
    useUpdateMiniTableMutation,
    useLazyFetchMiniTableQuery,
    useCreateHealthMutation,
    useFeatchHealthQuery,
    useUpdateHealthMutation,
    useUpdateHealthRegularMutation,
} = healthAPI;
