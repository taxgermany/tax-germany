import {createApi} from "@reduxjs/toolkit/query/react";
import { IMiniTable} from "../interfaces/TableInterface";
import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";



export const reviewPensionMiniTableAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({

        fetchReviewPensionMiniTable: build.query<IMiniTable, {userId:number | string,yearId:number}>({
            query: ({userId,yearId}) => ({
                url: `/rev/pension/get/table/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['reviewPensionMiniTableAPI']
        }),

        updateReviewPensionMiniTable: build.mutation<IMiniTable, { data: IMiniTable,yearId:number }>({
            query: (data) => ({
                url: `/rev/pension/update/table/${data.yearId}`,
                method: "PUT",
                body: data.data
            }),
            invalidatesTags: ['reviewPensionMiniTableAPI']
        }),
    })
})

export const {useFetchReviewPensionMiniTableQuery,useLazyFetchReviewPensionMiniTableQuery,useUpdateReviewPensionMiniTableMutation} = reviewPensionMiniTableAPI;
