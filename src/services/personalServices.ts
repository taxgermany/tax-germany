import { createApi } from "@reduxjs/toolkit/dist/query/react";
import { apiSlice, baseQuery } from "../api/api";
import { IBusinessDetails, IYourDetails } from "../interfaces/personalDataInterface";
import {IYourDetailsModified} from "../redux/personalData/personalDataSlice";
import {IReviewBussines} from "../interfaces/reaview";

export const personalAPI = apiSlice.injectEndpoints({
  endpoints: (build) => ({

    createYouDetails: build.mutation<void, { id: number, values: IYourDetailsModified }>({
      query: ({ values, id }) => ({
        url: `/yourDetails/save/${id}`,
        method: "POST",
        body: values
      }),
      invalidatesTags: ['personalAPI']
    }),

    updateYourDetails: build.mutation<void, { id: number, year: string, month: string }>({
      query: ({ id, year, month }) => ({
        url: `/yourDetails/setBecomeFreelancer/${id}?year=${year}&month=${month}`,
        method: "PUT"
      }),
      invalidatesTags: ['personalAPI']
    }),

    setGapForDocument: build.mutation<void, {
      id: number;
      fromYear: string;
      fromMonth: string;
      toYear: string;
      toMonth: string;
    }>({
      query: ({ id, fromYear, fromMonth, toYear, toMonth }) => ({
        url: `/yourDetails/setGapForDocument/${id}?fromYear=${fromYear}&fromMonth=${fromMonth}&toYear=${toYear}&toMonth=${toMonth}`,
        method: "PUT"
      }),
      invalidatesTags: ['personalAPI']
    }),

    createBusinessDetails: build.mutation<void, { id: number, values: IBusinessDetails }>({
      query: ({ values, id }) => ({
        url: `/businessDetails/create/${id}`,
        method: "POST",
        body: values
      }),
      invalidatesTags: ['personalAPI']
    }),

    setTypeOfFreelancer: build.mutation<void, { id: number | undefined, freelanceType: string }>({
      query: ({ id, freelanceType }) => ({
        url: `/businessDetails/setTypeOfFreelancer/${id}?freelanceType=${freelanceType}`,
        method: "PUT"
      }),
      invalidatesTags: ['personalAPI']
    }),

    uploadFile: build.mutation<void, { id: number, formData: FormData }>({
      query: ({ id, formData }) => ({
        url: `/file/business/${id}`,
        method: "POST",
        body: formData,
      }),
      invalidatesTags: ['personalAPI']
    }),

    canIUploadFile: build.mutation<void, { id: number | '', UploadFile: boolean, noWhyUploadFile: string }>({
      query: ({ id, UploadFile, noWhyUploadFile }) => ({
        url: `/businessDetails/${id}/canIUploadFile?canIUploadFile=${UploadFile}${!UploadFile ? `&canTUploadFile=${noWhyUploadFile}` : ''}`,
        method: 'PUT'
      }),
      invalidatesTags: ['personalAPI']
    }),

    setWorkPlace: build.mutation<IReviewBussines, { id: number ; workPlace: string; anotherWorkPlace: string | null; }>({
      query: ({ id, workPlace, anotherWorkPlace }) =>( {
        url:`/businessDetails/setWorkPlace/${id}?workPlace=${workPlace}${anotherWorkPlace ?`&anotherWorkPLace=${anotherWorkPlace}`:""}`,
        method: 'PUT',
      }),
      invalidatesTags: ['personalAPI']
    }),
  })
})

export const {
  useCreateYouDetailsMutation,
  useUpdateYourDetailsMutation,
  useSetGapForDocumentMutation,
  useCreateBusinessDetailsMutation,
  useSetTypeOfFreelancerMutation,
  useSetWorkPlaceMutation,
  useUploadFileMutation,
  useCanIUploadFileMutation,
   } = personalAPI;
