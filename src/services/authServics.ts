import { apiSlice } from "../api/api";
import {profitsType} from "../interfaces/ProfitsInterface";
import {IClient} from "../interfaces";

export const authApiSlice = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        reset: build.query<{ "message": string }, {email:string}>({
            query: (params) => ({
                url: `/auth/reset`,
                params
            })
        }),
        resetPassword: build.mutation<{ "message": string }, {resetToken:string,password :string,}>({
            query: ({resetToken,password}) => ({
                url: `/auth/reset/${resetToken}?password=${password}`,
                method: "POST",
                body: {resetToken,password}
            }),
            invalidatesTags: ['AuthAPI']
        }),
        resend: build.mutation<void, {email:string,id:number}>({
            query: ({id,email}) => ({
                url: `/auth/resend/${id}`,
                method: "PUT",
                params: {email}
            }),
            invalidatesTags: ['AuthAPI']
        }),
        register: build.mutation<{ "id": number, "email": string }, {
            "email": string,
            "password": string,
            "phoneNumber": string
        }>({
            query: (data) => ({
                url: `/auth/register`,
                method: "POST",
                body:data
            }),
            invalidatesTags: ['AuthAPI']
        }),
        confirmEmail: build.mutation<IClient, {userId:string,code :string}>({
            query: ({userId,code}) => ({
                url: `/auth/confirm/${userId}?code=${code}`,
                method: "POST",
                body:code
            }),
            invalidatesTags: ['AuthAPI']
        }),
        login: build.mutation<IClient, {email:string,password :string}>({
            query: (data) => ({
                url: `/auth/auth`,
                method: "POST",
                body:data
            }),
            invalidatesTags: ['AuthAPI']
        }),

    })
})

export const {useRegisterMutation,useConfirmEmailMutation,useLoginMutation,useResetPasswordMutation,useResendMutation,useLazyResetQuery} = authApiSlice
