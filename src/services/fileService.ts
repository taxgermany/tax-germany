
import { apiSlice, baseQueryWithReAuth } from "../api/api";
import { IFileName } from "../interfaces/ProfitsInterface";



export const fileApi = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchFile: build.query<File, string>({
            query: (nameFile) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/file/${nameFile}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['fileApi']
        }),


        fetchLogo: build.query<IFileName, { userId: string | number }>({
            query: ({ userId }) => ({
                url: `/file/getLogo/${userId}`,
            }),
            providesTags: (result, error, arg) => ['fileApi']
        }),


        fetchFileNameProfits: build.query<IFileName, { userId: number | string}>({
            query: ({ userId }) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/file/name/profits/${userId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['fileApi']
        }),
        fetchFileNameBusiness: build.query<IFileName, { userId: number | string }>({
            query: ({ userId }) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/file/name/business/${userId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['fileApi']
        }),
        createFileNameBusiness: build.mutation<IFileName, { userId: number | string, data: File | FormData }>({
            query: ({ userId, data }) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/file/business/${userId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
                method: "POST",
                body: data
            }),
            invalidatesTags: ['fileApi']
        }),
        createFile: build.mutation<File, { userId: number | string, data: File }>({
            query: ({ userId, data }) => ({
                url: `/file/${userId}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['fileApi']
        }),

        createUploadFile: build.mutation<File, { userId: number | string, data: FormData }>({
            query: ({ userId, data }) => ({
                url: `/file/upload/document/${userId}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['fileApi']
        }),

        uploadFileWithIdDoc: build.mutation<IFileName, { userId: number | string, idDoc: number, data: FormData }>({
            query: ({ userId, idDoc, data }) => ({
                url: `/file/upload/document/${userId}/${idDoc}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['fileApi']
        }),


        uploadLogo: build.mutation<void, { userId: number | string, data: FormData }>({
            query: ({ userId, data }) => ({
                url: `/file/uploadLogo/${userId}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['fileApi']
        }),


        createFileProfits: build.mutation<File, { userId: number, data: FormData }>({
            query: ({ userId, data }) => ({
                url: `/file/profits/${userId}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['fileApi']
        }),
        deleteFile: build.mutation<void, { fileId: number }>({
            query: ({ fileId }) => ({
                url: `/file/delete/document/${fileId}`,
                method: "DELETE",
            }),
            invalidatesTags: ['fileApi']
        }),
    })
})

export const {
    useCreateFileMutation, useFetchFileQuery, useCreateFileProfitsMutation,
    useLazyFetchFileNameProfitsQuery, useLazyFetchFileNameBusinessQuery, useCreateUploadFileMutation,
    useUploadLogoMutation, useFetchLogoQuery, useLazyFetchLogoQuery, useDeleteFileMutation,
    useUploadFileWithIdDocMutation, useCreateFileNameBusinessMutation
} = fileApi;
