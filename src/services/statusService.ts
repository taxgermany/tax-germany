import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { apiSlice } from "api/api";
import { IDetailsData , Notification, IFile } from "interfaces/statusInterface";

export const statusAPI = apiSlice.injectEndpoints({
  endpoints: (builder) => ({

    getNotificationsStatus: builder.query<Notification[], number>({
      query: (id) => `/status/notifications/${id}`
    }),

    getDetails: builder.query<IDetailsData, number>({
      query: (id) => `/status/details/${id}`
    }),

    getFiles: builder.query<IFile[], number>({
      query: (id) => `/status/files/${id}`
    }),

    confirmStatus: builder.mutation<void, number>({
      query: (id) => ({
        url: `/status/confirm/${id}`,
        method: 'POST',
      }),
      invalidatesTags: ['StatusAPI']
    }),


    sendMessage: builder.mutation<void, string>({
      query: (messageContent) => ({
        url: `/status/send/1`,
        method: 'POST',
        body: messageContent,
        headers: {
          "Content-Type": "application/json",
          accept: "*/*"
        }
      }), invalidatesTags: ['StatusAPI']
    }),

    downloadFile:  builder.query<Blob, string>({
      query: (fileName) => `/file/${fileName}`,
      transformResponse: (response: Response) => response.blob(),
    }),

  }),
});

export const {
  useGetNotificationsStatusQuery,
  useGetDetailsQuery,
  useGetFilesQuery,
  useConfirmStatusMutation,
  useSendMessageMutation ,
  useLazyDownloadFileQuery,
  useLazyGetFilesQuery
} = statusAPI;
