import {createApi} from "@reduxjs/toolkit/query/react";

import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";
import {getAllClients, IClients, IExport, INotifications, IPaginationClient} from "../interfaces/admin";


export const exportsAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({

        fetchAllExports: build.query<IExport[], void>({
            query: () => ({
                url: `/expert/experts/all`,
            }),
            providesTags: () => ['exportsAPI']
        }),


        fetchPaginationClients: build.query<IPaginationClient, { page: number, size: number }>({
            query: ({page, size}) => ({
                url: `/expert/pagination?page=${page}&size=${size}`,
            }),
            providesTags: () => ['exportsAPI']
        }),


        fetchNotifications: build.query<INotifications[], { expertId: number }>({
            query: ({expertId}) => ({
                url: `/expert/notifications/${expertId}`,
            }),
            providesTags: () => ['exportsAPI']
        }),


        fetchSearchExports: build.query<getAllClients, { expertId?: number, page: number, size: number, search?: string }>({
            query: ({page, size, search, expertId}) => ({
                url: `/expert/clients/search/${expertId}`,
                params: {request: search, page: page, size: size}
            }),
            providesTags: () => ['exportsAPI']
        }),


        fetchAllClients: build.query<getAllClients, { expertId: number, page: number, size: number }>({
            query: ({expertId, page, size}) => ({
                url: `/expert/clients/get/all/${expertId}?page=${page}&size=${size}`,
            }),
            providesTags: () => ['exportsAPI']
        }),

        fetchFilter: build.query<getAllClients, {id:number, docId?: string, status?: string, expertId?: string, page: number, size: number }>({
            query: ({id,...params}) => ({
                url: `/expert/clients/filter/${id}`,
                params: params
            }),
            providesTags: () => ['exportsAPI']
        }),

        updateClient: build.mutation<getAllClients, { status: string, clientId: number, expertId: number, selectedExpertId: string, page: number, size: number }>({
            query: ({status, clientId, expertId, selectedExpertId, page, size}) => ({
                url: `/expert/client/update/${expertId}/${clientId}?status=${status}&selectedExpertId=${selectedExpertId}&page=${page}&size=${size}`,
                method: "PUT",
                body: status
            }),
            invalidatesTags: ['exportsAPI']
        }),

    })
})

export const {
    useFetchAllClientsQuery, useFetchFilterQuery, useFetchPaginationClientsQuery, useLazyFetchFilterQuery,
    useLazyFetchPaginationClientsQuery, useLazyFetchAllExportsQuery, useLazyFetchAllClientsQuery,
    useLazyFetchSearchExportsQuery, useLazyFetchNotificationsQuery,
    useFetchAllExportsQuery, useFetchNotificationsQuery, useFetchSearchExportsQuery, useUpdateClientMutation,

} = exportsAPI;
