import { apiSlice } from "api/api";
import { IReviewBussines, IReviewHealth, IReviewLife, IReviewPension, IReviewProfits, IReviewUser, IReviewYour } from "interfaces/reaview";

export const reviewServicesAPI = apiSlice.injectEndpoints({
    endpoints:(build)=>({
        fetchReviewHealth: build.query<IReviewHealth, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/health/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewPension: build.query<IReviewPension, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/pension/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewLife: build.query<IReviewLife, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/life/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewYour: build.query<IReviewYour, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/your/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewBussines: build.query<IReviewBussines, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/business/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewProfits: build.query<IReviewProfits, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/profits/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewUser: build.query<IReviewUser, {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/user/get/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        fetchReviewDocuments: build.query<[string], {clientId: number | string}>({
            query:({clientId}) => ({
                url : `/rev/docs/${clientId}`,
            }),
            providesTags: ['reviewServicesAPI']
        }),
        updateReviewYour: build.mutation<IReviewYour, {clientId: number | string, data: IReviewYour}>({
            query:({clientId, data}) => ({
                url : `/rev/your/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        updateReviewLife: build.mutation<IReviewLife, {clientId: number | string, data: IReviewLife}>({
            query:({clientId, data}) => ({
                url : `/rev/life/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        updateReviewBussines: build.mutation<IReviewBussines, {clientId: number | string, data: IReviewBussines}>({
            query:({clientId, data}) => ({
                url : `/rev/business/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        updateReviewPension: build.mutation<IReviewPension, {clientId: number | string, data: IReviewPension}>({
            query:({clientId, data}) => ({
                url : `/rev/pension/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        updateReviewHealth: build.mutation<IReviewHealth, {clientId: number | string, data: IReviewHealth}>({
            query:({clientId, data}) => ({
                url : `/rev/health/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        updateReviewProfits: build.mutation<IReviewProfits, {clientId: number | string, data: IReviewProfits}>({
            query:({clientId, data}) => ({
                url : `/rev/profits/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        updateReviewUser: build.mutation<IReviewUser, {clientId: number | string, data: IReviewUser}>({
            query:({clientId, data}) => ({
                url : `/rev/user/update/${clientId}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
        confirmStatusReview: build.mutation<void, number>({
            query: (id) => ({
                url: `/rev/confirm/${id}`,
                method: 'POST',
            }),
            invalidatesTags: ['reviewServicesAPI']
        }),
    })
})
export const {
    useFetchReviewHealthQuery,
    useFetchReviewBussinesQuery,
    useFetchReviewDocumentsQuery,
    useFetchReviewLifeQuery,
    useFetchReviewPensionQuery,
    useFetchReviewProfitsQuery,
    useFetchReviewUserQuery,
    useFetchReviewYourQuery,
    useLazyFetchReviewBussinesQuery,
    useLazyFetchReviewDocumentsQuery,
    useLazyFetchReviewHealthQuery,
    useLazyFetchReviewLifeQuery,
    useLazyFetchReviewPensionQuery,
    useLazyFetchReviewProfitsQuery,
    useLazyFetchReviewUserQuery,
    useLazyFetchReviewYourQuery,
    useUpdateReviewBussinesMutation,
    useUpdateReviewHealthMutation,
    useUpdateReviewLifeMutation,
    useUpdateReviewPensionMutation,
    useUpdateReviewProfitsMutation,
    useUpdateReviewUserMutation,
    useUpdateReviewYourMutation,
    useConfirmStatusReviewMutation
} = reviewServicesAPI
