
import {ProfitAndExpenses} from "../interfaces/ProfitsInterface";
import {IEmployer, IMicroTable, ITable, ITableVot} from "../interfaces/TableInterface";
import {apiSlice} from "../api/api";


export const profitsAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchTable: build.query<ITableVot | ITable, { userId: number, yearId: number, vat: boolean | null }>({
            query: ({userId, yearId, vat}) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/profits/table/${vat ? "vat/" : ""}${userId}/${yearId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['ProfitsAPI']
        }),

        fetchTableEmployer: build.query<IMicroTable[], number>({
            query: (yearId) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/profits/table/empl/${yearId}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['ProfitsAPI']
        }),

        createProfitAndExpenses: build.mutation<ProfitAndExpenses, { data: ProfitAndExpenses, clientId: number }>({
            query: ({data, clientId}) => ({
                url: `/profits/create/${clientId}`,
                method: "POST",
                body: data,
                headers: {
                    'Content-Type': 'application/json'
                }
            }),
            invalidatesTags: ['ProfitsAPI']
        }),
        uploadFileProfitAndExpenses: build.mutation<void, {clientId:number, formData: FormData }>({
            query: ({clientId,formData}) => ({
                url: `/file/profits/${clientId}`,
                method: "POST",
                body: formData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }),
            invalidatesTags: ['ProfitsAPI']
        }),
        updateProfits: build.mutation<ProfitAndExpenses, { data: ProfitAndExpenses, clientId: number }>({
            query: ({data, clientId}) => ({
                url: `/profits/update/${clientId}`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['ProfitsAPI']
        }),
        updateTable: build.mutation<ITable | ITableVot, { data: ITable, yearId: number, vat: boolean | null }>({
            query: ({data, yearId, vat}) => ({
                url: `/profits/table/${vat ? "vat/" : ""}enter/${yearId}`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['ProfitsAPI']
        }),

        updateTableEmployer: build.mutation<IEmployer[], IEmployer[]>({
            query: (data: IEmployer[]) => ({
                url: `/profits/table/empl/enter`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['ProfitsAPI']
        }),
    })
})

export const {
    useUpdateTableMutation, useCreateProfitAndExpensesMutation,
    useUpdateTableEmployerMutation,
    useFetchTableEmployerQuery, useFetchTableQuery, useLazyFetchTableQuery,useUpdateProfitsMutation,
    useLazyFetchTableEmployerQuery,
    useUploadFileProfitAndExpensesMutation
} = profitsAPI;
