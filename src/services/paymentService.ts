import {createApi} from "@reduxjs/toolkit/query/react";
import {IPayment} from "../interfaces/PaymentInterface";
import {apiSlice, baseQueryWithReAuth} from "../api/api";


export const paymentAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchKeyPayment: build.query<IPayment,void>({
            query: () => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю//
                url: `/payment/publishable/key`,           //query  нужен для того чтобы получать get

            }),
            providesTags: (result, error, arg) => ['paymentAPI']
        }),
        fetchPrice: build.query<string, number>({
            query: (id) => ({
                url: `/payment/paymentPrice/${id}`,
            }),
            providesTags: (result, error, arg) => ['paymentAPI']
        }),
        fetchIsPaid: build.query<{isPaid:boolean}, { clientId:number |string }>({
            query: ({clientId}) => ({
                url: `/payment/isPaid/${clientId}`,
            }),
            providesTags: (result, error, arg) => ['paymentAPI']
        }),
        createProfit: build.mutation<IPayment, { data: string, userId: number }>({
            query: ({data, userId}) => ({
                url: `payment/create/intent/${userId}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['paymentAPI']
        }),
        createPay: build.mutation<void, {userId: number }>({
            query: ({userId}) => ({
                url: `payment/pay/${userId}`,
                method: "POST",
            }),
            invalidatesTags: ['paymentAPI']
        }),
    })
})

export const {useCreateProfitMutation,useFetchKeyPaymentQuery,useLazyFetchKeyPaymentQuery,useLazyFetchIsPaidQuery,
    useFetchPriceQuery,useCreatePayMutation} = paymentAPI;
