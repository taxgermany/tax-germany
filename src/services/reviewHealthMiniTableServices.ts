import {createApi} from "@reduxjs/toolkit/query/react";
import { IMiniTable} from "../interfaces/TableInterface";
import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";
import { IReviewHealth } from "interfaces/reaview";



export const reviewHealthMiniTableAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({

        fetchReviewHealthMiniTable: build.query<IMiniTable, {userId:number | string,yearId:number}>({
            query: ({userId,yearId}) => ({
                url: `/rev/health/get/table/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['reviewHealthMiniTableAPI']
        }),

        updateReviewHealthMiniTable: build.mutation<IMiniTable, { data: IMiniTable,yearId:number }>({
            query: (data) => ({
                url: `/rev/health/update/table/${data.yearId}`,
                method: "PUT",
                body: data.data
            }),
            invalidatesTags: ['reviewHealthMiniTableAPI']
        }),

    })
})

export const {useFetchReviewHealthMiniTableQuery,useLazyFetchReviewHealthMiniTableQuery,useUpdateReviewHealthMiniTableMutation} = reviewHealthMiniTableAPI;
