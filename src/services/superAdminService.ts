import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";
import {
    ApiData,
    getAllClients,
    IAddExport,
    IClients,
    IExportAdmin,
    INotifications,
    IPaginationClient
} from "../interfaces/admin";

export const expertAdminApi = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        getExperts: build.query<IExportAdmin[], void>({
            query: () => ({
                url: 'admin/getAllExpert'
            }),
            providesTags:(result,error,arg) =>['admin']
        }),
        getPaginationClients: build.query<getAllClients,{page:number,size:number}>({
            query: ({page,size}) => ({
                url: `/admin/paginationAllClients?page=${page}&size=${size}`,
            }),
            providesTags: () => ['admin']
        }),
        getSearchExports: build.query<IExportAdmin[],string>({
            query: (request) => ({
                url: `/admin/searchExpert?search=${request}`,
            }),
            providesTags: () => ['admin']
        }),
        getIncomeConfirmation: build.query<ApiData,number | string>({
            query: (clientId) => ({
                url: `/admin/incomeConfirmation/${clientId}`,
            }),
            providesTags: () => ['admin']
        }),
        getSearchAdmin: build.query<getAllClients, {page:number,size:number,search?:string }>({
            query: (params) => ({
                url: `/admin/searchForSuperAdmin`,
                params:params
            }),
            providesTags: () => ['admin']
        }),

        getFilter: build.query<getAllClients,{docId?:string,status?:string,expertId?:string,page:number,size:number}>({
            query: (params) => ({
                url: `/admin/filterForSuperAdmin`,
                params:params
            }),
            providesTags: () => ['admin']
        }),


        getNotifications: build.query<INotifications[], void>({
            query: () => ({
                url: `/admin/notifications`,
            }),
            providesTags: () => ['admin']
        }),


        addExpert: build.mutation<IExportAdmin, IAddExport>({
            query: (data) => ({
                url: `admin/save`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['admin']
        }),
        createFileLogo: build.mutation<File, { clientId:number,data:File }>({
            query: ({clientId,data}) => ({
                url: `/admin/uploadLogo/${clientId}`,
                method: "POST",
                body: data
            }),
            invalidatesTags: ['admin']
        }),
        updatePerson: build.mutation<IExportAdmin, {id:number, firstName:string, lastName:string}>({
            query: ({ id, firstName, lastName }) => ({
                url: `admin/update/${id}`,
                method: "PUT",
                body: { firstName, lastName },
            }),
            invalidatesTags: ['admin']
        }),

        updateCreatePassword: build.mutation<IExportAdmin, {email:string, password:number}>({
            query: (params) => ({
                url: `/admin/createPassword`,
                params:params,
                method: "PUT",
                body: params,
            }),
            invalidatesTags: ['admin']
        }),

        updateClientAdmin: build.mutation<getAllClients, { status: string, clientId: number,selectedExpertId:string,page:number,size:number}>({
            query: ({status, clientId,selectedExpertId,page,size}) => ({
                url: `/admin/updateFreelancerForSuperAdmin/${clientId}?page=${page}&size=${size}&status=${status}&selectedExpertId=${selectedExpertId}`,
                method: "PUT",
            }),
            invalidatesTags: ['admin']
        }),

        deleteExpert: build.mutation<{message: string }, number>({
            query: (expertId) => ({
                url: `admin/${expertId}`,
                method: "DELETE",
            }),
            invalidatesTags: ['admin']
        }),
        deleteLogo: build.mutation<{message: string }, number>({
            query: (logoId) => ({
                url: `/admin/deleteLogo/${logoId}`,
                method: "DELETE",
            }),
            invalidatesTags: ['admin']
        }),
        deleteClient: build.mutation<{message: string }, number>({
            query: (clientId) => ({
                url: `/admin/delete/client/${clientId}`,
                method: "DELETE",
            }),
            invalidatesTags: ['admin']
        }),

        createPassword: build.mutation<void, { email: string; password: string }>({
            query: ({ email, password }) => ({
              url: `/admin/createPassword`,
              method: 'PUT',
              params: { email, password }
            }),
          }),
    })
})

export const {useAddExpertMutation, useGetExpertsQuery,useLazyGetExpertsQuery, useDeleteExpertMutation,
    useUpdatePersonMutation,useLazyGetPaginationClientsQuery,useLazyGetSearchAdminQuery,useLazyGetFilterQuery,
useUpdateClientAdminMutation,useLazyGetSearchExportsQuery,useGetNotificationsQuery, useCreatePasswordMutation,
useGetIncomeConfirmationQuery,useLazyGetNotificationsQuery} = expertAdminApi;
