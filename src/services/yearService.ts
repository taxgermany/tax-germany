import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {profitsType} from "../interfaces/ProfitsInterface";
import {IEmployer, ITable, ITableVot} from "../interfaces/TableInterface";
import {IYear} from "../interfaces/YearInterface";
import {apiSlice, baseQuery, baseQueryWithReAuth} from "../api/api";


export const yearAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchAllYear: build.query<IYear[], number | string>({
            query: (id) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/years/get/all/${id}`,           //query  нужен для того чтобы получать get

            }),
            providesTags: (result, error, arg) => ['yearAPI']
        }),
        fetchYearId: build.query<IYear[], number | string>({
            query: (id) => ({  //некоторая фун. которая возвр. обьект сама функция будет принимать аргументы необхю
                // для запроса: это может быть тело запросо,какие-то параметры,url           //
                url: `/years/get/${id}`,           //query  нужен для того чтобы получать get
                //  mutation нужен для того чтобы изменять сервер post | put
                /* params: {
                     _limit: limit
                 }*/
            }),
            providesTags: (result, error, arg) => ['yearAPI']
        })


    })
})

export const {useFetchAllYearQuery, useFetchYearIdQuery} = yearAPI;
