import {apiSlice, } from "../api/api";
import { ILifeData } from "interfaces/pension";

export const lifeAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchLife: build.query<ILifeData, {clientId: number | string, }>({
            query: ({ clientId}) => ({
                url: `/lifeContribution/get/${clientId}`,
            }),
            providesTags: ['lifeAPI'],
        }),
        createLife: build.mutation<ILifeData, {clientId: number | string, data: ILifeData, }>({
            query: ({data, clientId}) => ({
                url: `/lifeContribution/create/${clientId}`,
                method: "POST",
                body: data,
            }),
            invalidatesTags: ['lifeAPI'],
        }),
        updateLife: build.mutation<ILifeData, {clientId: number, body: ILifeData, }>({
            query: ({body, clientId}) => ({
                url: `/lifeContribution/update/${clientId}`,
                method: "PUT",
                body: body,
            }),
            invalidatesTags: ['lifeAPI'],
        }),
    }),
});

export const { useCreateLifeMutation,useFetchLifeQuery,useUpdateLifeMutation,useLazyFetchLifeQuery } = lifeAPI;
