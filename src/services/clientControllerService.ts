import { apiSlice } from "api/api";
import { IEmployer, IMicroTable, IMiniTable, ITable, ITableVot } from "interfaces/TableInterface";
import { IDetailsDocuments } from "interfaces/details";
import { IReviewBussines, IReviewHealth, IReviewLife, IReviewPension, IReviewProfits, IReviewUser, IReviewYour } from "interfaces/reaview";

export const clientControllerAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchClientsHealth: build.query<IReviewHealth, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/health/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsPension: build.query<IReviewPension, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/pension/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsLife: build.query<IReviewLife, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/life/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsYour: build.query<IReviewYour, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/your/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsBussines: build.query<IReviewBussines, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/business/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsProfits: build.query<IReviewProfits, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/profits/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsUser: build.query<IReviewUser, { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/user/get/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        fetchClientsDocuments: build.query<IDetailsDocuments[], { Id: number | string }>({
            query: ({ Id }) => ({
                url: `/details/review/documents/${Id}`,
            }),
            providesTags: ['clientControllerAPI']
        }),
        updateClientsYour: build.mutation<IReviewYour, { Id: number | string, data: IReviewYour }>({
            query: ({ Id, data }) => ({
                url: `/details/your/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        updateClientsLife: build.mutation<IReviewLife, { Id: number | string, data: IReviewLife }>({
            query: ({ Id, data }) => ({
                url: `/details/life/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        updateClientsBussines: build.mutation<IReviewBussines, { Id: number | string, data: IReviewBussines }>({
            query: ({ Id, data }) => ({
                url: `/details/business/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        updateClientsPension: build.mutation<IReviewPension, { Id: number | string, data: IReviewPension }>({
            query: ({ Id, data }) => ({
                url: `/details/pension/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        updateClientsHealth: build.mutation<IReviewHealth, { Id: number | string, data: IReviewHealth }>({
            query: ({ Id, data }) => ({
                url: `/details/health/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        updateClientsProfits: build.mutation<IReviewProfits, { Id: number | string, data: IReviewProfits }>({
            query: ({ Id, data }) => ({
                url: `/details/profits/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        updateClientsUser: build.mutation<IReviewUser, { Id: number | string, data: IReviewUser }>({
            query: ({ Id, data }) => ({
                url: `/details/user/update/${Id}`,
                method: 'PUT',
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        // mini table pension
        fetchClientsPensionMiniTable: build.query<IMiniTable, { userId: number | string, yearId: number }>({
            query: ({ userId, yearId }) => ({
                url: `/details/pension/get/table/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['clientControllerAPI']
        }),
        updateClientsPensionMiniTable: build.mutation<IMiniTable, { data: IMiniTable, yearId: number }>({
            query: (data) => ({
                url: `/details/pension/update/table/${data.yearId}`,
                method: "PUT",
                body: data.data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        // mini table health
        fetchClientsHealthMiniTable: build.query<IMiniTable, { userId: number | string, yearId: number }>({
            query: ({ userId, yearId }) => ({
                url: `/details/health/get/table/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['clientControllerAPI']
        }),

        updateClientsHealthMiniTable: build.mutation<IMiniTable, { data: IMiniTable, yearId: number }>({
            query: (data) => ({
                url: `/details/health/update/table/${data.yearId}`,
                method: "PUT",
                body: data.data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        // Large table
        fetchClientsTable: build.query<ITableVot | ITable, { userId: number | string, yearId: number, vat: boolean }>({
            query: ({ userId, yearId, vat }) => ({
                url: `/details/profits/get/table${vat ? '/vat' : ''}/${userId}/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['clientControllerAPI']
        }),
        updateClientsTable: build.mutation<ITable | ITableVot, { data: ITable, yearId: number, vat: boolean }>({
            query: ({ data, yearId, vat }) => ({
                url: `/details/profits/update/table${vat ? '/vat' : ''}/${yearId}`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
        // employ table
        fetchClientsTableEmployer: build.query<IMicroTable[], number | string>({
            query: (yearId) => ({
                url: `/details/profits/get/empl/table/${yearId}`,
            }),
            providesTags: (result, error, arg) => ['clientControllerAPI']
        }),
        updateClientsTableEmployer: build.mutation<IEmployer[], IEmployer[]>({
            query: (data: IEmployer[]) => ({
                url: `/details/profits/update/empl/table`,
                method: "PUT",
                body: data
            }),
            invalidatesTags: ['clientControllerAPI']
        }),
    })
})
export const {
    useFetchClientsHealthQuery,
    useFetchClientsPensionQuery,
    useFetchClientsLifeQuery,
    useFetchClientsYourQuery,
    useFetchClientsBussinesQuery,
    useFetchClientsProfitsQuery,
    useFetchClientsUserQuery,
    useFetchClientsDocumentsQuery,
    useUpdateClientsYourMutation,
    useUpdateClientsLifeMutation,
    useUpdateClientsBussinesMutation,
    useUpdateClientsPensionMutation,
    useUpdateClientsHealthMutation,
    useUpdateClientsProfitsMutation,
    useUpdateClientsUserMutation,
    useLazyFetchClientsBussinesQuery,
    useLazyFetchClientsHealthQuery,
    useLazyFetchClientsUserQuery,
    useLazyFetchClientsYourQuery,
    // table
    useFetchClientsHealthMiniTableQuery,
    useFetchClientsPensionMiniTableQuery,
    useFetchClientsTableEmployerQuery,
    useFetchClientsTableQuery,
    useUpdateClientsHealthMiniTableMutation,
    useUpdateClientsPensionMiniTableMutation,
    useUpdateClientsTableEmployerMutation,
    useUpdateClientsTableMutation,
    useLazyFetchClientsHealthMiniTableQuery,
    useLazyFetchClientsPensionMiniTableQuery,
    useLazyFetchClientsTableEmployerQuery,
    useLazyFetchClientsTableQuery,
    useLazyFetchClientsProfitsQuery,
} = clientControllerAPI
