import { IDropDown } from "interfaces/personalDataInterface";
import {apiSlice} from "../api/api";



export const arayAPI = apiSlice.injectEndpoints({
    endpoints: (build) => ({
        fetchNationality: build.query<IDropDown, void>({
            query: () => ({
                url: `/yourDetails/nationality/get/all`,
            }),
            providesTags: ['arayAPI'],
        }),
        fetchCountry: build.query<IDropDown, void>({
            query: () => ({
                url: `/yourDetails/country/get/all`,
            }),
            providesTags: ['arayAPI'],
        }),
    })
})

export const {
    useFetchNationalityQuery,
    useFetchCountryQuery,
    useLazyFetchCountryQuery,
    useLazyFetchNationalityQuery
} = arayAPI;
