import { createSlice } from '@reduxjs/toolkit';

const initialState = {
 data: null
};

const businessDetailsSlice = createSlice({
  name: 'businessDetails',
  initialState,
  reducers: {
    updateData: (state, action) => {
     state.data = action.payload;
    },
  },
});

export const { updateData } = businessDetailsSlice.actions;

export default businessDetailsSlice.reducer;