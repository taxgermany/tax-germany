/** @format */
import {combineReducers, configureStore} from '@reduxjs/toolkit';

import businessDetailsSlice from "./businessDetailsSlice";
import pensionSlice from "./pensionSlice";
import auditSlice from "./Audit/Audit";
import statusSlice from "./auth/applicationStatus/statusSlice";
import lifeSlice from "./lifeSlice";
import priceCardSlice from "../redux/priceCard/priceCard";
import sidebarSlice from "./sidebar/sidebarSlice";
import personalDataSlice from "./personalData/personalDataSlice";
import authSlice from "./auth/authSlice";
import healthSlice from "./health/healthSlice";
import profitAndExpensesSlice from "./profitandexpension/profitAndExpensesSlice";
import {apiSlice} from "../api/api";


const rootReducer = combineReducers({
    auditSlice,
    sidebarSlice,
    statusSlice,
    authSlice,
    healthSlice,
    pensionSlice,
    lifeSlice,
    profitAndExpensesSlice,
    personalDataSlice,
    businessDetailsSlice,
    priceCardSlice,
    [apiSlice.reducerPath]: apiSlice.reducer,
})

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(
        apiSlice.middleware,
    ),
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
