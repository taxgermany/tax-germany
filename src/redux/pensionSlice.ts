// authSlice.js

import {createSlice, PayloadAction} from "@reduxjs/toolkit";



interface IState {
    id: number | null
}


const initialState: IState = {
    id: null 
};


const pensionSlice = createSlice({
    name: 'pension',
    initialState: initialState,
    reducers: {
        setPensionID: (state, action: PayloadAction<number>) => {
            state.id = action.payload;
        }
    },
});

export const {setPensionID} = pensionSlice.actions;

export default pensionSlice.reducer;