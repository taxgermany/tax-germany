import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import { profitsType} from "../../interfaces/ProfitsInterface";



interface IState {
    data: profitsType
}


const initialState: IState = {
    data: {
        accountingType: null,
        regularly: null,
        haveEmployees: null,
        employerNumber: null,
        vat: null,
    }
};

const profitAndExpensesSlice = createSlice({
    name: 'profitAndExpenses',
    initialState,
    reducers: {
        addAccounting: (state, action: PayloadAction<string>) => {
            state.data = {...state.data,accountingType: action.payload}
        },
        addRegularly: (state, action: PayloadAction<boolean>) => {
            state.data = {...state.data,regularly: action.payload}
        },

        addHaveEmployees: (state, action: PayloadAction<boolean>) => {
            state.data = {...state.data,haveEmployees: action.payload}
        },
        addEmployerNumber: (state, action: PayloadAction<string>) => {
            state.data = {...state.data,employerNumber: action.payload}
        },
        addVat: (state, action: PayloadAction<boolean>) => {
            state.data = {...state.data,vat: action.payload}
        },
    },
});

export const {addAccounting,addRegularly,addHaveEmployees,addEmployerNumber,addVat} = profitAndExpensesSlice.actions;

export default profitAndExpensesSlice.reducer;
