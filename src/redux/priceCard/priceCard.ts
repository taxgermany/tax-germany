// authSlice.js

import {createSlice, PayloadAction} from "@reduxjs/toolkit";



interface IState {
    data:{ [p: number]: boolean }
}


const initialState: IState = {
    data: {1: false, 2: false, 3: false, 4: false}
};


const priceCardSlice = createSlice({
    name: 'priceCard',
    initialState: initialState,
    reducers: {
        setActiveCard: (state, action: PayloadAction<{ [p: number]: boolean }>) => {
            state.data = action.payload;
        }
    },
});

export const {setActiveCard} = priceCardSlice.actions;

export default priceCardSlice.reducer;