import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface ILink {
    personal:boolean,
    profits:boolean,
    health:boolean,
    pension:boolean,
    review:boolean,
    status:boolean
}

interface IState {
    link: ILink
    active:string
}

type linkString = "personal" | "profits" | "health" | "pension" | "review" | "status"

const initialState: IState = {
    link: {
        personal:true,
        profits:false,
        health:false,
        pension:false,
        review:false,
        status:false

    },
    active:""


};

const sidebarSlice = createSlice({
    name: 'sideBar',
    initialState,
    reducers: {
        activeLink:(state, action: PayloadAction<string>) => {
            state.active = action.payload
        },
        changeLink: (state, action: PayloadAction<linkString>) => {
            if(action.payload === "profits") state.link.profits = true
            if(action.payload === "health") state.link.health = true
            if(action.payload === "pension") state.link.pension = true
            if(action.payload === "review") state.link.review = true
            if(action.payload === "status") state.link.status = true

        },
    },
});

export const {changeLink,activeLink} = sidebarSlice.actions;

export default sidebarSlice.reducer;
