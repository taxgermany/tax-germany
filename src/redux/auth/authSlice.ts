// authSlice.js
import {getStoreLocal} from "../../utils/local-storage/localStorage";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IClient} from "../../interfaces";


interface IState {
    client: IClient | null
    token: string | null
    role: string | null
    refreshToken: string | null,
    registerData: {
        email: string | null
        phone: string | null

    }
}


const initialState: IState = {
    client: getStoreLocal("user"),
    token: getStoreLocal("user")?.token,
    role: getStoreLocal("user")?.userRole,
    refreshToken: null,
    registerData: {
        email: null,
        phone: null
    }
};


const authSlice = createSlice({
    name: 'auth',
    initialState: initialState,
    reducers: {
        setUser: (state, action: PayloadAction<IClient>) => {
            const {token, userRole, id, email} = action.payload;
            state.client = action.payload;
            state.token = token
            /* state.userRole = userRole
             state.email = email
             state.id = id*/
        },
        tokenReceived: (state, action: PayloadAction<string>) => {
            state.refreshToken = action.payload;
        },
        loggedOut: (state) => {
            state.client = null
            state.token = null
            localStorage.removeItem("user")
        },
        setRegister: (state, action: PayloadAction<{ email: string, phone: string }>) => {
            state.registerData = action.payload
        }
    },
});

export const {setUser, loggedOut, tokenReceived, setRegister} = authSlice.actions;

export default authSlice.reducer;
