import { createSlice } from '@reduxjs/toolkit';

const initialState = {
 data: null
};

const statusSlice = createSlice({
  name: 'status',
  initialState,
  reducers: {
    updateData: (state, action) => {
     state.data = action.payload;
    },
  },
});

export const { updateData } = statusSlice.actions;

export default statusSlice.reducer;