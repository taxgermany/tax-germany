
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IAuditAddDocument} from "../../interfaces/auditDocument";


interface IState {
    data: IAuditAddDocument
}
const currentDate = new Date();
const currYear = currentDate.getFullYear();

const initialState: IState = {
    data: {
        nameExport: "",
        nameFirma: "",
        addressExpert: "",
        telFaxEmailExpert: "",
        randomSelection:false,
        submittedDocuments:true,
        randomSampling:false,
        pruferInterruption:false,
        pruferSupervised: false,
        violation: "",
        notInsolvent: true,
        notOverindebted: true,
        pruferFirst: true,
        Kein: false,
        currentYear: currYear

    }
};


const auditSlice = createSlice({
    name: 'audit',
    initialState: initialState,
    reducers: {
        updateData: (state, action: PayloadAction<IAuditAddDocument>) => {
            state.data = action.payload;
        },
    },
});

export const {updateData} = auditSlice.actions;

export default auditSlice.reducer;
