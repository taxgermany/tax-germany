// authSlice.js
import {getStoreLocal} from "../../utils/local-storage/localStorage";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IClient} from "../../interfaces";



interface IState {
    id: number| string | null
}


const initialState: IState = {
    id: getStoreLocal("healthId")
};


const healthSlice = createSlice({
    name: 'health',
    initialState: initialState,
    reducers: {
        setHealthID: (state, action: PayloadAction<number>) => {
            state.id = action.payload;
        }
    },
});

export const {setHealthID} = healthSlice.actions;

export default healthSlice.reducer;