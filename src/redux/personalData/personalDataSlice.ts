import {createSlice, PayloadAction} from '@reduxjs/toolkit';


export interface IYourDetailsModified {
  "gender": string,
  "firstname": string,
  "lastname": string,
  "dateOfBirth": string,
  "country": string,
  "nationality": string,
  "typeOfFreelance": string
}

interface IIncomeProofPeriod {
  fromMonth?: string;
  fromYear?: string;
  toMonth?: string;
  toYear?: string;
}

interface IState {
  data: null | IYourDetailsModified;
  monthStartedFreelancing?: string;
  yearStartedFreelancing?: string;
  incomeProofPeriod?: IIncomeProofPeriod;
}

const initialState: IState = {
  data: null,
  monthStartedFreelancing: undefined,
  yearStartedFreelancing: undefined,
  incomeProofPeriod: undefined
};

const personalDataSlice = createSlice({
  name: 'personalDataSlice',
  initialState,
  reducers: {
    updateData: (state, action: PayloadAction<IYourDetailsModified>) => {
     state.data = action.payload;
    },
    updateFreelancingStart: (state, action: PayloadAction<{month: string, year: string}>) => {
      state.monthStartedFreelancing = action.payload.month;
      state.yearStartedFreelancing = action.payload.year;
    },
   setIncomeProofPeriod: (state, action: PayloadAction<IIncomeProofPeriod>) => {
  state.incomeProofPeriod = action.payload;
}

  },
});

export const { updateData, updateFreelancingStart,  setIncomeProofPeriod  } = personalDataSlice.actions;

export default personalDataSlice.reducer;
