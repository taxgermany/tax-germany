// authSlice.js

import {createSlice, PayloadAction} from "@reduxjs/toolkit";



interface IState {
    id: number | null
}


const initialState: IState = {
    id: null 
};


const lifeSlice = createSlice({
    name: 'pension',
    initialState: initialState,
    reducers: {
        setLifeID: (state, action: PayloadAction<number>) => {
            state.id = action.payload;
        }
    },
});

export const {setLifeID} = lifeSlice.actions;

export default lifeSlice.reducer;