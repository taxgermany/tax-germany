import BigTitle from "components/ui/button/BigTitle";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import { useEffect } from "react";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import InputPassword from "components/ui/input/InputPassword";
import ConfirmPassword from "components/ui/input/ConfirmPassword";

import { Form, message } from "antd";
import { useCreatePasswordMutation } from "services/superAdminService";

function CreatePassword() {
  const navigate = useNavigate();
  const [searchParams,setSearchParams] = useSearchParams()
  const email = searchParams.get("email") || ""
  let pathname =useLocation().pathname
  console.log(email);

  const [createPassword, { isLoading, isError, isSuccess }] = useCreatePasswordMutation();

  useEffect(() => {
    if (isSuccess) {
      navigate("/freelancer/personal-data");
    }
    if (isError) {
      message.error('Ошибка при создании пароля.');
    }
  }, [isSuccess, isError, navigate]);

  
  const handleSubmit = async (data: any) => {
    try {
      await createPassword({ email, password: data.password });
      if (isSuccess) {
        navigate("/freelancer/personal-data");
      }
    } catch (error) {
      console.error("Ошибка при создании пароля:", error);
    }
  };


 
  return (
    <div className="h-[90vh] font-inter flex justify-center items-center">
      <div>
        <BigTitle text="Create Password " className="pb-[30px]" />
        <Form
          name="personal"
          layout="horizontal"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={handleSubmit}
          autoComplete="off"
        >
          <div className="grid gap-[30px]">
            <div className="grid gap-[15px]">
            <InputPassword inputName="password" background={true} className="!w-[663px]" />
            <ConfirmPassword inputName="password" background={true} text="Repeat password" className="!w-[663px]" />
            </div>
            <NewButtonGreen text="Create" loading={isLoading}/>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default CreatePassword;
