import BigTitle from "components/ui/button/BigTitle";
import {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import "../resetpassword/ResetPassword.css";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import InputPassword from "components/ui/input/InputPassword";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import {Form} from "antd";
import ValidationText from "components/ui/validation/ValidationText";
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {setUser} from "../../redux/auth/authSlice";
import {useUpdateClientAdminMutation} from "../../services/superAdminService";
import {useLoginMutation} from "../../services/authServics";
import {IClient} from "../../interfaces";
import {getStoreLocal} from "../../utils/local-storage/localStorage";
import {useAppSelector} from "../../hooks/useAppSelector";


const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};

const Login = () => {

    const {client} = useAppSelector(state => state.authSlice)

    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const [errorActive, setErrorActive] = useState("");

    const [login, {
        data: loginData = {} as IClient,
        error: errorLogin,
        isLoading: isLoadingLogin,
        isSuccess: isSuccessLogin
    }] = useLoginMutation()

    const onFinish = async (values: any) => {
        let path: { currentPath: string } | null = null
        if (client)
            path = getStoreLocal(`${client.id}`)
        setErrorActive("");
        try {
            const response: IClient = await login({email: values.email, password: values.password}).unwrap()
            dispatch(setUser(response))
            path = getStoreLocal(`${response.id}`)
            localStorage.setItem("user", JSON.stringify(response));
            if (response.userRole === "ROLE_SUPER_ADMIN") {
                navigate("/super-admin/clients")
                return
            }
            if (response.userRole === "ROLE_EXPERT") {
                navigate("/export/clients")
                return
            }
            if (path) {
                navigate(path.currentPath)
            } else {
                navigate("/freelancer/personal-data")
            }


        } catch (error: any) {
            if (typeof error === 'object' && error !== null && 'data' in error)
                setErrorActive(`${error.data}`);
            if (error?.data === "You didn't confirm your email! Please confirm your email!") {
                navigate("/freelancer/confirm-email")
            }

        }
    }


    return (
        <div className="ResetPassword h-[90vh] font-inter flex justify-center items-center relative">
            {errorActive ? <ValidationText text={errorActive}/> : ""}
            <div className="content inline">
                <Form
                    name="basic"
                    layout="horizontal"
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                    style={{maxWidth: 650}}
                    initialValues={{remember: true}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"

                    className={"grid gap-y-[10px]"}
                >
                    <BigTitle text="Log in" className="pb-[30px]"/>
                    <div className="grid inputs  gap-[30px]">
                        <div className="grid gap-[15px]">
                            <InputNoAppearance background={true} inputName="email" placeholder="Enter email"
                                               border={true} wh="!w-[500px]"/>
                            <InputPassword registStyle={false} inputName="password" background={true}
                                           className="!w-[500px]"/>
                        </div>
                        <Link to="/freelancer/forgot-password">
                            <div className="text-[12px] underline leading-[26px] font-[400] text-[#0090FD]">Forgot
                                password?
                            </div>
                        </Link>
                        <Form.Item>
                            <NewButtonGreen text="Log in" loading={isLoadingLogin}/>
                        </Form.Item>
                    </div>
                </Form>
            </div>
        </div>
    )
}

export default Login;
