import React, {RefObject, useState} from 'react';
import clsx from "clsx";
import add from "../../../assets/images/Rectangle 2658.png";
import {CloseOutlined, DownOutlined, LeftOutlined, RightOutlined, UpOutlined} from "@ant-design/icons";

interface IUploadLogo {
    size:number
    setSize:  (prev: number)  => void
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
    fetchDelete: () => void

}

const UploadLogo: React.FC<IUploadLogo> = ({logo, handleFileChange, fetchDelete,setSize,size}) => {

    const [active, setActive] = useState(false)
    const [edit, setEdit] = useState(false)
    /*const [size, setSize] = useState(70)*/

    const handleEdit = () => {
        setEdit(true)
        setActive(false)
    }
    const focusMouse = (value: boolean) => {
        setActive(value)

    }


    return (
        <div className="w-full flex justify-end my-[35px] h-[70px] relative">
            <div className="absolute top-0 right-0 ">
                <div className="flex  justify-between items-center gap-y-[10px]">
                    <div className="flex jusify-between  items-center gap-x-[22px]">

                        {logo ?
                            <div>
                                {edit &&
                                    <div>
                                        <div>
                                            <img src={logo} alt="" style={{height: size}}
                                                 className={`/*h-[70px]*/ ${active ? `opacity-20 hoverAnimation` : ""} `}/>
                                            <div className={"grid gap-[5px] absolute top-[-15px] right-[-90px] w-[70px]"}>
                                                <div onClick={() => setEdit(false)}
                                                     className={"flex justify-self-end hoverAnimation items-center hover:bg-[#F8F8F8] px-[4px]"}>
                                                    <CloseOutlined/></div>
                                                <div className={"grid justify-items-center gap-[2px]"}>
                                                    {/* <div className={"pr-[10px]"}>width</div>*/}
                                                    <button  onClick={()=>setSize(size+2)} disabled={size===90}
                                                        className={"hoverAnimation w-fit flex justify-center items-center hover:bg-[#F8F8F8] px-[4px]"}>
                                                        <UpOutlined className={"hoverAnimation hover:bg-[#F8F8F8] "}/>
                                                    </button>

                                                    <input value={size} type="text" max={10} min={0}
                                                           className={"w-[40px] px-[10px]"}/>
                                                    <button onClick={()=>setSize( size - 2)} disabled={size===40}
                                                            className={"hoverAnimation w-fit flex items-center justify-center hover:bg-[#F8F8F8] px-[4px]"}>
                                                        <DownOutlined className={"hoverAnimation hover:bg-[#F8F8F8]"}/>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                {!edit &&
                                    <div onMouseEnter={() => focusMouse(true)} >
                                        <div >
                                            <img src={logo} alt="" style={{height: size}}
                                                 className={`w-fit ${active ? `opacity-20 hoverAnimation` : ""} `}/>

                                        </div>

                                        <div onMouseLeave={() => focusMouse(false)}
                                            className={`absolute top-0 right-0 z-10  w-[150px] h-full
                                         ${active ? `flex justify-center items-center gap-x-[10px] ` : "hidden"}`}>
                                            <label
                                                htmlFor={"logo"}
                                                className={clsx(`
                                "text-green cursor-pointer  rounded  font-[500] block hoverAnimation`)}
                                            >
                                                <input
                                                    type="file"
                                                    id={"logo"}
                                                    onChange={handleFileChange}
                                                    className={`file-input !hidden`}
                                                />
                                                <div
                                                    className={"px-3 py-1 rounded-[10px] bg-[#F8F8F8] opacity-90 hover:opacity-100 text-[14px]"}>
                                                   Download
                                                </div>
                                            </label>

                                            <button
                                                className={"px-3 py-1 bg-[#F8F8F8] rounded-[10px] font-[500] text-[14px] opacity-90 hoverAnimation hover:opacity-100"}
                                                onClick={handleEdit}>Edit
                                            </button>

                                            <button
                                                className={"px-3 py-1 bg-[#F8F8F8] rounded-[10px] font-[500] text-[14px] opacity-90 hoverAnimation hover:opacity-100"}
                                                onClick={fetchDelete}>Delete
                                            </button>

                                        </div>
                                    </div>}
                            </div>
                            : <label
                                htmlFor={"logo"}
                                className={clsx(`
                                "text-green cursor-pointer  rounded /*hover:text-white*/ font-[500] block hoverAnimation`)}
                            >
                                <input
                                    type="file"
                                    id={"logo"}
                                    onChange={handleFileChange}
                                    className={`file-input !hidden`}
                                />
                                <img src={add} alt="" className="h-[70px]"/>
                            </label>
                        }


                    </div>
                </div>
                {/*<div className="absolute top-2 right-[-50px] justify-between items-center gap-y-[10px]">
                    <div>Add</div>
                    <div>Delete</div>
                </div>*/}
            </div>
        </div>
    );
};

export default UploadLogo;
