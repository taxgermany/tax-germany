import React, {FC, useEffect, useState} from 'react';
import {IAuditAddDocument, IAuditDocument} from "../../../interfaces/auditDocument";

import {ReactComponent as Delete} from "../../../assets/icons/close.svg"
import AuditDocument1 from "./AuditDocument1";
import AuditDocument2 from "./AuditDocument2";
import AuditDocument3 from "./AuditDocument3";
import AuditDocument4 from "./AuditDocument4";
import AuditDocument5 from "./AuditDocument5";
import AuditDocument6 from "./AuditDocument6";
import AuditDocument7 from "./AuditDocument7";
import AuditDocument8 from "./AuditDocument8";
import NewButtonGreen from "../../ui/button/NewButtonGreen";
import ReactToPrint from "react-to-print";
import {useLazyFetchDocumentReportQuery} from "../../../services/documentService";
import {Spin} from "antd";
import {
    useDeleteFileMutation,
    useFetchLogoQuery,
    useLazyFetchLogoQuery,
    useUploadLogoMutation
} from "../../../services/fileService";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import {useAppSelector} from "../../../hooks/useAppSelector";


interface IAudit {
    id: string
}


const Audit: FC<IAudit> = ({id}) => {
    const componentRef = React.useRef(null);
    const onBeforeGetContentResolve = React.useRef<(() => void) | null>(null);


    const [documents, setDocuments] = useState<IAuditDocument | null>(null)
    const {data: dataExp} = useAppSelector(state => state.auditSlice)

    const [logo, setLogo] = useState<string | null>(null)
    const [file, setFile] = useState<IFileName | null>(null)
    const [size, setSize] = useState(70)


    const [fetchReport, {isLoading,}] = useLazyFetchDocumentReportQuery()

    const [getLogo, {data: logoData, isLoading: isLoadingLogo,}] = useLazyFetchLogoQuery()

    const [uploadLogo, {isLoading: isLoadingLogoUpdate}] = useUploadLogoMutation()

    const [deleteFile, {isLoading: isLoadingDelete}] = useDeleteFileMutation()

    //logo
    const fetchDelete = async () => {
        if (file) {
            await deleteFile({fileId: file.id})
            setFile(null)
            setLogo(null)
        }

    }
    const setSizeLogo = (value: number) => {
        setSize(value)

    }

    const fetchingData = async () => {
        try {
            if (id) {
                const res = await fetchReport(+id).unwrap()
                setDocuments(res)
                const logoRes = await getLogo({userId: id}).unwrap()

                if (logoRes) {
                    if (logoRes.id) setFile(logoRes)
                    logoRes.file
                        ? setLogo(`data:${logoRes.type};base64,${logoRes.file}`)
                        : setLogo(null)
                }
            }
        } catch (error: any) {
            console.log(error.message);

        }
    }

    const handleFileChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const fileImg = event.target.files?.[0];
        if (fileImg) {
            const formData = new FormData();
            formData.append("file", fileImg)
            await uploadLogo({data: formData, userId: id}).unwrap()
            await fetchingData()
        }
    };


    useEffect(() => {
        fetchingData()
    }, [])

    const [currentPage, setCurrentPage] = useState(0);

    const itemsPerPage = 1; // Сколько элементов на одной странице

    const startIndex = currentPage * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    let auditDocument = [
        <AuditDocument1 key={1}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        componentRef={componentRef}
                        data={documents?.firstPage}
                        setData={setDocuments}
                        dataExp={dataExp}

        />,
        <AuditDocument2 key={2}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        componentRef={componentRef}
                        data={documents?.secondPage}
                        setData={setDocuments}
        />,
        <AuditDocument3 key={3}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        componentRef={componentRef}
                        data={documents?.thirdPage}
                        setData={setDocuments}/>,
        <AuditDocument4 key={4}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        componentRef={componentRef}
                        data={documents?.fourthPage}
                        setData={setDocuments}/>,
        <AuditDocument5 key={5}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        componentRef={componentRef}
                        data={documents?.fifthPage}
                        setData={setDocuments}/>,
        <AuditDocument6 key={6}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        componentRef={componentRef}
                        data={documents?.sixthPage}
                        setData={setDocuments}/>,
        <AuditDocument7 key={7} componentRef={componentRef}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}
                        data={documents?.sixthPage}/>,
        <AuditDocument8 key={8} componentRef={componentRef}
                        logo={logo}
                        file={file}
                        fetchDelete={fetchDelete}
                        handleFileChange={handleFileChange}
                        setSize={setSizeLogo}
                        size={size}/>,
    ]


    const contentToDisplay = auditDocument.slice(startIndex, endIndex);


    //react to print

    const [active, setActive] = useState(false);

    const reactToPrintTrigger = React.useCallback(() => {
        return <NewButtonGreen text={"Download pdf document"}/>
    }, []);


    const reactToPrintContent = React.useCallback(() => {
        return componentRef.current;
    }, [componentRef.current]);

    const handleOnBeforeGetContent = React.useCallback(() => {

        setActive(true)

        return new Promise<void>((resolve) => {
            onBeforeGetContentResolve.current = resolve;

            setTimeout(() => {
                resolve();
            }, 2000);
        });

    }, []);


    React.useEffect(() => {
        if (typeof onBeforeGetContentResolve.current === "function") {
            onBeforeGetContentResolve.current();
        }
    }, [onBeforeGetContentResolve.current]);

    const handleAfterPrint = () => {
        setActive(false)
    }


    // Генерация номеров страниц

    const renderPageNumbers = () => {
        const totalPages = auditDocument.length;
        let pagesToShow = 3;

        let startPage = Math.max(currentPage - 1, 0); // Начнем с текущей страницы - 1
        let endPage = Math.min(startPage + pagesToShow, totalPages);

        if (endPage === totalPages) {
            startPage = Math.max(endPage - pagesToShow, 0);
        }

        const pageNumbers = Array.from({length: endPage - startPage},
            (_, i) => startPage + i + 1);

        const setChoose = (current: number) => {
            setCurrentPage(current)
        }

        return (
            <ul className="flex gap-[5px]">
                <li
                    className={`${currentPage === 0 ? 'text-[#CCCCCC]' : 'hover:text-green'} text-[15px] flex items-center
                    cursor-pointer justify-center `}
                    onClick={() => currentPage === 0 ? "" : setChoose(currentPage - 1)}
                >
                    <button className="page-link">Prev</button>
                </li>
                {currentPage > 1 &&
                    <li
                        className={`${1 === currentPage + 1 ? 'bg-green' : ''}
                     w-[32px] h-[32px] text-[15px] rounded-[3px] border border-[#F1F1F1]
                     flex items-center justify-center cursor-pointer hover:bg-green`}
                        onClick={() => setChoose(0)}
                    >
                        <button>{1}</button>
                    </li>}
                {currentPage > 2 && <span>...</span>}
                {pageNumbers.map((pageNumber) => (
                    <li
                        key={pageNumber}
                        className={`${pageNumber === currentPage + 1 ? 'bg-green' : ''}
                     w-[32px] h-[32px] text-[15px] rounded-[3px] border border-[#F1F1F1]
                     flex items-center justify-center cursor-pointer hover:bg-green`}
                        onClick={() => setChoose(pageNumber - 1)}
                    >
                        <button>{pageNumber}</button>
                    </li>
                ))}
                {currentPage < (totalPages - 3) && <span>...</span>}
                {currentPage < (totalPages - 2) &&
                    <li
                        key={totalPages}
                        className={`${totalPages === currentPage + 1 ? 'bg-green' : ''}
                     w-[32px] h-[32px] text-[15px] rounded-[3px] border border-[#F1F1F1]
                     flex items-center justify-center cursor-pointer hover:bg-green`}
                        onClick={() => setChoose(totalPages - 1)}
                    >
                        <button>{totalPages}</button>
                    </li>}

                <li
                    className={`${currentPage + 1 === totalPages ? 'text-[#CCCCCC]' : 'hover:text-green'} text-[15px] flex items-center
                         cursor-pointer justify-center `}
                    onClick={() => currentPage + 1 === totalPages ? "" : setChoose(currentPage + 1)}
                >
                    <button className="page-link">Next</button>
                </li>

            </ul>
        );
    };


    return (
        <div>{(isLoading || isLoadingLogoUpdate || isLoadingLogo)
            ? <Spin size="large" className="absolute top-2/4 left-2/4"/>
            : documents && <div>
            <div className={"flex justify-end pb-[20px]"}>
                <ReactToPrint
                    content={reactToPrintContent}
                    onAfterPrint={handleAfterPrint}
                    documentTitle={"Audit_Report"}
                    trigger={reactToPrintTrigger}
                    onBeforeGetContent={handleOnBeforeGetContent}

                />
            </div>
            <div>
                {contentToDisplay}
            </div>

            <div className={"pt-[20px] flex justify-end"}>{renderPageNumbers()}</div>

            {active && <div className={"mt-100px"} ref={componentRef}>

                {auditDocument.map(item => item)}

            </div>}

        </div>}
        </div>
    );
};

export default Audit;
