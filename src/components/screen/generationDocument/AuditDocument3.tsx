import DocumentsSquare from "../ReviewDocumentsUi/DocumentsSquare"
import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import React, {FC, RefObject} from 'react'
import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg";
import add from "../../../assets/images/Rectangle 2658.png";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {updateData} from "../../../redux/Audit/Audit";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";

interface IAuditDocument3 {
    data?: {
        "totalProfitsTableDtoList": [
            {
                "year": number,
                "grossRevenueInclVat": number,
                "grossRevenueWithoutVat": number
            }
        ],
        "accountingRecordTypeDto": {
            "balanceSheet": boolean,
            "cashBased": boolean,
            "regularly": boolean,
            "irregularly": boolean
        },
        "employersTableForAuditDtoList": [
            {
                "year": number,
                "commercialEmployers": number,
                "trainees": number,
                "total": number,
                "fullTimeEmployees": number,
                "partTimeWorkers": number,
                "familyMembers": number
            }
        ],
        "employerNumber": number
    }
    setData?: (ph: any) => void
    componentRef: RefObject<HTMLDivElement>
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
}

const AuditDocument3: FC<IAuditDocument3> = ({
                                                 data,
                                                 setData,
                                                 componentRef, logo, handleFileChange,
    setSize,file,size,fetchDelete
                                             }) => {

    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }


    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            {data && <div className=" mt-[40px] mb-[20px] ml-[130px] mr-[110px] ">
                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>

                <div className="mt-[25px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[40px] flex items-center justify-center   border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="4.4."/>
                        </div>
                        <div
                            className="w-[789px] min-h-[40px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText text="Kein Mitgesellschafter hält höhere Anteile."/>
                        </div>
                        <div
                            className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare
                                active={dataExp.Kein}
                                setActive={() => setDataExp({...dataExp,
                                    Kein:!dataExp.Kein
                                })}/>

                        </div>
                    </div>
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[40px] flex items-center justify-center  border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="4.5."/>
                        </div>
                        <div
                            className="w-[789px] min-h-[40px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText
                                text="Der Antragsteller ist freiberuflich tatig. 4.1, 4.2, 4.3 und 4.4 sind nicht zutreffend. Erläuterungen, falls erforderlich:"/>
                        </div>
                        <div
                            className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare
                                active={true}/>
                        </div>
                    </div>
                </div>
                <div className="mt-[40px]">
                    <DocumentsTitleMini text="III. Angaben zu Umsatz und Ertrag"/>
                    <div className="mt-[20px] flex gap-x-[20px]">
                        <div className="w-[53px] min-h-[40px] text-center">
                            <DocumentsText text="1."/>
                        </div>
                        <DocumentsText
                            className="w-[815px]"
                            text="Im Prüfungszeitraum der letzten drei Geschäftsjahre belief sich der Jahresumsatz / das Jahresergebnis auf Euro: (bitte Jahres- / Monatsangaben vervollständigen)"/>
                    </div>
                </div>
                {/* table */}
                <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                    <div className="flex bg-[#EFEFEF]">
                        <div
                            className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="Jahr"/>
                        </div>
                        {
                            data && data.totalProfitsTableDtoList.map(item => <div
                                className="w-[161.2px] min-h-[40px] flex items-center justify-center   border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text={item.year===dataExp.currentYear?"Lfd.Geschäfts-jahr M-M":item.year}/>
                            </div>)
                        }
                    </div>
                    <div className="flex ">
                        <div
                            className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="Jahresumsatz"/>
                        </div>
                        {data && data.totalProfitsTableDtoList.map(item =>
                            <div className="w-[161.2px] min-h-[40px]
                         flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                {item.grossRevenueInclVat}
                            </div>)
                        }

                    </div>
                    <div className="flex ">
                        <div
                            className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="Ergebnis v. Unternehmenssteuern"/>
                        </div>
                        {
                            data && data.totalProfitsTableDtoList.map(item =>
                                <div className="w-[161.2px] min-h-[40px]
                         flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.grossRevenueWithoutVat}
                                </div>)
                        }

                    </div>
                    <div className="flex ">
                        <div
                            className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="Ergebnis n. Unternehmenssteuern"/>
                        </div>
                        {
                            data && data.totalProfitsTableDtoList.map(item =>
                                <div className="w-[161.2px] min-h-[40px]
                         flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {0}
                                </div>)
                        }
                    </div>
                </div>
                {/* IV 1-6 */}
                <div className="mt-[40px]">
                    <DocumentsTitleMini text="IV. Hinweis zur Art der Buchführung / Aufzeichnungen"/>
                    <div className="mt-[25px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div className="w-[58px] min-h-[40px] text-center  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="1."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Es wird eine kaufmännische Buchhaltung geführt"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.accountingRecordTypeDto.balanceSheet}
                                />
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] min-h-[40px] text-center  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="2."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Es wird eine Einnahme-Überschuss-Rechnung erstellt"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.accountingRecordTypeDto.cashBased}
                                />
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] min-h-[40px] text-center  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Die Buchführung / Aufzeichnungen erfolgt / erfolgen zeitnah"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.accountingRecordTypeDto.regularly}
                                />
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] min-h-[40px] text-center  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="4."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Die Buchführung / Aufzeichnungen erfolgt / erfolgen in periodischen Abständen (Quartal)"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] min-h-[40px] text-center  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="5."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Die Buchführung / Aufzeichnungen erfolgt / erfolgen unregelmäßig"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.accountingRecordTypeDto.regularly}
                                />
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] min-h-[40px] text-center  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="6."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Es wurde bisher keine ordentliche Buchhaltung / Aufzeichnungen geführt"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}/>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Table */}
                <div className="mt-[40px]">
                    <DocumentsTitleMini
                        className="grid items-center justify-start !gap-y-[22px] "
                        text="V. Angaben zur Beschäftigung von Mitarbeiterinnen und Mitarbeitern"/>
                    <DocumentsText
                        className="w-[651px] mt-[25px]"
                        text="Im Prüfungszeitraum wurden durchschnittlich Mitarbeiter/innen beschäftigt: Jahres- / Monatsangaben (bitte Jahres- / Monatsangaben vervollständigen) *"/>

                    <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex bg-[#EFEFEF]">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Jahr"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center   border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text={item.year===dataExp.currentYear?"Lfd.Geschäfts-jahr M-M":item.year}/>
                                </div>)
                            }

                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Kaufmännische MA"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.commercialEmployers}
                                </div>)}

                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Gewerbliche MA"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {" "}
                                </div>)}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Auszubildende"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.trainees}
                                </div>)}
                            {/*<div
                                className="w-[120px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                {data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].trainees}

                            </div>*/}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText
                                    className="!font-[600]"
                                    text="Gesamt"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.trainees + item.commercialEmployers + item.commercialEmployers}
                                </div>)}
                          {/*  <div
                                className="w-[120px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                {data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].trainees +
                                    data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].commercialEmployers +
                                    data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].commercialEmployers}

                            </div>*/}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Davon Vollzeit"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.commercialEmployers - item.trainees}
                                </div>)}
                          {/*  <div
                                className="w-[120px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                { data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].commercialEmployers -
                                 data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].trainees }

                            </div>*/}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Davon Teilzeit"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.partTimeWorkers}
                                </div>)}
                            {/*<div
                                className="w-[120px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                { data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].partTimeWorkers}

                            </div>*/}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Davon Familienangehörige"/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {item.familyMembers}
                                </div>)}
                           {/* <div
                                className="w-[120px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                { data?.employersTableForAuditDtoList[data?.employersTableForAuditDtoList.length - 1].familyMembers}

                            </div>*/}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[250px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Betriebsnummer "/>
                            </div>
                            {
                                data && data.employersTableForAuditDtoList.map(item => <div
                                    className="w-[160px] min-h-[40px] px-[5px] flex items-center justify-center  border border-[#7E7E7E] border-b-0 border-l-0">
                                    {data.employerNumber}
                                </div>)}
                           {/* <div
                                className="w-[120px] min-h-[40px] px-[5px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                {" "}

                            </div>*/}
                        </div>
                    </div>
                    <DocumentsText className="mt-[6px]"
                                   text="* Zu den Beschäftigten zählen nur Arbeitnehmer, ohne Vorstand, Geschäftsführer, Inhaber und Prokuristen!"/>
                </div>
            </div>}
        </div>
    )
}
export default AuditDocument3
