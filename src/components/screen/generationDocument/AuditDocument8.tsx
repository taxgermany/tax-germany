import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg"

import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import React, {FC, RefObject} from "react";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import add from "../../../assets/images/Rectangle 2658.png";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";

interface IAuditDocument8 {
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
    componentRef: RefObject<HTMLDivElement>
}


const AuditDocument8: FC<IAuditDocument8> = ({componentRef, logo, handleFileChange,
                                             file,fetchDelete,size,setSize}) => {
    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            <div className=" mt-[40px] mb-[40px] ml-[130px] mr-[110px] ">
                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>
                <div className="flex justify-between items-end mt-[25px]">
                    <div
                        className="w-[534px] min-h-[50px] border border-[#7E7E7E] border-l-0 border-b-0 border-r-0 pt-[15px]">
                        <DocumentsText
                            className="w-[430px]"
                            text="Name und Unterschrift des Wirtschaftsprüfers / Steuerberaters / Steuerbevollmächtigten"/>
                    </div>
                    <div className="text-center">
                        <Print/>
                        <DocumentsText text="Rundstempel"/>
                    </div>
                </div>
                <div className="w-[515px] mt-[60px]">
                    <DocumentsTitleMini text="ACHTUNG!"/>
                    <DocumentsText
                        className="mt-[10px]"
                        text="Bitte nicht vergessen einzureichen: "/>
                    <div className="ml-[5px]">
                        <DocumentsText text="· einen aktuellen Handelsregisterauszug"/>
                        <DocumentsText text="· Kopie/Kopien der Gewerbeanmeldung/Gewerbeummeldungen"/>
                        <DocumentsText text="· Original der Unbedenklichkeitsbescheinigung des Finanzamt"/>
                        <DocumentsText text="· Versicherungspolicen des Antragstellers/der Antragstellerin"/>
                        <DocumentsText text="· AGB des Prüfers/der Prüferin"/>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default AuditDocument8
