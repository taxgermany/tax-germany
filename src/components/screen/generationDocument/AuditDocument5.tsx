import DocumentsInput from "../ReviewDocumentsUi/DocumentsInput"
import DocumentsSquare from "../ReviewDocumentsUi/DocumentsSquare"
import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import React, {FC, RefObject} from 'react'

import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg";
import add from "../../../assets/images/Rectangle 2658.png";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {updateData} from "../../../redux/Audit/Audit";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";

interface IAuditDocument5 {
    data?: {
        "regularlyMonthly": boolean,
        "regularlyNotMonthly": boolean,
        "irregularly": boolean,
        "healthInsuranceTableDtoList": [
            {
                "year": number,
                "monthHealthForTableDtoList": [
                    {
                        "month": string,
                        "healthInsurance": number
                    }
                ],
                "total": 0
            }
        ],
        "healthInsuranceDetailsDto": {
            "doYouPayForHealthInsurance": boolean,
            "companyName": string,
            "companyAddress": string,
            "membershipNumber": number,
            "cityPostalCodeAddress": string,
            "contactPerson": string,
            "healthInsurancePhoneNumber": string,
            "regularly": string,
            "reasonWhyIrregularly": string,
        }
    },
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
    setData?: (ph: any) => void
    componentRef: RefObject<HTMLDivElement>;
}

const AuditDocument5: FC<IAuditDocument5> = ({
                                                 data,
                                                 setData,
                                                 componentRef, logo, handleFileChange,
    setSize,size,file,fetchDelete
                                             }) => {

    const months = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]
    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }
    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            {data && <div className=" mt-[40px] mb-[40px] ml-[130px] mr-[110px] ">
                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>

                {/* 7 */}
                <div>
                    <p className="text-[14px] font-roboto leading-[18px] font-[400]"><span className="font-[600]">VII. Angaben zu Aufwendungen für einen ausreichenden Krankenversicherungsschutz bzw. Pflegeversicherungsschutz </span>(dem
                        Umfang des Leistungskatalogs der gesetzlichen Kranken- und Pflegeversicherungen entsprechend):
                    </p>
                    <DocumentsText
                        className="w-[613px]"
                        text="Im Prüfungszeitraum bzw. seit Aufnahme der Tätigkeit hat der Antragsteller Aufwendungen für einen Krankenverscherungsschutz (KV) geleistet"/>
                    <div className="mt-[25px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[40px] text-center flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="1."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="KV - Prämien wurden für den gesamten Prüfungszeitraum in monatlichen Beträgen entrichtet"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.regularlyMonthly}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[40px] text-center flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="2."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="KV – Prämien wurden in unregelmäßigen Abständen entrichtet"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.regularlyNotMonthly}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[40px] text-center flex items-center justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Es wurden keine KV-Prämien entrichtet"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.irregularly}/>
                            </div>
                        </div>
                    </div>
                </div>
                {/* table */}
                <DocumentsText
                    className="w-[687px] mt-[15px]"
                    text="Bitte die entrichteten jährlichen Beiträge zur Krankenversicherung einfügen und die Jahreszahlen vervollständigen: (Einfügen von Jahresbeträgen ist nicht ausreichend!)"/>
                <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                    <div className="flex bg-[#EFEFEF]">
                        <div
                            className="w-[255.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="Jahr"/>
                        </div>
                        {
                            data && data.healthInsuranceTableDtoList.map(item => <div
                                className="w-[161.2px] min-h-[40px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text={item.year===dataExp.currentYear
                                    ?"Lfd.Jahr"
                                    :item.year}/>
                            </div>)
                        }

                    </div>
                    <div>
                        {
                            data && months.map((item, index) =>
                                <div key={item} className={"flex"}>
                                    <div  className="w-[255.2px] min-h-[45px] flex items-center  px-[24px] border border-[#7E7E7E] border-b-0">
                                        <DocumentsText text={item}/>
                                    </div>
                                    {data.healthInsuranceTableDtoList.map(item => <div
                                        className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                        {item.monthHealthForTableDtoList[index] ? item.monthHealthForTableDtoList[index].healthInsurance : 0}
                                    </div>)}
                                </div>
                            )}
                    </div>

                    <div className="flex ">
                        <div
                            className="w-[255.2px] min-h-[45px] flex items-center px-[24px]  border border-[#7E7E7E] border-b-0">
                            <DocumentsText
                                text="Gesamt"/>
                        </div>
                        {
                            data && data.healthInsuranceTableDtoList.map(item => <div
                                className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                {item.total}
                            </div>)
                        }
                    </div>

                </div>
                {/* 8 */}
                <div className="mt-[25px]">
                    <DocumentsText text="Angaben zur Krankenversicherung"/>
                    <div className="mt-[10px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[40px] flex items-center px-[24px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Name der Versicherung"/>
                            </div>
                            <div className="w-[600px] min-h-[40px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[40px]"
                                    value={data.healthInsuranceDetailsDto.companyName}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[40px] flex items-center px-[24px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Anschrift (Straße)"/>
                            </div>
                            <div className="w-[600px] min-h-[40px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[40px]"
                                    value={data.healthInsuranceDetailsDto.companyAddress}
                                    disablead={true}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[40px] flex items-center px-[24px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Anschrift (Straße)"/>
                            </div>
                            <div className="w-[600px] min-h-[40px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[40px]"
                                    value={data.healthInsuranceDetailsDto.cityPostalCodeAddress}
                                    disablead={true}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[40px] flex items-center px-[24px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Mitgliedsnummer"/>
                            </div>
                            <div className="w-[600px] min-h-[40px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[40px]"
                                    value={data.healthInsuranceDetailsDto.membershipNumber}
                                    disablead={true}/>

                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[40px] flex items-center px-[24px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Ansprechpartner"/>
                            </div>
                            <div className="w-[600px] min-h-[40px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[40px]"
                                    value={data.healthInsuranceDetailsDto.contactPerson}
                                    disablead={true}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[40px] flex items-center px-[24px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Telefonnummer"/>
                            </div>
                            <div className="w-[600px] min-h-[40px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[40px]"
                                    value={data.healthInsuranceDetailsDto.healthInsurancePhoneNumber}
                                    disablead={true}/>
                            </div>
                        </div>
                    </div>
                    <DocumentsText
                        className="mt-[15px]"
                        text="Bitte Versicherungspolice und Kopie der Versicherungskarte und Beitragsrechnung beifügen!"/>
                </div>
            </div>}
        </div>
    )
}
export default AuditDocument5
