import DocumentsInput from "../ReviewDocumentsUi/DocumentsInput"
import DocumentsSquare from "../ReviewDocumentsUi/DocumentsSquare"
import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import React, {FC, RefObject, useState} from "react"

import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg";
import add from "../../../assets/images/Rectangle 2658.png";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {updateData} from "../../../redux/Audit/Audit";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";

interface IAuditDocument7 {
    data?:  {
        "auditPensionContributionDto": {
            "makePensionContribution": boolean,
            "pensionNumber": number,
            "lifeInsurance": boolean,
            "otherForm": boolean,
            "no": boolean,
            "outsideThePeriodText": string,
            "otherFormPension": string,
        },
        "lifeInsuranceDetailsDto": {
            "insuranceAmount": 0,
            "companyName": string,
            "address": string,
            "insuranceNumber": string,
        },
        "yearDtoPensions": [
            {
                "year": string,
                "monthDtoPensionList": [
                    {
                        "id": number,
                        "month": string,
                        "value": number
                    }
                ],
                "total": number
            }
        ]
    },
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
    componentRef: RefObject<HTMLDivElement>;
}

const AuditDocument7:FC<IAuditDocument7> = ({componentRef,handleFileChange,logo,data,
                                            setSize,file,size,fetchDelete}) => {

    /*const [dataExp, setDataExp] = useState<IDataExp>({
        notInsolvent: true,
        notOverindebted: true,
        pruferFirst: true
    } as IDataExp)*/

    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }

    const date = new Date()
    const day = date.getDate()
    const month = date.getMonth()
    const year = date.getFullYear()

    const months = ["September", "Oktober", "November", "Dezember"]
    return (
        <div  className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            <div className=" mt-[40px] mb-[40px] ml-[130px] mr-[110px] ">
                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>

                <div>
                    <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div>
                            {
                                data && months.map((item, index) =>
                                    <div key={item} className={"flex"}>
                                        <div  className="w-[255.2px] min-h-[45px] flex items-center  px-[24px] border border-[#7E7E7E] border-b-0">
                                            <DocumentsText text={item}/>
                                        </div>
                                        {data.yearDtoPensions.map(item => <div
                                            className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                            {item.monthDtoPensionList[index] ? item.monthDtoPensionList[index].value : 0}
                                        </div>)}
                                    </div>
                                )}
                        </div>
                    </div>
                    <DocumentsText text="Nachweise bitte beifügen!" />

                </div>
                <div className="mt-[30px]">
                    <DocumentsTitleMini text="IX. Prüfungsvermerk" />
                    <DocumentsText
                        className="mr-[10px] mt-[15px]"
                        text="1. Nach dem abschließenden Ergebnis meiner Überprüfung bestätige ich, dass die Firma" />
                    <div className="mt-[15px]">
                        <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                            <div className="flex">
                                <div className="w-[58px] min-h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                    <DocumentsText text="1.1" />
                                </div>
                                <div className="w-[789px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text="nicht insolvent ist und auch kein Insolvenzverfahren anhängig ist" />
                                </div>
                                <div className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsSquare
                                    active={dataExp.notInsolvent}
                                    setActive={() => setDataExp({...dataExp, notInsolvent: !dataExp.notInsolvent})}
                                    />
                                </div>
                            </div>
                            <div className="flex">
                                <div className="w-[58px] min-h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                    <DocumentsText text="1.2." />
                                </div>
                                <div className="w-[789px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text="nicht überschuldet ist" />
                                </div>
                                <div className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsSquare
                                    active={dataExp.notOverindebted}
                                    setActive={() => setDataExp({...dataExp, notOverindebted: !dataExp.notOverindebted})} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-[25px]">
                    <DocumentsText
                        className="mr-[10px] mt-[15px]"
                        text="2. Folgende Unregelmäßigkeiten sind mir im Prüfungszeitraum aufgefallen:" />
                    <div className="mt-[15px] w-full min-h-[45px] border border-[#7E7E7E]">
                        <DocumentsInput
                            size="w-full min-h-[45px]"
                            initialValue={dataExp.violation}
                            onChange={e => setDataExp({...dataExp,
                            violation: e.target.value})}
                        />
                    </div>
                </div>
                <div className="mt-[25px]">
                    <DocumentsTitleMini text="IX. Prüfungsvermerk" />
                    <DocumentsText
                        className="mr-[10px] mt-[15px]"
                        text="3. Die Firma wird von mir:" />
                    <div className="mt-[15px]">
                        <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                            <div className="flex">
                                <div className="w-[58px] min-h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                    <DocumentsText text="3.1" />
                                </div>
                                <div className="w-[789px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text="seit dem . . ununterbrochen als Prufer betreut." />
                                </div>
                                <div className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsSquare
                                    active={dataExp.pruferInterruption}
                                    setActive={() => setDataExp({...dataExp,
                                        pruferFirst: false,
                                        pruferInterruption: true,
                                        pruferSupervised: false
                                    })}/>
                                </div>
                            </div>
                            <div className="flex">
                                <div className="w-[58px] min-h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                    <DocumentsText text="3.2." />
                                </div>
                                <div className="w-[789px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text="seit dem . . gelegentlich als Prufer betreut." />
                                </div>
                                <div className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsSquare
                                    active={dataExp.pruferSupervised}
                                    setActive={() => setDataExp({...dataExp,
                                        pruferFirst: false,
                                        pruferInterruption: false,
                                        pruferSupervised: true
                                    })}/>
                                </div>
                            </div>
                            <div className="flex">
                                <div className="w-[58px] min-h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                    <DocumentsText text="3.3." />
                                </div>
                                <div className="w-[789px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text="erstmals als Prufer betreut." />
                                </div>
                                <div className="w-[53px] min-h-[50px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsSquare
                                    active={dataExp.pruferFirst}
                                    setActive={() => setDataExp({...dataExp,
                                        pruferFirst: true,
                                        pruferInterruption: false,
                                        pruferSupervised: false
                                    })}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-[40px]">
                    <DocumentsText
                        text="Vorliegender Prüfungsbericht wurde von mir ausschließlich gemäß meinem derzeitigen Erkenntnisstand sowohl auf der Grundlage der von" />
                    <DocumentsText
                        className="my-[15px]"
                        text="(bitte Namen der externen Gesellschaft unten einfügen!)" />
                    <DocumentsText
                        text="vorgelegten Unterlagen und erteilten Auskünfte als auch entsprechend sonstiger Ausführungen des Antragstellers gefertigt." />
                </div>
                <div className="mt-[31px] w-[652px]">
                    <DocumentsTitleMini text="Ich versichere die Richtigkeit der vorstehend abgegebenen Erklärungen. Meine allgemeinen Geschäftsbedingungen füge ich bei!" />
                    <div className="w-[534px] mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div className="w-[317px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Berlin" />
                            </div>
                            <div className="w-[217px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text={`${day}.${month}.${year}`} />
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[317px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Berlin" />
                            </div>
                            <div className="w-[217px] min-h-[45px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text={`${day+1}.${month}.${year}`} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default AuditDocument7
