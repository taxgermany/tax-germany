import DocumentsSquare from "../ReviewDocumentsUi/DocumentsSquare"
import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import React, {FC, RefObject, useState} from 'react'
import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import add from "../../../assets/images/Rectangle 2658.png";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {updateData} from "../../../redux/Audit/Audit";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";


interface IAuditDocument2 {
    data?: any,
    setData?: (ph: any) => void
    componentRef: RefObject<HTMLDivElement>
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
}

const AuditDocument2: FC<IAuditDocument2> = ({
                                                 componentRef,
                                                 data,
                                                 setData,
                                                 handleFileChange,
                                                 logo,size,setSize,file,fetchDelete
                                             }) => {

    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }

    /*
        const [dataExp, setDataExp] = useState<IDataExp>({
            submitDoc: true,
            deviations: false
        } as IDataExp)*/
    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            {data &&
                <div className=" mt-[40px] mb-[40px] ml-[130px] mr-[110px] ">
                    <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                                setSize={setSize}/>

                    {/* 2 */}
                    <div className="border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div
                                className="w-[58px] h-[45px] py-[14px] flex justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="2."/>
                            </div>
                            <div
                                className="w-[842px] h-[45px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Die Überprüfung erfolgte"/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[45px] border border-[#7E7E7E] border-b-0">
                            </div>
                            <div
                                className="w-[58px] h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="2.1"/>
                            </div>
                            <div
                                className="w-[731px] h-[45px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Die Überprüfung erfolgte"/>
                            </div>
                            <div
                                className="w-[53px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={dataExp.randomSelection}
                                    setActive={() => setDataExp({
                                        ...dataExp,
                                        randomSelection: !dataExp.randomSelection
                                    })}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[45px] border border-[#7E7E7E] border-b-0">
                            </div>
                            <div
                                className="w-[58px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="2.2"/>
                            </div>
                            <div
                                className="w-[731px] h-[45px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="durch lückenlose Einsichtnahme in die vorgelegten Unterlagen"/>
                            </div>
                            <div
                                className="w-[53px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={dataExp.submittedDocuments}
                                    setActive={() => setDataExp({
                                        ...dataExp,
                                        submittedDocuments: !dataExp.submittedDocuments,
                                    })}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[45px] border border-[#7E7E7E] border-b-0">
                            </div>
                            <div
                                className="w-[58px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="2.3 "/>
                            </div>
                            <div
                                className="w-[731px] h-[45px] px-[14px] py-[6px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="teils durch eine geeignete Anzahl von Stichproben, teils durch lückenlose Einsichtnahme in die vorgelegten Buchhaltungsunterlagen"/>
                            </div>
                            <div
                                className="w-[53px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={dataExp.randomSampling}
                                    setActive={() => setDataExp({
                                        ...dataExp,
                                        randomSampling: !dataExp.randomSampling
                                    })}
                                    check="random/submitDocument"/>
                            </div>
                        </div>
                    </div>
                    {/* 3 */}
                    <div className="mt-[50px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div className="w-[58px] h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3."/>
                            </div>
                            <div
                                className="w-[842px] h-[45px] p-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="hat eine Vollständigkeitserklärung abgegeben, in der versichert, dass alle zur Überprüfung der wirtschaftlichen Nachhaltigkeit erforderlichen Nachweisevorgelegt und alle notwendigen Auskünfte erteilt wurden."/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[45px] border border-[#7E7E7E] border-b-0">
                            </div>
                            <div
                                className="w-[58px] h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="3.1"/>
                            </div>
                            <div
                                className="w-[731px] h-[45px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Abweichungen hiervon wurden nicht festgestellt"/>
                            </div>
                            <div
                                className="w-[53px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={true}
                                    /*setActive={() => setDataExp({
                                        ...data,
                                    })}*/
                                />
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[45px] border border-[#7E7E7E] border-b-0">
                            </div>
                            <div
                                className="w-[58px] h-[45px] text-center py-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="3.2"/>
                            </div>
                            <div
                                className="w-[731px] h-[45px] px-[14px] flex items-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Abweichungen wurden wie unter IX. (Prüfungsvermerk) festgestellt"/>
                            </div>
                            <div
                                className="w-[53px] h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}
                                    /*setActive={() => setDataExp({
                                        ...data,
                                    })}*/
                                />
                            </div>
                        </div>
                    </div>
                    {/* II */}
                    <div className="mt-[80px]">
                        <DocumentsTitleMini text="II. Feststellungen"/>
                        <DocumentsText
                            className="mt-[30px]"
                            text="Nach der Erklärung und nach den Feststellungen ist"/>
                    </div>
                    <div className="mt-[40px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="1."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Das Unternehmen beim Registergericht im Handelsregister (HR) unter der Nr. eingetragen."/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="2."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText>
                                    <p className=" font-roboto font-[400]">Das Gewerbe ordnungsgemäß unter der o.a.
                                        Anschrift beim zuständigen Gewerbeamt angemeldet.
                                        <span
                                            className="text-[#D8AD00]">The form is attached. OR (trade_form_explanation)</span> (Kopien
                                        des Handelsregisterauszuges und der Gewerbeanmeldung bitte beifügen)</p>
                                </DocumentsText>
                            </div>
                            <div
                                className="w-[53px] min-h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.commercial}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Der Antragsteller ist freiberuflich tätig. 1. Und 2. sind nicht zutreffend"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.intellectualOrArtistic}/>
                            </div>
                        </div>
                    </div>
                    {/* 3 */}
                    <div className="mt-[50px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3."/>
                            </div>
                            <div className="w-[842px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Das Untermehmen ubt seinen Gewerbezweck aus in"/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3.1."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="privat genutzten Wohnräumen"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.privateHome}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3.2."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="separaten Geschäftsräumen"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.separateBusinessOffice}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3.3."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="in einem angemieteten Wohnbüro"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.rentedBedroomOfficeCombo}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3.4."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="in anderen Räumlichkeiten (bittle nachfolgend erläutern)"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.otherPlace}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center border border-[#7E7E7E] border-b-0">
                            </div>
                            <div className="w-[842px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text={data.otherPlaceText}/>
                            </div>
                        </div>
                    </div>
                    {/* 4 */}
                    <div className="mt-[50px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="4."/>
                            </div>
                            <div className="w-[842px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Das Unternehmen besitzt die Rechtsform einer ..."/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="4.1."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="ist ein Geschäftsführer"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="4.2."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="ist alleiniger Gesellschafter"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div className="w-[58px] h-[40px] text-center py-[14px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="4.3."/>
                            </div>
                            <div className="w-[789px] h-[40px] p-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="hält Anteile von %"/>
                            </div>
                            <div
                                className="w-[53px] h-[40px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={false}/>
                            </div>
                        </div>
                    </div>
                </div>}
        </div>
    )
}
export default AuditDocument2
