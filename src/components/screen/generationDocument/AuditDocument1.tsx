import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitle from "../ReviewDocumentsUi/DocumentsTitle"
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg"
import DocumentsInput from "../ReviewDocumentsUi/DocumentsInput"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import React, {useState, FC, RefObject} from 'react'
import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import add from "../../../assets/images/Rectangle 2658.png";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {updateData} from "../../../redux/Audit/Audit";
import UploadLogo from "./UploadLogo";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import {size} from "lodash";

interface IAuditDocument1 {

    data?: any,
    dataExp: any,
    setData?: (ph: any) => void
    fetchDelete: () => void
    componentRef: RefObject<HTMLDivElement>;
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void

}


const AuditDocument1: FC<IAuditDocument1> = ({
                                                 componentRef,
                                                 data,
                                                 setData,
                                                 handleFileChange, logo, file,
                                                 fetchDelete, size, setSize
                                             }) => {
    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }

    const updateName = (textName: string) => {
        const updateText = textName.charAt(0) + textName.slice(1).toLowerCase()
        return updateText
    }
    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            {data && <div className=" mt-[40px] ml-[130px] mr-[110px] ">

                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>


                <div className="">
                    <div className={"flex gap-x-[20px] mb-[15px]"}>
                        <div className="w-1/2 flex flex-wrap items-center">
                            <DocumentsText
                                className="w-1/4 h-[36px] flex items-center justify-start px-[9px] border
                                 border-[#7E7E7E]"
                                text="AZ"/>
                            <div className="w-3/4 h-[36px] border border-[#7E7E7E] border-l-0">
                            </div>
                            <div className="h-[36px] w-full border flex items-center justify-start border-[#7E7E7E]
                            border-t-0 px-[9px]">
                                <DocumentsText
                                    className="flex items-center"
                                    children={<div>Bitte Aktenzeichen der IHK einfügen, sofern <br/> bekannt"</div>}/>
                            </div>
                        </div>
                        <div className="w-1/2 flex flex-wrap items-center">
                            <DocumentsText
                                className="w-1/3 h-[36px] flex items-center justify-start px-[9px] border border-[#7E7E7E]"
                                text="Reg. OM"/>
                            <div className="w-2/3 h-[36px] border border-[#7E7E7E] border-l-0">
                            </div>
                            <div
                                className="h-[36px] w-full border flex items-center justify-start border-[#7E7E7E] border-t-0 px-[9px]">
                                <DocumentsText
                                    className="flex items-center"
                                    text="Bitte Aktenzeichen der Ausländerbehörde einfügen, sofern bekannt"/>
                            </div>
                        </div>
                    </div>
                    <DocumentsText
                        className="w-[746px]"
                        children={<span>Bitte die grau unterlegten Felder ausfüllen und bereits vorgegebene Angaben durch Mausklick
                           <br/> darauf verändern/auswählen</span>}/>
                </div>
                <div className="mt-[65px]  flex flex-col justify-center items-center gap-y-[35px]">
                    <DocumentsTitle
                        text="Prüfungsbericht"/>
                    <DocumentsText
                        className="text-center w-[445px]"
                        text="Zur Vorlage bei der Ausländerbehörde Berlin bescheinige ich, zwecks Erteilung oder
                        Verlängerung des zweckgebundenen Aufenthaltstitels oder zur Erteilung einer Niederlassungserlaubnis,
                         gemäß der Bestimmungen des deutschen Aufenthaltsgesetzes:"/>
                </div>
                <div
                    className="mt-[30px] mb-[20px] flex justify-between items-center border border-[#7E7E7E] border-l-0 z-[5] ">
                    <div className="w-full">
                        <div className="flex items-center justify-center">
                            <DocumentsText
                                className="w-1/3 h-[36px] px-[9px] flex items-center border border-[#7E7E7E] border-t-0"
                                text="Name des Prüfers:"/>
                            <div className="w-2/3 h-[36px] border border-[#7E7E7E] border-t-0 border-l-0">
                                <DocumentsInput
                                    initialValue={dataExp.nameExport}
                                    disabled={false}
                                    /* size=" h-[49px]"*/
                                    onBlur={((e) => setDataExp({...dataExp, nameExport: e.target.value}))}
                                />
                            </div>
                        </div>
                        <div className="flex items-center justify-center">
                            <DocumentsText
                                className="w-1/3 h-[36px] px-[9px] flex items-center border border-[#7E7E7E] border-t-0"
                                text="Firma:"/>
                            <div className="w-2/3 h-[36px] border border-[#7E7E7E] border-t-0 border-l-0">
                                < DocumentsInput
                                    initialValue={dataExp.nameFirma}
                                    disabled={false}
                                    /*  size="h-[49px]"*/
                                    onBlur={((e) => setDataExp({...dataExp, nameFirma: e.target.value}))}
                                />
                            </div>
                        </div>
                        <div className="flex items-center justify-center">
                            <DocumentsText
                                className="w-1/3 h-[36px] px-[9px] flex items-center border border-[#7E7E7E] border-t-0"
                                text="Anschrift:"/>
                            <div className="w-2/3 h-[36px] border border-[#7E7E7E] border-t-0 border-l-0">
                                < DocumentsInput
                                    initialValue={dataExp.addressExpert}
                                    disabled={false}
                                    /*size=" h-[49px]"*/
                                    onBlur={((e) => setDataExp({...dataExp, addressExpert: e.target.value}))}
                                />
                            </div>
                        </div>
                        <div className="flex items-center justify-center">
                            <DocumentsText
                                className="w-1/3 h-[36px] px-[9px] flex items-center border border-[#7E7E7E] border-t-0 border-b-0"
                                text="Tel./ Fax / E-Mail:"/>
                            <div className="w-2/3 h-[36px]  border border-[#7E7E7E] border-t-0 border-l-0 border-b-0">
                                <DocumentsInput
                                    initialValue={dataExp.telFaxEmailExpert}
                                    disabled={false}
                                    /* size="h-[49px]"*/
                                    onBlur={((e) => setDataExp({...dataExp, telFaxEmailExpert: e.target.value}))}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="w-[145px] p-[2px]">
                        <Print width={140} height={140}/>
                    </div>
                </div>
                <DocumentsText
                    text="(Name und Anschrift des Wirtschaftsprüfers / Steuerberaters / Steuerbevollmächtigten)"/>
                <div className="mt-[55px] mb-[30px] grid gap-y-[20px] w-full">
                    <DocumentsTitleMini text="I. Prüfungsauftrag und Auftragsdurchführung"/>
                    <div className="border border-[#7E7E7E] border-l-0 border-t">
                        <div className="flex items-center  border-b border-[#7E7E7E]">
                            <div
                                className="!w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]">
                                <DocumentsText text="1."/>
                            </div>
                            <div
                                className="h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Am ________________  wurde aufgrund des erteilten Auftrages"/>
                            </div>
                        </div>
                        {/*1.1*/}
                        <div className="flex items-center border-b border-[#7E7E7E] ">
                            <div
                                className="!w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"></div>
                            <div
                                className="w-[40px] h-[36px] block flex items-center justify-center border-l border-[#7E7E7E]">
                                <DocumentsText text="1.1."/>
                            </div>
                            <div
                                className="h-[36px] block flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="die Überprüfung der persönlichen Einkünfte von"/>
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Anrede"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size="h-[36px]"
                                    value={updateName(data.auditPersonalDetailsDto.gender)}
                                />
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Name, Vornamen"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size="h-[36px]"
                                    value={`${data.auditPersonalDetailsDto.firstname}, ${data.auditPersonalDetailsDto.lastname}`}/>
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="geboren am, am"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size=" h-[36px]"
                                    value={data.auditPersonalDetailsDto.dateOfBirth}/>
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="geboren am, in"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size=" h-[36px]"
                                    value={data.auditPersonalDetailsDto.country}/>
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Nationalität"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size=" h-[36px]"
                                    value={data.auditPersonalDetailsDto.nationality}/>
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Funktion"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size="h-[36px]"
                                    value={data.auditBusinessDetailsDto.gender === 'MR' ? 'Inhaber' : 'Inhaberin'}
                                />
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="!w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"></div>
                            <div
                                className="w-[40px] h-[36px] block flex items-center justify-center border-l border-[#7E7E7E]">
                                <DocumentsText text="1.2."/>
                            </div>
                            <div
                                className="h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="und die Überprüfung der Gesellschaft des Auftraggebers"/>
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Name der Firma"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size=" h-[36px]"
                                    value={data.auditBusinessDetailsDto.businessName}
                                />
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Anschrift der Firma"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size=" h-[36px]"
                                    value={data.auditBusinessDetailsDto.businessAddress}
                                />
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Telefon"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size="h-[36px]"
                                    value={data.auditBusinessDetailsDto.businessPhone}
                                />
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Telefax"/>
                            </div>
                            <div
                                className=" h-[36px] flex items-center border border-[#7E7E7E] border-r-0 border-b-0">

                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Mailadresse"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size=" h-[36px]"
                                    value={data.auditBusinessDetailsDto.businessEmail}
                                />
                            </div>
                        </div>
                        <div className="flex items-center border-b border-[#7E7E7E]">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>
                            <div
                                className="w-[200px] h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="Internetadresse"/>
                            </div>
                            <div
                                className="h-[36px] flex items-center border-l border-[#7E7E7E]">
                                < DocumentsInput
                                    disabled={true}
                                    size="h-[36px]"
                                    value={data.auditBusinessDetailsDto.website}
                                />
                            </div>
                        </div>
                        <div className="flex items-center">
                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>

                            <div
                                className="w-[40px] h-[36px] flex items-center justify-center border-l border-[#7E7E7E]"/>

                            <div
                                className="h-[36px] flex items-center px-[14px] border-l border-[#7E7E7E]">
                                <DocumentsText text="In meinem Geschdurchgeführt ">
                                    {`(`}<span className="font-roboto font-[400]">wenn in anderen Geschäftsräumen bitte in der nächste Zeile die abweichende <br/> Firmierung und  Anschrift eintragen</span>{`)`}
                                </DocumentsText>
                            </div>
                        </div>
                    </div>
                </div>
            </div>}
        </div>
    )
}
export default AuditDocument1
