import React, {FC, useEffect, useState} from "react";
import {useAppSelector} from "hooks/useAppSelector";
import {ReactComponent as Review} from "../ReviewDocumentsUi/picture/steuerberater.svg";
 import Print from "../../../assets/images/Rectangle 2660.png";
import {Spin} from "antd";
import ReactToPrint from "react-to-print";
import NewButtonGreen from "../../ui/button/NewButtonGreen";
import {useGetIncomeConfirmationQuery} from "../../../services/superAdminService";

interface IIncomeConfirmDocument {
    id: string | undefined
}

const IncomeConfirmDocument:FC<IIncomeConfirmDocument> = ({id}) => {
    const componentRef = React.useRef(null);
    const {data: apiData, isLoading} = useGetIncomeConfirmationQuery(id || 0)

    const reactToPrintContent = React.useCallback(() => {
        return componentRef.current;
    }, [componentRef.current]);


    const reactToPrintTrigger = React.useCallback(() => {

        return <NewButtonGreen text={"Download pdf document"}/>

    }, []);

    return (
        <>{isLoading
            ? <Spin size="large" className="absolute top-2/4 left-2/4"/>
            : <div>
                <div className={"flex justify-end pb-[20px]"}>
                    <ReactToPrint
                        content={reactToPrintContent}
                        documentTitle={"Income_Confirmation"}
                        trigger={reactToPrintTrigger}
                    />
                </div>
                {apiData &&
                    <div ref={componentRef} className="w-[997px]  border pt-4">
                        <div className="flex justify-end mr-12">
                            <Review className="w-[470px]"/>
                        </div>
                        <div className={"px-[85px]"}>
                            <div className="flex justify-between gap-x-[50px]">
                                <div className={"w-3/4 grid content-between"}>
                                    <div className="flex justify-between ">
                                        <div>
                                            <p className="text-[10px] pt-[70px]">
                                                Steuerberater Suat Göydeniz & Grit Birka GbR <br/> Kottbusser
                                                Damm 68 in 10967 Berlin
                                            </p>
                                            <div className="text-[14px]">
                                                <p>{apiData.gender === "MR" ? "Herr" : "Frau"}</p>
                                                <p>
                                                    {apiData.firstName} {apiData.lastName}
                                                </p>
                                                <p>{apiData.street}</p>
                                                <p>
                                                    {apiData.postalCode} {apiData.address}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="self-end text-[14px]">
                                            <p className="font-[500] pt-[430px]">
                                                {apiData.generationMonth.slice(0, 3)} {apiData.generationYear}
                                            </p>
                                            <p>OK/GB/99999</p>
                                        </div>
                                    </div>

                                    <div>
                                        <p className="font-bold mb-6">
                                            Einkommensbestätigung {apiData.fromMonth.slice(0, 3)}. - {apiData.toMonth.slice(0, 3)}.{" "}
                                            {apiData.toYear}
                                        </p>
                                        <p className="pb-[15px]">Sehr geehrter Herr Kar,</p>
                                    </div>
                                </div>
                                <div className="text-[14px] grid gap-y-[10x] mt-[12px] w-1/4">
                                    <div>
                                        divSuat Göydeniz
                                        Steuerberater
                                    </div>
                                    <div>
                                        Grit Birka <br/>
                                        Steuerberater
                                    </div>
                                    <div>
                                        Kottbusser Damm 68 <br/>
                                        10967 Berlin
                                    </div>
                                    <div>
                                        Fon: 030 355 214 65 <br/>
                                        Fax: 030 922 86 398
                                    </div>
                                    <div>
                                        mail@stb-goeydeniz-birka.de <br/>
                                        www.stb-goeydeniz-birka.de
                                    </div>
                                    <div>
                                        Steuernummer: <br/>
                                        16/311/00545
                                    </div>
                                    <div>
                                        Bankverbindung: <br/>
                                        IBAN: <br/>
                                        DE78120300001051531885 <br/>
                                        BIC: <br/>
                                        BYLADEM1001 <br/>
                                        Kreditinstitut: <br/>
                                        Deutsche Kreditbank Berlin
                                    </div>
                                </div>
                            </div>
                            <div className="text-[14px] ">
                                <p className="w-[579px]">
                                    anhand der uns vorliegenden Unterlagen können wir Ihnen für den
                                    Zeitraum Januar bis einschließlich April 2023 ein durchschnittliches
                                    monatliches <span className="font-[500]">Nettoeinkommen</span> von{" "}
                                    <span className="font-[500]">EUR {apiData.total}</span> aus gewerblicher
                                    Tätigkeit bestätigen.
                                </p>
                                <p className="my-8">
                                    Für Rückfragen stehen wir Ihnen bzw. Ihrem Ansprechpartner gerne zur
                                    Verfügung.
                                </p>
                                <p className="mb-3">Mit freundlichen Grüßen</p>
                                <p>Suat Göydeniz & Grit Birka GbR </p>
                                <p>Stereuberater</p>
                                <p>Kottbuser Damm 68 * 10967 Berlin</p>
                                <p>Fon: 030 355 214 65</p>
                                <p>mail@stb-goeydeniz-birka.de</p>
                            </div>
                            <div className="flex justify-between">
                                <div className="self-end w-3/4">
                                    <p className="text-[14px]">
                                        Grit Birka <br/>
                                        Steuerberater
                                    </p>
                                </div>
                                <div className="w-1/4">
                                     <img src={Print} className="h-[160px]" alt=""/>
                                </div>
                            </div>
                        </div>
                        <div className={"text-[14px] mt-[200px] mb-2"}>
                            <hr className="w-[900px] mx-auto "/>
                            <p className="mt-2 text-center">
                                Steuerberater Suat Göydeniz & Grit Birka GbR ● Fon: 030 355 214 65 ●
                                Fax: 030 922 863 98 <br/> Mail: mail@stb-goeydeniz-birka.de ●
                                Kottbusser Damm 68 in 10967 Berlin
                            </p>
                        </div>
                    </div>}
            </div>
        }</>
    );
};
export default IncomeConfirmDocument;
