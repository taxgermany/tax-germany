import DocumentsInput from "../ReviewDocumentsUi/DocumentsInput"
import DocumentsSquare from "../ReviewDocumentsUi/DocumentsSquare"
import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import React, {FC, RefObject} from 'react'

import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg";
import add from "../../../assets/images/Rectangle 2658.png";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {updateData} from "../../../redux/Audit/Audit";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";

interface IAuditDocument6 {
    data?:  {
        "auditPensionContributionDto": {
            "makePensionContribution": boolean,
            "pensionNumber": number,
            "lifeInsurance": boolean,
            "otherForm": boolean,
            "no": boolean,
            "outsideThePeriodText": string,
            "otherFormPension": string,
        },
        "lifeInsuranceDetailsDto": {
            "insuranceAmount": 0,
            "companyName": string,
            "address": string,
            "insuranceNumber": string,
        },
        "yearDtoPensions": [
            {
                "year": string,
                "monthDtoPensionList": [
                    {
                        "id": number,
                        "month": string,
                        "value": number
                    }
                ],
                "total": number
            }
        ]
    },
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
    setData?: (ph: any) => void
    componentRef: RefObject<HTMLDivElement>;
}

const AuditDocument6: FC<IAuditDocument6> = ({
                                                 data,
                                                 logo,
                                                 setData,
                                                 componentRef, handleFileChange,
                                                 file,size,setSize,fetchDelete
                                             }) => {
    const months = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August"]
    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }


    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            {data && <div className=" mt-[40px] mb-[40px] ml-[130px] mr-[110px] ">
                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>

                {/* VIII */}
                <div>
                    <DocumentsTitleMini text="VIII. Angaben zur Altersvorsorge"/>
                    <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[45px] text-center flex items-center  justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="1."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[45px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Die Gesellschaft hat fur den Antragsteller regelmäßig monatliche RV-Beiträge zur gesetzlichen Rentenversicherung abgeführt"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.auditPensionContributionDto.makePensionContribution}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[45px] text-center flex items-center  justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="1.1"/>
                            </div>
                            <div
                                className="w-[842px] min-h-[45px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text={`Rentenversicherungsnummer: ${data.auditPensionContributionDto.pensionNumber}`}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[45px] text-center flex items-center  justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="2."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[45px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Die Firma hat fur den Antragsteller keine RV-Beiträge zur gesetzlichen Rentenversicherung abgeführt"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={!data.auditPensionContributionDto.makePensionContribution}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[45px] text-center flex items-center  justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="3."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[45px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Der Antragsteller hat eine private Altersvorsorge in Form einer Lebensversicherung abgeschlossen (genauere Angaben siehe nächster Absatz)"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.auditPensionContributionDto.lifeInsurance}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[45px] text-center flex items-center  justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="4."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[45px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText
                                    text="Der Anstragsteller hat Aufwendungen für eine andere Form der Altersvorsorge vorgenommen"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.auditPensionContributionDto.otherForm}/>
                            </div>
                        </div>
                        <div className="flex">
                            <div
                                className="w-[58px] min-h-[45px] text-center flex items-center  justify-center border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="5."/>
                            </div>
                            <div
                                className="w-[789px] min-h-[45px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="hat noch keine Altersvosorge vorgenommen"/>
                            </div>
                            <div
                                className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsSquare
                                    active={data.auditPensionContributionDto.no}/>
                            </div>
                        </div>
                        <div
                            className="w-full min-h-[126px] flex items-center px-[20px] border border-[#7E7E7E] border-b-0 ">
                            <DocumentsText
                                text={`Erlauterungen zu VIII.4. ${data.auditPensionContributionDto.outsideThePeriodText} ${data.auditPensionContributionDto.otherFormPension}`}/>
                        </div>
                    </div>
                </div>
                {/* VIII.3 */}
                <div className="mt-[80px]">
                    <DocumentsText text="Ergänzende Angaben zu Punkt VIII.3"/>
                    <div className="mt-[10px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[45px] px-[24px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Versicherungssumme in Euro"/>
                            </div>
                            <div className="w-[600px] min-h-[45px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[45px]"
                                    value={`${data.lifeInsuranceDetailsDto.insuranceAmount}`}
                                    disablead={true}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[45px] px-[24px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Policen-Nr."/>
                            </div>
                            <div className="w-[600px] min-h-[45px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[45px]"
                                    value={data.lifeInsuranceDetailsDto.insuranceNumber}
                                    disablead={true}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[45px] px-[24px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Name der Versicherung"/>
                            </div>
                            <div className="w-[600px] min-h-[45px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[45px]"
                                    value={data.lifeInsuranceDetailsDto.companyName}
                                    disablead={true}/>
                            </div>
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[300px] min-h-[45px] px-[24px] flex items-center  px-[20px] border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Anschrift"/>
                            </div>
                            <div className="w-[600px] min-h-[45px] border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsInput
                                    className="w-[600px] min-h-[45px]"
                                    value={data.lifeInsuranceDetailsDto.address}
                                    disablead={true}/>
                            </div>
                        </div>
                    </div>
                    <DocumentsText
                        className="mt-[15px]"
                        text="Bitte Kopie der Versicherungspolice beifügen!"/>
                </div>
                {/* table */}
                <div className="mt-[25px]">
                    <DocumentsText
                        text="Bitte die entrichteten jährlichen Beiträge zur Altersvorsorge einfügen und die Jahreszahlen vervollständigen:"/>
                    <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex bg-[#EFEFEF]">
                            <div
                                className="w-[255.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Jahr"/>
                            </div>
                            {
                                data && data.yearDtoPensions.map(item => <div
                                    className="w-[161.2px] min-h-[40px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                    <DocumentsText text={+item.year.split("-")[0]===dataExp.currentYear
                                        ?"Lfd.Jahr"
                                        :+item.year.split("-")[0]}/>
                                </div>)
                            }
                      {/*      <div
                                className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Lfd.Jahr"/>
                            </div>*/}
                        </div>
                        <div>
                            {
                                data && months.map((item, index) =>
                                    <div key={item} className={"flex"}>
                                        <div  className="w-[255.2px] min-h-[45px] flex items-center  px-[24px] border border-[#7E7E7E] border-b-0">
                                            <DocumentsText text={item}/>
                                        </div>
                                        {data.yearDtoPensions.map(item => <div
                                            className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                            {item.monthDtoPensionList[index] ? item.monthDtoPensionList[index].value : 0}
                                        </div>)}
                                    </div>
                                )}
                        </div>
                    </div>

                </div>
            </div>}
        </div>
    )
}

export default AuditDocument6
