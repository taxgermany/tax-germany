import DocumentsSquare from "../ReviewDocumentsUi/DocumentsSquare"
import DocumentsText from "../ReviewDocumentsUi/DocumentsText"
import DocumentsTitleMini from "../ReviewDocumentsUi/DocumentsTitleMini"
import React, {FC, RefObject, useState} from 'react'

import {ReactComponent as Review} from "../../../assets/images/Rectangle 2658.svg";
import clsx from "clsx";
import styles from "../../ui/upload/Upload.module.scss";
import {ReactComponent as Print} from "../ReviewDocumentsUi/picture/Rectangle 2660.svg";
import add from "../../../assets/images/Rectangle 2658.png";
import {useAppDispatch} from "../../../hooks/useAppDispatch";
import {useAppSelector} from "../../../hooks/useAppSelector";
import {IAuditAddDocument} from "../../../interfaces/auditDocument";
import {updateData} from "../../../redux/Audit/Audit";
import {IFileName} from "../../../interfaces/ProfitsInterface";
import UploadLogo from "./UploadLogo";

interface IAuditDocument4 {
    data?: {
        "monthlyProfitsTableDtoList": [
            {
                "year": number,
                "months": [
                    {
                        "month": string,
                        "monthlyProfits": number
                    }
                ],
                "total": number
            }
        ]
    },
    logo: string | null
    handleFileChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    file: IFileName | null
    size: number
    setSize:  (prev: number)  => void
    fetchDelete: () => void
    setData?: (ph: any) => void
    componentRef: RefObject<HTMLDivElement>;
}

const AuditDocument4: FC<IAuditDocument4> = ({
                                                 data,
                                                 setData,
                                                 componentRef, logo, handleFileChange,
    file,size,setSize,fetchDelete
                                             }) => {
    const months = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]
    const [income, setIncome] = useState('income_monthly')
    const dispatch = useAppDispatch();
    const {data: dataExp} = useAppSelector(state => state.auditSlice)
    const setDataExp = (data: IAuditAddDocument) => {
        dispatch(updateData(data))
    }


    return (
        <div className="w-[997px] h-[1411px] border border-[1px] bg-white mx-auto  box-border ">
            {data && <div className=" mt-[40px] mb-[40px] ml-[130px] mr-[110px] ">
                <UploadLogo logo={logo} handleFileChange={handleFileChange} fetchDelete={fetchDelete} size={size}
                            setSize={setSize}/>

                {/* table */}
                <div className="mt-[25px]">
                    <DocumentsTitleMini
                        text="VI. Angaben zu den Einkünften aus selbstständiger Tätigkeit des Antragstellers"/>
                    <DocumentsText
                        className="mt-[20px]"
                        text="(Mr/Mrs) (first name) (second name)"/>
                    <DocumentsText
                        className="mt-[10px]"
                        text="hat im Prüfungszeitraum der letzten drei Geschäftsjahre über ein Jahreseinkommen aus selbstständiger Tätigkeit von … Euro verfügt: (Bitte die Brutto-Entgelte einfügen und wenn möglich durch Steuererklärungen belegen.) Jahreszahlen sind in der Tabelle bitte zu vervollständigen."/>
                </div>
                {/* table */}
                <div className="mt-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                    <div className="flex bg-[#EFEFEF]">
                        <div
                            className="w-[255.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="Jahr"/>
                        </div>
                        {
                            data && data.monthlyProfitsTableDtoList.map(item => <div
                                className="w-[161.2px] min-h-[40px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">

                                <DocumentsText text={item.year === dataExp.currentYear
                                    ? "Lfd.Jahr"
                                    : item.year}/>
                            </div>)
                        }
                    </div>
                    <div>
                        {
                            data && months.map((item, index) =>
                                <div key={item} className={"flex"}>
                                    <div
                                        className="w-[255.2px] min-h-[45px] flex items-center  px-[24px] border border-[#7E7E7E] border-b-0">
                                        <DocumentsText text={item}/>
                                    </div>
                                    {data.monthlyProfitsTableDtoList.map(item => <div
                                        className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                        {item.months[index] ? item.months[index].monthlyProfits : 0}
                                    </div>)}

                                </div>
                            )}
                    </div>

                    <div className="flex ">
                        <div
                            className="w-[255.2px] min-h-[45px] flex items-center px-[24px]  border border-[#7E7E7E] border-b-0">
                            <DocumentsText
                                className="!font-[600]"
                                text="Gesamt"/>
                        </div>
                        {
                            data && data.monthlyProfitsTableDtoList.map(item => <div
                                className="w-[161.2px] min-h-[40px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text={item.total}/>
                            </div>)
                        }
                    </div>
                </div>
                <div className="mt-[30px]">
                    <DocumentsText
                        text="Zusätzlich an den Auftraggeber der letzten drei Geschäftsjahre nach Bilanzvorlage/Gesellschafterbeschluss (bitte Jahresangaben vervollständigen)"/>
                    <div className="mt-[15px] mb-[15px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                        <div className="flex bg-[#EFEFEF]">
                            <div
                                className="w-[255.2px] min-h-[45px] flex items-center px-[24px]  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="für das Jahr (Zufluss)"/>
                            </div>
                            {
                                data && data.monthlyProfitsTableDtoList.map(item => <div
                                    className="w-[161.2px] min-h-[40px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">

                                    <DocumentsText text={item.year === dataExp.currentYear
                                        ? "Lfd.Jahr"
                                        : item.year}/>
                                </div>)
                            }
                            {/*<div
                                className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="2023"/>
                            </div>
                            <div
                                className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="2022"/>
                            </div>
                            <div
                                className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="2021"/>
                            </div>
                            <div
                                className="w-[161.2px] min-h-[45px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                <DocumentsText text="Lfd.Jahr"/>
                            </div>*/}
                        </div>
                        <div className="flex ">
                            <div
                                className="w-[255.2px] min-h-[45px] flex items-center px-[24px]  border border-[#7E7E7E] border-b-0">
                                <DocumentsText text="Betrag in Euro"/>
                            </div>
                            {
                                data && data.monthlyProfitsTableDtoList.map(item => <div
                                    className="w-[161.2px] min-h-[40px] flex items-center justify-center px-[24px]  border border-[#7E7E7E] border-b-0 border-l-0">
                                    { ""}
                                </div>)
                            }
                        </div>
                    </div>
                    <DocumentsText text="“* Der Antragsteller ist freiberuflich tätig, daher nicht zutreffend"/>
                </div>
                {/* 6 */}
                <div className="mt-[25px] border border-[#7E7E7E] border-l-0 border-t-0 border-r-0">
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[45px] flex items-center justify-center text-center  border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="1."/>
                        </div>
                        <div
                            className="w-[789px] min-h-[45px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText
                                text="Die Einkünfte wurden kontinuierlich in monatlichen Beträgen seit Beginn der Tätigkeit ausgezahlt"/>
                        </div>
                        <div
                            className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare
                                active={income}
                                setActive={() => setIncome('income_monthly')}
                                check="income_monthly"/>
                        </div>
                    </div>
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[45px] flex items-center justify-center text-center  border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="2."/>
                        </div>
                        <div
                            className="w-[789px] min-h-[45px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText
                                text="Die Einkünfte wurden in unregelmäßigen zeitlichen Abständen erzielt"/>
                        </div>
                        <div
                            className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare
                                active={income}
                                setActive={() => setIncome('income_irregular_interval')}
                                check="income_irregular_interval"/>
                        </div>
                    </div>
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[45px] flex items-center justify-center text-center py-[14px] border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="3."/>
                        </div>
                        <div
                            className="w-[789px] min-h-[45px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText text="Die Einkünfte wurden in unregelmäßigen Teilbeträgen erzielt"/>
                        </div>
                        <div
                            className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare
                                active={income}
                                setActive={() => setIncome('income_irregular_installments')}
                                check="income_irregular_installments"/>
                        </div>
                    </div>
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[45px] flex items-center justify-center text-center py-[14px] border border-[#7E7E7E] border-b-0">
                            <DocumentsText text="4."/>
                        </div>
                        <div
                            className="w-[789px] min-h-[45px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText text="Es keine Einkünfte erzielt"/>
                        </div>
                        <div
                            className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare
                                active={income}
                                setActive={() => setIncome('no_income')}
                                check="no_income"/>
                        </div>
                    </div>
                    <div className="flex">
                        <div
                            className="w-[58px] min-h-[45px] flex items-center text-center py-[14px] border border-[#7E7E7E] border-b-0">
                        </div>
                        <div
                            className="w-[789px] min-h-[45px] flex items-center px-[14px] border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsText text="Die Buchführung / Aufzeichnungen erfolgt / erfolgen unregelmäßig"/>
                        </div>
                        <div
                            className="w-[53px] min-h-[45px] flex items-center justify-center border border-[#7E7E7E] border-b-0 border-l-0">
                            <DocumentsSquare/>
                        </div>
                    </div>
                </div>
            </div>
            }
        </div>
    )
}
export default AuditDocument4
