import {FC} from 'react'
interface ITitleMiniDocuments{
    text: string,
    className?:string,

}

const DocumentsTitleMini: FC<ITitleMiniDocuments> = ({
    text,
    className
}) =>{
    return(
        <p className={`${className} text-[16px] font-roboto leading-[20px] font-[700]`}>{text}</p>
    )
}
export default DocumentsTitleMini
