import {FC} from 'react'
interface ITitleDocuments{
    text: string,
    className?:string,

}

const DocumentsTitle: FC<ITitleDocuments> = ({
    text,
    className
}) =>{
    return(
        <p className={`${className} text-[32px] font-roboto leading-[35px] font-[700] md:text-[28px]`}>{text}</p>
    )
}
export default DocumentsTitle