import { FC, ReactNode } from 'react';

interface ITextDocuments {
    children?: ReactNode;
    className?: string;
    text?: string |number;
}

const DocumentsText: FC<ITextDocuments> = ({
    children,
    className,
    text
}) => {

    return (
        <div className={`${className}  text-[13px] font-roboto leading-[15px] font-[400]`}>
            {text}
            {children}
        </div>
    )
};

export default DocumentsText;
