import {FC, ChangeEvent} from 'react';
import {Form, Input} from 'antd';
import cls from './Documents.module.scss'

interface IDocumentsInput {
    inputName?: string;
    initialValue?: string | any;
    label?: string;
    className?: string;
    size?: string,

    [key: string]: string | ((event: ChangeEvent<HTMLInputElement>) => void) | boolean | undefined | number;
}

const DocumentsInput: FC<IDocumentsInput> = ({
                                                 inputName,
                                                 className,
                                                 label,
                                                 initialValue,
                                                 size = 'w-full h-full',
                                                 ...props
                                             }) => {

    return (
        <Form.Item
            className='!m-0 !p-0'
            label={label}
            name={inputName}
            initialValue={initialValue}

            rules={[{required: true, message: `Please enter your ${inputName}`}]}>
            <Input
                defaultValue={initialValue}
                className={`${className} ${cls.i} ${size} !text-black text-[12px] font-roboto leading-[15px] font-[400]
             border-0 flex justify-start items-center gap-x-[10px]  !bg-white/0 hover:!border-0 focus:!border-0
              focus:!shadow-none`}
                placeholder=''
                {...props}
            />
        </Form.Item>
    )
};
export default DocumentsInput;
