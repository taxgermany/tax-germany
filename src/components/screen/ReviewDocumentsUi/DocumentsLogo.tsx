import { Input } from 'antd';
import React, { useState } from 'react';

const DocumentsLogo = () => {
    const [logos, setLogos] = useState<(File | string)[]>([]);
    const [logoUrl, setLogoUrl] = useState<string>('')
    const handleLogoChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const selectedFiles = event.target.files;

        if (selectedFiles) {
            const newLogos: string[] = [];
            for (let i = 0; i < selectedFiles.length; i++) {
                const file = selectedFiles[i];
                const logoUrl = URL.createObjectURL(file);
                newLogos.push(logoUrl);
            }
            setLogos((prevLogos) => [...prevLogos, ...newLogos]);
        }
    };



    return (
        <div
            className='flex items-center gap-x-[20px]'>
            {logos.map((item, index) => <div
                className='w-[77px] h-[77px] rounded-[50rem]'>
                <img src={`${item}`} alt={`Logo ${index}`}
                    className='w-full h-full  rounded-[50rem]' />
            </div>)
            }
            <label
                htmlFor="fileInput"
                className={`w-fit py-[9px] px-[17px] bg-[#E8E8E896] block flex flex-col items-center justify-center cursor-pointer rounded-[6px] `}
            >
                <input
                    type="file"
                    id="fileInput"
                    onChange={handleLogoChange}
                    className={`file-input !hidden`}
                />
                <p className='text-[#7E7E7E66]'>Upload logo</p>
                <div className='w-[30px] h-[30px] flex items-center justify-center relative border border-[#7E7E7E66] rounded-[3px]'>
                    <span className='w-[14px] border border-[#7E7E7E66] absolute left-[7px] top-[12.5px]'>
                    </span>
                    <span className='h-[14px] border border-[#7E7E7E66] absolute left-[13px] top-[7px]'>
                    </span>
                </div>
            </label>
        </div>
    );
}

export default DocumentsLogo;
