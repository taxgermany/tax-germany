import {FC} from 'react'

import {ReactComponent as IconCheck} from "../../../assets/icons/check.svg";
interface IDocumentsSquare{
    active?: string | boolean,
    check?:string
    setActive?: (ps:any) => void
}

const DocumentsSquare: FC<IDocumentsSquare> = ({
    check ,
    active,
    setActive
}) =>{
    return(
        <div className='w-[27px] h-[27px] border border-[#7E7E7E] flex items-center justify-center cursor-pointer' onClick={setActive}>
            {check === active || active === true
            ? <IconCheck/>
            : ''}
        </div>
    )
}
export default DocumentsSquare
