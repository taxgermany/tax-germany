import {FC} from 'react'
interface ITextDocumentsMini{
    text: string | any,
    className?:string,
    roboto?:boolean
}

const DocumentsTextMini: FC<ITextDocumentsMini> = ({
    text,
    className,
    roboto = true
}) =>{
    return(
        <p className={`
        ${className}
        ${roboto ? 'font-roboto' : 'font-inter'}
         text-[14px] leading-[18px] font-[600]`}>{text}</p>
    )
}
export default DocumentsTextMini