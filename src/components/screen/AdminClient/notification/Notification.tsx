import React, {FC} from 'react';
import {ReactComponent as Message} from '../../../../assets/icons/sms.svg'
import {notification} from "antd";
import {INotifications} from "../../../../interfaces/admin";
import {Link} from "react-router-dom";
import clsx from "clsx";
import {keys} from "lodash";

interface INotificationComponent {
    notifications: INotifications[]
}

const color: { [key: string]: string } = {
    "APPROVED": "text-[#26AF60]",
    "PAID": "text-[#DC7070]",
    "UPDATED": "text-[#0090FF]",
    "NEW": "text-[#ECBD00]",
    "NEW_APPLICATION": "text-[#ECBD00]",
}

const Notification: FC<INotificationComponent> = ({notifications}) => {

    const wordDivide = (status: string) => {

        const statusWords = status.split('_').map(word => word.charAt(0) + word.slice(1)).join(" ")
        return statusWords
    }

    return (
        <div onClick={e => e.stopPropagation()}>

            <div className="absolute right-[40px] top-[159px] w-[400px]  p-[20px] bg-white
            rounded-[10px] shadow-4xl overflow-auto">
                {notifications.length === 0
                    ? <div>No notifications</div>
                    : notifications.map(item =>
                        <div key={item.userName} className="flex gap-[11px]  border-b border-[#ECECEC]  mb-[10px] last:border-b-0 last:mb-0">
                            <Message className="pt-[5px]"/>
                            <div className="grid gap-[5px] mb-[10px]">
                                <div className="flex items-end flex-wrap">
                                    <div className="text-[16px] text-[#707070] uppercase w-fit font-bold  break-normal">
                                        {item.userName}
                                    </div>
                                    <div className="text-[14] w-fit"><span className="px-[5px]">&#183;</span>
                                        <span className="w-fit">{item.timeOfCreation}</span></div>
                                </div>
                                <div className="text-[14px]">
                                    {!item.status || item.status === "EXPERT"
                                        ? <span>{item.textMessage}</span>
                                        : item.status === "NEW_APPLICATION"
                                            ? <span>A <span
                                                className={clsx(color[item.status])}>New</span> request has been submitted through the system.</span>
                                            : <span>Application is <span
                                                className={clsx(color[item.status])}>{wordDivide(item.status)}</span>. </span>
                                    }
                                    <span className="text-[#209DFD] underline">
                                        <Link to={item.link}
                                              className="text-[#209DFD] underline  font-[300]">{" "}{item.textLink}</Link>
                                    </span>
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default Notification;
