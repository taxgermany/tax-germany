import React, {FC} from 'react';
import {IClients} from "../../../../interfaces/admin";
import {Select} from "antd";
import clsx from "clsx";

const color = [
    {status: "None", color: 'bg-[#EEEEEE]'},
    {status: "NEW_APPLICATION", color: 'bg-[#FFF4C8]'},
    {status: "IN_PROGRESS", color: 'bg-[#B0C6FF]'},
    {status: 'COMPLETED', color: 'bg-[#D2CFF2]'},
    {status: 'PAID', color: 'bg-[#FFCE84]'},
    {status: 'APPROVED_BY_CLIENT', color: 'bg-[#BAF0D6]'},
    {status: 'REJECTED_BY_CLIENT', color: 'bg-[#EEEEEE]'},
    {status: 'APPLICATION_UPDATED_BY_CLIENT', color: 'bg-[#CFE9FD]'},
    {status: 'INFORMATION_REQUESTED_BY_EXPERT_ADMIN', color: 'bg-[#D9FFC2]'},
    {status: 'NEW_MESSAGE_FROM_CLIENT', color: 'bg-[#FDDADA]'},
]

const statusArr = ["In Progress", "Completed", "Information requested by expert", "Paid"]

const statusArray = [
    {value: 'IN_PROGRESS', label: 'In Progress'},
    {value: 'COMPLETED', label: 'Completed'},
    {value: 'INFORMATION_REQUESTED_BY_EXPERT_ADMIN', label: 'Information requested by expert'},
]

interface IDropdownStatus {
    status: string | null
    setEdit: (pt:string) => void
}


const StatusClient: FC<IDropdownStatus> = ({status,  setEdit}) => {
    const statusColor = color.find(item => status === item.status)
    let statusWords = ""
    if (status) {
        statusWords = status.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1)
            .toLowerCase()).join(" ")
    } else {
        statusWords = "None"
    }

    const onDrop = statusArr.find(item => statusWords === item)

    const handleChange = (value: string) => {
        setEdit(value)

    };

    return (
        <div>{
            onDrop ?
                <Select
                    defaultValue={statusWords}
                    className={` ${status==="" || status=== "PAID" || status==="COMPLETED"?"w-[110px]":"w-fit"} 
                    ${statusColor?.color}`}
                    onSelect={handleChange}
                    options={statusArray}
                />
                : <div className={clsx(` w-fit px-[10px] py-[5px] text-sm  ${statusColor?.color}`)}>
                    {statusWords}</div>
        }

        </div>
    );
};

export default StatusClient;
