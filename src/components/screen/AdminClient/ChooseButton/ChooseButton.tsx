import React, {FC, useState} from 'react';
import {ReactComponent as AddLogo} from '../../../../assets/icons/addMenu.svg'
import {ReactComponent as Check} from '../../../../assets/icons/check_circle.svg'
import {ReactComponent as Cancel} from '../../../../assets/icons/cancel.svg'
import {IClients} from "../../../../interfaces/admin";
import {Popover} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";


interface IChooseButton {
    edit: IClients | null
    handleUpdate: (client:IClients) => void
    setEdit: (pt: IClients|null) => void
    client:IClients


}

const ChooseButton: FC<IChooseButton> = ({client,edit,setEdit,handleUpdate}) => {
    const [open, setOpen] = useState(false);
    const content = (item: IClients) => {

        const handleEdit = (item: IClients) => {
            setEdit(item)
            setOpen(false)

        }
        const handleDelete = (item: IClients) => {

            setEdit(item)
            setOpen(false)

        }

        return (
            <div className="flex flex-col gap-5">
                <div onClick={() => handleEdit(item)}
                     className="flex items-center hover:opacity-50 cursor-pointer">
                    <EditOutlined className="mr-2"/>
                    Edit
                </div>
                <div
                    className="flex items-center opacity-100 hover:opacity-50 text-red cursor-pointer">
                    <DeleteOutlined className="mr-2"/>
                    Delete
                </div>
            </div>)
    };

    const handleOpenChange = (newOpen: boolean) => {
        setOpen(newOpen);
    };

    const update=(client:IClients)=>{
        handleUpdate(client)
        setEdit(client)
        setEdit(null)
    }
    return (
        <div className="cursor-pointer  ">
            {
                client.id === edit?.id
                    ? <div className="flex gap-4">
                        <Check onClick={()=>  update(edit)}/>
                        <Cancel onClick={()=>setEdit(null)}/>
                    </div>
                    :
                    <Popover placement="bottomRight" title={false} content={()=>content(client)}
                             open={open} trigger="click" onOpenChange={handleOpenChange}>
                        <div className={"px-[15px] py-[10px]"} onClick={()=>setOpen(true)}>
                            <AddLogo />
                        </div>

                    </Popover>
            }

        </div>
    );
};

export default ChooseButton;
