import React, {FC, useEffect, useState} from 'react';
import {Select} from "antd";
import clsx from "clsx";
import {IClients, IExportAdmin} from "../../../../interfaces/admin";
import {useLazyGetExpertsQuery} from "../../../../services/superAdminService";
import {IOption} from "../../../../interfaces/antdInterface";


interface IDropdownExpert {
    edit:IClients | null
    setEdit: (pt: IClients) => void
    client:IClients
    experts:IOption[]
}
const DropdownExpert:FC<IDropdownExpert> = ({edit,setEdit,client,experts}) => {

    const handleChange = (option:IOption) => {

        if (edit)
            setEdit({...edit,responsibleAgent:option})
    };

    return (
        <div>{
            edit?.id===client.id && experts.length!==0

                ?
                <Select
                    defaultValue={client.responsibleAgent?client.responsibleAgent.label:"None"}
                    className={`bg-[#EEEEEE] w-fit`}
                    onSelect={(value, option)=>handleChange(option)}
                    options={[{value:"0",label:"None"},...experts]}
                />
                :  <div className={clsx(` !font-[400] bg-[#EEEEEE] w-fit px-[10px] py-[5px] `)}>
                    {client.responsibleAgent? client.responsibleAgent.label : "None"}</div>
        }

        </div>
    );
};

export default DropdownExpert;
