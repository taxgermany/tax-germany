import React, {FC, useState} from 'react';
import {ReactComponent as EditLogo} from "../../../../assets/icons/edit-3.svg";
import DropdownStatus from "../Dropdawn/DropdownStatus";
import ChooseButton from "../ChooseButton/ChooseButton";
import {IClients, IExportAdmin} from "../../../../interfaces/admin";
import Pagination from "../pagination/Pagination";
import {useLazyGetExpertsQuery} from "../../../../services/superAdminService";
import {IOption} from "../../../../interfaces/antdInterface";
import DropdownExpert from "../DropdawnExpert/DropdownExpert";
import {useNavigate, useSearchParams} from "react-router-dom";
import {useAppSelector} from "../../../../hooks/useAppSelector";
import {getStoreLocal} from "../../../../utils/local-storage/localStorage";

interface IAdminClientTable {
    allClients: IClients[]
    edit: IClients | null
    setEdit: (pt: IClients | null) => void
    currentPage: number,
    totalPages: number,
    sizePage: number,
    totalNumber: number,
    onPageChange: (pt: number) => void
    setTotalNumber: (pt: number) => void
    experts: IOption[]
    handleUpdate: (client: IClients) => void

}

const AdminClientTable: FC<IAdminClientTable> = ({
                                                     setTotalNumber, sizePage, totalNumber,
                                                     allClients, edit,
                                                     setEdit, totalPages,
                                                     onPageChange, currentPage, experts, handleUpdate
                                                 }) => {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();
    const numbers = Array.from({length: allClients.length}, (_, index) => index + 1 + totalNumber);

    const setNavigate = (item: IClients) => {
        const role = getStoreLocal("user")?.userRole

        if (role === "ROLE_EXPERT") {

            return navigate({
                pathname: `/expert/clients-details/${item.id}`,
                search: `?status=${item.status}`
            })
        }
        if (role === "ROLE_SUPER_ADMIN") {

            return navigate({
                pathname: `/super-admin/clients-details/${item.id}`,
                search: `?status=${item.status}`,
            });

        }

    }

    return (
        <div>
            <table className="w-full">
                <thead>
                <tr className="flex justify-between text-base text-[#A9A8A8] border-b border-b-[#EEE]
                    text-left w-full py-[8px]">
                    <th className="w-[4%] text-center !font-[400]">
                        <div className=" text-center !font-[400]">№</div>
                    </th>
                    <th className="w-[15%] !font-[400]">
                        <div className=" !font-[400]">Client Name</div>
                    </th>
                    <th className="w-[10%] !font-[400]">
                        <div className="!font-[400]">Type</div>
                    </th>
                    <th className="w-[16%] !font-[400]">
                        <div className=" !font-[400]">Document Name</div>
                    </th>
                    <th className="w-[19%] !font-[400]">
                        <div className=" !font-[400]">Status</div>
                    </th>
                    <th className="w-[16%]  !font-[400]">
                        <div className=" !font-[400]">Responsible Agent</div>
                    </th>
                    <th className="w-[13%]  !font-[400]">
                        <div className=" !font-[400]">Apllication Date</div>
                    </th>
                    <th className="w-[8%]  !font-[400] flex justify-center ">
                        <div className="!font-[400]"><EditLogo/></div>
                    </th>
                </tr>
                </thead>
                <tbody>

                {allClients && allClients.map((item, index) =>
                    <tr key={item.id} className={`${edit?.id === item.id ? "bg-[#F8F8F8]" : ""} 
                    flex justify-between items-center  hover:bg-[#F8F8F8]
                    text-base  border-b border-b-[#EEE] text-left w-full py-[12px]`}>
                        <td className="w-[4%] text-center !font-[400]">
                            <div className=" text-center !font-[400]">{numbers[index]}</div>
                        </td>
                        <td className="w-[15%]  !font-[400]" onClick={() => setNavigate(item)}>

                            <div className=" !font-[600] py-[12px] cursor-pointer">{item.clientName}</div>
                        </td>
                        <td className="w-[10%] !font-[400]">
                            <div className="!font-[400] truncate">
                                {item.type === "ROLE_FREELANCER" ? "Freelancer" : "Company"}
                            </div>
                        </td>

                        <td className="w-[16%]  !font-[400]">
                            {item.documentNames.map(docName =>
                                <div
                                    className=" !font-[400] ">{docName}{item.documentNames.length > 1 ? ", " : ""}</div>
                            )}

                        </td>
                        <td className="w-[19%] !font-[400] mr-3">
                            <DropdownStatus status={item.status} client={item} setEdit={setEdit} edit={edit}/>
                        </td>
                        <td className="w-[16%]  !font-[400]">
                            <DropdownExpert experts={experts} client={item} setEdit={setEdit} edit={edit}/>
                            {/*<div
                                className=" !font-[400] bg-[#EEEEEE] w-fit px-[10px] py-[5px] ">
                                {item.responsibleAgent}</div>*/}
                        </td>
                        <td className="w-[13%]  !font-[400]">
                            <div className=" !font-[400]">{item.applicationDate}</div>
                        </td>
                        <td className="w-[8%]  !font-[400] flex justify-center items-center">
                            <div className="!font-[400]">
                                <ChooseButton handleUpdate={handleUpdate} edit={edit} client={item} setEdit={setEdit}/>
                            </div>

                        </td>
                    </tr>)
                }

                </tbody>
            </table>
            <div className={"flex justify-end pt-[10px]"}>
                <Pagination currentPage={currentPage}
                            sizePage={sizePage}
                            totalNumber={totalNumber}
                            setTotalNumber={setTotalNumber}
                            totalPages={totalPages}
                            onPageChange={onPageChange}/>
            </div>
        </div>
    );

};

export default AdminClientTable;
