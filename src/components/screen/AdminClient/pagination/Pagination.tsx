import React, {FC} from 'react';

interface IPagination {
    currentPage: number,
    totalPages: number,
    sizePage: number,
    totalNumber: number,
    onPageChange: (pt: number) => void
    setTotalNumber: (pt: number) => void
}

const Pagination: FC<IPagination> = ({
                                         currentPage, totalPages, onPageChange, setTotalNumber, sizePage,
                                         totalNumber
                                     }) => {

    const pageNumbers = Array.from({length: totalPages}, (_, index) => index + 1);

    const handlePrev = (currentPage: number) => {
        if (currentPage !== 1 && totalNumber !== 0) {
            onPageChange(currentPage - 1)
            setTotalNumber(totalNumber - sizePage)

        }

    }
    const handleNext = (currentPage: number) => {
        if (currentPage !== totalPages) {
            onPageChange(currentPage + 1)
            setTotalNumber(totalNumber + sizePage)
        }

    }
    const handleClick = (pageNumber: number) => {
        onPageChange(pageNumber)
        setTotalNumber(sizePage * (pageNumber - 1))
    }
    return (
        <ul className="flex gap-[5px]">
            <li
                className={`${currentPage === 1 ? 'text-[#CCCCCC]' : 'hover:text-green'} text-[15px] flex items-center justify-center
                cursor-pointer `}
                onClick={() => handlePrev(currentPage)}
            >
                <button className="page-link">Prev</button>
            </li>
            {pageNumbers.map(pageNumber => (
                <li
                    key={pageNumber}
                    className={`${currentPage === pageNumber ? 'bg-green' : ''} 
                     w-[32px] h-[32px] text-[15px] rounded-[3px] border border-[#F1F1F1] 
                     flex items-center justify-center cursor-pointer hover:bg-green`}
                    onClick={() => handleClick(pageNumber)}
                >
                    <button>{pageNumber}</button>
                </li>
            ))}
            <li
                className={`${currentPage === totalPages ? 'text-[#CCCCCC]' : 'hover:text-green'} text-[15px] flex items-center 
                justify-center cursor-pointer `}
                onClick={() => handleNext(currentPage)}
            >
                <button className="page-link">Next</button>
            </li>
        </ul>
    );
};

export default Pagination;
