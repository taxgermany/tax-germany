import React, {FC, MutableRefObject, RefObject} from 'react';
import {INetMonth} from "../../../../interfaces/adminNet";
import clsx from "clsx";
import s from "./AdminNetTable.module.scss";
import {format, parse} from "date-fns";

const title = [
    {"operatingProfit": "Betriebseinnahmen"},
    {"empty": " "},
    {"empty": "Erlöse aus"},
    {"grossRevenue": "betrieblicher Tätigkeit"},
    {"otherIncome": "Sonstige Erlöse"},
    {"amountOfRevenue": "=Summe der Erlöse"},
    {"empty": " "},
    {"vatReceived": "Umsatzsteuer"},
    {"vatRefund": "Umsatzsteuer-Erstattung"},
    {"empty": " "},
    {"advancesReceived": "Erhaltene Anzahlungen"},
    {"empty": " "},
    {"accountsReceivable": "-Zugang Forderungen"},
    {"empty": " "},
    {"grossRevenueInclVat": "Betriebseinnahmen"},
    {"empty": " "},
    {"operatingExpenses": "Betriebsausgaben"},
    {"empty": " "},
    {"officeSupplies": "Material-/Wareneinkauf"},
    {"telephone": "Fremdleistungen"},
    {"personnelCosts": "Personalkosten"},
    {"officeRent": "Raumkosten"},
    {"insuranceExclHealth": "Steuern/Versicherungen/Beiträge"},
    {"publicTransport": "Fahrzeugkosten"},
    {"advertising": "Werbe‐/Reisekosten"},
    {"goods": "Kosten Warenabgabe"},
    {"maintenance": "Instandhaltung/Werkzeuge"},
    {"depreciation": "Abschreibungen"},
    {"other": "Verschiedene Kosten"},
    {"amountOfExpenses": "=Summe der Kosten"},
    {"empty": " "},
    {"prepayments": "Geleistete Anzahlungen"},
    {"empty": " "},
    {"carryingAmount": "Buchwert Anlagenabgänge"},
    {"otherEx": "Sonstige Aufwendungen"},
    {"empty": " "},
    {"tax": "Vorsteuer"},
    {"vatPaidToTaxOffice": "Umsatzsteuer-Zahlung"},
    {"empty": " "},
    {"calculated": "- Verrechnete kalkulatorische Kosten"},
    {"receipt": "- Zugang Verbindlichkeiten"},
    {"empty": " "},
    {"expenses": "Betriebsausgaben"},
    {"empty": " "},
    {"preliminary": "Vorläufiges betriebswirt. Ergebnis"},
    {"empty": " "},
    {"monthlyProfits": " "},
    {"empty": " "},

]


const fieldNamesToAdd10After = [
    "month",
    "operatingProfit",
    "amountOfRevenue",
    "vatRefund",
    "advancesReceived",
    "accountsReceivable",
    "grossRevenueInclVat",
    "operatingExpenses",
    "amountOfExpenses",
    "prepayments",
    "otherEx",
    "vatPaidToTaxOffice",
    "receipt",
    "expenses"
];

interface INetTable {
    dataList: INetMonth[];
    componentRef: RefObject<HTMLDivElement>; // Используйте тип HTMLDivElement, если вы ожидаете ссылку на div
}
const NetTable: FC<INetTable> = ({dataList, componentRef}) => {

    const newList = dataList.map(item => {
        const newMonth = {...item, operatingProfit: null, operatingExpenses: null, preliminary: null}
        const valuesArray = Object.values(newMonth).flatMap((value, index) => {
            return [value, ...(fieldNamesToAdd10After.includes(Object.keys(newMonth)[index]) ? [null] : [])]
        });
        valuesArray.push(null)
        valuesArray.push(null)

        return valuesArray

    })
    const dateMonth = String(newList[0][1]);

    const currentDate = new Date();

    const date = format(currentDate, 'dd.MM.yyyy')
    const currYear = currentDate.getFullYear();

    const parsedDate = parse(dateMonth, 'yyyy-MM-dd', new Date());
    const formattedDate = format(parsedDate, 'MMM yyyy');

    const changeData = (data: string) => {
        const parsedDate = parse(data, 'yyyy-MM-dd', new Date());
        return format(parsedDate, 'MMM/yyyy')
    }

    return (
        <div className={"border px-[20px] text-[11px]  max-w-[1400px]"} ref={componentRef}>
            <div className="flex justify-between py-[23px]">
                <div className="flex justify-center items-center">
                    <div>Max mustermann</div>
                </div>
                <div className="grid justify-items-center ">
                    <div className="text-[12px] font-[500]">Jahresübersicht</div>
                    <div>{formattedDate}</div>
                    <div>
                        <span>SKR : 04 </span>
                        <span>BWA‐Nr.: 1   BWA‐Form : Einnahmen‐Ausgaben‐BWA </span>
                        <span>Wareneinsatz:  Wareneinkauf</span>
                    </div>
                </div>
                <div className="grid items-center">
                    <div>{date}</div>
                    <div>Blatt 1</div>
                </div>
            </div>
            <table className={clsx("border border-[#888888]", s.table)}>
                <thead>
                <tr className={"border-b px-[10px]"}>
                    <th>
                        <div className={"text-left pl-[6px] pr-[6px] text-[11px] font-[400]"}>Bezeichnung</div>
                    </th>
                    {dataList.map(item => <th key={item.id}>
                        <div className={`text-[11px] font-[400] ${dataList.length > 6 ? "" : "px-[10px]"}`}>{
                            changeData(item.month)
                        }</div>
                    </th>)}
                </tr>
                </thead>
                <tbody className={clsx(s.body)}>
                {title.map((item, index) =>
                    <tr>
                        <td>
                            <div className={"pl-[11px] pr-[6px]"}>{Object.values(item)}</div>
                        </td>
                        {newList.map((monthNet,) => (
                            <td key={index} className="text-right border-l border-[#EFEFEF] text-left">
                                <div className={`px-[4px] h-[17.5px]  ${dataList.length > 6 ? "" : "px-[10px]"}`}>
                                    {monthNet[index + 2] === null
                                        ? " "
                                        : Number(monthNet[index + 2]).toFixed(2)}
                                </div>
                            </td>
                        ))}
                    </tr>
                )}

                </tbody>
            </table>
            <div className="py-[12px] ] text-[9px]">
                <div>
                    Das vorläufige Ergebnis entspricht dem derzeitigen Stand der Buchführung.
                    Abschluss‐/Abgrenzungsbuchungen können es noch verändern.
                </div>
                <div className="text-right">
                    <div>Status {currYear}*FAJ</div>
                    <div>Werte in: EUR</div>
                </div>
            </div>
        </div>
    );
};

export default NetTable;
