import React, {FC, useEffect, useState} from 'react';
import NetTable from "./NetTable";
import Pagination from "../pagination/Pagination";
import {useLazyNetDocumentQuery} from "../../../../services/documentService";
import NewButtonGreen from "../../../ui/button/NewButtonGreen";
import ReactToPrint from "react-to-print";
import {Spin} from "antd";
import {INet} from "../../../../interfaces/adminNet";

interface INetDoc {
    id: string | undefined
}

const Net: FC<INetDoc> = ({id}) => {
    const [currentPage, setCurrentPage] = useState(1);
    const [dataNet, setDataNet] = useState<INet | null>(null);
    const [sizePage, setSizePage] = useState(1);
    const [totalPages, setTotalPage] = useState<number | undefined>(undefined);
    const [totalNumber, setTotalNumber] = useState(0);

    const [getNet, {data, isLoading, isSuccess, error}] = useLazyNetDocumentQuery()

    const handlePageChange = (pageNumber: number) => {
        setCurrentPage(pageNumber);
    }

    const getNetDocument = async () => {
        if (id) {
            const res = await getNet({id, page: currentPage, size: sizePage}).unwrap()
            setTotalPage(res.total)
            setCurrentPage(res.page)
            setDataNet(res)
        }
    }


    useEffect(() => {
        getNetDocument()
    }, [currentPage])

    //print
    const componentRef = React.useRef(null);


    const handleAfterPrint = React.useCallback(() => {
        console.log("`onAfterPrint` called"); // tslint:disable-line no-console
    }, []);

    const handleBeforePrint = React.useCallback(() => {
        console.log("`onBeforePrint` called"); // tslint:disable-line no-console
    }, []);


    const reactToPrintContent = React.useCallback(() => {
        return componentRef.current;
    }, [componentRef.current]);


    const reactToPrintTrigger = React.useCallback(() => {

        return <NewButtonGreen text={"Download pdf document"}/>

    }, []);


    return (
        <> {(isLoading)
            ? <Spin size="large" className="absolute top-2/4 left-2/4"/>
            : <div>
                <div className={"flex justify-end pb-[20px]"}>
                    <ReactToPrint
                        content={reactToPrintContent}
                        documentTitle={"Net_profit_determination"}
                        trigger={reactToPrintTrigger}
                    />
                </div>
                {dataNet && <NetTable componentRef={componentRef} dataList={dataNet.responseList}/>}
                <div className={"flex justify-end pt-[10px]"}>
                    {totalPages && <Pagination currentPage={currentPage}
                                               sizePage={sizePage}
                                               totalPages={totalPages}
                                               totalNumber={totalNumber}
                                               setTotalNumber={setTotalNumber}
                                               onPageChange={handlePageChange}/>}
                </div>
            </div>}
        </>
    );
};

export default Net;
