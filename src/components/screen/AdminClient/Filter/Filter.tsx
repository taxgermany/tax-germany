import React, {ChangeEventHandler, FC, useEffect, useState} from 'react';
import FilterSelect from "../../../ui/select/selectForAdmin/FilterSelect";
import {optionsDocument, optionsStatus} from "../../../../utils/admin/Admin";
import SearchInput from "../../../ui/input/inputForAdmin/SearchInput";
import {
    getAllClients,
    IClients,
    IExport,
    IExportAdmin,
    IFilterParams,
    ISearchParams, ISearchParamsAdmin
} from "../../../../interfaces/admin";
import {
    useLazyFetchFilterQuery,
    useLazyFetchSearchExportsQuery,
} from "../../../../services/exportsServices";
import {useDebounce} from "../../../../hooks/useDebounce";
import {IOption} from "../../../../interfaces/antdInterface";
import {useAppSelector} from "../../../../hooks/useAppSelector";


interface IFilter {
    setClients: (pt: getAllClients|undefined) => void
    experts:IOption[]
    isSuccessExperts:boolean,
    currentPage:number
    sizePage:number,
    getSearch:(pt:ISearchParams)=>void
    getFilter:(pt:IFilterParams)=>void
}

const Filter: FC<IFilter> = ({ setClients,experts,isSuccessExperts,
                             sizePage,currentPage,getSearch,getFilter,
                                 }) => {

    const [searchText, setSearchText] = useState<string>("")
    const [searchParams, setSearchParams] = useState<{docId?:string,status?:string,expertId?:string}>({})
  /*  const [experts, setExpert] = useState<IOption[]>([] as IOption[])*/
    const {client} = useAppSelector(state => state.authSlice)
    const debouncedSearch = useDebounce(searchText)


    const onChangeSearch: ChangeEventHandler<HTMLInputElement> = (e) => {
        if(client) getSearch({search:e.target.value, page:currentPage,size:sizePage,expertId:client.id})
    }
    const handleChange = (params: { docId?: string, status?: string, expertId?: string }) => {

        setSearchParams(prev => ({
            ...prev,
            ...params
        }));
        getFilter({...searchParams, ...params,size:sizePage,page:currentPage})

    };


    return (
        <div>
            {isSuccessExperts &&
                <div className="flex gap-x-[25px] justify-between items-center">
                    <div className="flex gap-x-[15px] ">
                        <FilterSelect text={"select"} selectName={"filterSelect"}
                                      placeholder="Filter by : Document Name"
                                      onChange={(value) => handleChange({docId: value})}
                                      className="!w-[230px] !h-[38px]" options={optionsDocument}/>

                        <FilterSelect text={"select"} selectName={"filterSelect"} placeholder="Filter by : Status"
                                      onChange={(value) =>handleChange({status: value})}
                                      className="!w-[150px] !h-[38px]" options={optionsStatus}/>

                        {client && client?.userRole==="ROLE_SUPER_ADMIN" && <FilterSelect text={"select"} selectName={"filterSelect"}
                                       placeholder="Filter by : Responsible agent" className="!w-[230px] !h-[38px]"
                                       onChange={(value) => handleChange({expertId: value})}
                                       options={[{value: "", label: 'None'},...experts,]}/>}
                    </div>
                    <SearchInput onChange={onChangeSearch}/>
                </div>
            }
        </div>
    );
};

export default Filter;
