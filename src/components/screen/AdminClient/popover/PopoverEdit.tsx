import React, {FC} from 'react';
import {Popover} from "antd";
interface IPopoverEdit {

}
const PopoverEdit:FC<IPopoverEdit> = () => {
    const content = (
        <div>
            <p>Content</p>
            <p>Content</p>
        </div>
    );
    return (
        <div>
            <Popover placement="bottomRight" title={false} content={content} trigger="click">
                {}
            </Popover>
        </div>
    );
};

export default PopoverEdit;