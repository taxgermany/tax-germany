import { ReactComponent as Arrow } from "../../../assets/icons/edit.svg";
import { ReactComponent as IconInfon } from "../../../assets/icons/info.svg";
import ButtonGray from "components/ui/button/ButtonGray";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import { Form } from "antd";
import { ChangeEvent, FC, useState, useEffect } from 'react'
import cls from './ApplicationReview.module.scss'
import SelectComponent from "components/ui/select/SelectComponent";
import { fourYears, gender, months, monthsEnums, years } from "utils/Arrays";
import LoadingMain from "components/ui/loading/LoadingMain";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import Upload from "components/ui/upload/Upload";
import { useAppSelector } from "hooks/useAppSelector";
import { IReviewBussines, IReviewUser, IReviewYour, dateMonthe } from "interfaces/reaview";
import { useFetchReviewBussinesQuery, useFetchReviewUserQuery, useFetchReviewYourQuery, useUpdateReviewBussinesMutation, useUpdateReviewUserMutation, useUpdateReviewYourMutation } from "services/reviewServices";
import { useCreateFileNameBusinessMutation, useLazyFetchFileNameBusinessQuery } from "services/fileService";
import { IFileName } from "interfaces/ProfitsInterface";
import { useFetchCountryQuery, useFetchNationalityQuery } from "services/arayServices";


interface IappReaviewData {

    setActive: (pr: string) => void;
    active?: string,
    text: string
}

const AppReaviewData: FC<IappReaviewData> = ({
    setActive,
    active,
    text }) => {

    const [data, setData] = useState<IReviewYour>({} as IReviewYour)
    const [oldData, setOldData] = useState<IReviewYour>({} as IReviewYour)
    const [loading, setLoading] = useState(true)
    const [dataBussines, setDataBussines] = useState<IReviewBussines>({} as IReviewBussines)
    const [oldDataBusiness, setOldDataBusiness] = useState<IReviewBussines>({} as IReviewBussines)
    const [dataUser, setDataUser] = useState<IReviewUser>({} as IReviewUser)
    const [oldDataUser, setOldDataUser] = useState<IReviewUser>({} as IReviewUser)
    const [fileBusiness, setFileBusiness] = useState<File | null | IFileName>(null);
    const [country, setCountry] = useState<any[]>([])
    const [nation, setNation] = useState<any[]>([])
    const [becomeFreelancerYear, setBecomeFreelancerYear] = useState('')
    const [becomeFreelancerMonthe, setBecomeFreelancerMonthe] = useState('')

    const { client } = useAppSelector(state => state.authSlice)


    // get
    const { data: getUser, isSuccess: isSuccessUser } = useFetchReviewUserQuery({ clientId: client?.id || '' })
    const { data: getYour, isSuccess } = useFetchReviewYourQuery({ clientId: client?.id || '' })
    const { data: getBusiness, isSuccess: isSuccessBusiness } = useFetchReviewBussinesQuery({ clientId: client?.id || '' })
    const [getFileBusiness] = useLazyFetchFileNameBusinessQuery()
    const { data: getContr } = useFetchCountryQuery()
    const { data: getNation } = useFetchNationalityQuery()
    // update

    const [updateData] = useUpdateReviewYourMutation()
    const [updateBusiness] = useUpdateReviewBussinesMutation()
    const [updateUser] = useUpdateReviewUserMutation()
    const [createFile] = useCreateFileNameBusinessMutation()
    //

    const fetchingUserDatas = async () => {
        try {
            setLoading(true)
            if (getUser && getYour && getBusiness) {
                const businessCopy = { ...getBusiness };
                const userCopy = { ...getUser };
                const yourCopy = { ...getYour };
                const resFile = await getFileBusiness({ userId: client?.id || 0 }).unwrap()

                await setDataUser(getUser);
                await setOldDataUser(userCopy);
                await setData(getYour);
                await setOldData(yourCopy);
                await setDataBussines(getBusiness);
                await setOldDataBusiness(businessCopy);
                await setFileBusiness(resFile)
                setLoading(false);
            }

        } catch (error: any) {
            console.log(error.message);

        }
    }
    async function fetchDropDauwn() {
        try {
            if (getContr && getNation) {
                await setCountry(getContr);
                await setNation(getNation);
            }
        } catch (error) {
            console.error(error);
        }
    }


    useEffect(() => {

        const initialFetch = async () => {
            await fetchingUserDatas();
            fetchDropDauwn();
        };

        initialFetch();
    }, [isSuccess, isSuccessUser, isSuccessBusiness]);

    const updateName = (textName: string) => {
        const updateText = textName.charAt(0) + textName.slice(1).toLowerCase()
        return updateText
    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const btnPut = async () => {
        try {
            const becomeDate = data?.becomeFreelancerDate;

            if (becomeFreelancerMonthe !== '') {
                setData({
                    ...data,
                    becomeFreelancerDate: `${becomeDate.split('-')[0]}-${becomeFreelancerMonthe}-${becomeDate.split('-')[2]}`
                });
            } else if (becomeFreelancerYear !== '') {
                setData({
                    ...data,
                    becomeFreelancerDate: `${becomeFreelancerYear}-${becomeDate.split('-')[1]}-${becomeDate.split('-')[2]}`
                });
            }
            console.log(client?.id);

            await updateData({
                clientId: client?.id || '',
                data: data
            });

            await updateBusiness({
                clientId: client?.id || '',
                data: dataBussines
            });

            await updateUser({
                clientId: client?.id || '',
                data: dataUser
            });
            if (fileBusiness) {
                let fileToUpload: File | null = null;

                // Проверяем, является ли fileBusiness экземпляром FormData
                if (fileBusiness instanceof FormData) {
                    // Извлекаем File из FormData
                    const file = fileBusiness.get("file") as File;

                    // Проверяем, является ли извлеченное значение File
                    if (file instanceof File) {
                        fileToUpload = file;
                    } else {
                        // Обработка случая, если fileBusiness - это FormData, но внутри нет File
                        // Вы можете обработать этот случай в соответствии с логикой вашего приложения
                    }
                } else if (fileBusiness instanceof File) {
                    fileToUpload = fileBusiness;
                }

                if (fileToUpload) {
                    await createFile({
                        userId: client?.id || 0,
                        data: fileToUpload
                    });
                }
            }

            setActive('')
        } catch (error) {
            // Обработка ошибки здесь
            console.error('Произошла ошибка:', error);
        }
    }

    const btnCancel = () => {
        setData(oldData)
        setDataBussines(oldDataBusiness)
        setDataUser(oldDataUser)
        setActive('')
    }
    return (
        <>
            {loading && data !== getYour && dataUser !== getUser && dataBussines !== getBusiness ? <LoadingMain /> :
                <Form
                    name="appData"
                    layout="horizontal"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    onFinish={btnPut}
                    initialValues={{
                        'email': dataUser.email,
                        'password': '12345678',
                        'phone number': dataUser.phoneNumber,
                        gender: data.gender,
                        FirstName: data.firstname,
                        SecondName: data.lastname,
                        DateOfBirdth: data.dateOfBirth,
                        ContrBirth: updateName(data?.country || ''),
                        Nationality: updateName(data?.nationality || ''),
                        typeFreelancing: data.typeOfFreelance,
                        TimeFreelancerM: data.becomeFreelancerDate ? dateMonthe.filter((item, index) => index + 1 === parseInt(data.becomeFreelancerDate.split('-')[1]) ? item : '') : '',
                        TimeFreelancerY: data.becomeFreelancerDate ? data.becomeFreelancerDate.split('-')[0] : '',
                        FromM: data.fromMonth,
                        FromY: `${data.fromYear}`,
                        ToM: data.toMonth,
                        ToY: `${data.toYear}`,
                        businessName: dataBussines.businessName,
                        bisinessAddress: dataBussines.city,
                        postalCod: dataBussines.postalCode,
                        street: dataBussines.street,
                        building: dataBussines.building,
                        businessPhone: dataBussines.businessPhone,
                        businessEmail: dataBussines.businessEmail,
                        businessWebsite: dataBussines.website,
                        FreelanceType: `${dataBussines.freelanceType === 'COMMERCIAL' ? 'Commercial (Gewerblich)' : dataBussines.freelanceType === 'INTELLECTUAL' ? 'Intellectual / artisitic (Freiberuflich)' : ''}`,
                        workPlace: textNomal(`${dataBussines.workPlace}`),
                        anotherWorkPlace: dataBussines.anotherWorkPlace,
                    }}
                >
                    <div className="pt-[10px] m-0 !w-full pr-[40px] ease-[1s] sticky top-[-40px] bg-white flex justify-between items-center flex-wrap z-[10]">
                        <div className="w-3/5 flex justify-between items-center">
                            <p className="text-[28px] font-[600]">Personal Data</p>
                            <div className="cursor-pointer"
                                onClick={() => setActive(text)}
                            >
                                <Arrow />
                            </div>
                        </div>
                        <div className={`${cls.fadeInOut} ${active === text ? cls.blockFlex : 'hidden'}`}>
                            <NewButtonGreen
                                text="Confirm"
                                height="45px"
                                className="!w-[140px]" />
                            <ButtonGray
                                onClick={() => btnCancel()}
                                text='Cancel'
                                size="!w-[140px] !h-[45px] hover:shadow-md hover:shadow-lightGray"
                                className="rounded-[4px] " />
                        </div>
                        <div className="w-full mt-[10px] h-[1px] bg-green"></div>
                    </div>
                    <div className="w-3/5 mt-[25px] z-[1] grid gap-y-[20px]">
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Email</p>
                            <InputNoAppearance
                                type="email"
                                border={true}
                                disabled={true}
                                inputName="email"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Password</p>
                            <InputNoAppearance
                                type="password"
                                border={true}
                                disabled={true}
                                inputName="password"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Phone Number</p>
                            <InputNoAppearance
                                type="text"
                                border={true}
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setDataUser({ ...dataUser, phoneNumber: e.target.value })}
                                inputName="phone number"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Mr / Mrs</p>
                            <SelectComponent
                                background={false}
                                options={gender}
                                disabled={active === text ? false : true}
                                border={true}
                                onChange={(selectedOption) => setData({ ...data, gender: `${selectedOption}` })} // Обновите активную опцию при изменении значения
                                selectName="gender"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">First name</p>
                            <InputNoAppearance
                                type="text"
                                border={true}
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, firstname: e.target.value })}
                                inputName="FirstName"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Second name</p>
                            <InputNoAppearance
                                type="text"
                                border={true}
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, lastname: e.target.value })}
                                inputName="SecondName"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Date of birth</p>
                            <InputNoAppearance
                                type="date"
                                border={true}
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, dateOfBirth: e.target.value })}
                                inputName="DateOfBirdth"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Contr of birth</p>
                            <SelectComponent
                                options={country}
                                disabled={active === text ? false : true}
                                border={true}
                                onChange={(selectedOption) => setData({ ...data, country: `${selectedOption}` })}
                                selectName="ContrBirth"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Nationality</p>
                            <SelectComponent
                                background={false}
                                options={nation}
                                disabled={active === text ? false : true}
                                border={true}
                                onChange={(selectedOption) => setData({ ...data, nationality: `${selectedOption}` })}
                                selectName="Nationality"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Type of freelancing {`(industry)`}</p>
                            <InputNoAppearance
                                background={false}
                                disabled={active === text ? false : true}
                                border={true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, typeOfFreelance: e.target.value })}
                                inputName="typeFreelancing" />

                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Time to be a freelancer</p>
                            <div className="flex gap-x-[15px] items-center">
                                <div className=" m-0">
                                    <SelectComponent
                                        background={false}
                                        options={months}
                                        // defaultValue={freelanceGetDate(data?.becomeFreelancerDate, true)}
                                        disabled={active === text ? false : true}
                                        border={true}
                                        size="!w-[272.5px] !h-[48px]"
                                        onChange={(e) => setBecomeFreelancerMonthe(`${e}`)}
                                        selectName="TimeFreelancerM"
                                    />
                                </div>
                                <div className="relative m-0">
                                    <SelectComponent
                                        background={false}
                                        // defaultValue={freelanceGetDate(data?.becomeFreelancerDate, false, true)}
                                        options={years.reverse()}
                                        disabled={active === text ? false : true}
                                        border={true}
                                        size="!w-[272.5px] !h-[48px]"
                                        onChange={(selectedOption) => setBecomeFreelancerYear(`${selectedOption}`)}
                                        selectName="TimeFreelancerY"
                                    />
                                    <IconInfon className="absolute right-[-30px] top-[13px]" />
                                </div>
                            </div>
                        </div>
                        <div >
                            <p className="mb-[8px] text-[14px] text-[#B6B6B6]">Income proof</p>
                            <div className="!w-[107%] grid gap-y-[20px] m-0">
                                <div className="flex justify-between items-center">
                                    <p className="font-[400]">FROM</p>
                                    <div className="flex gap-x-[15px] items-center w-[90%]">
                                        <div>
                                            <SelectComponent
                                                background={false}
                                                options={monthsEnums}
                                                disabled={active === text ? false : true}
                                                border={true}
                                                size="!w-[272.5px] !h-[48px]"
                                                selectName="FromM"
                                            />
                                        </div>
                                        <div className="w-[47%]">
                                            <SelectComponent
                                                background={false}
                                                options={fourYears}
                                                disabled={active === text ? false : true}
                                                border={true}
                                                size="!w-[272.5px] !h-[48px]"
                                                selectName="FromY"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="flex justify-between items-center">
                                    <p className="font-[400]">TO</p>
                                    <div className="flex gap-x-[15px]  items-center w-[90%]">
                                        <div className="">
                                            <SelectComponent
                                                background={false}
                                                options={monthsEnums}
                                                disabled={active === text ? false : true}
                                                border={true}
                                                size="!w-[272.5px] !h-[48px]"
                                                selectName="ToM"
                                            />
                                        </div>
                                        <div className="">
                                            <InputNoAppearance
                                                background={false}
                                                readOnly
                                                disabled={true}
                                                border={true}
                                                wh="!w-[272.5px] !h-[48px]"
                                                className="text-gray-500" // Применяем класс text-gray-500
                                                inputName="ToY"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <p className="font-[400] text-[22px]">Business details</p>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Business name</p>
                            <InputNoAppearance
                                border={true}
                                type='text'
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, businessName: e.target.value })}
                                inputName="businessName"
                            />
                        </div>
                        <div className="w-[650px] flex flex-wrap gap-y-[20px] gap-x-[19px]">
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Business address: City</p>
                                <InputNoAppearance
                                    type='text'
                                    border={true}
                                    disabled={active === text ? false : true}
                                    wh="!w-[270px] !h-[48px]"
                                    onChange={(selectedOption) => setDataBussines({ ...dataBussines, city: `${selectedOption.target.value}` })}
                                    inputName="bisinessAddress"
                                />
                            </div>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Postal code</p>
                                <InputNoAppearance
                                    type='number'
                                    border={true}
                                    disabled={active === text ? false : true}
                                    wh="!w-[270px] !h-[48px]"
                                    onChange={(selectedOption) => setDataBussines({ ...dataBussines, postalCode: `${selectedOption}` })}
                                    inputName="postalCod"
                                />
                            </div>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Street</p>
                                <InputNoAppearance
                                    type='text'
                                    border={true}
                                    disabled={active === text ? false : true}
                                    wh="!w-[270px] !h-[48px]"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, street: e.target.value })}
                                    inputName="street"
                                />
                            </div>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Building</p>
                                <InputNoAppearance
                                    type='text'
                                    border={true}
                                    disabled={active === text ? false : true}
                                    wh="!w-[270px] !h-[48px]"
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, building: e.target.value })}
                                    inputName="building"
                                />
                            </div>
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Business phone</p>
                            <InputNoAppearance
                                border={true}
                                type='text'
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, businessPhone: e.target.value })}
                                inputName="businessPhone"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Business email address</p>
                            <InputNoAppearance
                                border={true}
                                type='email'
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, businessEmail: e.target.value })}
                                inputName="businessEmail"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-[14px] text-[#B6B6B6]">Business website</p>
                            <InputNoAppearance
                                border={true}
                                type='text'
                                disabled={active === text ? false : true}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setDataBussines({ ...dataBussines, website: e.target.value })}
                                inputName="businessWebsite"
                            />
                        </div>
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-14px text-[#B6B6B6]">Freelace type</p>
                            <SelectComponent
                                background={false}
                                options={[
                                    { value: 'COMMERCIAL', label: 'Commercial (Gewerblich)' },
                                    { value: 'INTELLECTUAL', label: 'Intellectual / artisitic (Freiberuflich)' }]}
                                disabled={active === text ? false : true}
                                border={true}
                                onChange={(selectedOption) => setDataBussines({ ...dataBussines, freelanceType: `${selectedOption}` })}
                                selectName="FreelanceType"
                            />
                        </div>
                        {`COMMERCIAL` === dataBussines.freelanceType ?
                            <>
                                <div className={`${active === text ? '' : fileBusiness?.name ? '' : 'hidden'}`}>
                                    <Upload
                                        fileId="file"
                                        fileCheck={fileBusiness as File | null}
                                        setFileCheck={setFileBusiness as React.Dispatch<React.SetStateAction<File | null>>}
                                        fileTitle="Trade registration form (Gewerbeanmeldung)"
                                        disabled={active === text ? false : true}

                                    />
                                </div>
                                <div className={`w-[80%]`}>
                                    <textarea
                                        className={`w-[100%] h-[92px] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none ${active === text ? '' : 'hidden'}`}
                                        name="freelanceType"
                                        id="freelanceType"
                                        placeholder="Why you can’t submit the document?"
                                        defaultValue={dataBussines.cantUploadTradeRegistration}
                                        disabled={active === text ? false : true}
                                    ></textarea>
                                </div>
                            </>
                            : ''

                        }
                        <div className="grid gap-y-[8px] m-0">
                            <p className="text-14px text-[#B6B6B6]">Work plays</p>
                            <SelectComponent
                                background={false}
                                options={[
                                    { value: 'PRIVATE_HOME', label: 'Private home' },
                                    { value: 'SEPARATE_BUSINESS_OFFICE', label: 'Separate business office' },
                                    { value: 'RENTED_BEDROOM_OFFICE_COMBO', label: 'Rented bedroom office combo' },
                                    { value: 'OTHER_PLACE', label: 'Other place' },]}
                                disabled={active === text ? false : true}
                                border={true}
                                onChange={(selectedOption) => setDataBussines({ ...dataBussines, workPlace: `${selectedOption}` })}
                                selectName="workPlace"
                            />
                            <div className={`w-[80%] ${active === text && dataBussines.workPlace === 'OTHER_PLACE' ? '' : 'hidden'}`}>
                                <textarea
                                    className={`w-[100%] h-[92px] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none `}
                                    name="workPlace"
                                    id="workPlace"
                                    placeholder="What is the other place?"
                                    onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setDataBussines({ ...dataBussines, anotherWorkPlace: e.target.value })}
                                    defaultValue={dataBussines.anotherWorkPlace}
                                    disabled={active === text ? false : true}
                                ></textarea>
                            </div>
                        </div>
                    </div>
                </Form>}
        </>
    )
};
export default AppReaviewData
