import { FC, useState, useEffect, ChangeEvent } from "react";
import { ReactComponent as Arrow } from "../../../assets/icons/edit.svg";
import { ReactComponent as IconInfon } from "../../../assets/icons/info.svg";
import ButtonGray from "components/ui/button/ButtonGray";
import cls from './ApplicationReview.module.scss'
import { Button, Form } from "antd";
import SelectComponent from "components/ui/select/SelectComponent";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import { yesNo } from "utils/Arrays";
import CheckBox from "components/ui/button/CheckBox";
import LoadingMain from "components/ui/loading/LoadingMain";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import { useAppSelector } from "hooks/useAppSelector";
import { IMiniTable, IMiniTableWithTotal } from "../../../interfaces/TableInterface";
import { useLazyFetchReviewPensionMiniTableQuery, useUpdateReviewPensionMiniTableMutation } from "services/reviewPensionMiniTableServices";
import { useFetchAllYearQuery } from "services/yearService";
import { IYear } from "interfaces/YearInterface";
import RowTable from "components/ui/table/RowTable";
import { IReviewLife, IReviewPension } from "interfaces/reaview";
import { useFetchReviewLifeQuery, useFetchReviewPensionQuery, useUpdateReviewLifeMutation, useUpdateReviewPensionMutation } from "services/reviewServices";
interface IAppReviewPensionContr {
    setActive: (pr: string) => void;
    active?: string,
    text: string
}

const AppReviewPensionContr: FC<IAppReviewPensionContr> = ({
    setActive,
    active,
    text }) => {



    const [data, setData] = useState<IReviewPension>({} as IReviewPension)
    const [oldData, setOldData] = useState<IReviewPension>({} as IReviewPension)
    const [dataLife, setDataLife] = useState<IReviewLife>({} as IReviewLife);
    const [oldDataLife, setOldDataLife] = useState<IReviewLife>({} as IReviewLife)
    const [loading, setLoading] = useState(true)

    const { client } = useAppSelector(state => state.authSlice)
    // Table
    const currentDate = new Date();
    const currYear = currentDate.getFullYear();
    const [currentYear, setCurrentYear] = useState(0)
    const [dataMiniChange, setDataMiniChange] = useState<boolean>(false);
    const [dataMini, setDataMini] = useState<IMiniTableWithTotal | null>(null);

    const [getTable, { data: table = {} as IMiniTableWithTotal, error, isLoading, isSuccess }] = useLazyFetchReviewPensionMiniTableQuery()

    const [UpdateTableMini] = useUpdateReviewPensionMiniTableMutation()

    const updateData = async () => {
        if (dataMiniChange) {
            const { total, ...newData } = dataMini as IMiniTableWithTotal
            const res = await UpdateTableMini({ data: newData as IMiniTable, yearId: currentYear })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini(res.data as IMiniTableWithTotal)
            setDataMiniChange(false)
        }
    }

    const handleYear = (id: number) => {

        setCurrentYear(id)
        if (client) {
            getTable({
                userId: client.id,
                yearId: id,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        } updateData()

    }

    const {
        data: years = [] as IYear[],
        isSuccess: isSuccessYear

    } = useFetchAllYearQuery(client?.id || 0);



    useEffect(() => {
        if (isSuccessYear && client && years.length > 0) {
            setCurrentYear(years[0].id || 0)
            getTable({
                userId: client.id,
                yearId: years[0].id || 0,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        }
    }, [isSuccessYear])
    // end Table

    const { data: getData, isSuccess: isSuccessPension } = useFetchReviewPensionQuery({ clientId: client?.id || '' })
    const { data: getLifeData, isSuccess: isSuccessLife } = useFetchReviewLifeQuery({ clientId: client?.id || '' })
    // Update
    const [updateDataPension] = useUpdateReviewPensionMutation()
    const [updateDataLife] = useUpdateReviewLifeMutation()
    //
    const fetchingUserDatas = async () => {
        try {
            setLoading(true)
            if (getData && getLifeData) {
                console.log(getData);
                await setData(getData)
                await setOldData({ ...getData })
                await setDataLife(getLifeData)
                await setOldDataLife({ ...getLifeData })
                await setLoading(false)
            }
        } catch (error: any) {
            console.log(error.message);

        }
    }
    useEffect(() => {
        fetchingUserDatas()
    }, [isSuccessPension, isSuccessLife])

    const ofTextBoolean = (item: string, text: string) => {
        if ('Yes' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: true
            }))
        } else if ('No') {
            setData((prevData) => ({
                ...prevData,
                [item]: false
            }))

        }
    }
    const ofBooleanText = (item: string) => {

        if (data[item as keyof IReviewPension] === true) {
            return 'Yes';
        } else {
            return 'No';
        }

    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const initialValuess = (dat: IReviewPension, datLife: IReviewLife) => (
        {
            makePensionContribution: ofBooleanText('makePensionContribution'),
            pensionNumber: dat.pensionNumber || ' ',
            pensionSecurity: textNomal(`${dat.pensionSecurity}`) || ' ',
            otherFormPension: dat.otherFormPension || ' ',
            outsideThePeriod: dat.outsideThePeriod || ' ',
            outsideThePeriodText: dat.outsideThePeriodText || ' ',
            paidFully: ofBooleanText('planningToHaveForm'),
            insuranceAmount: datLife.insuranceAmount || ' ',
            companyName: datLife.companyName || ' ',
            address: datLife.address || ' ',
            insuranceNumber: datLife.insuranceNumber || ' '
        }
    )
    const btnPut = async () => {
        console.log(dataLife);

        if (data.makePensionContribution === false) {
            // Не выполнять проверку на заполненность данных, если makePensionContribution === false
            setData({
                ...data,
                pensionNumber: '',
                pensionSecurity: '',
                otherFormPension: '',
                planningToHaveForm: false,
                outsideThePeriod: false,
                outsideThePeriodText: '',
                paidFully: false,
            })

            await updateDataPension({
                clientId: client?.id || '',
                data: data
            });
        } else {
            // Выполнить проверку на заполненность данных, если makePensionContribution === true

            // Далее выполните обновление данных пенсионных взносов
            await updateDataPension({
                clientId: client?.id || '',
                data: data
            });

            if (data.pensionSecurity !== 'LIFE_INSURANCE') {
                await setDataLife({
                    ...dataLife,
                    insuranceAmount: '',
                    companyName: '',
                    address: '',
                    insuranceNumber: ''
                })
                await updateDataLife({
                    clientId: client?.id || '',
                    data: dataLife
                });

            } else {
                await updateDataLife({
                    clientId: client?.id || '',
                    data: dataLife
                });
            }
        }

        setActive('');
    };

    const btnCancel = () => {
        setData(oldData)
        setDataLife(oldDataLife)
        setActive('')
    }
    return (
        <>
            {loading ? <LoadingMain /> : <Form
                name="appPension"
                layout="horizontal"
                labelCol={{ span: 30 }}
                wrapperCol={{ span: 30 }}
                onFinish={btnPut}
                initialValues={initialValuess(data, dataLife)}
            >

                <div className="pt-[10px] m-0 pr-[40px] ease-[1s] sticky top-[-40px] bg-white flex justify-between items-center flex-wrap z-[10]">
                    <div className="w-3/5 flex justify-between items-center">
                        <p className="text-[28px] font-[600]">Pension contributions</p>
                        <div className="cursor-pointer"
                            onClick={() => setActive(text)}
                        >
                            <Arrow />
                        </div>
                    </div>
                    <div className={`${cls.fadeInOut} ${active === text ? cls.blockFlex : 'hidden'}`}>
                        <NewButtonGreen
                            text="Confirm"
                            height="45px"
                            className="!w-[140px]" />
                        <ButtonGray
                            onClick={() => btnCancel()}
                            text='Cancel'
                            size="!w-[140px] !h-[45px] hover:shadow-md hover:shadow-lightGray"
                            className="rounded-[4px] " />
                    </div>
                    <div className="w-full mt-[10px] h-[1px] bg-green"></div>
                </div>
                <div className="w-3/5 mt-[25px] grid gap-y-[20px]">
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Did you make any pension contributions?   </p>
                        <SelectComponent
                            background={false}
                            options={yesNo}
                            disabled={active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => ofTextBoolean('makePensionContribution', `${selectedOption}`)}
                            selectName="makePensionContribution"
                        />
                    </div>
                    <div className="grid gap-y-[8px] m-0 relative">
                        <p className="text-[14px] text-[#B6B6B6]">What is your pension number?</p>
                        <InputNoAppearance
                            type='number'
                            border={true}
                            disabled={data.makePensionContribution && active === text ? false : true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, pensionNumber: e.target.value })}
                            inputName="pensionNumber"
                        />
                        <IconInfon className="absolute right-[2rem] bottom-[15px]" />
                    </div>
                    <div className="grid gap-y-[30px] mt-[30px] m-0">
                        <p className="text-[20px] font-[500]">State your pension contributions for the audited period</p>
                        <p>Expenses should be based on the date of payment, not invoice.</p>

                    </div>
                    <div className="flex gap-x-[10px] m-0">
                        {
                            isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                className={`${currentYear === item.id
                                    ? "border border-black"
                                    : ""}    !h-[32px] rounded-[15px]`}

                                onClick={() => handleYear(item.id)}>
                                {currYear === item.yearValue
                                    ? "Current year"
                                    : item.yearValue}</Button>)
                        }
                    </div>
                    <div className={`!w-[160.5%] max-w-[1625px] ${cls.scroll}`}>
                        {dataMini && isSuccess && <div className={cls.scroll}>
                            <RowTable name={"Pension contributions"}
                                disabled={data.makePensionContribution && active === text ? false : true}
                                data={dataMini}
                                setDataSource={setDataMini}
                                setDataMiniChange={setDataMiniChange} />
                        </div>}
                    </div>
                    <div className="!mb-[30px] m-0 flex items-center gap-x-[30px] w-[110%]">
                        <div className="w-[25%]">
                            <p className="flex justify-start items-end gap-x-[5px] font-[500] text-[12px]">
                                I made contributions outside of the audited period
                                <span>
                                    <IconInfon />
                                </span>
                            </p>
                        </div>
                        <CheckBox
                            className={`${active === text ? 'cursor-pointer' : '!cursor-not-allowed'}`}
                            active={data.outsideThePeriod}
                            setActive={active === text ? (newValue) => setData({ ...data, outsideThePeriod: newValue }) : () => { }}
                        />

                        <div className={`w-[80%] ${data.outsideThePeriod ? '' : 'hidden'}`}>
                            <textarea
                                className={`w-[100%] text-[#B6B6B6] h-[92px] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none ${active === text ? 'text-black' : 'hidden'}`}
                                name="outsideThePeriod"
                                id="outsideThePeriod"
                                disabled={data.makePensionContribution && active === text ? false : true}
                                defaultValue={data.outsideThePeriodText}
                                placeholder="Explain reason why not during the audited period ?"
                                onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setData({ ...data, outsideThePeriodText: e.target.value })}
                            ></textarea>
                        </div>
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Do you have any other form of pension security?</p>
                        <SelectComponent
                            background={false}
                            options={[{ value: 'LIFE_INSURANCE', label: 'Life insurance' },
                            { value: 'OTHER_FORM', label: 'Other from' },
                            { value: 'NO', label: 'No' }]}
                            disabled={data.makePensionContribution && active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => setData({ ...data, pensionSecurity: `${selectedOption}` })}
                            selectName="pensionSecurity"
                        />
                        {
                            data.pensionSecurity === 'OTHER_FORM' ? (
                                <div className={`w-[80%]`}>
                                    <textarea
                                        className={`w-[100%] h-[92px] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none ${active === text ? '!text-black' : 'hidden'}`}
                                        name="OTHER_FROM"
                                        id="OTHER_FROM"
                                        placeholder="What other form of penstion security do you have?"
                                        disabled={data.makePensionContribution && active === text ? false : true}
                                        defaultValue={data.outsideThePeriodText}
                                        onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setData({ ...data, outsideThePeriodText: e.target.value })}
                                    ></textarea>
                                </div>
                            ) : data.pensionSecurity === 'NO' ? (

                                <div className="flex gap-x-[50px] m-0">
                                    <div className="w-[150px]">
                                        <span className="text-[12px] font-[500] flex items-end justify-start">
                                            Are you planning to have any form of pension security?
                                            <span><IconInfon /></span>
                                        </span>
                                    </div>
                                    <div className="w-[30%]">
                                        <SelectComponent
                                            background={false}
                                            options={yesNo}
                                            disabled={active === text ? false : true}
                                            border={true}
                                            size="!w-[210px] !h-[48px]"
                                            onChange={(selectedOption) => ofTextBoolean('planningToHaveForm', `${selectedOption}`)}
                                            selectName="paidFully"
                                        />

                                    </div>
                                </div>
                            ) : ''
                        }
                    </div>
                    {
                        data.pensionSecurity === 'LIFE_INSURANCE' ? <>

                            <p className="text-[22px] mt-[20px] font-[inter]">Share life insurance details</p>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Insurance amount</p>
                                <InputNoAppearance
                                    type='number'
                                    border={true}
                                    disabled={active === text ? false : true}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataLife({ ...dataLife, insuranceAmount: (e.target.value) })}
                                    inputName="insuranceAmount"
                                />
                            </div>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Name of life insurance company</p>
                                <InputNoAppearance
                                    type='text'
                                    border={true}
                                    disabled={data.makePensionContribution && active === text ? false : true}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataLife({ ...dataLife, companyName: e.target.value })}
                                    inputName="companyName"
                                />
                            </div>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">{`Address of life insurance company (Postal code, City, Street, Building number)`}</p>
                                <InputNoAppearance
                                    type='text'
                                    border={true}
                                    disabled={data.makePensionContribution && active === text ? false : true}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataLife({ ...dataLife, address: e.target.value })}
                                    inputName="address"
                                />
                            </div>
                            <div className="grid gap-y-[8px] m-0">
                                <p className="text-[14px] text-[#B6B6B6]">Insurance number</p>
                                <InputNoAppearance
                                    type='number'
                                    border={true}
                                    disabled={data.makePensionContribution && active === text ? false : true}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setDataLife({ ...dataLife, insuranceNumber: e.target.value })}
                                    inputName="insuranceNumber"
                                />
                            </div>
                        </> : ''
                    }
                </div>
            </Form>}
        </>
    )
}
export default AppReviewPensionContr
