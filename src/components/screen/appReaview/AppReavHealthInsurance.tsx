import { ReactComponent as IconInfon } from "../../../assets/icons/info.svg";
import { ReactComponent as Arrow } from "../../../assets/icons/edit.svg";
import { FC, useState, useEffect, ChangeEvent } from 'react'
import ButtonGray from "components/ui/button/ButtonGray";
import cls from './ApplicationReview.module.scss'
import SelectComponent from "components/ui/select/SelectComponent";
import { paymentFrequencyOptions, yesNo } from "utils/Arrays";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import { Button, Form } from "antd";
import { dataSourceMini, DataType } from "../../../utils/MiniTable";
import LoadingMain from "components/ui/loading/LoadingMain";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import { useAppSelector } from "hooks/useAppSelector";
import { IMiniTable, IMiniTableWithTotal } from "interfaces/TableInterface";
import RowTable from "components/ui/table/RowTable";
import { useFetchAllYearQuery } from "services/yearService";
import { IYear } from "interfaces/YearInterface";
import { useLazyFetchReviewHealthMiniTableQuery, useUpdateReviewHealthMiniTableMutation } from "services/reviewHealthMiniTableServices";
import { IReviewHealth } from "interfaces/reaview";
import { useFetchReviewHealthQuery, useUpdateReviewHealthMutation } from "services/reviewServices";

interface IAppReavHealthInsurance {
    setActive: (pr: string) => void;
    active?: string,
    text: string
}

const AppReavHealthInsurance: FC<IAppReavHealthInsurance> = ({ active, setActive, text }) => {

    const [data, setData] = useState<IReviewHealth>({} as IReviewHealth)
    const [oldData, setOldData] = useState<IReviewHealth>({} as IReviewHealth)
    const [loading, setLoading] = useState(true)

    const { client } = useAppSelector(state => state.authSlice)

    // Table

    const currentDate = new Date();
    const currYear = currentDate.getFullYear();
    const [currentYear, setCurrentYear] = useState(0)
    const [dataMiniChange, setDataMiniChange] = useState<boolean>(false);
    const [dataMini, setDataMini] = useState<IMiniTableWithTotal | null>(null);

    const [getTable, { data: table = {} as IMiniTableWithTotal, error, isLoading, isSuccess }] = useLazyFetchReviewHealthMiniTableQuery()

    const [UpdateTableMini] = useUpdateReviewHealthMiniTableMutation()


    const updateData = async () => {
        if (dataMiniChange) {
            const { total, ...newData } = dataMini as IMiniTableWithTotal
            const res = await UpdateTableMini({ data: newData as IMiniTable, yearId: currentYear })
            if (typeof res === 'object' && res !== null && 'data' in res)
                setDataMini(res.data as IMiniTableWithTotal)
            setDataMiniChange(false)
        }
    }

    const handleYear = (id: number) => {

        setCurrentYear(id)
        if (client) {
            getTable({
                userId: client.id,
                yearId: id,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        } updateData()

    }

    const {
        data: years = [] as IYear[],
        isSuccess: isSuccessYear

    } = useFetchAllYearQuery(client?.id || 0);



    useEffect(() => {
        if (isSuccessYear && client && years.length > 0) {
            setCurrentYear(years[0].id || 0)
            getTable({
                userId: client.id,
                yearId: years[0].id || 0,
            }).then((data) => setDataMini(data.data as IMiniTableWithTotal))
        }

    }, [isSuccessYear])

    // end Table
    // get
    const { data: getData, isSuccess: isSuccessHealth } = useFetchReviewHealthQuery({ clientId: client?.id || '' })
    // Update
    const [updateDataHealth] = useUpdateReviewHealthMutation()
    //
    useEffect(() => {
        (async () => {
            if (getData) {
                await setData(getData)
                await setOldData({ ...getData })
                setLoading(false)
            }
        })()
    }, [isSuccessHealth])

    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const ofTextBoolean = (item: string, text: string) => {
        if ('Yes' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: true
            }))
        } else if ('No') {
            setData((prevData) => ({
                ...prevData,
                [item]: false
            }))

        }
    }
    const ofBooleanText = (item: string) => {
        if (data[item as keyof IReviewHealth]) {
            return 'Yes';
        } else {
            return 'No';
        }

    }
    const btnPut = async () => {
        if (data.doYouPayForHealthInsurance) {
            updateDataHealth({
                clientId: client?.id || '',
                data: data
            })
        } else {
            await setData({
                ...data,
                companyName: '',
                address: '',
                membershipNumber: 0,
                cityPostalCodeAddress: '',
                contactPerson: '',
                phoneNumber: '',
                regularly: '',
                reasonWhyIrregularly: '',
            })
            updateDataHealth({
                clientId: client?.id || '',
                data: data
            })
        }

        setActive('')
    }

    const btnCancel = () => {
        setData(oldData)
        setActive('')
    }

    return (
        <>
            {loading ? <LoadingMain /> : <Form
                name="appHealth"
                layout="horizontal"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={btnPut}

                initialValues={{
                    doYouPayForHealthInsurance: ofBooleanText('doYouPayForHealthInsurance'),
                    companyName: data.companyName || ' ',
                    address: data.address || ' ',
                    membershipNumber: data.membershipNumber || ' ',
                    cityPostalCodeAddress: data.cityPostalCodeAddress,
                    contactPerson: data.contactPerson || ' ',
                    phoneNumber: data.phoneNumber || ' ',
                    regularly: textNomal(`${data.regularly}`) || ' ',
                    reasonWhyIrregularly: data.reasonWhyIrregularly || ' ',
                    paidFully: ofBooleanText('paidFully') || ' ',
                    // explainWhyNot: data.explainWhyNot || ' ',
                }}>
                <div className="pt-[10px] m-0 pr-[40px] ease-[1s] sticky top-[-40px] bg-white flex justify-between items-center flex-wrap z-[10]">
                    <div className="w-3/5 flex justify-between items-center">
                        <p className="text-[28px] font-[600]">Health insurance</p>
                        <div className="cursor-pointer"
                            onClick={() => setActive(text)}
                        >
                            <Arrow />
                        </div>
                    </div>
                    <div className={`${cls.fadeInOut} ${active === text ? cls.blockFlex : 'hidden'}`}>
                        <NewButtonGreen
                            text="Confirm"
                            height="45px"
                            className="!w-[140px]" />
                        <ButtonGray
                            onClick={() => btnCancel()}
                            text='Cancel'
                            size="!w-[140px] !h-[45px] hover:shadow-md hover:shadow-lightGray"
                            className="rounded-[4px] " />
                    </div>
                    <div className="w-full mt-[10px] h-[1px] bg-green"></div>
                </div>
                <div className="w-3/5 mt-[25px] grid gap-y-[20px] m-0">
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Do you pay for health insurance?</p>
                        <SelectComponent
                            background={false}
                            options={yesNo}
                            disabled={active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => ofTextBoolean('doYouPayForHealthInsurance', `${selectedOption}`)}
                            selectName="doYouPayForHealthInsurance"
                        />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Health insurance company name</p>
                        <InputNoAppearance
                            border={true}
                            type='text'
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, companyName: e.target.value })}
                            inputName="companyName"
                        />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Health insurance company address {`(Postal code, City, Street, Building number)`}</p>
                        <InputNoAppearance
                            border={true}
                            type='text'
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, address: e.target.value })}
                            inputName="address" />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Membership number</p>
                        <InputNoAppearance
                            border={true}
                            type='number'
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, membershipNumber: Number(e.target.value) })}
                            inputName="membershipNumber" />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Contact person</p>
                        <InputNoAppearance
                            border={true}
                            type='text'
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, contactPerson: e.target.value })}
                            inputName="contactPerson" />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Health insurance phone number</p>
                        <InputNoAppearance
                            border={true}
                            type='text'
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setData({ ...data, phoneNumber: e.target.value })}
                            inputName="phoneNumber" />
                    </div>
                    <div className="grid gap-y-[40px] m-0">
                        <p className="text-[20px] font-[500]">State your pension contributions for the audited period</p>
                        <p>Expenses should be based on the date of payment, not invoice.</p>

                    </div>
                    <div className="flex gap-x-[10px] m-0 ">
                        {
                            isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                className={`${currentYear === item.id
                                    ? "border border-black"
                                    : ""}    !h-[32px] rounded-[15px]`}

                                onClick={() => handleYear(item.id)}>
                                {currYear === item.yearValue
                                    ? "Current year"
                                    : item.yearValue}</Button>)
                        }
                    </div>
                    <div className={`${cls.scroll} w-[160.5%] `}>
                        {dataMini && isSuccess && <div className={cls.scroll}>
                            <RowTable name={"Health insurance"} disabled={data.doYouPayForHealthInsurance && active === text ? false : true} data={dataMini} setDataSource={setDataMini}
                                setDataMiniChange={setDataMiniChange} />
                        </div>}
                    </div>
                    <div className="flex gap-x-[50px] m-0">
                        <div className="w-[150px]">
                            <p className="text-[12px] font-[500] flex items-end justify-start">
                                Health insurance for this year was paid fully during this year
                                <span><IconInfon /></span>
                            </p>
                        </div>
                        <div className="w-[30%]">
                            <SelectComponent
                                background={false}
                                options={yesNo}
                                disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                                border={true}
                                size="!w-[210px] !h-[48px]"
                                onChange={(selectedOption) => ofTextBoolean('paidFully', `${selectedOption}`)}
                                selectName="paidFully"
                            />

                        </div>
                    </div>
                    <div>
                        <textarea
                            className={`w-[100%] h-[92px] text-[#B6B6B6] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none ${data.doYouPayForHealthInsurance && active === text ? 'text-black' : 'hidden'}`}
                            name="paidFullyFalse"
                            id="paidFullyFalse"
                            defaultValue={data.explainWhyNot}
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            placeholder="Explain reason why not ?"
                            onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setData({ ...data, explainWhyNot: e.target.value })}
                        ></textarea>
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">How regularly do you pay for health insurance?</p>
                        <SelectComponent
                            background={false}
                            options={paymentFrequencyOptions}
                            disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => setData({ ...data, regularly: `${selectedOption}` })}
                            selectName="regularly" />
                        <div className={`w-[80%] ${data.regularly === 'IRREGULARLY' ? '' : 'hidden'}`}>
                            <textarea
                                className={`w-[100%] h-[92px] text-[#B6B6B6] bg-[#F7F7F7] rounded-[3px] p-[10px] outline-none ${data.doYouPayForHealthInsurance && active === text ? 'text-black' : 'hidden'}`}
                                name="regularly"
                                id="regularly"
                                defaultValue={data.reasonWhyIrregularly}
                                disabled={data.doYouPayForHealthInsurance && active === text ? false : true}
                                placeholder="What is the reason of irreglar payments?"
                                onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setData({ ...data, reasonWhyIrregularly: e.target.value })}
                            ></textarea>
                        </div>
                    </div>
                </div>
            </Form>}
        </>
    )
}
export default AppReavHealthInsurance
