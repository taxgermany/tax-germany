import ButtonGray from "components/ui/button/ButtonGray";
import { ReactComponent as Arrow } from "../../../assets/icons/edit.svg";
import cls from './ApplicationReview.module.scss'
import { FC, useState, useEffect, } from 'react'
import { Button, Form } from "antd";
import SelectComponent from "components/ui/select/SelectComponent";
import { yesNo } from "utils/Arrays";
import MicroTable from "components/ui/table/microTable";
import { IEmployer } from "../../../interfaces/TableInterface";
import LoadingMain from "components/ui/loading/LoadingMain";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
// large talbe
import LargeTable from "components/ui/table/LargeTable"
import { ITable, ITableVot } from "interfaces/TableInterface"
import { useFetchAllYearQuery } from "services/yearService"
import {
    useCreateFileMutation,
    useCreateFileProfitsMutation,
    useLazyFetchFileNameProfitsQuery
} from "services/fileService"
import { IYear } from "interfaces/YearInterface"
import { totalNull } from "pages/freelancerPage/profitandexpenses/ProfitAndExpensesLargeTable"
import { useLazyFetchReviewTableQuery, useUpdateReviewTableEmployerMutation, useUpdateReviewTableMutation, } from "services/reviewProfitsServices"
import { useLazyFetchTableEmployerQuery } from "services/profitServices";
import { useAppSelector } from "../../../hooks/useAppSelector";
import {
    useFetchReviewProfitsQuery,
    useLazyFetchReviewProfitsQuery,
    useUpdateReviewProfitsMutation
} from "services/reviewServices";
import { IFileName } from "../../../interfaces/ProfitsInterface";

//

interface IApplicationReviewProfitAndExpenses {

    setActive: (pr: string) => void;
    active?: string,
    text: string
}

const ApplicationReviewProfitAndExpenses: FC<IApplicationReviewProfitAndExpenses> = ({
    setActive,
    active,
    text
}) => {
    interface IData {
        accountingType: string
        regularly: boolean
        haveEmployees: boolean
        employerNumber: string
        vat: boolean
    }

    const { client } = useAppSelector(state => state.authSlice)


    const [data, setData] = useState<IData>({} as IData)
    const [oldData, setOldData] = useState<IData>({} as IData)
    const [loading, setLoading] = useState(true)


    // Large table
    const currentDate = new Date();
    const currYear = currentDate.getFullYear();


    const [dataLarge, setDataLarge] = useState<ITableVot | ITable | null>(null);
    const [fileCheck, setFileCheck] = useState<File | null>(null);
    const [file, setFile] = useState<IFileName | null>(null);
    const [currentYear, setCurrentYear] = useState(0)
    const [dataLargeChange, setDataLargeChange] = useState<boolean>(false);


    // microtable

    /*  const { data: tableEmployer = [], } = useFetchTableEmployerQuery(currentYear)*/
    const [getEmployer, {
        data: tableEmployer = [] as IEmployer[],
        error: errorEmployer,
        isLoading: isLoadingEmployer,
        isSuccess: isSuccessEmployer
    }] = useLazyFetchTableEmployerQuery()

    const [dataMicroTable, setDataMicroTable] = useState<IEmployer[]>([...tableEmployer]);
    const {
        data: years = [] as IYear[],
        error: errorYear,
        isLoading: isLoadingYear,
        isSuccess: isSuccessYear
    } = useFetchAllYearQuery(client?.id || 0);


    const [getTable, {
        data: table = {} as ITable | ITableVot,
        error,
        isLoading,
        isSuccess
    }] = useLazyFetchReviewTableQuery()

    const [UpdateTable] = useUpdateReviewTableMutation()
    const [updateEmployTable] = useUpdateReviewTableEmployerMutation()
    const [CreatFile] = useCreateFileProfitsMutation()

    const updateData = async () => {

        let newDataLarge = {
            ...JSON.parse(JSON.stringify(dataLarge)),
            total: totalNull,
            totalYearProfit: 0
        } as ITableVot | ITable

        if (dataLargeChange) {

            newDataLarge?.months.forEach(item => {
                item.grossRevenueInclVat = 0;
                item.expenses = 0;
                item.monthlyProfits = 0;
            });
            console.log(newDataLarge)
            UpdateTable({ data: newDataLarge as ITableVot | ITable, yearId: currentYear, vat: data.vat })
            setDataLargeChange(false)
        }

        if (fileCheck && client) {
            const formData = new FormData();
            formData.append("file", fileCheck)
            await CreatFile({ data: formData, userId: client.id || 0 })
        }

    }

    const handleYear = async (id: number) => {
        setCurrentYear(id)
        if (client) {
            getTable({
                userId: client.id,
                yearId: id,
                vat: data.vat
            }).then((res) => setDataLarge(res.data as ITableVot | ITable))
            setDataLarge(table)

            updateData()
        }

        if (fileCheck && client) {
            const formData = new FormData();
            formData.append("file", fileCheck)
            await CreatFile({ data: formData, userId: client.id || 0 })
        }

    }
    // End Large table
    // get and update
    /*
        const { data: getData, isSuccess: isSuccessProfits, } = useFetchReviewProfitsQuery({ clientId: client?.id || '' })*/
    const [getProfitsData, { data: getData, isSuccess: isSuccessProfits, }] = useLazyFetchReviewProfitsQuery()

    const [UpdateProfits] = useUpdateReviewProfitsMutation()
    const [getFileProfits, { isLoading: isLoadingFileProfits }] = useLazyFetchFileNameProfitsQuery()

    const changeVat = (vat: string) => {

        if (client)
            getTable({
                userId: client.id,
                yearId: currentYear,
                vat: vat === 'Yes'
            }).then((data) => setDataLarge(data.data as ITableVot | ITable))
    }


    const getDataProfits = async () => {
        const res = await getProfitsData({ clientId: client?.id || '' }).unwrap()
        const copyData = await { ...res }
        await setData(res)
        await setOldData(copyData)
        await setLoading(false)
        if (isSuccessYear && client && years.length > 0 && data) {
            const resFile = await getFileProfits({ userId: client.id }).unwrap()
            setFileCheck(resFile.file)
            setFile(resFile)
            setCurrentYear(years[0].id || 0)
            getTable({
                userId: client.id,
                yearId: years[0].id || 0,
                vat: res.vat
            }).then((data) => setDataLarge(data.data as ITableVot | ITable))
            getEmployer(client.id || 0).then(res => setDataMicroTable(res.data as IEmployer[]))
        }
    }

    useEffect(() => {
        getDataProfits()
    }, [isSuccessYear])


    const ofTextBoolean = (item: string, text: string) => {
        console.log(text);

        if ('Yes' === text) {
            setData((prevData) => ({
                ...prevData,
                [item]: true
            }))
        } else if ('No') {
            setData((prevData) => ({
                ...prevData,
                [item]: false
            }))

        }
    }
    const ofBooleanText = (item: string) => {
        if (data[item as keyof IData]) {
            return 'Yes';
        } else {
            return 'No';
        }

    }
    const textNomal = (text: string) => {
        const words = text
            .split('_')
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
        return words.join(' ');
    }
    const btnPut = async () => {
        updateData()
        await updateEmployTable(dataMicroTable)
        UpdateProfits({
            clientId: client?.id || '',
            data: data
        })
    }
    const btnCancel = () => {
        setData(oldData)
        setActive('')
    }
    return (
        <>
            {loading && data !== getData ? <LoadingMain /> : <Form
                name="appProfits"
                layout="horizontal"
                labelCol={{ span: 30 }}
                wrapperCol={{ span: 30 }}
                onFinish={btnPut}
                initialValues={{
                    accountingType: textNomal(`${data.accountingType}`),
                    regularly: ofBooleanText('regularly'),
                    haveEmployees: ofBooleanText('haveEmployees'),
                    employerNumber: data.employerNumber,
                    vat: ofBooleanText('vat')
                }}>
                <div
                    className="pt-[10px] m-0 pr-[40px] ease-[1s] sticky top-[-40px] bg-white flex justify-between items-center flex-wrap z-[10]">
                    <div className="w-3/5 flex justify-between items-center">
                        <p className="text-[28px] font-[600]">Profits and expenses</p>
                        <div className="cursor-pointer"
                            onClick={() => setActive(text)}
                        >
                            <Arrow />
                        </div>
                    </div>
                    <div className={`${cls.fadeInOut} ${active === text ? cls.blockFlex : 'hidden'}`}>
                        <NewButtonGreen
                            text="Confirm"
                            height="45px"
                            className="!w-[140px]" />
                        <ButtonGray
                            onClick={() => btnCancel()}
                            text='Cancel'
                            size="!w-[140px] !h-[45px] hover:shadow-md hover:shadow-lightGray"
                            className="rounded-[4px] " />
                    </div>
                    <div className="w-full mt-[10px] h-[1px] bg-green"></div>
                </div>
                <div className="w-3/5 !mt-[25px] grid gap-y-[20px] m-0">
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Acounting type</p>
                        <SelectComponent
                            background={false}
                            options={[{ value: 'BALANCE_SHEET', label: 'Business sheet' },
                            { value: 'CASH_BASED_ACCOUNTING', label: 'Cash based accounting' }]}
                            disabled={active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => setData({ ...data, accountingType: `${selectedOption}` })}
                            selectName="accountingType"
                        />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Do you prepare your books regularly?</p>
                        <SelectComponent
                            background={false}
                            options={yesNo}
                            disabled={active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => ofTextBoolean('regularly', `${selectedOption}`)}
                            selectName="regularly"
                        />
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">Did you have any employees during the audited
                            period?</p>
                        <SelectComponent
                            background={false}
                            options={yesNo}
                            disabled={active === text ? false : true}
                            border={true}
                            onChange={(selectedOption) => ofTextBoolean('haveEmployees', `${selectedOption}`)}
                            selectName="haveEmployees"
                        />
                    </div>
                    <div className=''>
                        <p className="text-[20px] font-[600]">Average number of employees per year</p>
                        <p className="text-[14px]">Specify how many of empoyees were by function. Also mention, how many
                            of them were part-time or your family members.</p>
                    </div>
                    <div className={`${data.haveEmployees ? '' : 'hidden'}`}>
                        {isSuccessEmployer && !errorEmployer &&
                            <MicroTable data={dataMicroTable} setDataSource={setDataMicroTable}
                                disabled={active !== text} />}
                    </div>
                    <div className="grid gap-y-[8px] m-0">
                        <p className="text-[14px] text-[#B6B6B6]">VAT pay</p>
                        <SelectComponent
                            background={false}
                            options={yesNo}
                            disabled={active === text ? false : true}
                            border={true}
                            selectName="vat"
                            onChange={(selectedOption) => {
                                ofTextBoolean('vat', `${selectedOption}`)
                                changeVat(`${selectedOption}`)
                            }
                            }
                        />
                    </div>
                    <div className={`grid gap-y-[30px] !mt-[20px] m-0`}>
                        <p className="text-[28px] font-[600]">Profits</p>
                        <p>Mention expenses, related only to your freelancer activities. Expenses should be based on the
                            date of payment, not invoice. Please, exclude VAT from your expenses and include this VAT in
                            a separate line called Sum of VAT (from all mentioned expenses) . Also in a separate line
                            mention VAT paid to the Tax Office (its also an expense). Don t include health insurance and
                            pension contribution.</p>
                        <div>
                            {isLoadingYear && <h1>loading</h1>}
                            {errorYear && <h1>error</h1>}
                            {isSuccessYear && years.map(item => <Button key={item.id} type="text"
                                className={`${currentYear === item.id
                                    ? "border border-black"
                                    : ""} !h-[32px] rounded-[15px]`}

                                onClick={() => handleYear(item.id)}>
                                {currYear === item.yearValue
                                    ? "Current year"
                                    : item.yearValue}</Button>)
                            }

                        </div>
                    </div>
                    <div className={`!w-[122%] ${cls.scroll}`}>

                        {dataLarge && file && isSuccess && !error &&
                            <div className={`${cls.scroll}`}>
                                <LargeTable
                                    file={file}
                                    vat={data.vat} data={dataLarge}
                                    setDataLargeChange={setDataLargeChange}
                                    setDataSource={setDataLarge}
                                    fileCheck={fileCheck}
                                    setFileCheck={setFileCheck}
                                    disabled={active === text ? false : true} />

                            </div>
                        }
                    </div>
                </div>
            </Form>}
        </>
    )
}

export default ApplicationReviewProfitAndExpenses
