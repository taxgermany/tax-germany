import BigTitle from 'components/ui/button/BigTitle';
import { ReactComponent as Mark } from "../../assets/icons/mark_email_read.svg";


const CheckEmail = () => {
  return(
   <div className="ForgotPassword h-[90vh] font-inter flex justify-center items-center">
     <div className="content grid gap-[10px]">
        <Mark className='mx-auto my-0 h-[25px]  '/>
        <BigTitle className='text-center' text='Check your email'/>
        <p className="text-[12px] text-left pb-[15px]">An email to update your password has been sent to. </p>
     </div>
   </div>
  )
}

export default CheckEmail;