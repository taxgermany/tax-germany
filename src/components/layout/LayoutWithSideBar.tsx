import Header from './Header'
import Sidebar from './SideBar';
import UserLogo from "../ui/userLogo/UserLogo";
import {Outlet} from "react-router-dom";
import clsx from 'clsx';

const LayoutWithSideBar = () => {


    return (
        <div className="grid ">
            <Header children={<UserLogo/>}/>
            <div className="flex">
                <Sidebar/>
                <div className={clsx(`m-x-auto w-full overflow-y-auto h-[calc(100vh_-_59px)] p-[40px]`)}>
                    <Outlet/>
                </div>
            </div>
        </div>
    )
}

export default LayoutWithSideBar;
