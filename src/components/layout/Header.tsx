import React, {FC, ReactNode, useState} from "react";
import {useNavigate} from 'react-router-dom';
import NewButtonGreen from 'components/ui/button/NewButtonGreen';
import {useAppSelector} from "../../hooks/useAppSelector";
import {ReactComponent as UserLogo} from "../../assets/icons/account_circle.svg";
import {Button, Dropdown, MenuProps} from "antd";
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {loggedOut} from "../../redux/auth/authSlice";
import Modal from "../screen/AdminClient/Modal/modal";
import log from "../../assets/icons/logout.png";

interface IHeader {
    children?: ReactNode
}


const Header: FC<IHeader> = ({children}) => {
    const {client} = useAppSelector(state => state.authSlice)
    const navigate = useNavigate();
    const dispatch = useAppDispatch()
    const [showModal, setShowModal] = useState(false)


    const Logout = () => {
        dispatch(loggedOut())
        navigate('/')
    }
    const items: MenuProps['items'] = [
        {
            key: '1',
            label: (
                <div onClick={Logout}>
                    logout
                </div>
            ),
        },
    ];


    return (
        <div className="shadow py-0 px-2 z-[999] select-none">
            <div className="flex items-center justify-between px-[20px] lg:px-[40px] h-[58px] ">
                <p onClick={() => navigate('/')}
                   className="text-green text-[24px] font-[600] md:text-[20ypx] cursor-pointer">Tax</p>
                {
                    !client
                        ? <NewButtonGreen onClick={() => navigate("/login")} text='Sign in'/>
                        : <div className="flex cursor-pointer gap-[10px]"><UserLogo/>
                            <div onClick={() => setShowModal(!showModal)}> {client.email}</div>
                        </div>
                }

            </div>
            {showModal && <Modal active={showModal} setActive={setShowModal}>

                <div className="absolute right-[28px] top-[8px]   bg-white
            rounded-[10px] shadow-4xl overflow-auto cursor-pointer select-none">
                    <div className="flex cursor-pointer gap-[10px] px-[15px] py-[8px] hover:bg-[#F8F8F8]"><UserLogo/>
                        <div onClick={() => setShowModal(!showModal)}> {client?.email}</div>
                    </div>
                    <div className={"flex gap-[10px] items-center px-[15px] py-[8px] hover:bg-[#F8F8F8]"} onClick={Logout}>
                        <img src={log} className={"w-[28px]"} alt=""/>
                        <div>Logout</div>
                    </div>

                </div>
            </Modal>}
        </div>
    );
};

export default Header;
