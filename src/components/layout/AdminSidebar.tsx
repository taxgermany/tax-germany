import {useEffect, useState} from "react";
import { NavLink, useLocation } from "react-router-dom";
import clsx from "clsx";
import s from "./Sidebar.module.scss";
import LogOutModal from "components/ui/modal/LogOutModal";

const AdminSidebar = () => {



  let pathname = useLocation().pathname;

  let location = "";
  if (pathname !== "/") {
    location = pathname.split("/")[2];
  }
  location = pathname.split("/")[2].split("-")[0];

  const [active, setActive] = useState(location);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [mobile] = useState(false);


useEffect(()=>{
  setActive(location)
},[pathname])

  return (
    <div
      className={`block font-inter font-bold px-[20px] py-[40px] bg-lightSilver
              h-[calc(100vh_-_59px)] w-[220px]`}
    >
      <ul className="text-gray leading-[26px] grid gap-[18px]">
        <li>
          <NavLink
            className={clsx(s.link, active === "clients" ? `!text-green` : ``)}
            to={"/super-admin/clients"}
            onClick={() => setActive("clients")}
          >
            Clients
          </NavLink>
        </li>
        <li>
          <NavLink
            className={clsx(
              s.link,
              active === "pricing" ? `!text-green` : ``
            )}
            to={"/super-admin/pricing-policy"}
            onClick={() => setActive("pricing-policy")}
          >
            Pricing policy
          </NavLink>
        </li>
        <li>
          <NavLink
            className={clsx(
              s.linkAdmin,
              active === "manage" ? `!text-green` : ``
            )}
            to={"/super-admin/manage-access"}
            onClick={() => setActive("manage-access")}
          >
            Manage access
          </NavLink>
        </li>

        <div className="cursor-pointer font-[500] text-black" onClick={() => setIsModalOpen(true)}>Log Out</div>
        {isModalOpen && (
          <LogOutModal
            isOpen={isModalOpen}
            setIsOpen={setIsModalOpen}
            title={"Log Out"}
          />
        )}
      </ul>
    </div>
  );
};

export default AdminSidebar;
