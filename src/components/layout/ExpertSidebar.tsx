import {useEffect, useState} from "react";
import {NavLink, useLocation} from "react-router-dom";
import clsx from "clsx";
import s from "./Sidebar.module.scss";
import LogOutModal from "components/ui/modal/LogOutModal";

const AdminSidebar = () => {

    let pathname = useLocation().pathname;
    let location = "";
    if (pathname !== "/") {
        location = pathname.split("/")[2];
    }
    location = pathname.split("/")[2].split("-")[0];

    const [active, setActive] = useState(location);

    const [isModalOpen, setIsModalOpen] = useState(false);

    const [mobile] = useState(false);


    useEffect(()=>{
        setActive(location)
    },[pathname])

    return (
        <aside
            className={`block font-inter font-bold px-[20px] py-[40px] bg-lightSilver
              h-[calc(100vh_-_59px)] w-[220px]`}>
            <ul className="text-gray leading-[26px] grid gap-[18px]">
                <li>
                    <NavLink
                        className={clsx(s.link, active === "clients" ? `!text-green` : ``)}
                        to={"/expert/clients"}
                        onClick={() => setActive("clients")}
                    >
                        Clients
                    </NavLink>
                </li>
                <li>
                    <div className="cursor-pointer font-[500] text-black" onClick={() => setIsModalOpen(true)}>Log Out
                    </div>
                    {isModalOpen && (
                        <LogOutModal
                            isOpen={isModalOpen}
                            setIsOpen={setIsModalOpen}
                            title={"Log Out"}
                        />
                    )}
                </li>
            </ul>
        </aside>
    );
};

export default AdminSidebar;
