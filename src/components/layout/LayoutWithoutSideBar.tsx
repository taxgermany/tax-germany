import Header from './Header'
import ButtonGreen from "../ui/button/ButtonGreen";
import {Outlet} from "react-router-dom";

const LayoutWithSideBar = () => {
    return (
        <div className="grid ">
            <Header children={<ButtonGreen text={"Sign in"}/>}/>
            <Outlet/>
        </div>
    )
}

export default LayoutWithSideBar;