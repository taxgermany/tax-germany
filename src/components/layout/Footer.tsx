import React from 'react'

const Footer = () => {
  return (
    <div className='mt-48 text-center'>
      <p className='text-lightGray text-[15px] border-t-[1px] border-lightGray pb-10 pt-4'>Copyright © 2023. Taxventures. All rights reserved.Created by Fortylines IO</p>
    </div>
  )
}

export default Footer;

