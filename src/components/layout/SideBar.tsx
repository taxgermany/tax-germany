import {useEffect, useState} from 'react';
import {NavLink, useLocation, useNavigate} from 'react-router-dom';
import clsx from "clsx";
import s from "./Sidebar.module.scss"
import {activeLink, ILink} from "../../redux/sidebar/sidebarSlice"
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {useAppSelector} from "../../hooks/useAppSelector";
import {getStoreLocal} from "../../utils/local-storage/localStorage";
import {useLazyFetchIsPaidQuery} from "../../services/paymentService";

const Sidebar = () => {
    const dispatch = useAppDispatch()
    const {client} = useAppSelector(state => state.authSlice)
    const navigate = useNavigate();

    const [getPaid, {data}] = useLazyFetchIsPaidQuery()


    let pathname = useLocation().pathname
    let location = ""

    const {active} = useAppSelector(state => state.sidebarSlice)
    /*
        const [active, setCurrentLink] = useState(currentLink)*/
    let path: { currentPath: string } | null = null
    if (client) path = getStoreLocal(`${client.id}`)

    location = pathname.split("/")[2].split("-")[0];


    if (typeof path === 'object' && path !== null && 'currentPath' in path) {
        location = path.currentPath.split("/")[2].split("-")[0];
    }

    dispatch(activeLink(location))
    /*    setCurrentLink(location)*/

    const [status, setStatus] = useState(false)

    const [storage, setStorage] = useState({
        profits: false,
        health: false,
        pension: false,
        review: false,
        status: false
    });

    const fetchPaid = async () => {
        const res = await getPaid({clientId: client?.id || 0}).unwrap()

        if (res.isPaid) {
            setStatus(true)
        } else {
            setStatus(false)
        }

    }

    useEffect(() => {
        fetchPaid()
    }, [])
    useEffect(() => {
        let storedItems: string | null = null
        if (client)
            storedItems = localStorage.getItem(`storage-${client.id}`);
        if (storedItems !== null) {
            const items = JSON.parse(storedItems);
            setStorage(items);
        }
    }, [active, pathname]);
    return (

        <aside className={`font-inter font-bold px-[20px] py-[40px] bg-lightSilver h-[calc(100vh_-_59px)] w-[220px]`}>
            <ul className='text-gray leading-[26px] grid gap-[18px]'>
                <li>
                    <NavLink className={clsx(s.link, active === "personal" ? `!text-green` : ``)}
                             to={'/freelancer/personal-data'}
                             onClick={() => dispatch(activeLink("personal"))}>
                        Personal Data</NavLink>
                </li>
                <li>
                    <NavLink className={clsx(storage.profits ? s.link : s.disable,
                        active === "profits" ? `!text-green` : ``)}
                             to="/freelancer/profits-and-expenses"
                             onClick={() => dispatch(activeLink("profits"))}
                    >Profits and expenses</NavLink>
                </li>
                <li>
                    <NavLink className={clsx(storage.health ? s.link : s.disable,
                        active === "health" ? `!text-green` : ``)}
                             to="/freelancer/health-insurance-pay"
                             onClick={() => dispatch(activeLink("health"))}>Health insurance</NavLink>
                </li>
                <li>
                    <NavLink className={clsx(storage.pension ? s.link : s.disable,
                        active === "pension" ? `!text-green` : ``)}
                             to="/freelancer/pension-contributions"
                             onClick={() => dispatch(activeLink("pension"))}>Pension contributions</NavLink>
                </li>
                <li>
                    <NavLink className={clsx(storage.review ? s.link : s.disable,
                        active === "review" ? `!text-green` : ``)}
                             to="/freelancer/review"
                             onClick={() => dispatch(activeLink("review"))}>Application review</NavLink>
                </li>
                {status && <li>
                    <NavLink className={clsx(storage.status ? s.linkAdmin : s.disable,
                        active === "status" ? `!text-green` : ``)}
                             to="/freelancer/status"
                             onClick={() => dispatch(activeLink("status"))}>Application status</NavLink>
                </li>}
            </ul>
        </aside>
    );
};

export default Sidebar;
