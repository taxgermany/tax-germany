import { ChangeEvent, FC } from "react"
interface ICheckBox {
    setActive: (pr: boolean) => void | null;
    active?: boolean;
    className?: string;
    [key: string]: string | ((pr: boolean) => void) | ((event: ChangeEvent<HTMLInputElement>) => void) | boolean | undefined;
}


const CheckBox: FC<ICheckBox> = ({ setActive, active = false, className, ...props }) => {
    return (

        <div className={`${className} w-[24px] h-[24px] cursor-pointer border border-[#D9D9D9] rounded-[3px] flex gap-x-[20px]` }>
            <div
                className={` w-[150%] flex justify-center items-center`}
                onClick={() => setActive(!active)}
                
            >
                {active ? (
                    <svg
                        width="16"
                        height="16"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className="text-center"
                    >
                        <mask
                            id="mask0_3495_7005"
                            maskUnits="userSpaceOnUse"
                            x="0"
                            y="0"
                            width="24"
                            height="24"
                        >
                            <rect width="24" height="24" fill="#D9D9D9" />
                        </mask>
                        <g mask="url(#mask0_3495_7005)">
                            <path
                                d="M9.5501 18L3.8501 12.3L5.2751 10.875L9.5501 15.15L18.7251 5.97498L20.1501 7.39998L9.5501 18Z"
                                fill="#26AF60"
                            />
                        </g>
                    </svg>
                ) : (
                    ""
                )}
            </div>
        </div>
    )
}

export default CheckBox