import { FC } from "react"

interface IButtonAddInfoClient{
    onClick?:() => void,
}
const ButtonAddInfoClient: FC<IButtonAddInfoClient> = ({onClick}) =>{
    return(
        <div className="w-[386px] h-[65px] py-[6px] px-[5px] border border-[#D9D9D9] bg-[#F6F6F6CC] flex justify-center items-center gap-x-[40px]">
            <p className="font-roboto leading-[22px] w-[215px]">Do you need additional information from the client?</p>
            <button className="bg-green text-white rounded-[3px] px-[30px] py-[6px]"
            onClick={onClick}>Yes</button>
        </div>
    )
}
export default ButtonAddInfoClient
