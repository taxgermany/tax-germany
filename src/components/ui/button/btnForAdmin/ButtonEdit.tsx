import { ReactComponent as Edit } from "../../../../assets/icons/Vector.svg";
import {FC} from 'react'
interface IButtonEdit{    
    onClick?:() => void,
}
const ButtonEdit: FC<IButtonEdit> = ({onClick}) =>{

    return (
        <button className="w-[94px] h-[41px] border-0 rounded-[3px] bg-green text-white flex items-center justify-center gap-x-[6px] hover:shadow-md hover:shadow-green/50"
            onClick={onClick}>
            <Edit className=""/> Edit
        </button>
    )
}
export default ButtonEdit