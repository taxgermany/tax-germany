import React from "react";
import { Button } from "antd";
import clsx from "clsx";
import cls from "./Button.module.scss";

interface IButtonRed {
  text: string;
  onClick?: () => void;
  loading?: boolean;
  className?: string;
  height?: string;
  setActive?: (pr: string) => void;
  active?: string;
  background?: "white" | "red" | "gray";
  width?: string;
  type?:
    | "text"
    | "primary"
    | "link"
    | "ghost"
    | "default"
    | "dashed"
    | undefined;
  htmlType?: "submit" | "button" | "reset" | undefined;
}

const ButtonRed: React.FC<IButtonRed> = ({
  text,
  onClick,
  className,
  height = "52px",
  htmlType = "submit",
  background = "red",
  type = "primary",
  loading = false,
  active,
  setActive,
}) => {
  return (
    <Button
      htmlType={htmlType}
      className={clsx(
        className,
        `w-fit !h-[${height}] !px-[35px] text-[16px] leading-[26px]
                !rounded-[4px] border  hover:!bg-red hover:shadow-md hover:shadow-lightGray !mb-[0px]
                ${
                  active === text
                    ? "bg-[#E9F7EF] border-red text-red"
                    : background === "red"
                    ? "bg-red  !text-white hover:!opacity-80"
                    : background === "white"
                    ? "bg-transparent !text-red hover:!text-white border-[#D9D9D9] hover:!border-red"
                    : "bg-[#DCDCDC] !text-gray hover:!text-white border-none"
                }
                        ${cls}`
      )}
      onClick={onClick}
    >
      {text}
    </Button>
  );
};

export default ButtonRed;
