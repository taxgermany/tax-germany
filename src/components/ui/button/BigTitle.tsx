import React, {FC} from 'react';
import clsx from "clsx";


interface IBigTitle {
    text:string,
    className?: string;
}


const BigTitle:FC<IBigTitle> = ({text,className}) => {   
    return (
      <h3 className={clsx("font-[Inter] font-[600] text-[26px] leading-[34px]",className)}>{text}</h3>
    );
};

export default BigTitle;