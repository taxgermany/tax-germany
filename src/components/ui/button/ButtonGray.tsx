import clsx from 'clsx';
import  {FC} from 'react';

interface IButtonGray {
    text:string,
    onClick?:() => void,
    className?:string,
    size?:string
    backraund?: boolean
}


const ButtonGray:FC<IButtonGray> = ({text,onClick, className, size="w-[132px] h-[52px]"},background = false) => {   
    return (
        <div onClick={onClick}  className={clsx(className,size,`${background ? 'border bg-transparent !text-green transition hover:!bg-green hover:shadow-md hover:shadow-lightGray  hover:!text-white border-[#D9D9D9] hover:!border-green' 
        : 'bg-[#F4F4F4]/70'}
        rounded-[4px] flex justify-center items-center cursor-pointer text-[16px]  px-[24px] text-[#B6B6B6] py-[8px] w-fit  `)}>
            {text}
        </div>
    );
};

export default ButtonGray;