import React from 'react';
import { Button } from "antd";
import clsx from "clsx";
import cls from './Button.module.scss'
import { PoweroffOutlined } from "@ant-design/icons";

interface INewButtonGreen {
    text: string,
    onClick?: () => void,
    loading?: boolean
    className?: string,
    height?: string
    setActive?: (pr: string) => void
    active?: string,
    background?: "white" | "green" | "gray"
    width?: string
    type?: "text" | "primary" | "link" | "ghost" | "default" | "dashed" | undefined
    htmlType?: "submit" | "button" | "reset" | undefined
}


const NewButtonGreen: React.FC<INewButtonGreen> = ({
    text, onClick,
    className,
    height = "52px",
    htmlType = "submit",
    background = "green",
    type = "primary",
    loading = false,
    active, setActive
}) => {

    return (
        <Button htmlType={htmlType}
            loading={loading}
            className={clsx(className, `w-fit !h-[${height}] !px-[35px] text-[16px] leading-[26px] flex items-center justify-center
                !rounded-[4px] border  hover:!bg-green hover:shadow-md hover:shadow-lightGray !mb-[0px]
                ${active === text ? "bg-[#E9F7EF] border-green text-green" :
                    background === "green"
                        ? "bg-green  !text-white hover:!opacity-80"
                        : background === "white"
                            ? "bg-transparent !text-green hover:!text-white border-[#D9D9D9] hover:!border-green"
                            : "bg-[#DCDCDC] !text-gray hover:!text-white border-none"}
                        ${cls}`)}
            onClick={onClick}>
            {text}
        </Button>
    );
};

export default NewButtonGreen;