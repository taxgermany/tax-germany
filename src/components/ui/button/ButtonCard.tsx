import React, { useState , ChangeEvent} from "react";
import clsx from "clsx";
import styles from "./Button.module.scss";
import GreenPopover from "../popover/GreenPopover";
import { Form } from "antd";

interface IButtonCard {
  className?: string;
  setActive: (pr: string) => void;
  text: string;
  popoverText?: string;
  active: string;
  size?: string;
  turn?: boolean;
  onClick? : (ph : any) => void,
  onChange?:(event: ChangeEvent<HTMLTextAreaElement> ) => void
}

const ButtonCard: React.FC<IButtonCard> = ({
  className,
  size = "w-[210px] h-[94px]",
  text,
  setActive,
  onClick,
  active,
  popoverText = "текст Popover",
  turn = false,
  onChange,
}) => {
  

  return (
    <div>
      <button 
        className={clsx(
          styles.buttonStyle,
          className,
          size,
          `${
            active === text
              ? "bg-[#E9F7EF] border-green text-green"
              : active === "error"
              ? "border-[#ff4d4f] text-[#ff4d4f]"
              : "text-green"
          } relative cursor-pointer`
        )}
        onClick={(e) => {
          setActive(text);
          onClick && onClick(e);
        }}
      >
        {text}
        {turn && (
          <GreenPopover
            popoverText={popoverText}
            placement={"rightTop"}
            classNamePopover={"absolute top-[8px] right-[9px]"}
            width={"17px"}
            height="17px"
          />
        )}
      </button>
    </div>
  );
};

export default ButtonCard;
