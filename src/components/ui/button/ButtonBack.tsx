import React, { FC } from "react";
import { useNavigate } from "react-router-dom";
import { ReactComponent as Arrow } from "../../../assets/icons/east.svg";

interface IButtonBack{
  text? :string
  className? : string
}

export const ButtonBack: FC<IButtonBack> = ({text = 'Back', className}) => {
  const history = useNavigate();
  return (
      <div
        onClick={() => history(-1)}
        className={`${className} flex gap-[11px] w-[70px] items-center cursor-pointer`}
      >
        <Arrow />
        <button className="inter text-green font-[500]">{text}</button>
      </div>
  );
};
