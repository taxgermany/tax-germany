import React from 'react';
import clsx from "clsx";
import {Button} from "antd";
import styles from "./Button.module.scss";


interface IButtonWhiteBg{
    className?:string,
    setActive:(pr:string)=>void
    text:string,
    active?:string,
    size?:string,
    loading?:boolean
}
const ButtonWhiteBg:React.FC<IButtonWhiteBg> = ({className,
                                                    size ="w-[240px] h-[65px]",
                                                    text,setActive,active,loading}) => {

    return (
            <Button loading={loading}
                className={clsx(styles.buttonStyle,className,size,`${active === text 
                    ? "bg-[#E9F7EF] border-green text-green" 
                    : active==="error"?"border-[#ff4d4f] text-[#ff4d4f]":"text-green"} 
                       `)} onClick={() => setActive(text)}
            >{text}
            </Button>
    );
};

export default ButtonWhiteBg;