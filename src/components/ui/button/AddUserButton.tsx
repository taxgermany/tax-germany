import React from "react";
import { Button } from "antd";
import clsx from "clsx";
import cls from "./Button.module.scss";


interface IAddUserButton {
  text: string;
  icon?: React.ReactNode;
  onClick?: () => void;
  className?: string;
  height?: string;
  setActive?: (pr: string) => void;
  active?: string;
  background?: "white" | "green" | "gray";
  width?: string;
  type?:
    | "text"
    | "primary"
    | "link"
    | "ghost"
    | "default"
    | "dashed"
    | undefined;
  htmlType?: "submit" | "button" | "reset" | undefined;
}

const AddUserButton: React.FC<IAddUserButton> = ({
  text,
  onClick,
  className,
  icon,
  height = "52px",
  htmlType = "submit",
  background = "green",
  type = "primary",
  active,
  setActive,
}) => {

  return (
    <Button
      htmlType={htmlType}
      className={clsx(
        className,
        `w-fit !h-[40px] !px-[14px] text-[14px] leading-[26px]
                                                          !rounded-[4px] border hover:!bg-green hover:shadow-md hover:shadow-lightGray !mb-[0px]
                                                          ${
                                                            active === text
                                                              ? "bg-[#E9F7EF] border-green text-green"
                                                              : background ===
                                                                "green"
                                                              ? "bg-green !text-white hover:!opacity-80"
                                                              : background ===
                                                                "white"
                                                              ? "bg-transparent !text-green hover:!text-white border-[#D9D9D9] hover:!border-green"
                                                              : "bg-[#DCDCDC] !text-gray hover:!text-white border-none"
                                                          }
                                                          ${cls}`
      )}
      onClick={onClick}
    >
      {icon} {text}
    </Button>
    
  );
};

export default AddUserButton;
