import React, {FC} from 'react';
import clsx from "clsx";


interface ITitle {
    text:string,
    className?: string;
}


const Title:FC<ITitle> = ({text,className}) => {   
    return (
      <h3 className={clsx("font-[600] text-left",className)}>{text}</h3>
    );
};

export default Title;