import React from 'react';
import {Button} from "antd";
import clsx from "clsx";
import cls from './Button.module.scss'

interface IButtonGreen {
    text: string,
    loading?: boolean
    onClick?: () => void,
    className?: string,
    height?: string
    background?: "white" | "green" | "gray"
    width?: string,
    size?: string,
    type?: "text" | "primary" | "link" | "ghost" | "default" | "dashed" | undefined
    htmlType?: "submit" | "button" | "reset" | undefined
    disabled?: boolean
}


const ButtonGreen: React.FC<IButtonGreen> = ({
                                                 text, onClick,
                                                 className,
                                                 height = "52px",
                                                 size = "w-[150px] h-[48px]",
                                                 htmlType = "submit",
                                                 background = "green",
                                                 type = "primary",
                                                 width = "150px",
                                                 disabled,
                                                 loading
                                             }) => {
    return (
        <Button htmlType={htmlType} loading={loading} disabled={disabled}
                className={clsx(className, size, `  text-[16px] leading-[26px] text-center
                !rounded-[4px] border  hover:!bg-green hover:shadow-md hover:shadow-lightGray !mb-[0px]
                ${background === "green"
                    ? "bg-green  !text-white hover:!opacity-80"
                    : background === "white"
                        ? "bg-transparent !text-green hover:!text-white border-[#D9D9D9] hover:!border-green"
                        : "bg-[#DCDCDC] !text-gray hover:!text-white border-none"}
                        ${cls}`)}
                onClick={onClick}>
            {text}
        </Button>
    );
};

export default ButtonGreen;