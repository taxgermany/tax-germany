import clsx from 'clsx';
import React, {FC} from 'react';
import "./InputGray.css";

interface IInputGray {
    text:string,
    switchType?:boolean,
    className?:string,
    type?:string

}

const InputGray:FC<IInputGray> = ({text,switchType,className,type="text",}) => {
    return (
        <input className={clsx(className,'pl-[10px] w-[390px] text-[12px] h-[30px]  border-[#D9D9D9]')} placeholder={text}
        type={switchType ? 'password' : type}
        required
        pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}"
        />
    );
};

export default InputGray;