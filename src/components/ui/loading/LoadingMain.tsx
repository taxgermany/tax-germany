import React from "react";
import cls  from './LoadingMain.module.scss'

interface ILoadingMain{
  className?: string
}
 const LoadingMain: React.FC <ILoadingMain> = ({className}) =>{
  return (
    <div className={`${cls.spinner} ${className}`}>
      <div className={cls.loading}>
      </div>
    </div>
  );
};
export default LoadingMain