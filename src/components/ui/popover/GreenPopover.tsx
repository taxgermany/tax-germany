import React from 'react';
import {ReactComponent as Exclamation} from "../../../assets/icons/info.svg";
import {Popover} from "antd";
import {TooltipPlacement} from "antd/es/tooltip";

interface IGreenInfoPopover {
    popoverText:string
    classNamePopover?:string
    width:string
    height:string
    placement?:TooltipPlacement
}

const GreenPopover:React.FC<IGreenInfoPopover> = ({popoverText,
                                                          placement="rightTop",
                                                          classNamePopover,
                                                          width,height}) => {
    return (
        <Popover title = {popoverText}
                 placement = {placement}
                 className = {classNamePopover}>
            <Exclamation width={width} height={height}/>
        </Popover>
    );
};

export default GreenPopover;