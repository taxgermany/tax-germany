import React, { useState, useEffect, useRef  } from "react";
import {
  EditOutlined,
  DeleteOutlined,
  HolderOutlined,
} from "@ant-design/icons";
import { useDeleteExpertMutation } from "services/superAdminService";
import DeleteAccountModal from "../modal/DeleteAccountModal";
import EditAccountModal from "../modal/EditAccountModal";



interface IHolder {
  id: number;
  firstname: string;
  lastname: string;
  email: string
}

const Holder: React.FC<IHolder> = ({ id, firstname, lastname, email }) => {
  const [isShowDropDown, setShowDropdown] = useState<boolean>(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const [deleteExpert, { isLoading: isLoadingDelete }] =
    useDeleteExpertMutation();

    const handleClickDropdown = () => {
      setShowDropdown(!isShowDropDown);
    };
    
  console.log(isShowDropDown);
  
  const showEditModalHandler = () => {
    setShowEditModal(true);
    setShowDropdown(false);
  };
  const showDeletModalHandler = () => {
    setShowDeleteModal(true);
    setShowDropdown(false);
  };

  const handleDelete = async () => {
    try {
      await deleteExpert(id).unwrap();
      setShowDeleteModal(false);
    } catch (error: any) {
      console.log(error.message);
    }
  };

  const holderRef = useRef<HTMLDivElement | null>(null);


  const handleClickOutside = (event: MouseEvent) => {
    if (holderRef.current && !holderRef.current.contains(event.target as Node)) {
      setShowDropdown(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <div className="Holder"  ref={holderRef}>
      <div className="relative">
        <div className="icon cursor-pointer" onClick={handleClickDropdown}>
          <HolderOutlined  />
        </div>

        <div
          className={`${!isShowDropDown ? "hidden" : ""}
       absolute right-[30px] bg-white shadow z-10 w-[150px] py-6 px-5`}
        >
          <div className="flex flex-col gap-5">
            <div
              onClick={showEditModalHandler}
              className="flex items-center hover:text-lightGray cursor-pointer"
            >
              <EditOutlined className="mr-2" />
              Edit
            </div>
            <div
              className="flex items-center text-red cursor-pointer"
              onClick={showDeletModalHandler}
            >
              <DeleteOutlined className="mr-2" />
              Delete
            </div>
          </div>
        </div>

        <EditAccountModal
          userId={id}
          isOpen={showEditModal}
          setIsOpen={setShowEditModal}
          title={"Edit Account"}
          firstname={firstname}
          lastname={lastname}
          email={email}
        />

        <DeleteAccountModal
          isLoading={isLoadingDelete}
          onDelete={handleDelete}
          isOpen={showDeleteModal}
          setIsOpen={setShowDeleteModal}
          title={"Delete account"}
        />
      </div>
    </div>
  );
};

export default Holder;
