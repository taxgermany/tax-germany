import { Modal } from "./Modal";
import { Form } from "antd";
import ButtonRed from "../button/ButtonRed";

interface IDeleteAccountModal {
  isOpen: boolean;
  title: string;
  setIsOpen: (isOpen: boolean) => void;
  onDelete: () => void; 
  isLoading: boolean;
}

function DeleteAccountModal({
  isOpen = true,
  setIsOpen,
  onDelete,
  isLoading,
}: IDeleteAccountModal) {

  if (isOpen) {
    return (
      <div>
        <Modal
          className="!w-[540px] !h-[300px]"
          isOpen={isOpen}
          setOpen={setIsOpen}
          title="Delete account"
        >
          <Form
            name="personal"
            layout="horizontal"
            wrapperCol={{ span: 22 }}
            autoComplete="off"
          >
            <p className="ml-8 w-[450px]">
              You're trying to delete an expert from your organization. This
              means they will lose access to the administration panel connected
              to your organization. Continue anyway?
            </p>

            <div className="flex justify-end mr-8 mt-12">
            <ButtonRed text={isLoading ? 'Loading...' : 'Delete'} onClick={onDelete} />
              <button className="ml-2 h-[50px] border-2 border-lightGray text-lightGray px-[35px] text-[16px] leading-[26px] rounded-[4px]" onClick={() => setIsOpen(false)}>
                Cancel
              </button>
            </div>
          </Form>
        </Modal>
      </div>
    );
  } else {
    return null;
  }
}

export default DeleteAccountModal;
