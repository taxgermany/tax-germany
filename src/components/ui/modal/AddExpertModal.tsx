import { Form } from "antd";
import { useState } from "react";
import NewButtonGreen from "../button/NewButtonGreen";
import InputNoAppearance from "../input/InputNoAppearance";
import { Modal } from "./Modal";

interface IAddExpertModal {
  isOpen: boolean;
  title: string;
  setIsOpen: (isOpen: boolean) => void;
  onAddUser: (userData: any) => void;
  isLoading: boolean;
}

function AddExpertModal({
  isOpen = true,
  setIsOpen,
  isLoading,
  onAddUser,
}: IAddExpertModal) {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
  });

  const handleAdd = async () => {
    try {
      await onAddUser(formData);

      setIsOpen(false);
    } catch (error) {
      console.error("Ошибка при добавлении пользователя:", error);
    }
  };

  if (isOpen) {
    return (
      <div>
        <Modal isOpen={isOpen} setOpen={setIsOpen} title="Add Expert">
          <Form
            name="personal"
            layout="horizontal"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            onFinish={handleAdd}
            autoComplete="off"
          >
            <div className="flex flex-col justify-center items-center gap-4 ml-6">
              <InputNoAppearance
                wh="w-[400px]"
                inputName={"firstname"}
                placeholder={"First name"}
                registStyle={false}
                background={true}
                border={true}
                type="text"
                value={formData.firstName}
                onChange={(e) =>
                  setFormData({ ...formData, firstName: e.target.value })
                }
              />
              <InputNoAppearance
                wh="w-[400px]"
                inputName={"lastname"}
                placeholder={"Last name"}
                registStyle={false}
                background={true}
                border={true}
                type="text"
                value={formData.lastName}
                onChange={(e) =>
                  setFormData({ ...formData, lastName: e.target.value })
                }
              />
              <InputNoAppearance
                wh="w-[400px]"
                inputName={"email"}
                placeholder={"Email"}
                registStyle={false}
                background={true}
                border={true}
                type="text"
                value={formData.email}
                onChange={(e) =>
                  setFormData({ ...formData, email: e.target.value })
                }
              />
            </div>
            <div className="flex justify-end mr-6 mt-4">
              <NewButtonGreen
                className="items-center"
                text={isLoading ? "Loading..." : "Add"}
              />
            </div>
          </Form>
        </Modal>
      </div>
    );
  } else {
    return null;
  }
}

export default AddExpertModal;
