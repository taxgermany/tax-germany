import BigTitle from '../button/BigTitle';
import style from './Modal.module.css';
import { useNavigate } from 'react-router-dom';
import { CloseOutlined } from '@ant-design/icons';



interface IModal {
  className?: string;
  title: string;
  isOpen: boolean;
  setOpen: (pr: boolean) => void;
  children: React.ReactNode;
}

export function Modal ({ isOpen, setOpen, title, children, className }: IModal) {

  const navigate = useNavigate();

  const handleSubmit = () => {
    
  };

  if (isOpen) {
    return (
      <div className={style.background} onClick={e => e.stopPropagation()}>
        <div className={`${className} ${style.modal}`}>
        <div className="flex justify-end mt-4 mr-4">
          <CloseOutlined className="cursor-pointer" onClick={() => setOpen(!isOpen)}/>
          </div>
        <BigTitle className='ml-8 mt-4 mb-6' text={title}/>
       {children}
        </div>
      </div>
    )
  }else {
    return <></>
  }
}