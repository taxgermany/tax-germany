import { Form, message } from "antd";
import id from "date-fns/esm/locale/id/index.js";
import React, { useState } from "react";
import { useUpdatePersonMutation } from "services/superAdminService";
import NewButtonGreen from "../button/NewButtonGreen";
import InputNoAppearance from "../input/InputNoAppearance";
import { Modal } from "./Modal";

interface IEditAccountModal {
  firstname?: string;
  lastname?: string;
  userId: number;
  email?: string;
  isOpen: boolean;
  title: string;
  setIsOpen: (isOpen: boolean) => void;
}

function EditAccountModal({
  isOpen = true,
  setIsOpen,
  firstname,
  lastname,
  email,
  userId,

}: IEditAccountModal) {
  const [form] = Form.useForm();
  const [editedFirstname, setEditedFirstname] = useState(firstname || "");
  const [editedLastname, setEditedLastname] = useState(lastname || "");
  const [updatePerson, { isLoading, isError, isSuccess }] = useUpdatePersonMutation();

  const handleSave = async () => {
    try {
      const values = await form.validateFields();
      const { firstname, lastname } = values;

      await updatePerson({
        id: userId,
        firstName: firstname,
        lastName: lastname,
      });

      console.log("Изменения сохранены:", firstname, lastname);
      setIsOpen(false);
    } catch (error) {
      console.error("Ошибка при сохранении изменений:", error);
      message.error("Ошибка при сохранении изменений. Пожалуйста, попробуйте еще раз.");
    }
  };

  if (isOpen) {
    return (
      <div>
        <Modal
          className="!w-[450px] !h-[500px]"
          isOpen={isOpen}
          setOpen={setIsOpen}
          title="Edit Account"
        >
          <Form
            form={form}
            name="personal"
            layout="horizontal"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            autoComplete="off"
            initialValues={{
              firstname: firstname,
              lastname: lastname,
              email: email,
            }}
          >
            <div className="flex flex-col justify-center items-center gap-4">
              <div>
                <p className="font-[400] text-lightGray">First name</p>

                <InputNoAppearance
                  wh="w-[400px]"
                  inputName={"firstname"}
                  placeholder={"First name"}
                  registStyle={false}
                  background={true}
                  border={true}
                  type="text"
                />
              </div>
              <div>
                <p className="font-[400] text-lightGray">Last name</p>

                <InputNoAppearance
                  wh="w-[400px]"
                  inputName={"lastname"}
                  placeholder={"Last name"}
                  registStyle={false}
                  background={true}
                  border={true}
                  type="text"
                />
              </div>
              <div>
                <p className="font-[400] text-lightGray">Email</p>
                <InputNoAppearance
                  wh="w-[400px]"
                  inputName={"email"}
                  placeholder={"Email"}
                  registStyle={false}
                  background={true}
                  border={true}
                  type="text"
                  disabled={true}
                />
              </div>
            </div>
            <div className="flex justify-end mr-6 mt-4">
              <NewButtonGreen
                className="items-center"
                text="Save"
                onClick={handleSave}
                loading={isLoading}
              />
            </div>
          </Form>
        </Modal>
      </div>
    );
  } else {
    return null;
  }
}

export default EditAccountModal;
