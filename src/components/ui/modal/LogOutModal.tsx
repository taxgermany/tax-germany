import { Modal } from "./Modal";
import { Form } from "antd";
import ButtonRed from "../button/ButtonRed";
import { useAppDispatch } from "../../../hooks/useAppDispatch";
import { loggedOut } from "../../../redux/auth/authSlice";

interface ILogOutModal {
  isOpen: boolean;
  title: string;
  setIsOpen: (isOpen: boolean) => void;
}

function LogOutModal({ isOpen = true, setIsOpen, 
}: ILogOutModal) {
  const dispatch = useAppDispatch();

  const handleLogOut = () => {
    dispatch(loggedOut());
    localStorage.removeItem("user");

    window.location.href = "/";
  };


  if (isOpen) {
    return (
      <Modal
        className="!w-[450px] !h-[260px]"
        isOpen={isOpen}
        setOpen={setIsOpen}
        title="Log out"
      >
        <Form
          name="personal"
          layout="horizontal"
          wrapperCol={{ span: 22 }}
          autoComplete="off"
        >
          <p className="ml-8 font-[600]">Do you really want to log out?</p>

          <div className="flex justify-end mr-8 mt-12">
            <ButtonRed text="Log Out" onClick={handleLogOut}/>
            <button
              className="ml-2 h-[50px] border-2 border-lightGray text-lightGray px-[35px] text-[16px] leading-[26px] rounded-[4px]"
              onClick={() => setIsOpen(false)}
            >
              Cancel
            </button>
          </div>
        </Form>
      </Modal>
    );
  } else {
    return null;
  }
}

export default LogOutModal;
