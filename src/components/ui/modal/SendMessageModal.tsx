import { Modal } from "./Modal";
import { Form, message } from "antd";
import NewButtonGreen from "../button/NewButtonGreen";
import CustomTextArea from "../input/CustomTextArea";
import axios from "axios";
import { URL_API } from "utils/url";
import { useAppSelector } from "hooks/useAppSelector";
import { useSendMessageMutation } from "services/statusService";

interface ISendMessageModal {
  isOpen: boolean;
  title: string;
  setIsOpen: (isOpen: boolean) => void;
}

function SendMessageModal({
  isOpen = true,
  setIsOpen,
  title,
}: ISendMessageModal) {
  const { client } = useAppSelector((state) => state.authSlice);

  const [sendMessage, { isLoading, isError, isSuccess }] =
    useSendMessageMutation();

  const [form] = Form.useForm();

  const handleSumbit = async () => {
    try {
      const values = await form.validateFields();
      await sendMessage(values.textArea);
      message.success("Message sent successfully");
      form.resetFields();
      setIsOpen(false);
    } catch (error) {
      console.error("Error sending message:", error);
    }
  };

  if (isOpen) {
    return (
      <div>
        <Modal isOpen={isOpen} setOpen={setIsOpen} title="Send Message">
          <Form
            form={form}
            name="personal"
            layout="horizontal"
            wrapperCol={{ span: 22 }}
            autoComplete="off"
          >
            <div className="ml-8">
              <CustomTextArea
                wh="!w-[500px] !h-[193px]"
                inputName="textArea"
                placeholder={"What is the other place?"}
              />
            </div>
            <div className="flex justify-end my-5 mr-8">
              <NewButtonGreen text="Send" onClick={handleSumbit} loading={isLoading}/>
            </div>
          </Form>
        </Modal>
      </div>
    );
  } else {
    return null;
  }
}

export default SendMessageModal;
