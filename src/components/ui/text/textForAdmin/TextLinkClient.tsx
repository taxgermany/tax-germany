import { FC } from "react"

interface ITextLinkClient{
    text : string,
    setActive ?:(pr:string)=>void
    active?:string,
}
const TextLinkClient:FC<ITextLinkClient> = ({text, active, setActive}) =>{
    return (
        <div  className={`${text === active 
        ? 'font-[600] text-green border border-r-0 border-l-0 border-t-0 border-b-green' 
        : ' font-[500] text-[#B6B6B6]'} cursor-pointer font-inter md:text-[14px]`}
        onClick={() => setActive ? setActive(text) : null}>
            {text}
        </div>
    )
}
export default TextLinkClient