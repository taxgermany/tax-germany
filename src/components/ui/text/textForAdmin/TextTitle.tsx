import {FC} from 'react'
interface ITextTitle{
    title: string,
    className? : string
}

const TextTitle: FC<ITextTitle> = ({title, className}) =>{
    return <p className={`${className} font-[700] font-inter text-[#0F0E0E] leading-[26px] text-[24px]`}>{title}</p>
}
export default TextTitle