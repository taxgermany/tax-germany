import React from 'react';
import clsx from "clsx";

interface IValidationText {
    text?:string
    className?:string
}

const ValidationText:React.FC<IValidationText> = ({text="Please select!",className}) => {
    return (
        <div className={clsx("text-[#ff4d4f] font-inter absolute text-[20px] border-0 border-t-1 border-r-0 border-l-0 border-b-1 top-[0] px-[20px] p-[5px] w-full overflow-none",className)}>
            {text}
        </div>
    );
};

export default ValidationText;