import React from 'react';

import {ReactComponent as User} from "../../../assets/icons/account_circle.svg"

const UserLogo = () => {
    return (
        <div className="flex gap-[5px]">
            <User />
            <div>My Account</div>
        </div>
    );
};

export default UserLogo;