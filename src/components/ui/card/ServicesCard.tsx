import {FC, useState} from 'react';
import GreenPopover from '../popover/GreenPopover';

interface IServicesCard {
    currentId: number;
    price: number;
    title: string;
    text: string;
    popoverText: string;
    className?: string;
    active: boolean;
    disabled?: boolean
    handleChoose: (id: number) => void
}


export const ServicesCard: FC<IServicesCard> = ({
                                                    price,
                                                    title,
                                                    className,
                                                    text,
                                                    active,
                                                    disabled = false,
                                                    currentId, handleChoose,
                                                     popoverText
                                                }) => {


    function onClick() {
        handleChoose(currentId)
    }

    return (
        <div
            onClick={onClick}
            className={`${disabled ? "pointer-events-none " : "hover:shadow-4xl"}
             ${active ? "shadow-4xl border-green" : "border-lightGray"}
            relative cursor-pointer p-[16px] text-center rounded-[12px] w-[240px] border h-[295px] select-none 
            `}
        >
            <div className="flex flex-row-reverse">
                <GreenPopover
                    popoverText={popoverText}
                    height="20px"
                    width="20px"
                    placement="rightBottom"

                    //   classNamePopover={"absolute top-[16px] right-[16px]"}
                />
            </div>
            <div className="flex flex-col gap-[40px]">
                <p className="h-[55px] text-green text-center text-[16px] font-[600] mt-[25px] w-full">
                    {title}
                </p>
                <p className="h-[35px] text-lightGray text-[15px] font-[500] mx-auto max-w-[200px]">
                    {text}
                </p>
                <span className="text-[19px] font-[600]">€{price}</span>
            </div>
        </div>
    );
};
