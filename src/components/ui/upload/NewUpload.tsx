import React, {useState} from 'react';
import {ReactComponent as IconTask} from "../../../assets/icons/task.svg";
import {ReactComponent as IconDelete} from "../../../assets/icons/delete.svg";
import clsx from "clsx";
import styles from "./Upload.module.scss";
import {useDeleteFileMutation, useLazyFetchFileNameProfitsQuery} from "../../../services/fileService";
import {IFileName, IFileNameNew} from "../../../interfaces/ProfitsInterface";

interface INewUpload {
    setFileCheck: (el: File) => void
    fileCheck: File | null,
    fileText?: string
    file: IFileName | null
}

const NewUpload: React.FC<INewUpload> = ({setFileCheck, fileCheck, fileText, file}) => {
    const [isDisabled, setIsDisabled] = useState<boolean>(false);
    const [fileUploaded, setFileUploaded] = useState(false);

    const [fileName, setFileName] = useState(file ? file.name : "");
    const [fileSize, setFileSize] = useState(fileCheck ? fileCheck.size / 1024 : 0);

    const [deleteFile, {isLoading: isLoadingDelete}] = useDeleteFileMutation()
    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];

        if (file) {
            setFileCheck(file);
            setFileName(file.name);
            setFileSize(Math.round(file.size / 1024));
        } else {
            setFileSize(0);
        }
    }
    const fileDelete = async () => {
        if (fileCheck && file) {
            await deleteFile({fileId: file.id})
            setFileUploaded(false);

        }
        setFileUploaded(false);
        setFileName('');
    };

    return (
        <div>
            {fileName ? (
                <div className="flex justify-start items-center gap-y-[10px]">

                    <p className="text-14px text-[#B6B6B6] ">{fileText}</p>
                    <div className="flex jusify-between items-center gap-x-[10px]">
                        <div>
                            <div className="flex items-center gap-x-[10px]">
                                <IconTask/>
                                <p className="w-[145px] text-[14px] text-green underline decoration-solid cursor-pointer truncate ">
                                    {" "}
                                    {fileName}
                                </p>

                            </div>
                            <p className="text-[14px] whitespace-nowrap px-[30px]">{`${fileSize} Kb`}</p>
                        </div>
                        <button onClick={fileDelete}>
                            <IconDelete/>
                        </button>
                    </div>
                </div>
            ) : (
                <label
                    htmlFor="fileInput"

                    className={clsx(styles.upload, `${isDisabled
                        ? "text-lightGray cursor-not-allowed bg-gray-200 rounded"
                        : "text-green cursor-pointer hover:bg-green rounded hover:text-white"
                    } w-fit whitespace-nowrap font-[500] border border-lightGray shadow py-2 px-4 bg-white block`)}
                >
                    <input
                        disabled={isDisabled}
                        type="file"
                        id="fileInput"
                        onChange={handleFileChange}
                        className={`file-input !hidden`}
                    />
                    Upload file
                </label>
            )}

        </div>
    );
};

export default NewUpload;
