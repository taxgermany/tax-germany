import { FC } from "react"
interface IAdminRewDocument {
    text: string,
    onClick: () => void,
}
const AdminRewDocument: FC<IAdminRewDocument> = ({text, onClick}) => {

    return (
        <div className="flex justify-between items-center max-w-[690px] gap-x-[35px] pb-[15px] border border-b-[#EEEEEE] border-t-0 border-r-0 border-l-0">
            <p className="max-w-[395px] w-fit text-[#0F0E0E] font-[400] font-inter leading-[26px] ">{text}</p>
            <div className="border border-[#26AF60] p-[10px] rounded-[3px] cursor-pointer"
            onClick={onClick}>
                <p className="text-[#26AF60] font-[400] font-inter leading-[26px] ">Start generating the document</p>
            </div>
        </div>
    )
}
export default AdminRewDocument
