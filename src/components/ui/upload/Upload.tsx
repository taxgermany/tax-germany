import React from 'react';
import { ReactComponent as IconTask } from "../../../assets/icons/task.svg";
import { ReactComponent as IconDelete } from "../../../assets/icons/delete.svg";
import clsx from "clsx";
import styles from "./Upload.module.scss";

interface INewUpload {
    setFileCheck: (el: File | null) => void
    fileCheck: File | null,
    fileTitle?: string,
    background?: string,
    fileId: string,
    classNameText?: string,
    disabled?: boolean
}

const Upload: React.FC<INewUpload> = ({
    setFileCheck,
    fileCheck,
    fileTitle,
    fileId,
    classNameText = "text-green",
    disabled,
    background = 'bg-white' }) => {

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            setFileCheck(file);
        }
    };

    return (
        <div>
            {fileCheck?.name ? (
                <div className="flex  justify-between items-center gap-y-[10px]">
                    <p className="text-14px text-[#B6B6B6] ">{fileTitle}</p>
                    <div className="flex jusify-between items-center gap-x-[22px]">
                        <span className="flex items-center gap-x-[10px]">
                            <IconTask />
                            <p className={`${classNameText} text-[14px] underline decoration-solid cursor-pointer`}>
                                {fileCheck?.name}
                            </p>
                        </span>
                        <button
                        disabled={disabled}
                        onClick={() => setFileCheck(null)}
                        className='cursor-pointer'>
                            <IconDelete />
                        </button>
                    </div>
                </div>
            ) : (

                <div className="flex  justify-between items-center gap-y-[10px]">
                    <p className="text-14px text-[#B6B6B6] ">{fileTitle}</p>
                    <div className="flex jusify-between items-center gap-x-[22px]">
                        <label
                            htmlFor={fileId}
                            className={clsx(styles.upload, `${disabled
                                ? "text-lightGray cursor-not-allowed bg-gray-200 rounded"
                                : "text-green cursor-pointer hover:bg-green rounded hover:text-white"
                                }w-[100px] font-[500] border border-lightGray shadow py-2 px-6 ${background} block`)}
                        >
                            <input
                                disabled={disabled}
                                type="file"
                                id={fileId}
                                onChange={handleFileChange}
                                className={`file-input !hidden`}
                            />
                            Upload file
                        </label>
                    </div>
                </div>
            )}

        </div>
    );
};

export default Upload;
