import React, {FC, useState, FocusEvent} from 'react'
import {IMonths, IMonthsVot, ITable, ITableVot} from "../../../interfaces/TableInterface";
import {parseISO, format} from "date-fns";
import cls from './Table.module.scss'
import clsx from "clsx";
import GreenPopover from "../popover/GreenPopover";
import NewUpload from "../upload/NewUpload";
import {IFileName, IFileNameNew} from "../../../interfaces/ProfitsInterface";


export const titleList = [
    ['Gross revenue, incl. VAT'],
    ['Gross revenue', 'Without VAT'],
    ['VAT received', 'Without VAT'],
    ['Expenses'],
    ['Office supplies', 'excl. VAT'],
    ['Office rent', "excl. VAT"],
    ['Insurance excl. Health', "excl. VAT"],
    ['Public transportation', "excl. VAT"],
    ['Telephone', "excl. VAT"],
    ['Sum of VAT (from all mentioned expenses)'],
    ['VAT paid to Tax Office'],
    ['Other expenses'],
    ['Monthly profits'],
]
export const titleListWithoutVot = [
    ['Gross revenue, incl. VAT'],
    ['Gross revenue', 'Without VAT'],
    ['Expenses'],
    ['Office supplies', 'excl. VAT'],
    ['Office rent', "excl. VAT"],
    ['Insurance excl. Health', "excl. VAT"],
    ['Public transportation', "excl. VAT"],
    ['Telephone', "excl. VAT"],
    ['Other expenses'],
    ['Monthly profits'],
]

type keyType =
    "grossRevenueInclVat"
    | "grossRevenue"
    | "expenses"
    | "officeSupplies"
    | "officeRent"
    | "insuranceExclHealth"
    | "publicTransport"
    | "telephone"
    | "otherExpenses"
    | "monthlyProfits"


interface ILargeTable {
    data: ITableVot | ITable
    setDataSource: (obj: ITableVot | ITable) => void
    vat: boolean | null
    setFileCheck: (el: File) => void
    setDataLargeChange: (el: boolean) => void
    fileCheck: File | null
    file: IFileName | null
    disabled: boolean

}


const LargeTable: FC<ILargeTable> = ({
                                         setDataLargeChange, file,
                                         data, setDataSource, vat, setFileCheck,
                                         fileCheck, disabled,
                                     }) => {


    let title = vat ? titleList : titleListWithoutVot

    const onChangeFormat = (event: React.FocusEvent<HTMLInputElement>, item: IMonthsVot | IMonths, el: [string, any],
                            indexArr: number, indexObj: number) => {
        setDataLargeChange(true)
        const newData = {...data, months: [...data.months], total: {...data.total}}; // deep copy
        let key: string = el[0]
        let key1 = el[0] as keyType
        let value: number = el[1]
        let monthObj = {
            ...item, // copy obj

            [key]: Number(event.target.value), //rewrite key and value

            grossRevenueInclVat: +(key === "grossRevenue" || key === "vatReceived"
                ? item.grossRevenueInclVat - value + Number(event.target.value)    //меняет занчение обьекта grossRevenueInclVat
                : item.grossRevenueInclVat).toFixed(2),

            expenses: +(key !== "grossRevenue" && key !== "vatReceived"
                ? item.expenses - value + Number(event.target.value)          //меняет занчение обьекта expenses
                : item.expenses).toFixed(2),
        }
        let newMonthObj = {...monthObj, monthlyProfits: +(monthObj.grossRevenueInclVat - monthObj.expenses).toFixed(2)}


        newData.months.splice(indexArr, 1, {
            ...newMonthObj,
        });

        newData.total = {
            ...newData.total,

            [key]: (newData.total[key1] - value + Number(event.target.value)).toFixed(2), //rewrite key and value

            grossRevenueInclVat: Number((key === "grossRevenue" || key === "vatReceived"
                ? (newData.total.grossRevenueInclVat - value + Number(event.target.value))  //меняет занчение обьекта grossRevenueInclVat
                : (newData.total.grossRevenueInclVat)).toFixed(2)),

            expenses: Number((key !== "grossRevenue" && key !== "vatReceived"
                ? (newData.total.expenses - value + Number(event.target.value))      //меняет занчение обьекта expenses
                : newData.total.expenses).toFixed(2)),
        }

        newData.total = {
            ...newData.total,
            monthlyProfits: Number((newData.total.grossRevenueInclVat - newData.total.expenses).toFixed(2))
        }
        newData.totalYearProfit = newData.total.monthlyProfits
        setDataSource(newData)
    };

    return (
        <div>
            <div>
                <table className={clsx(!vat ? cls.largeTable : cls.largeTableVat, `overflow-x-auto !m-0 `)}>
                    <thead>
                    <tr className="h-[40px]">
                        <th className='border-r border-[#80808026] border-l-[transparent]'></th>
                        {data && data.months.map(item =>
                            <th key={item.id} className={`${data.months.length > 3 ? "w-[100px]" : ""} 
                            border border-y-[transparent] text-[15px] font-[100]
                                font-Inter text-lightGray border-[#80808026]`}>
                                {format(parseISO(item.month), "LLLL")}
                            </th>
                        )}
                        <th className={`${data.months.length > 3 ? "w-[100px]" : ""} border-x border-r-[transparent] border-[#80808026] font-Inter
                            font-[100] text-lightGray`}>
                            Total
                        </th>

                    </tr>
                    </thead>
                    <tbody className={clsx(cls.largeTable, disabled ? "pointer-events-none" : "")}>

                    <td className='border-x border-l-[transparent] border-[#80808026] p-0 w-[250px]'>
                        {
                            title.map((item, index) => <tr key={item[0]}
                                                           className=' border-y border-[#80808026] whitespace-nowrap'>
                                    <div className=" h-[40px] px-[8px] flex items-center gap-x-5 w-[290px]">
                                        <div className="grid content-center">
                                            <div className='flex font-Inter font-[500]  text-[14px] items-center'>
                                                {item[0]}
                                            </div>
                                            <div
                                                className='flex text-[#B6B6B6] font-Inter text-[12px] items-center'>
                                                {item[1]}
                                            </div>
                                        </div>
                                        {(item[0] === 'VAT received' || item[0] === 'Public transportation'
                                                || item[0] === 'VAT paid to Tax Office')
                                            && <GreenPopover popoverText={item[0]} width={"14px"} height={"14px"}/>
                                        }
                                    </div>
                                </tr>
                            )
                        }
                    </td>
                    {data && data.months.map((item, indexArr) =>

                        <td key={item.id} className=' border-x border-[#80808026] p-0 '>{
                            Object.entries(item).map((el, indexObj) => (indexObj === 0 || indexObj === 1)
                                ? ""
                                : el[0] === "grossRevenueInclVat" || el[0] === "expenses" || el[0] === "monthlyProfits"
                                    ? <tr key={el[0]} className=' border-y border-[#80808026]'>
                                        <input
                                            className={`${data.months.length > 3 ? "w-[100px]" : "150px"} h-[40px]
                                             bg-transparent placeholder:text-black
                                            text-black text-center font-[500] border-none outline-none
                                focus:bg-[#FAFAFA] focus:!border-green hover:bg-[#FAFAFA] text-[17px]`}
                                            value={el[1]}// Pass event.target.value as the argument
                                        />
                                    </tr>
                                    : <tr key={el[0]} className='border-y border-[#80808026]'>
                                        <input type="number"
                                               onFocus={(e: FocusEvent<HTMLInputElement>) => e.target.select()}
                                               className={clsx(cls.inputStyle, `${data.months.length > 3 ? "w-[100px]" : "150px"}
                                                h-[40px] bg-transparent placeholder:text-black text-[17px]
                                              text-black text-center font-[500] border-none outline-none 
                                              focus:bg-[#FAFAFA] focus:!border-green hover:bg-[#FAFAFA]`)}
                                               defaultValue={el[1]}
                                               min={0}
                                            /*value={el[1]}*/
                                               onBlur={event => onChangeFormat(event, item, el, indexArr, indexObj)}  // Pass event.target.value as the argument
                                        />
                                    </tr>
                            )
                        }
                        </td>
                    )}
                    <td className=' border-x border-r-[transparent] border-[#80808026] p-0 w-[100px]'>
                        {
                            Object.entries(data.total).map((item) =>
                                <tr key={item[0]}
                                    className={`${data.months.length > 3 ? "w-[100px]" : "w-full"}  
                                    border-y border-[#80808026] whitespace-nowrap bg-[#FAFAFA] pointer-events-none`}>
                                    <div
                                        className={`${data.months.length > 3 ? "w-[100px]" : ""}
                                        h-[40px] px-[8px] w-[100px] flex items-center text-black
                                            justify-center font-[500]`}>
                                        <div
                                            className='font-[500] text-[17px]'>
                                            {item[1]}
                                        </div>
                                    </div>

                                </tr>
                            )
                        }
                    </td>
                    <tr className='h-[60px]'>
                        <td className='font-[Inter] px-[8px] leading-[26px] text-[#0C0056] text-[16px] whitespace-nowrap'>
                            <div className='items-center font-bold'>
                                Yearly tax return
                            </div>
                        </td>
                        <td className='rounded-[9px] px-2 py-1 border-x border-[#80808026]'
                            colSpan={data.months.length > 3 ? data.months.length - 2 : data.months.length > 1 ? 2 : 1}>
                            <NewUpload setFileCheck={setFileCheck} fileCheck={fileCheck} file={file}/>
                        </td>

                        <td className={`${data.months.length > 2 ? "" : "hidden"} font-[Inter] py-3 leading-[26px] 
                        text-[#0C0056] text-[16px] whitespace-nowrap`}
                            colSpan={data.months.length > 3 ? 2 : 1}>
                            <div className='items-center font-bold'>
                                Total yearly profit
                            </div>
                        </td>
                        <td className='p-0  flex items-center justify-between'>
                            <p className={`border-l border-[#80808026] flex justify-center items-center
                            w-[100px] h-[60px] font-[500] text-[17px] `}>
                                {data.totalYearProfit}
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default LargeTable;
