import React, {useContext, useEffect, useRef, useState} from 'react';
import {Form, Input, InputRef, Table} from "antd";
import clsx from "clsx";
import s from "./NewMiniTable.module.scss"
import "./MiniTable.scss"
import {FormInstance} from "antd/es/form";
import {ColumnTypes, DataType} from "../../../utils/MiniTable";


const EditableContext = React.createContext<FormInstance<any> | null>(null);

interface EditableRowProps {
    index: number;
}

const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
        <Form form={form} component={false}>
            <EditableContext.Provider value={form}>
                <tr {...props} />
            </EditableContext.Provider>
        </Form>
    );
};


interface EditableCellProps {
    title: React.ReactNode;
    editable: boolean;
    children: React.ReactNode;
    dataIndex: keyof DataType;
    record: DataType;
    handleSave: (record: DataType) => void;
}

const EditableCell: React.FC<EditableCellProps> = ({
                                                       title,
                                                       editable,
                                                       children,
                                                       dataIndex,
                                                       record,
                                                       handleSave,
                                                       ...restProps
                                                   }) => {
    const [editing, setEditing] = useState(false);
    const inputRef = useRef<InputRef>(null);
    const form = useContext(EditableContext)!;

    useEffect(() => {
        if (editing) {
            inputRef.current!.focus();
        }
    }, [editing]);

    const toggleEdit = () => {
        setEditing(!editing);
        form.setFieldsValue({ [dataIndex]: record[dataIndex] });
    };

    const save = async () => {
        try {
            const values = await form.validateFields();

            toggleEdit();
            handleSave({ ...record, ...values });
        } catch (errInfo) {
            console.log('Save failed:', errInfo);
        }
    };

    let childNode = children;

    if (editable) {
        childNode = editing ? (<div>
            <Form.Item
                className="!m-0 !py-0 !h-[38
            px] overflow-auto"
                name={dataIndex}
                /*rules={[
                    {
                        required: true,
                        message: `${title} is required.`,
                    },
                ]}*/
            >
                <Input ref={inputRef} onPressEnter={save} onBlur={save}  rootClassName={clsx(s.inputForTable)} type={"number"}
                       className={clsx(s.inputForTable, `'w-full  hover:!border-green bg-transparent
                   font-inter rounded-none outline-none border-none  hover:bg-[#FAFAFA]
                   text-center focus:bg-[#FAFAFA] focus:!border-green  w-[100%] cursor-pointer`)}/>
            </Form.Item>
            </div>
        ) : (
            <div className="editable-cell-value-wrap" style={{ paddingRight: 24 }} onClick={toggleEdit}>
                {children}
            </div>
        );
    }

    return <td {...restProps}>{childNode}</td>;
};





interface INewMiniTable{
    columns: (ColumnTypes[number] & { editable?: boolean; dataIndex: string })[]
    dataSource: DataType[]
    setDataSource:(ar:DataType[])=>void
}

const NewMiniTable:React.FC<INewMiniTable> = ({columns,dataSource,setDataSource}) => {


// Вычисляем общую сумму и обновляем значение "total" для каждого элемента в массиве "data"


    const handleSave = (row: DataType) => {
        const newData = [...dataSource];
        const index = newData.findIndex((item) => row.key === item.key);
        const item = newData[index];
        let total = 0;

        for (const [key, value] of Object.entries(row)) {
            if (key !== "key" && key !== "titleRaw" && key !== "total") {
                total += parseInt(value);
            }
        }

        newData.splice(index, 1, {
            ...item,
            ...row,total:String(total)
        });


        setDataSource(newData);
    };

    const components = {
        body: {
            row: EditableRow,
            cell: EditableCell,
        },
    };

    const columnsEdit = columns.map((col) => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record: DataType) => ({
                record,
                editable: col.editable,
                dataIndex: col.dataIndex,
                title: col.title,
                handleSave,
            }),
        };
    });


    return (
        <div className={"miniTable"}>
            <Table rootClassName={clsx(s.table)}
                   className={clsx(s.table)}
                   /*rowClassName={() => 'editable-row'}*/
                   columns={columnsEdit as ColumnTypes}
                   components={components}
                   dataSource={dataSource} pagination={false}
                   bordered scroll={{x: 1350}} size="small"/>
        </div>

    );
};

export default NewMiniTable;