import React, {FC, FocusEvent} from 'react'
import {IMiniTableMonths, IMiniTableWithTotal} from "../../../interfaces/TableInterface";
import {parseISO, format} from "date-fns";
import cls from './Table.module.scss'
import clsx from "clsx";


/*export const years = [
    {
        "id": 1,
        "yearValue": 2023
    }, {
        "id": 2,
        "yearValue": 2022
    }, {
        "id": 3,
        "yearValue": 2021
    }
]*/

interface MiniTableType {
    data: IMiniTableWithTotal
    setDataSource: (obj: IMiniTableWithTotal) => void
    setDataMiniChange: (el: boolean) => void
    disabled: boolean
    name:string
}


const RowTable: FC<MiniTableType> = ({data, setDataSource, disabled, setDataMiniChange,name}) => {
    console.log(data);
    
    const onChangeFormat = (event: React.FocusEvent<HTMLInputElement>, item: IMiniTableMonths,
                            indexArr: number) => {

        setDataMiniChange(true)
        const newData = {...data, months: [...data.months]}; // deep copy
        let newMonthObj = {
            ...item, // copy obj
            "value": Number(event.target.value), //rewrite key and value
        }
        newData.months.splice(indexArr, 1, {
            ...newMonthObj,
        });
        setDataSource({...newData,total:newData.total + Number(event.target.value)})
    };
    return (
        <div>
            <table className={clsx(`overflow-x-auto !m-0 `)}>
                <thead>
                <tr className="h-[40px]">
                    <th className='w-[180px] border-r border-[#80808026] border-l-[transparent]'></th>
                    {data.months.map(item =>
                        <th key={item.id} className='w-[100px] border border-t-[transparent] border-[#80808026]
                        text-[15px] font-[100] font-Inter text-lightGray border-[#80808026]'>
                            {format(parseISO(item.month), "LLLL")}
                        </th>
                    )}
                    <th className='w-[100px] border-x border-[#80808026] font-[100]
                     font-Inter text-lightGray'>
                        Total
                    </th>

                </tr>
                </thead>
                <tbody className={clsx(cls.largeTable, disabled ? "pointer-events-none" : "")}>

                <tr className=' border border-[#80808026] p-0 w-[100px]'>
                    <td
                        className=' border-x border-l-[transparent] border-[#80808026] p-0 whitespace-nowrap'>
                        <div className="h-[40px] px-[8px] flex items-center gap-x-5 w-[180px]">
                            <div className='flex font-Inter font-[500]  text-[14px] items-center'>
                                {name}
                            </div>
                        </div>
                    </td>
                    {data.months.map((item, indexArr) =>
                        <td key={item.id} className='border-x  border-[#80808026]'>
                            <input type="number"
                                   onFocus={(e: FocusEvent<HTMLInputElement>) => e.target.select()}
                                   className={clsx(cls.inputStyle,`w-[100px] h-[40px] bg-transparent placeholder:text-black
                                              text-black text-center font-[500] border-none outline-none focus:bg-[#FAFAFA]
                                               focus:!border-green hover:bg-[#FAFAFA] text-[17px]`)}
                                   defaultValue={item.value}
                                   onBlur={event => onChangeFormat(event, item, indexArr)}  // Pass event.target.value as the argument
                            />
                        </td>
                    )}
                    <td className='w-[100px]  border-y border-[#80808026] whitespace-nowrap bg-[#FAFAFA] pointer-events-none'>
                        <div className="w-[100px] h-[40px] px-[8px] w-[100px] flex items-center text-black
                                        justify-center font-[500]">
                            <div className='font-[500] text-[17px]'>
                                {data.total}
                            </div>
                        </div>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    );
};

export default RowTable;
