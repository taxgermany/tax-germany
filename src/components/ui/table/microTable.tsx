


import React, {FC, FocusEvent} from "react";
import cls from './Table.module.scss'
import {format, parseISO} from "date-fns";
import {IEmployer, IMicroTable, IMiniTableMonths} from "../../../interfaces/TableInterface";
import clsx from "clsx";
import GreenPopover from "../popover/GreenPopover";

const title= [
    "Commercial employees",
    "Trainees",
    "Of which part-time workers",
    "Family members"
]

interface MicroTableType {
  data: IEmployer[]
  setDataSource: (obj: IEmployer[]) => void
  disabled: boolean
}

const MicroTable: FC<MicroTableType> = ({disabled ,data,setDataSource}) => {

    const currentDate = new Date();

  const formattedDate = format(currentDate, "yyyy");

  const onChangeFormat = (event: React.FocusEvent<HTMLInputElement>, item: IEmployer, el: [string, any],
                          indexArr: number) => {
    const newData = [...data]; // deep copy
    let key: string = el[0]
    let newMonthObj = {
      ...item, // copy obj
      [key]: Number(event.target.value), //rewrite key and value
    }
    newData.splice(indexArr, 1, {
      ...newMonthObj,
    });
    setDataSource(newData)
  };


  return (
    <table className={cls.tableDisabled }>
      <thead>
        <tr>
          <th className='px-[25px] border-solid border-[1px] border-[#80808026] border-t-[transparent] border-l-[transparent]' ></th>
          {
              data.map(item=> <th className='px-[10px] border-solid border-[1px] border-t-[transparent] text-[14px]
           font-[100] font-[Inter] text-lightGray border-[#80808026]'>
                  {`${formattedDate}-01-01` === item.year
            ? "Current year"
            : format(parseISO(item.year),"yyyy")}
          </th>)
          }
        </tr>
      </thead>
      <tbody className={clsx(disabled ? "pointer-events-none" : "")}>
      <td className='border-x border-l-[transparent] border-[#80808026] p-0 '>
        {
          title.map((item, index) => <tr key={item}
                  className=' border-y border-[#80808026] whitespace-nowrap'>
                <div className="h-[40px] px-[8px] flex items-center gap-x-5 w-[250px]">
                  <div className="grid content-center">
                    <div className='flex font-Inter font-[500]  text-[14px] items-center'>
                      {item}
                    </div>
                  </div>
                  {(item === "Commercial employees" || item === "Of which part-time workers")
                      && <GreenPopover popoverText={item} width={"14px"} height={"14px"}/>
                  }
                </div>
              </tr>
          )
        }
      </td>
      {data.map((item, indexArr) =>

          <td className=' border-x border-[#80808026] p-0 w-[120px] hover:bg-[#FAFAFA]'>{
            Object.entries(item).map((el, indexObj) => (indexObj === 0 || indexObj === 1)
                ? ""
                : <tr className='border-y border-[#80808026]'>
                  <input type="number"
                         onFocus={(e: FocusEvent<HTMLInputElement>) => e.target.select()}
                         className={clsx(cls.inputStyle,`w-[120px] h-[40px] bg-transparent placeholder:text-black
                                              text-black text-center font-[500] border-none outline-none focus:bg-[#FAFAFA]`)}
                         defaultValue={el[1]}
                         min={0}
                         onBlur={event => onChangeFormat(event, item, el, indexArr)}  // Pass event.target.value as the argument
                  />
                </tr>
            )
          }
          </td>
      )}
      </tbody>
    </table>
  );
};

export default MicroTable;
