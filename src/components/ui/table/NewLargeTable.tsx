import React, { useContext, useEffect, useRef, useState } from 'react';
import { Form, Input, InputRef, Table } from "antd";
import clsx from "clsx";
import s from "./NewLargeTable.module.scss"
import { FormInstance } from "antd/es/form";
import { ColumnTypes, dataSourceMiniObj, DataType } from "../../../utils/MiniTable";


const EditableContext = React.createContext<FormInstance<any> | null>(null);

interface EditableRowProps {
    index: number;
}

const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
        <Form form={form} component={false}>
            <EditableContext.Provider value={form}>
                <tr {...props} />
            </EditableContext.Provider>
        </Form>
    );
};


interface EditableCellProps {
    title: React.ReactNode;
    editable: boolean;
    children: React.ReactNode;
    dataIndex: keyof DataType;
    record: DataType;
    dataSource: DataType[]
    handleSave: (record: DataType, dataIndex: string, value: string) => void;
    disabled: boolean
}

const EditableCell: React.FC<EditableCellProps> = ({
    title,
    editable,
    children,
    dataIndex,
    record,
    handleSave,
    dataSource,
    disabled,

    ...restProps
}) => {
    const [editing, setEditing] = useState(false);
    const inputRef = useRef<InputRef>(null);
    const form = useContext(EditableContext)!;

    const [gross, setGross] = useState<DataType>({ ...dataSourceMiniObj, titleRaw: ['Gross revenue, incl. VAT'], })
    const [expenses, setExpenses] = useState<DataType>({ ...dataSourceMiniObj, titleRaw: ['Expenses'], })
    const [profits, setProfits] = useState<DataType>({ ...dataSourceMiniObj, titleRaw: ['Monthly profits'], })


    useEffect(() => {
        if (editing) {
            inputRef.current!.focus();
        }
    }, [editing]);

    const toggleEdit = () => {
        setEditing(!editing);
        form.setFieldsValue({ [dataIndex]: record[dataIndex] });
    };

    const save = async () => {
        try {
            const values = await form.validateFields();
            let currentSum = Number(record["total"]) + Number(values[dataIndex])

            if (record.key === "2" || record.key === "3") {
                let sum = Number(gross[dataIndex]) + Number(values[dataIndex]) //складывает текуший с месяцом
                let sumTotal = Number(gross["total"]) + sum
                let newObj = { ...gross, [dataIndex]: String(sum), ["total"]: String(sumTotal) }
                let profitObj = { ...profits, [dataIndex]: String(sum) }
                setGross((prev) => ({ ...prev, [dataIndex]: String(sum), ["total"]: String(sumTotal) }))
                setProfits(profitObj)
            }
            /* if (record.key === "5" || record.key === "6" || record.key === "7" || record.key === "8" || record.key === "9" || record.key === "10" || record.key === "11" || record.key === "12") {
                 let sum = Number(expenses[dataIndex]) + Number(values[dataIndex])
                 let sub = Number(gross[dataIndex]) - sum
                 let subTotal = Number(gross["total"]) - sum
                 let newObj = {...expenses,[dataIndex]:String(sub)}
                 setExpenses(newObj)
             }*/

            toggleEdit();
            handleSave({ ...record, ...values, ["total"]: String(currentSum) }, dataIndex, values[dataIndex]);
        } catch (errInfo) {
            console.log('Save failed:', errInfo);
        }
    };

    let childNode = children;

    if (editable) {
        childNode = editing ? (<div>
            <Form.Item
                className="!m-0 !py-0 !h-[38
            px] overflow-auto"
                name={dataIndex}
            /*rules={[
                {
                    required: true,
                    message: `${title} is required.`,
                },
            ]}*/
            >
                <Input ref={inputRef} onPressEnter={save} onBlur={save} rootClassName={clsx(s.inputForTable)}
                    type={"number"}
                    className={clsx(s.inputForTable, `'w-full  hover:!border-green bg-transparent
                   font-inter rounded-none outline-none border-none  hover:bg-[#FAFAFA]
                   text-center focus:bg-[#FAFAFA] focus:!border-green  w-[100%] cursor-pointer`)} />
            </Form.Item>
        </div>
        ) : (
            <div className="editable-cell-value-wrap" style={{ paddingRight: 24 }} onClick={toggleEdit}>
                {children}
            </div>
        );
    }

    return <td {...restProps}>{childNode}</td>;
};


interface INewMiniTable {
    columns: (ColumnTypes[number] & { editable?: boolean; dataIndex: string; disabled?: boolean })[]
    dataSource: DataType[]
    setDataSource: (ar: DataType[]) => void
}

const NewLargeTable: React.FC<INewMiniTable> = ({ columns, dataSource, setDataSource }) => {


    const handleSave = (row: DataType, dateIndex: string, value: string) => {
        const newData = [...dataSource];
        const index = newData.findIndex((item) => row.key === item.key);
        const indexGross = newData.findIndex((item) => "1" === item.key);
        const indexExpenses = newData.findIndex((item) => "4" === item.key);
        const indexProfits = newData.findIndex((item) => "13" === item.key);
        const item = newData[index];


        /* for (const [key, value] of Object.entries(row)) {
             if (key !== "key" && key !== "titleRaw" && key !== "total") {
                 total += parseInt(value);
             }

         }*/
        const a = [
            {
                "id": 0,
                "year": "2022-07-28",
                "totalPension": 0,
                "months": [
                    {
                        "id": 0,
                        "month": "2023-01-28",
                        "grossRevenueInclVat": 0,
                        "grossRevenue": 0,
                        "vatReceived": 0,
                        "expenses": 0,
                        "officeSupplies": 0,
                        "officeRent": 0,
                        "insuranceExclHealth": 0,
                        "publicTransport": 0,
                        "telephone": 0,
                        "sumOfVat": 0,
                        "vatPaidToTaxOffice": 0,
                        "otherExpenses": 0,
                        "monthlyProfits": 0,
                        "healthInsurance": 0,
                        "pensionContribution": 0,
                    },
                    {
                        "id": 0,
                        "month": "2023-02-28",
                        "grossRevenueInclVat": 0,
                        "grossRevenue": 0,
                        "vatReceived": 0,
                        "expenses": 0,
                        "officeSupplies": 0,
                        "officeRent": 0,
                        "insuranceExclHealth": 0,
                        "publicTransport": 0,
                        "telephone": 0,
                        "sumOfVat": 0,
                        "vatPaidToTaxOffice": 0,
                        "otherExpenses": 0,
                        "monthlyProfits": 0,
                        "healthInsurance": 0,
                        "pensionContribution": 0,
                    },
                    {
                        "id": 0,
                        "month": "2023-03-28",
                        "grossRevenueInclVat": 0,
                        "grossRevenue": 0,
                        "vatReceived": 0,
                        "expenses": 0,
                        "officeSupplies": 0,
                        "officeRent": 0,
                        "insuranceExclHealth": 0,
                        "publicTransport": 0,
                        "telephone": 0,
                        "sumOfVat": 0,
                        "vatPaidToTaxOffice": 0,
                        "otherExpenses": 0,
                        "monthlyProfits": 0,
                        "healthInsurance": 0,
                        "pensionContribution": 0,
                    },
                ],
            },
            {
                "id": 1,
                "year": "2023-07-28",
                "totalPension": 0,
                "months": [
                    {
                        "id": 0,
                        "month": "2023-01-28",
                        "grossRevenueInclVat": 0,
                        "grossRevenue": 0,
                        "vatReceived": 0,
                        "expenses": 0,
                        "officeSupplies": 0,
                        "officeRent": 0,
                        "insuranceExclHealth": 0,
                        "publicTransport": 0,
                        "telephone": 0,
                        "sumOfVat": 0,
                        "vatPaidToTaxOffice": 0,
                        "otherExpenses": 0,
                        "monthlyProfits": 0,
                        "healthInsurance": 0,
                        "pensionContribution": 0,
                    },
                    {
                        "id": 0,
                        "month": "2023-02-28",
                        "grossRevenueInclVat": 0,
                        "grossRevenue": 0,
                        "vatReceived": 0,
                        "expenses": 0,
                        "officeSupplies": 0,
                        "officeRent": 0,
                        "insuranceExclHealth": 0,
                        "publicTransport": 0,
                        "telephone": 0,
                        "sumOfVat": 0,
                        "vatPaidToTaxOffice": 0,
                        "otherExpenses": 0,
                        "monthlyProfits": 0,
                        "healthInsurance": 0,
                        "pensionContribution": 0,
                    },
                    {
                        "id": 0,
                        "month": "2023-03-28",
                        "grossRevenueInclVat": 0,
                        "grossRevenue": 0,
                        "vatReceived": 0,
                        "expenses": 0,
                        "officeSupplies": 0,
                        "officeRent": 0,
                        "insuranceExclHealth": 0,
                        "publicTransport": 0,
                        "telephone": 0,
                        "sumOfVat": 0,
                        "vatPaidToTaxOffice": 0,
                        "otherExpenses": 0,
                        "monthlyProfits": 0,
                        "healthInsurance": 0,
                        "pensionContribution": 0,
                    },
                ],
            },
        ]


        newData.splice(index, 1, {
            ...item,
            ...row,
        });

        // const arr = [
        //     {
        //         year : '20220',
        //         profits : [
        //             {
        //                 pensionContributions : 0,
        //                 month : 'jule',
        //             },
        //             {
        //                 pensionContributions : 0,
        //                 month : 'jule',
        //             },
        //             {
        //                 pensionContributions : 0,
        //                 month : 'jule',
        //             },
        //             {
        //                 pensionContributions : 0,
        //                 month : 'jule',
        //             },
        //         ]
        //     }
        // ]


        /*   newData.splice(indexGross, 1, {
               ...itemGross,
               ...gross,
           });
           newData.splice(indexGross, 1, {
               ...itemExpenses,
               ...expenses,
           });
           newData.splice(indexGross, 1, {
               ...itemProfits,
               ...profits,
           });
   */
        setDataSource(newData);
    };

    const components = {
        body: {
            row: EditableRow,
            cell: EditableCell,
        },
    };

    const columnsEdit = columns.map((col) => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record: DataType) => ({
                record,
                editable: col.editable,
                dataIndex: col.dataIndex,
                title: col.title,
                handleSave,
                dataSource,
            }),
        };
    });

    return (
        <div className={"miniTable"}>
            <Table rootClassName={clsx(s.largeTable)}
                className={clsx(s.largeTable)}
                /*rowClassName={() => 'editable-row'}*/
                columns={columnsEdit as ColumnTypes}
                components={components}
                dataSource={dataSource} pagination={false}
                bordered scroll={{ x: 1350 }} size="small"/>
        </div>

    );
};

export default NewLargeTable;