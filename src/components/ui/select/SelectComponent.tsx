import clsx from "clsx";
import cls from "../../ui/select/select.module.scss";
import { FC, ChangeEvent } from "react";
import { Form, Select } from "antd";
import { IOption } from "pages/freelancerPage/personalData/PersonalData";
interface iSelectComponent {
  label?: string;
  background?: boolean;
  className?: string;
  size?: string;
  border?: boolean;
  options?: Array<IOption>;
  placeholder?: string;
  selectName?: string;
  disabled?: boolean;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  [key: string]:
    | string
    | ((event: ChangeEvent<HTMLInputElement>) => void)
    | boolean
    | undefined
    | Array<object>;
}

const SelectComponent: FC<iSelectComponent> = ({
  size,
  selectName,
  className,
  background = false,
  border = false,
  label,
  options,
  onChange,
  placeholder,
  ...props
}) => {
  return (
    <Form.Item
      className="!m-0"
      label={label}
      name={selectName}
      rules={[{ required: true, message: `Please choose your ${placeholder}` }]}
    >
      <Select
        className={clsx(`${cls.i} ${size ? size : "!w-[560px] !h-[48px]"} ${className} ${
          background ? "!bg-[#F4F4F4]/70" : "!bg-white/0"
        } 
        ${border ? '!border !border-black/0.23 hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'} 
        !rounded-[4px] font-[400] focus:!border-green cursor-pointer  shadow-none`)}
        options={options}
        onChange={onChange}
        placeholder={placeholder}
        
        {...props}
      />
    </Form.Item>
  );
};
export default SelectComponent;
