import React, { ChangeEvent, FC } from 'react';
import { Form, Select } from "antd";
import clsx from "clsx";
import cls from "./selectForAdmin.module.scss";

import { IOption } from "../../../../interfaces/antdInterface";
import { CloseOutlined } from "@ant-design/icons";


interface IFilterSelect {
    label?: string;
    className?: string;
    options?: Array<IOption>;
    placeholder?: string;
    selectName?: string;
    text?: string;
    onChange: (value: any, option: IOption | IOption[]) => void
}

const FilterSelect: FC<IFilterSelect> = ({
    selectName,
    className,
    label,
    options,
    onChange,
    placeholder,
    text,
    ...props
}) => {
    return (
        <>
            <Form.Item
                className="!m-0"
                label={label}
                name={selectName}
            /*      rules={[{ required: true, message: `Please choose your ${placeholder}` }]}*/
            >
                <Select
                    clearIcon={<CloseOutlined className="w-[10px] h-[10px] bg-[#F6F6F6]" />}
                    options={options}
                    onChange={onChange}
                    placeholder={placeholder}
                    rootClassName={clsx(cls.select)}
                    allowClear={true}
                    className={clsx(cls.select, className, `!border-none bg-[#F6F6F6] !rounded-[4px] font-[400] 
                    focus:!border-green cursor-pointer shadow-none hover:border-green  focus:!border-green
                     focus:!shadow-none`)}
                    {...props}
                />
            </Form.Item>
        </>
    );
};

export default FilterSelect;