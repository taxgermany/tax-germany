
import clsx from "clsx";
import cls from "../../../ui/select/select.module.scss";
import { FC, ChangeEvent } from "react";
import { IOption } from "pages/freelancerPage/personalData/PersonalData";
import { Form, Select } from "antd";
interface ISelectClient {
  label?: string;
  background?: boolean;
  className?: string;
  size?: string;
  border?: boolean;
  options?: Array<IOption>;
  placeholder?: string;
  selectName?: string;
  disabled?: boolean;
  text?: string;
  inputError?: string;
  sizeText?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  [key: string]:
  | string
  | ((event: ChangeEvent<HTMLInputElement>) => void)
  | boolean
  | undefined
  | Array<object>;
}

const SelectClient: FC<ISelectClient> = ({
  size = "!w-[254px] !h-[46px]",
  selectName,
  className,
  background = false,
  border = false,
  label,
  options,
  onChange,
  placeholder,
  disabled = true,
  text,
  sizeText = 'w-[254px] ',
  inputError,
  ...props
}) => {
  return (
    <>
      {
        !disabled
          ? <Form.Item
            className="!m-0"
            label={label}
            name={selectName}
            rules={[{ required: true, message: `Please choose your ${placeholder}` }]}
          >
            <Select
              className={clsx(`${cls.i} ${size} ${className} ${background ? "!bg-[#F4F4F4]/70" : "!bg-white/0"
                } 
                ${border ? '!border !border-[#D9D9D980] hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'}
          font-roboto !rounded-[4px] font-[400] focus:!border-green cursor-pointer  shadow-none`)}
              options={options}
              onChange={onChange}


              {...props}
            />
          </Form.Item>
          : <div className={`${sizeText} flex items-center justify-start`}>
            <p className="font-roboto font-[400] leading-[18.75px] 1%">{text}</p>
          </div>
      }
    </>
  );
};
export default SelectClient;
