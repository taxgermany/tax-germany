import { Button, Upload } from 'antd';
import clsx from 'clsx';
import {FC} from 'react'
interface IUploadFile{
    content?: boolean
}

const UploadFile: FC<IUploadFile> = ({content = false}) => {

  return (
    <>
      <Upload className='w-full text-end '>
        <Button className={clsx(`${content ? 'bg-green text-white border-0 hover:bg-green hover:!text-white hover:!border-0 hover:shadow-0' : 
        'text-green border border-[#D9D9D9] hover:bg-green hover:!text-white hover:!border-0 hover:shadow-0'} 
        w-[155px] xl:w-[120px] h-[50px] xl:h-[40px] flex justify-center items-center rounded-[6px]`)}>Upload File</Button>
      </Upload>
    </>
  );
};

export default UploadFile;
