import React, {ChangeEvent, FC} from 'react';
import {Form, Input, InputRef} from "antd";
import clsx from "clsx";
import s from "./InputForTable.module.scss";


interface IInputForTable {
    name?: string;
    label?: string;
    wh?: string;
    initialValue?: string
    [key: string]: string | ((event: ChangeEvent<HTMLInputElement>) => void) | boolean | undefined;
}


const InputForTable: FC<IInputForTable> = ({
                                               name,
                                               wh,
                                               initialValue = "0",
                                               inputRef,
                                               label, ...props
                                           }) => {
    return (
        <Form.Item
            label={label}
            name={name}
            className="!m-0 !py-0 !h-[38
            px] overflow-auto"
            initialValue={initialValue}>
            <Input defaultValue={initialValue} rootClassName={clsx(s.inputForTable)} type={"number"}
                   className={clsx(s.inputForTable, `${wh ? wh : 'w-full '} 
                  font-inter rounded-none outline-none border-none hover:!border-green bg-transparent hover:bg-[#FAFAFA]
                   text-center focus:bg-[#FAFAFA] focus:!border-green  w-[100%] cursor-pointer`)}
                   {...props}
                   min={0}

            />
        </Form.Item>
    );
};

export default InputForTable;
/*


className={clsx(s.inputForTable, `${wh ? wh : 'w-full '}
                  font-inter rounded-none outline-none border-none hover:!border-green bg-transparent hover:bg-transparent
                   text-center focus:bg-[#FAFAFA] focus:!border-green  w-[100%] cursor-pointer`)}*/
