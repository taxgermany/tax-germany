import React from 'react';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { Button, Form, Input, Space } from 'antd';
import clsx from 'clsx';
import "./InputPassword.scss";



interface IInputPassword {
  className?: string,
  registStyle?: boolean,
  background?: boolean,
  text?: string,
  border?: boolean,
  inputName?: string}


const App: React.FC<IInputPassword> = ({ className, inputName, border = false, registStyle, background = false, text = "Enter password" }) => {

  return (
    <Space direction="vertical">
      <Form.Item
        className='!m-0'
        name={inputName}
        rules={[{ required: true, message: `Please input your password` }]}>
        <Input.Password  placeholder={text} className={clsx(className, `${registStyle ? 'pl-[10px]   border-[#D9D9D9]' : 'pl-[10px] [w-663px] h-[48px] border-[#D9D9D9]'} inputpass  !px-[18px] !border !border-black/0.23 hover:!border-green  focus:!border-green focus:!shadow-none`, `${background ? 'bg-[#F4F4F4]/70' : '!bg-white/0 '} `)} />
      </Form.Item>
    </Space>
  );
};

export default App;