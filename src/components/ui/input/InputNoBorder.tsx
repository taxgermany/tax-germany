import React from 'react'
import cls from './inputNoBorder.module.css'

interface IInputNoBorder {
    text:string,
    switchType:boolean,
    classNames:string
}


const InputNoBorder:React.FC<IInputNoBorder> = ({text,switchType,classNames}) => {
    return (
        <input className={`${classNames} ${cls.i}`} placeholder={text}
        type={switchType ? 'number' : 'text' }
        />
    );
};

export default InputNoBorder;