import React from 'react';
import { Checkbox, Form } from 'antd';
import "./CheckBoxRequired.scss";



interface ICheckBoxRequired {
  className?: string,
  registStyle?: boolean,
  background?: boolean,
  text?: string,
  inputName?: string
}


const App: React.FC<ICheckBoxRequired> = ({ className, inputName, registStyle, background = false, text = "Enter password" }) => {

  return (
    <Form.Item
    name={inputName}
    valuePropName="checked"
    rules={[
      {
        validator: (_, value) =>
          value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
      },
    ]}
   
  >
    <Checkbox className='check h-[30px] w-[30px]'>
    </Checkbox>
  </Form.Item>
  );
};

export default App;