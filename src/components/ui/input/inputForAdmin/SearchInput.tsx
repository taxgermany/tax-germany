import React, {ChangeEventHandler, FC, useState} from 'react';
import {Input} from "antd";
import {ReactComponent as Search} from '../../../../assets/icons/Search.svg'
import clsx from "clsx";

interface ISearchInput {
    onChange: ChangeEventHandler<HTMLInputElement>
}
const SearchInput:FC<ISearchInput> = ({onChange}) => {
    
    return (
        <Input
            placeholder="Enter your username"
            prefix={<Search/>}
            onChange={onChange}
            className={clsx(`w-full !h-[38px] border shadow-none active:shadow-green 
            hover:!border-green focus:!border-green focus:!shadow-none`)}/>
    );
};

export default SearchInput;