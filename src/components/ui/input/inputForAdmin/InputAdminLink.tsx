import clsx from 'clsx';
import cls from '../InputNoAppearance.module.scss'
import { FC, ChangeEvent, } from 'react';
import { useNavigate } from 'react-router-dom';
import { Form, Input } from 'antd';
interface IInputAdminLink {
    inputname?: string;
    label?: string;
    background?: boolean;
    className?: string;
    wh?: string;
    border?: boolean;
    disabled?: boolean;
    text?: string,
    placeholder?:string
    [key: string]: string | ((event: ChangeEvent<HTMLInputElement>) => void) | boolean | undefined;
}

const InputAdminLink: FC<IInputAdminLink> = ({ inputname,
    wh = "!w-[254px] !h-[46px]",
    className,
    background = false,
    border = true,
    disabled = false,
    text,
    placeholder,
    label, ...props }) => {

    const navigate = useNavigate()

    const btnLink = () =>{
        navigate(`${text}`)
    }
    return (
        <>
            {
                !disabled
                    ?
                    <Form.Item
                        className='!m-0 '
                        label={label}
                        name={inputname}
                        rules={[{ required: true, message: `Please enter your ${placeholder}` }]}
                    >
                        <Input
                            className={clsx(`${cls.i} ${wh} ${className} ${background
                                ? 'bg-[#F4F4F4]/70'
                                : '!bg-white/0'} 
                                ${border ? '!border !border-[#D9D9D980] hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'} 
                                !px-[14px] !py-[12px]  rounded-[4px] font-[400] focus:!border-green cursor-pointer text-[#0090FD]`)}
                            placeholder={placeholder}
                            {...props}
                        />
                    </Form.Item>
                    : <div className={`max-w-[254px] min-w-[105px] w-fit flex items-center justify-start cursor-pointer`} onClick={btnLink}>
                        <p className="font-roboto font-[400] leading-[18.75px] text-[#0090FD] 1%">{text}</p>
                    </div>
            }
        </>
    )
};
export default InputAdminLink;   