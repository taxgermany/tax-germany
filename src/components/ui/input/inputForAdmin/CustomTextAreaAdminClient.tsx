import React, { ChangeEvent } from "react";
import { Form, Input } from "antd";
import clsx from "clsx";
import cls from "../../../ui/input/ScrollInputMoth.module.scss";

interface IInputNoAppearance {
    className?: string;
    placeholder?: string;
    inputname?: string;
    label?: string;
    background?: boolean;
    wh?: string;
    statesEror?: boolean;
    disabled?: boolean;
    text?: string;
    onChanges?: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    [key: string]:
    | string
    | ((event: ChangeEvent<HTMLTextAreaElement>) => void)
    | boolean
    | undefined;
}

const CustomTextAreaAdminClient: React.FC<IInputNoAppearance> = ({
    inputname,
    placeholder,
    wh='!min-w-[260px] !w-[100%] h-[75px] ',
    className,
    background = true,
    onChanges,
    label,
    text,
    disabled = true,
    statesEror = false,
    ...props
}) => {
    return (
        <>
            {
                !disabled
                    ?
                    <Form.Item
                        label={label}
                        name={inputname}
                        validateStatus={`${statesEror ? `error` : ""}`}
                        help={statesEror ? `Please enter your ${placeholder}` : undefined}
                        rules={[{ required: true, message: `Please enter your ${placeholder}` }]}
                        className="!m-0"
                    >
                        <Input.TextArea
                            className={clsx(
                                cls.textArea,
                                `${wh} ${background ? "#F4F4F4B2" : ""
                                } 
                            !px-[15px] py-[18px] rounded-[4px] font-[400] focus:!border-0 focus:!shadow-none ${className}`
                            )}
                            {...props}
                            placeholder={placeholder}
                            onChange={onChanges}
                            title={text}
                        />
                    </Form.Item>
                    : <div className={`max-w-[254px] min-w-[105px] w-fit h-[46px] flex justify-start`}>
                        <p className="font-roboto font-[400] leading-[18.75px] 1%">{text}</p>
                    </div>
            }
        </>
    );
};

export default CustomTextAreaAdminClient;
