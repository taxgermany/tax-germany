import clsx from "clsx";
import cls from "../InputNoAppearance.module.scss";
import { FC, ChangeEvent } from "react";
import { Form } from 'antd';
import { DatePicker } from 'antd';
interface IInputDate {
  inputName?: string;
  background?: boolean;
  className?: string;
  wh?: string;
  border?: boolean;
  disabled? : boolean,
  text? : string
  [key: string]:
    | string
    | ((event: ChangeEvent<HTMLInputElement>) => void)
    | boolean
    | undefined;
}


const InputAdminDate: FC<IInputDate> = ({
  inputName,
  wh = "w-[254px] h-[46px]",
  className,
  text,
  background = false,
  border = true,
  disabled = true,
  
  ...props
}) => {
  
  return (
    <>
    
    { !disabled 
        ? 
        <Form.Item
          className={`ant-form-item-has-error m-0`}
          name={inputName}
          rules={[{ required: true, message: `Please enter your ${inputName}` }]}
        >
            <DatePicker 
             className={clsx(`${cls.i} ${wh ? wh : 'w-full h-[48px]'} ${className} ${background 
              ? 'bg-[#F4F4F4]/70' 
              : '!bg-white/0'} 
              ${border ? '!border !border-black/0.23 hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'} 
            rounded-[4px] font-[400] focus:!border-green cursor-pointer`)}
           
          {...props}
            />
        </Form.Item>
        : <p className="font-roboto font-[400] leading-[18.75px] 1%">{text}</p>

    }</>
  );
};
export default InputAdminDate;
