import clsx from 'clsx';
import cls from '../InputNoAppearance.module.scss'
import { FC, ChangeEvent } from 'react';
import { Form, Input } from 'antd';
interface IInputAdminClient {
    initialValue?: any;
    inputname?: string;
    label?: string;
    background?: boolean;
    className?: string;
    wh?: string;
    sizeText? : string;
    border?: boolean;
    disabled?: boolean;
    text?: string | any;
    placeholder?:string,
    [key: string]: string | ((event: ChangeEvent<HTMLInputElement>) => void) | boolean | undefined;
}

const InputAdminClient: FC<IInputAdminClient> = ({ inputname,
    wh = "w-[254px] h-[46px]",
    className,
    background = false,
    border = true,
    disabled = true,
    text,
    sizeText = 'w-[254px] ',
    initialValue,
    placeholder,
    label, ...props }) => {

    return (
        <>
            {
                !disabled
                    ?
                    <Form.Item
                        className='!m-0 '
                        label={label}
                        name={inputname}
                        rules={[{ required: true, message: `Please enter your ${placeholder}` }]}
                        initialValue={initialValue}
                    >
                        <Input
                            className={clsx(`${cls.i} ${wh} ${className} ${background
                                ? 'bg-[#F4F4F4]/70'
                                : '!bg-white/0'} 
                                ${border ? '!border border-[#D9D9D980] hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'} 
                                !px-[14px] !py-[12px]  rounded-[4px] font-[400] focus:!border-green cursor-pointer`)}
                            placeholder={placeholder}
                            {...props}
                        />
                    </Form.Item>
                    : <div className={`${sizeText} flex items-center justify-start`}>
                        <div className="font-roboto font-[400] leading-[18.75px] break-words">{text}</div>
                    </div>
            }
        </>
    )
};
export default InputAdminClient;
