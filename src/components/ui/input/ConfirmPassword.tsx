import React from 'react';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { Button, Form, Input, Space } from 'antd';
import clsx from 'clsx';
import "./InputPassword.scss";



interface IConfirmPassword {
  className?: string,
  registStyle?: boolean,
  background?: boolean,
  text?: string,
  inputName?: string
}


const App: React.FC<IConfirmPassword> = ({ className, inputName, registStyle, background = false, text = "Enter password" }) => {

  return (
    <Form.Item
    name="confirm"
    dependencies={[`${inputName}`]}
    className='!m-0'
    rules={[
      {
        required: true,
        message: 'Please confirm your password!',
      },
      ({ getFieldValue }) => ({
        validator(_, value) {
          if (!value || getFieldValue('password') === value) {
            return Promise.resolve();
          }
          return Promise.reject(new Error('The new password that you entered do not match!'));
        },
      }),
    ]}
  >
     <Input.Password  placeholder={text} className={clsx(className, `${registStyle ? 'pl-[10px]   border-[#D9D9D9]' : 'pl-[10px] [w-663px] h-[48px] border-[#D9D9D9]'} inputpass !px-[18px] !border !border-black/0.23 hover:!border-green  focus:!border-green focus:!shadow-none`, `${background ? 'bg-[#F4F4F4]/70' : '!bg-white/0 '} `)} />
  </Form.Item>
  );
};

export default App;