import clsx from 'clsx';
import cls from './InputNoAppearance.module.scss'
import  {  FC, ChangeEvent } from 'react';
import { Form, Input } from 'antd';
interface IInputNoAppearance {
    inputName? : string;
    label? : string;
    background?: boolean;
    className?: string;
    wh?: string;
    border?: boolean;
    [key: string]: string | ((event: ChangeEvent<HTMLInputElement>) => void) | boolean | undefined;
    }
    
const InputNoAppearance:FC<IInputNoAppearance>= ({ inputName,
                                                     wh="w-[400px] md:w-[560px]",
                                                     className,
                                                     background = false,
                                                     border = false,
                                                     label,...props}) =>{
    
    return(
        <Form.Item 
        className='!m-0'
        label={label}
        name={inputName}
        rootClassName='w-full h-full'
        rules={[{required: true, message: `Please enter your ${inputName}`}]}>        
            <Input 
            className={clsx(`${cls.i} ${wh ? wh : 'w-[560px] !h-[48px]'} ${className} ${background 
                ? 'bg-[#F4F4F4]/70' 
                : '!bg-white/0'} 
                ${border ? '!border !border-black/0.23 hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'} 
            !px-[15px] !py-[18px]  rounded-[4px] font-[400] focus:!border-green cursor-pointer`)}
            {...props}
            />
        </Form.Item>
    )
};
export default InputNoAppearance;   