import React, { ChangeEvent } from "react";
import { Form, Input } from "antd";
import clsx from "clsx";
import cls from "./InputNoAppearance.module.scss";

interface IInputNoAppearance {
  className?: string;
  placeholder: string;
  inputName?: string;
  label?: string;
  background?: boolean;
  wh?: string;
  statesEror?: boolean;
  onChanges?: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  [key: string]:
    | string
    | ((event: ChangeEvent<HTMLTextAreaElement>) => void)
    | boolean
    | undefined;
}

const CustomTextArea: React.FC<IInputNoAppearance> = ({
  inputName,
  placeholder,
  wh,
  className,
  background = true,
  onChanges,
  label,
  statesEror = false,
  ...props
}) => {
  return (
    <Form.Item
      label={label}
      name={inputName}
      validateStatus={`${statesEror ? `error` : ""}`}
      help={statesEror ? `Please enter your ${inputName}` : undefined}
      rules={[{ required: true, message: `Please enter your ${inputName}` }]}
      className="!m-0 "
    >
      <Input.TextArea
        className={clsx(
          cls.textArea,
          `${wh ? wh : "w-full !h-[92px]"} ${
            background ? "bg-[#F4F4F4]/70" : ""
          } 
            !p-[15px 18px]  rounded-[4px] font-[400] focus:!border-0 focus:!shadow-none ${className}`
        )}
        {...props}
        placeholder={placeholder}
        onChange={onChanges}
      />
    </Form.Item>
  );
};

export default CustomTextArea;
