import clsx from "clsx";
import cls from "./InputDate.module.scss";
import { FC, ChangeEvent } from "react";
import { Form } from 'antd';
import { DatePicker } from 'antd';
interface IInputDate {
  inputName?: string;
  background?: boolean;
  className?: string;
  wh?: string;
  border?: boolean;
  [key: string]:
    | string
    | ((event: ChangeEvent<HTMLInputElement>) => void)
    | boolean
    | undefined;
}


const InputDate: FC<IInputDate> = ({
  inputName,
  wh = "w-[400px] md:w-[560px]",
  className,
  background = false,
  border = false,
  ...props
}) => {
  
  return (
    <Form.Item
      className={`ant-form-item-has-error m-0`}
      name={inputName}
      rules={[{ required: true, message: `Please enter your ${inputName}` }]}
    >
        <DatePicker 
         className={clsx(`${cls.i} ${wh ? wh : 'w-full h-[48px]'} ${className} ${background 
          ? '!bg-[#F4F4F4]/70' 
          : '!bg-white/0'} 
          ${border ? '!border !border-black/0.23 hover:border-green  focus:!border-green focus:!shadow-none' : '!border-0 hover:!border-0 focus:!border-0 focus:!shadow-none'} 
      !p-[15px 18px]  rounded-[4px] font-[400] focus:!border-green cursor-pointer`)}
      inputReadOnly={true} 
      {...props}
        />
    </Form.Item>
  );
};
export default InputDate;
