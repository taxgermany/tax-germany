import React, { useState } from "react";
import cls from './ScrollInputMoth.module.scss'

interface IScrollInputMonth {
    input1: string;
    input2: string;
    input3: string;
    input4: string;
    input5: string;
    input6: string;
    input7: string;
    input8: string;
    input9: string;
    input10: string;
    input11: string;
    input12: string;
    input13: string;
}

const ScrollInputMonth: React.FC = () => {
    const [state, setState] = useState<IScrollInputMonth>({
        input1: "",
        input2: "",
        input3: "",
        input4: "",
        input5: "",
        input6: "",
        input7: "",
        input8: "",
        input9: "",
        input10: "",
        input11: "",
        input12: "",
        input13: "",
    });

    const inputChange = (
        e: React.ChangeEvent<HTMLInputElement>,
        inputKey: number
    ) => {
        const { value } = e.target;

        setState((prevState) => ({
            ...prevState,
            [`input${inputKey + 1}`]: value,
        }));
    };

    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
        "TOTAL",
    ];
    const calculateTotal = () => {
        let sum = 0;

        for (let i = 1; i <= 12; i++) {
            const inputValue = Number(state[`input${i}` as keyof IScrollInputMonth]);
            if (!isNaN(inputValue)) {
                sum += inputValue;
            }
        }

        return sum;
    };
    return (
        <div className={cls.scroll}>
            {Array.from(Array(13).keys()).map((inputKey) => (
                months[inputKey] !== 'TOTAL' ?
                    <div
                        key={inputKey}
                        className={`w-[109px] border-x border-[#EEEEEE] snap-center`}
                    >
                        <div className="text-[14px] h-[28px] text-center">
                            {months[inputKey] }
                        </div>
                        <div className="border-y border-[#EEEEEE]">
                            <input
                                type="number"
                                value={state[`input${inputKey + 1}` as keyof IScrollInputMonth]}
                                inputMode="numeric"
                                placeholder="0"
                                onChange={(e) => inputChange(e, inputKey)}
                                className={`text-center border-0 w-[107px] h-[38px] ${cls.i}  focus:border-0 focus:bg-[#D9D9D9]`}
                            />
                        </div>
                    </div> :
                    <div
                        className={`w-[109px] border-x border-[#EEEEEE] snap-center relative mb-[2px]`}
                    >
                        <div className="text-[14px] w-[109px] h-[28px] text-center">
                            TOTAL
                        </div>
                        <div className="border-y border-[#EEEEEE] w-[107px] h-[38px] botom-0">
                            <input
                                inputMode="numeric"
                                value={`${calculateTotal()}`}
                                className={`text-center items-center border-0 w-[107px] bg-[#D9D9D9] h-[39px] focus:border-0`}
                            />
                        </div>
                    </div>
            ))}
        </div>
    );
};

export default ScrollInputMonth;
