import BigTitle from "components/ui/button/BigTitle";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import {useEffect, useState} from "react";
import InputPassword from "components/ui/input/InputPassword";
import {useNavigate} from "react-router-dom";
import {Checkbox, Form} from "antd";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import ConfirmPassword from "components/ui/input/ConfirmPassword";
import ValidationText from "components/ui/validation/ValidationText";
import CheckBox from "components/ui/button/CheckBox";
import {useRegisterMutation} from "../../services/authServics";
import {useAppDispatch} from "../../hooks/useAppDispatch";
import {setRegister} from "../../redux/auth/authSlice";
import {useAppSelector} from "../../hooks/useAppSelector";

const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};

const Register = () => {
    const { registerData } = useAppSelector((state) => state.authSlice);
    const [errorActive, setErrorActive] = useState("");
    const [active, setActive] = useState(false);

    const [register, {data, error, isSuccess, isError, isLoading}] = useRegisterMutation()
    const dispatch = useAppDispatch()

    const navigate = useNavigate()


    const onFinish = async (values: any) => {
        setErrorActive("");
        dispatch(setRegister({email:values.email,phone:values.phone}))
        try {
            const data = await register({
                email: values.email,
                password: values.password,
                phoneNumber: values.phone
            }).unwrap();

            localStorage.setItem("userId", String(data.id));
            localStorage.setItem("email", String(data.email));
            navigate('/freelancer/confirm-email');

        } catch (error: any) {
            setErrorActive(`${error.data}`);

        }
    }

    return (
        <div className="h-[90vh] font-inter flex justify-center items-center relative">
            {errorActive ? <ValidationText text={errorActive}/> : ""}
            <div className="content grid gap-[30px]">
                <Form
                    name="basic"
                    layout="horizontal"
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                    style={{maxWidth: 650}}
                    initialValues={{...registerData,remember: active}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    className={"grid gap-y-[10px]"}
                >
                    <BigTitle text="Register to start"/>
                    <p className="text-[14px] max-w-[700px] leading-[26px] font-[400]">Securely upload your data and a
                        certified tax advisor will prepare the freelancer audit report for your residence permit
                        extension</p>
                    <div className="flex flex-col gap-[10px]">
                        <InputNoAppearance inputName="email" background={true} type='email' placeholder="Enter email"
                                           border={true} wh="w-[663px]"/>
                        <InputPassword inputName="password" background={true} className="!w-[663px]"/>
                        <ConfirmPassword inputName="password" background={true} text="Repeat password"
                                         className="!w-[663px]"/>
                        <InputNoAppearance background={true} placeholder="Phone number" inputName="phone" border={true}
                                           wh="w-[663px]"/>
                    </div>
                    <div className="flex items-center justify-between w-[678px]">
                        <Form.Item
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}
                        >
                            <Checkbox>
                                <p className="text-[12px] font-[Inter] leading-[17px] font-[500] max-w-[644px]">I’ve read and
                                    consent to the term & conditions and acknowledge the privacy policy and agree to the
                                    processing of my sensitive personal data</p>
                            </Checkbox>
                        </Form.Item>
                       {/* <CheckBox active={active} setActive={setActive}/>*/}


                    </div>
                    <NewButtonGreen text={"Register"} className="!rounded-[4px]"/>
                </Form>
            </div>
        </div>
    )
}

export default Register;
