import ButtonGreen from "components/ui/button/ButtonGreen";
import InputGray from "components/ui/button/InputGray";
import BigTitle from "components/ui/button/BigTitle";
import { useState } from "react";
import "./ResetPassword.css";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import InputPassword from "components/ui/input/InputPassword";
import ConfirmPassword from "components/ui/input/ConfirmPassword";
import { useNavigate } from "react-router-dom";
import { Form } from "antd";
import NewButtonGreen from "components/ui/button/NewButtonGreen";
import { useMatch } from "react-router-dom"
import {useLazyResetQuery, useResetPasswordMutation} from "../../services/authServics";


const onFinishFailed = (errorInfo: any) => {
   console.log('Failed:', errorInfo);
};

const ResetPassword = () => {

   const [passwordVisible, setPasswordvisible] = useState(false);
   const [confirmpasswordVisible, setConfirmPasswordVisible] = useState(false);

   const navigate = useNavigate()

   const params = useMatch("/freelancer/reset/:slug");


   console.log(params);
   const [resetPassword,{data,error,isLoading}] = useResetPasswordMutation()


   const onFinish = async (values: any) => {
      console.log(values.password);
      if(params && params.params && params.params.slug ) {
         await resetPassword({password:values.password,resetToken:params.params.slug})
         navigate("/login")
      }

/*
      fetch(`${process.env.REACT_APP_URL}/auth/reset/${params?.params.slug}?password=${values.password}`, {
         method: "POST",
         headers: {
            'Content-Type': 'application/json'
         }
      })
         .then(response => {
            return response.text();
         })
         .then(data => {
            navigate("/freelancer/confirm-email")
            console.log(data);
         })
         .catch(error => {
            // Handle any errors that might occur during the fetch
            console.error('Error:', error);
         });*/
   }






   return (
      <div className="ResetPassword h-[90vh] font-inter flex justify-center items-center">
         <div className="content inline">
            <Form
               name="basic"
               layout="horizontal"
               labelCol={{ span: 8 }}
               wrapperCol={{ span: 16 }}
               style={{ maxWidth: 650 }}
               initialValues={{ remember: true }}
               onFinish={onFinish}
               onFinishFailed={onFinishFailed}
               autoComplete="off"
               className={"grid gap-y-[10px]"}
            >
               <BigTitle text="Reset password" className="pb-[30px]" />
               <div className="inputs mb-[40px] grid gap-[15px] ">
                  <InputPassword inputName="password" text="New password" background={true} className="!w-[500px]" />
                  <ConfirmPassword inputName="password" background={true} text="Confirm password" className="!w-[500px]" />
               </div>
               <Form.Item>
                  <NewButtonGreen text={"Log in"} />
               </Form.Item>
            </Form>
         </div>
      </div>
   )
}

export default ResetPassword;