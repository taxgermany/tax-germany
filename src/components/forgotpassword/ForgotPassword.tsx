import NewButtonGreen from "components/ui/button/NewButtonGreen";
import BigTitle from "components/ui/button/BigTitle";
import InputNoAppearance from "components/ui/input/InputNoAppearance";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import {useLazyResetQuery, } from "../../services/authServics";

const ForgotPassword = () => {
   const navigate = useNavigate()
   const [emailCode, setEmailCode] = useState("");
   const [errorActive, setErrorActive] = useState(true);
   const [reset,{data,error,isLoading}] = useLazyResetQuery()

   const  onSendCode  = async () => {
      await reset ({email:emailCode})
      navigate('/freelancer/check-email')
     /* fetch(`${process.env.REACT_APP_URL}/auth/reset?email=${emailCode}`, {
         headers: {
            'Content-Type': 'application/json'
         }
      })
         .then(response => {
            if (!response.ok) {
               setErrorActive(false);
            }
            return response.text();
         })
         .then(data => {

            if (errorActive) {
               navigate('/freelancer/check-email')
            }
        


         })
         .catch(error => {
            // Handle any errors that might occur during the fetch
            console.error('Error:', error);
         });*/
   }


   return (
      <div className="ForgotPassword h-[90vh] font-inter flex justify-center items-center">
         <div className="content inline">
            <BigTitle text="Forgot password" className="pb-[15px]" />
            <p className="text-[12px] text-left pb-[15px]">Enter your email for your account</p>
            <InputNoAppearance background={true} placeholder="Enter your email" onChange={(e) => setEmailCode(e.target.value)} border={true} wh="w-[500px] " />
            <div className="flex pt-[30px] ">
               <button onClick={() => navigate("/login")} className="text-[16px] mr-[19px] leading-[26px] bg-[#D9D9D9] px-[38px] text-black py-[8px] w-fit rounded-[3px]">Cancel</button>
               <NewButtonGreen text="Log in" onClick={onSendCode} />
            </div>
         </div>
      </div>
   )
}

export default ForgotPassword;