/** @format */
import PersonalData from "pages/freelancerPage/personalData/PersonalData";

import OpenPage from "pages/openPage/OpenPage";
import {Route, Routes} from "react-router-dom";
import LayoutWithSideBar from "components/layout/LayoutWithSideBar";
import LayoutWithoutSideBar from "../components/layout/LayoutWithoutSideBar";
import HealthInsurancePay from "../pages/freelancerPage/healthInsurance/HealthInsurancePay";
import ResetPassword from "../components/resetpassword/ResetPassword";
import ForgotPassword from "../components/forgotpassword/ForgotPassword";
import CheckEmail from "../components/checkemail/CheckEmail";
import TwoPage from "pages/freelancerPage/personalData/TwoPage";
import ThreePage from "pages/freelancerPage/personalData/ThreePage";
import FourPage from "pages/freelancerPage/personalData/FourPage";
import FivePage from "pages/freelancerPage/personalData/FivePage";
import SixPage from "pages/freelancerPage/personalData/SixPage";
import PensionNumber from "pages/freelancerPage/pensionContr/PensionNumber";
import PensionContr from "pages/freelancerPage/pensionContr/PensionContr";
import PensionAuditPeriod from "pages/freelancerPage/pensionContr/PensionAuditPeriod";

import HealthInsuranceDetails from "../pages/freelancerPage/healthInsurance/HealthInsuranceDetails";
import ProfitAndExpenses from "../pages/freelancerPage/profitandexpenses/ProfitAndExpenses";
import ProfitAndExpensesRegularly from "../pages/freelancerPage/profitandexpenses/ProfitAndExpensesRegularly";
import ProfitAndExpensesEmployer from "../pages/freelancerPage/profitandexpenses/ProfitAndExpensesEmployer";
import ProfitAndExpensesVat from "../pages/freelancerPage/profitandexpenses/ProfitAndExpensesVat";
import ProfitAndExpensesLargeTable from "../pages/freelancerPage/profitandexpenses/ProfitAndExpensesLargeTable";
import PensionSecurity from "pages/freelancerPage/pensionContr/PensionSecurity";
import PensionInsurance from "pages/freelancerPage/pensionContr/PensionInsurance";
import HealthInsurancePeriod from "pages/freelancerPage/healthInsurance/HealthInsurancePeriod";
import HealthInsuranceRegular from "../pages/freelancerPage/healthInsurance/HealthInsuranceRegular";
import Login from "components/login/Login";
import Register from "components/register/Register";
import PleaseConfirm from "pages/pleaseconfirm/PleaseConfirm";
import RegisterToStart from "pages/registertostart/RegisterToStart";
import ConfirmEmail from "pages/confirmemail/ConfirmEmail";
import ApplicationReview from "pages/freelancerPage/applicationReview/ApplicationReview";
import ApplicationStatus from "../pages/freelancerPage/applicationStatus/ApplicationStatus";
import Payment from "../pages/payment/Payment";
import ChoosePackage from "pages/choosepackage/ChoosePackage";
import EightPage from "pages/freelancerPage/personalData/EightPage";
import ProfitAndExpensesEmployerNumber from "../pages/freelancerPage/profitandexpenses/ProfitAndExpensesEmployerNumber";
import ProfitAndExpensesPeriod from "../pages/freelancerPage/profitandexpenses/ProfitAndExpensesPeriod";
import ProtectedRoute from "./ProtectedRoute";


const AppRouter = () => {
    return (
        <Routes>
            {/*open routes*/}
                <Route element={<LayoutWithoutSideBar/>}>
                    <Route path='/' element={<OpenPage/>}/>
                    <Route path='/login' element={<Login/>}/>
                    <Route path='/freelancer/reset/:slug' element={<ResetPassword/>}/>
                    <Route path='/freelancer/forgot-password' element={<ForgotPassword/>}/>
                    <Route path='/freelancer/check-email' element={<CheckEmail/>}/>
                    <Route path='/freelancer/register' element={<Register/>}/>
                    <Route path='/freelancer/register-to-start' element={<RegisterToStart/>}/>
                    <Route path='/freelancer/confirm-email' element={<ConfirmEmail/>}/>
                </Route>

            <Route element={<ProtectedRoute isRole={'ROLE_FREELANCER'}/>}>
                <Route path='/freelancer/please-confirm' element={<PleaseConfirm/>}/>
                <Route path='/freelancer/choose-package' element={<ChoosePackage/>}/>
            </Route>

            <Route element={<ProtectedRoute isRole={'ROLE_FREELANCER'}/>}>
                <Route element={<LayoutWithSideBar/>}>
                    <Route path='/freelancer/personal-data' element={<PersonalData/>}/>
                    <Route path='/freelancer/personal-data/two-page' element={<TwoPage/>}/>
                    <Route path='/freelancer/personal-data/three-page' element={<ThreePage/>}/>
                    <Route path='/freelancer/personal-data/four-page' element={<FourPage/>}/>
                    <Route path='/freelancer/personal-data/five-page' element={<FivePage/>}/>
                    <Route path='/freelancer/personal-data/six-page' element={<SixPage/>}/>
                    <Route path='/freelancer/personal-data/eight-page' element={<EightPage/>}/>
                </Route>
            </Route>

            <Route element={<ProtectedRoute isRole={'ROLE_FREELANCER'}/>}>
                <Route element={<LayoutWithSideBar/>}>
                    <Route path='/freelancer/pension-contributions' element={<PensionContr/>}/>
                    <Route path='/freelancer/pension-number' element={<PensionNumber/>}/>
                    <Route path='/freelancer/pension-audit-period' element={<PensionAuditPeriod/>}/>
                    <Route path='/freelancer/pension-security' element={<PensionSecurity/>}/>
                    <Route path='/freelancer/pension-insurance' element={<PensionInsurance/>}/>
                </Route>
            </Route>

            {/*Profit And Expenses*/}
            <Route element={<ProtectedRoute isRole={'ROLE_FREELANCER'}/>}>
                <Route element={<LayoutWithSideBar/>}>
                    <Route path='/freelancer/profits-and-expenses' element={<ProfitAndExpenses/>}/>
                    <Route path='/freelancer/profits-and-expenses-regularly' element={<ProfitAndExpensesRegularly/>}/>
                    <Route path='/freelancer/profits-and-expenses-period' element={<ProfitAndExpensesPeriod/>}/>
                    <Route path='/freelancer/profits-and-expenses-employer' element={<ProfitAndExpensesEmployer/>}/>
                    <Route path='/freelancer/profits-and-expenses-number' element={<ProfitAndExpensesEmployerNumber/>}/>
                    <Route path='/freelancer/profits-and-expenses-vat' element={<ProfitAndExpensesVat/>}/>
                    <Route path='/freelancer/profits-and-expenses-table' element={<ProfitAndExpensesLargeTable/>}/>
                </Route>
            </Route>
            {/*    </Route>*/}

            {/*  <Route element={<ProtectedRoute isRole={'FREELANCER'}/>}>*/}
            {/*health insurance routes*/}
            <Route element={<ProtectedRoute isRole={'ROLE_FREELANCER'}/>}>
                <Route element={<LayoutWithSideBar/>}>
                    <Route path='/freelancer/health-insurance-pay' element={<HealthInsurancePay/>}/>
                    <Route path='/freelancer/health-insurance-details' element={<HealthInsuranceDetails/>}/>
                    <Route path='/freelancer/health-insurance-period' element={<HealthInsurancePeriod/>}/>
                    <Route path='/freelancer/health-insurance-regular' element={<HealthInsuranceRegular/>}/>
                </Route>
            </Route>
            {/*   </Route>*/}

            application
            <Route element={<ProtectedRoute isRole={'ROLE_FREELANCER'}/>}>
                <Route element={<LayoutWithSideBar/>}>
                    <Route path='/freelancer/review' element={<ApplicationReview/>}/>
                    <Route path='/freelancer/status' element={<ApplicationStatus/>}/>
                    <Route path="/freelancer/payment" element={<Payment/>}/>
                </Route>
            </Route>
            {/* </Route>*/}



        </Routes>
    );
};
export default AppRouter;
