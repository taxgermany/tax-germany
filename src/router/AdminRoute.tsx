import {Route, Routes} from "react-router-dom";
import LayoutWithSideBarAdmin from "../components/layout/LayoutWithSideBarAdmin";
import ManageAccess from "pages/manageAccess/ManageAccess";
import CreatePassword from "components/createPassword/CreatePassword";
import LayoutWithoutSideBar from "../components/layout/LayoutWithoutSideBar";
import ClientDetials from "pages/admin/ClientDetials/ClientDetials";
import Clients from "../pages/admin/Clients";
import PricingPolicy from "pages/superAdmin/pircingPolicy/PricingPolicy";
import ClientsSuperAdmin from "../pages/superAdmin/ClientsSuperAdmin";
import LayoutWithSideBarExpert from "../components/layout/LayoutWithSideBarExpert";
import ProtectedRoute from "./ProtectedRoute";


const AdminRoute = () => {
    return (
        <Routes>
            <Route element={<ProtectedRoute isRole={'ROLE_EXPERT'}/>}>
                <Route element={<LayoutWithoutSideBar/>}>
                    <Route path="/create-password" element={<CreatePassword/>}/>
                </Route>
                <Route element={<LayoutWithSideBarExpert/>}>
                    <Route path="/expert/clients" element={<Clients/>}/>
                    <Route path="/expert/clients-details/:id" element={<ClientDetials/>}/>
                </Route>
            </Route>
            <Route element={<ProtectedRoute isRole={'ROLE_SUPER_ADMIN'}/>}>
                <Route element={<LayoutWithSideBarAdmin/>}>
                    <Route path="/super-admin/clients" element={<ClientsSuperAdmin/>}/>
                    <Route path="/super-admin/clients-details/:id" element={<ClientDetials/>}/>
                    <Route path="/super-admin/pricing-policy" element={<PricingPolicy/>}/>
                    <Route path="/super-admin/manage-access" element={<ManageAccess/>}/>
                </Route>
            </Route>
        </Routes>
    );
};

export default AdminRoute;
