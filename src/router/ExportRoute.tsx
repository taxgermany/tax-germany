import React from 'react';
import {Route, Routes} from "react-router-dom";
import Clients from "../pages/admin/Clients";
import ClientDetials from "../pages/admin/ClientDetials/ClientDetials";
import LayoutWithSideBarExpert from "../components/layout/LayoutWithSideBarExpert";

const ExportRoute = () => {
    return (
        <Routes>
            <Route element={<LayoutWithSideBarExpert/>}>
                <Route path="/export/clients" element={<Clients/>} />
                <Route path="/export/clients-details" element={<ClientDetials/>} />
            </Route>
        </Routes>
    );
};

export default ExportRoute;