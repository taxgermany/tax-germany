import {FC} from 'react';
import {Navigate, Outlet} from "react-router-dom";

interface IProps {
    isRole: string
}

const ProtectedRoute: FC<IProps> = ({isRole}) => {
    let role = null
    const user = localStorage.getItem('user');
    if (user) role = JSON.parse(user).userRole

    if ((role === isRole) && user) {
        return <Outlet/>
    }
    if ((isRole==='ROLE_FREELANCER' )) {
        return <Navigate to={'/login'}/>
    }
    return <Navigate to={'/'}/>

};

export default ProtectedRoute;
